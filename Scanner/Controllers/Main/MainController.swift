//
//  MainController.swift
//  Scanner
//
//  Created by Artem  on 24.09.2018.
//  Copyright © 2018 VladimirKuzmin. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import StoreKit
//import SimplePDF
import AdSupport
import AudioToolbox

@available(iOS 10.0, *)
class MainController: UIViewController, SKProductsRequestDelegate, SKPaymentTransactionObserver, DocEditDelegate
{
	private var _controllers = [ControllerModel]()

	var currentControllerName = ""

	private var _notRemoveControllers = ["test"]

	var products = [SKProduct]()

	var premium: Premium!

	func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction])
	{
		print("*** updatedTransactions")

		for transaction:AnyObject in transactions {
			let trans = transaction as! SKPaymentTransaction
			print(trans)

			switch trans.transactionState {

			case .purchased, .restored:
				var trans_id = transaction.transaction?.transactionIdentifier

				transaction.transaction?.transactionIdentifier

				var id = transaction.payment.productIdentifier

				do {
					let receiptData = try Data(contentsOf: Bundle.main.appStoreReceiptURL!)

					let receiptString = receiptData.base64EncodedString(options: NSData.Base64EncodingOptions())

					print("buy, ok ID \(id) TRANS \(trans_id) RECEIPT \(receiptString)")

					Core.shared().ValidateRecept { (success) in
						queue.finishTransaction(trans)
					}

				} catch {
					queue.finishTransaction(trans)
				}
				break;
			case .failed:
				print("buy error")
				print(trans.error)
				queue.finishTransaction(trans)
				break;
			case .deferred:
				print("buy, deffered ")
				break
			case .purchasing:
				print("buy, purchasing ")
				break
			default:
				print("default")
				break;
			}
		}
	}

	func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse)
	{
		let products = response.products

		print("*** productsRequest")
//		print(products)

		var productsData = [SubscriptionModel]()

		for i in products {
			if #available(iOS 11.2, *) {
				print(i.subscriptionPeriod?.unit.rawValue)
			} else {
				// Fallback on earlier versions
			}
			print("** PRODUCT PRICE \(i.price) LOCALE \(i.priceLocale.currencyCode) \(i.priceLocale) ID \(i.productIdentifier) \(i.localizedTitle)  \(i.localizedDescription)")

			self.products.append(i)

			var product = SubscriptionModel()
			product.PurchaseId = i.productIdentifier
			product.PriceLocale = Double(truncating: i.price)
			product.Currency = i.priceLocale.currencyCode!
			product.Title = i.localizedTitle
			product.Description = i.localizedDescription

			productsData.append(product)
		}

		try! Core.shared().getRealm.write {
			Core.shared().getRealm.add(productsData, update: true)
		}

//		print("*** productsRequest invalid")
//		print(response.invalidProductIdentifiers)
	}

	@objc func observerDoPurchaseResore(_ notification: NSNotification)
	{
		DispatchQueue.main.async(execute: {
			if (SKPaymentQueue.canMakePayments()) {
				SKPaymentQueue.default().restoreCompletedTransactions()
			}
		})
	}

	@objc func observerDoPurchase(_ notification: NSNotification)
	{
		print("*** observerDoPurchase")

		DispatchQueue.main.async(execute: {
			let params = notification.userInfo as! Dictionary<String, AnyObject>

			let product_id = params["product_id"] as? String ?? ""

			print("*** observerDoPurchase ID \(product_id)")

			if(SKPaymentQueue.canMakePayments())
			{
				for product in self.products {
					if product.productIdentifier == product_id
					{
						let pay = SKPayment(product: product)
						SKPaymentQueue.default().add(self)
						SKPaymentQueue.default().add(pay)
					}
				}
			}
			else
			{
				print("*** CANNOT PAYMENTS")
			}
		})
	}

//	override var preferredStatusBarStyle : UIStatusBarStyle
//	{
//		return UIStatusBarStyle.
//	}

	func registerProducts()
	{
		if(SKPaymentQueue.canMakePayments())
		{
			print("*** CAN PAYMENTS")

			var purchasedProductIdentifiers1: Set<String> = Set<String>()

			for i in Core.shared().getRealm.objects(SubscriptionModel.self)
			{
				purchasedProductIdentifiers1.insert(i.PurchaseId)
			}

//			let productID: NSSet = NSSet(objects: "ru.VladimirKuzmin.1000coins")
			let request: SKProductsRequest = SKProductsRequest(productIdentifiers: purchasedProductIdentifiers1)

			request.delegate = self
			request.start()
		}
		else
		{
			print("*** CANNOT PAYMENTS")
		}
	}

	@objc func observerOpenDoc(_ notification: NSNotification)
	{
		DispatchQueue.main.async(execute: {
			let params = notification.userInfo as! Dictionary<String, AnyObject>

			let doc_id = params["doc_id"] as? String ?? ""

			var preview = DocEdit(id: doc_id)
			preview.delegate = self
			self.view.addSubview(preview)

		})
	}

	@objc func ImageSaved(_ image: UIImage!, didFinishSavingWithError error:NSError!, contextInfo:UnsafeRawPointer)
	{

	}

	var IsShowNoConnection = false

	func showNoConnection()
	{
		if(IsShowNoConnection)
		{
			return
		}

		IsShowNoConnection = true

		let alertController = UIAlertController(title: s("alert_no_internet"), message: s("alert_no_internet_desc"), preferredStyle: .alert)

		let okAction = UIAlertAction(title: "OK", style: .default, handler: {
			(action : UIAlertAction!) -> Void in
			self.IsShowNoConnection = false
		})

		alertController.addAction(okAction)

		self.present(alertController, animated: true, completion: nil)
	}

	@objc func observerShare(_ notification: NSNotification)
	{
		print("** observerShare")

		DispatchQueue.main.async(execute: {

			if(Core.shared().NetworkStatus.connection == .none)
			{
				self.showNoConnection()
				return 
			}

			Core.shared().checkTrial() { status in
				if(!Constants.IS_DEV && status != .Premium)
				{
					Core.shared().showPremium()
					return
				}

				let params = notification.userInfo as! Dictionary<String, AnyObject>

				print(params)

				let doc_id = params["id"] as? String ?? ""

				let type = params["type"] as? String ?? ""

				print("** observerShare 2")

				if let doc = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(doc_id)'").first
				{
					print("** observerShare 2")

					if(type == "image_save")
					{
						print("** observerShare 3")

						let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)

						for i in Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(doc_id)'")
						{
							let path = docDirURL.appendingPathComponent(i.FileName)

							let data = try? Data(contentsOf: path)

							var image = UIImage(data: data!)!

							UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.ImageSaved(_:didFinishSavingWithError:contextInfo:)), nil)
						}

						var successSave = UIView()
						successSave.backgroundColor = UIColor.black.withAlphaComponent(0.7)
						successSave.layer.cornerRadius = 15
						successSave.setSize(ScreenSize.SCREEN_WIDTH / 3, ScreenSize.SCREEN_WIDTH / 3)
						successSave.toCenterX(self.view)
						successSave.toCenterY(self.view)
						self.view.addSubview(successSave)

						var successSaveIcon = UILabel()
						successSaveIcon.setX(0)
						successSaveIcon.setY(0)
						successSaveIcon.setSize(successSave.getWidth(), successSave.getHeight())
						successSaveIcon.textAlignment = .center
						successSaveIcon.textColor = .white
						successSaveIcon.font = UIFont.systemFont(ofSize: 56)
						successSaveIcon.text = "✓"
						successSave.addSubview(successSaveIcon)

						var successSaveTitle = UILabel()
						successSaveTitle.setX(0)
						successSaveTitle.setY((successSave.getHeight() / 3) * 2)
						successSaveTitle.setSize(successSave.getWidth(), 20)
						successSaveTitle.textAlignment = .center
						successSaveTitle.textColor = .white
						successSaveTitle.font = UIFont.systemFont(ofSize: 14)
						successSaveTitle.text = "Saved"
						successSave.addSubview(successSaveTitle)

						UIView.animate(withDuration: 0.5, delay: 1.5, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveEaseInOut, animations:
							{
								successSave.alpha = 0
						}, completion: { (finished: Bool) -> Void in
							successSave.removeFromSuperview()
						})

					}

					if(type == "image")
					{
						var dataA = [UIImage]()

						let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)

						for i in Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(doc_id)'")
						{
							let path = docDirURL.appendingPathComponent(i.FileName)

							let data = try? Data(contentsOf: path)

							var image = UIImage(data: data!)!

							dataA.append(image)
						}

						let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: dataA, applicationActivities: nil)

						activityViewController.popoverPresentationController?.sourceView = self.view

						self.present(activityViewController, animated: true, completion: nil)
					}

					if(type == "text")
					{
						var data = [String]()

						for i in Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(doc_id)'")
						{
							data.append(i.ORCText)
						}

						let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: data, applicationActivities: nil)

						activityViewController.popoverPresentationController?.sourceView=self.view

						self.present(activityViewController, animated: true, completion: nil)
					}

					if(type == "pdf")
					{
						var files = [DocModelFile]()

						let pdfData = NSMutableData()

						let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)

						for i in Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(doc_id)'")
						{
							files.append(i)
						}

						let path = docDirURL.appendingPathComponent(files[0].FileName)

						let data = try? Data(contentsOf: path)

						var image = UIImage(data: data!)!

						let imgView = UIImageView.init(image: image)

						let imageRect = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)

						UIGraphicsBeginPDFContextToData(pdfData, imageRect, nil)

						UIGraphicsBeginPDFPage()

						let context = UIGraphicsGetCurrentContext()

						imgView.layer.render(in: context!)

						UIGraphicsEndPDFContext()


						//try saving in doc dir to confirm:
						let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last

						let path1 = dir?.appendingPathComponent("imagepdf.pdf")

						do {
							try pdfData.write(to: path1!, options: NSData.WritingOptions.atomic)
						} catch {
							print("error catched")
						}

						let myPDF = NSData(contentsOf: path1!)

						//				let documento = NSData(contentsOfFile: myPDF.pat)

						let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [myPDF!], applicationActivities: nil)

						activityViewController.popoverPresentationController?.sourceView=self.view

						activityViewController.excludedActivityTypes = [
							UIActivity.ActivityType.airDrop,
							UIActivity.ActivityType.postToFacebook,
							UIActivity.ActivityType.addToReadingList,
							UIActivity.ActivityType.assignToContact,
							UIActivity.ActivityType.print,
							UIActivity.ActivityType.saveToCameraRoll,
							UIActivity.ActivityType.copyToPasteboard,
							UIActivity.ActivityType.airDrop,
							UIActivity.ActivityType.postToTwitter,
							UIActivity.ActivityType.message
						]

						self.present(activityViewController, animated: true, completion: nil)
					}

					if(type == "ocrpdf")
					{

					}
				}
			}




		})
	}

	override func viewDidLoad()
	{
		super.viewDidLoad()

		if ASIdentifierManager.shared().isAdvertisingTrackingEnabled
		{
			Core.shared().DeviceId = ASIdentifierManager.shared().advertisingIdentifier.uuidString
		}
		else
		{
			Core.shared().DeviceId = (UIDevice.current.identifierForVendor?.uuidString)!
		}

		if(Core.shared().IsDebug)
		{
			let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)

			var testFiles = ["test1.jpg", "test2.jpg", "test3.jpg", "test4.jpg", "test5.jpg"]

			if Core.shared().getRealm.objects(DocModel.self).count == 0
			{
				var ii = 0

				for i in testFiles
				{
					let documentsURL1 = Bundle.main.resourceURL?.appendingPathComponent(i)

					do
					{
						try FileManager.default.copyItem(atPath: (documentsURL1?.path)!, toPath: docDirURL.appendingPathComponent(i).path)
					} catch let error as NSError
					{
						print("Couldn't copy file to final location! Error:\(error.description)")
					}
				}

				let doc = DocModel()
				doc.Id = testFiles[0]
				doc.CreatedAt = Int64(NSDate().timeIntervalSince1970)
				doc.Name = testFiles[0]

				var docFile = DocModelFile()
				docFile.Id = testFiles[0]
				docFile.DocId = doc.Id
				docFile.Width = 0
				docFile.Height = 0
				docFile.FileName = testFiles[0]

				var docFile2 = DocModelFile()
				docFile2.Id = testFiles[1]
				docFile2.DocId = doc.Id
				docFile2.Width = 0
				docFile2.Height = 0
				docFile2.FileName = testFiles[1]

				var docFile3 = DocModelFile()
				docFile3.Id = testFiles[2]
				docFile3.DocId = doc.Id
				docFile3.Width = 0
				docFile3.Height = 0
				docFile3.FileName = testFiles[2]



				try! Core.shared().getRealm.write
				{
					Core.shared().getRealm.add(doc, update: true)
					Core.shared().getRealm.add(docFile, update: true)
					Core.shared().getRealm.add(docFile2, update: true)
					Core.shared().getRealm.add(docFile3, update: true)
				}

				//-----

				let doc2 = DocModel()
				doc2.Id = testFiles[3]
				doc2.CreatedAt = Int64(NSDate().timeIntervalSince1970)
				doc2.Name = testFiles[3]

				var docFile2_1 = DocModelFile()
				docFile2_1.Id = testFiles[3]
				docFile2_1.DocId = doc2.Id
				docFile2_1.Width = 0
				docFile2_1.Height = 0
				docFile2_1.FileName = testFiles[3]

				let doc3 = DocModel()
				doc3.Id = testFiles[4]
				doc3.CreatedAt = Int64(NSDate().timeIntervalSince1970)
				doc3.Name = testFiles[4]

				var docFile3_1 = DocModelFile()
				docFile3_1.Id = testFiles[4]
				docFile3_1.DocId = doc3.Id
				docFile3_1.Width = 0
				docFile3_1.Height = 0
				docFile3_1.FileName = testFiles[4]

//				let doc5 = DocModel()
//				doc5.Id = testFiles[5]
//				doc5.CreatedAt = Int64(NSDate().timeIntervalSince1970)
//				doc5.Name = testFiles[5]
//
//				var docFile5_1 = DocModelFile()
//				docFile5_1.Id = testFiles[5]
//				docFile5_1.DocId = doc5.Id
//				docFile5_1.Width = 0
//				docFile5_1.Height = 0
//				docFile5_1.FileName = testFiles[5]

				try! Core.shared().getRealm.write
				{
					Core.shared().getRealm.add(doc2, update: true)
					Core.shared().getRealm.add(doc3, update: true)
//					Core.shared().getRealm.add(doc5, update: true)
					Core.shared().getRealm.add(docFile2_1, update: true)
					Core.shared().getRealm.add(docFile3_1, update: true)
//					Core.shared().getRealm.add(docFile5_1, update: true)
				}
			}


		}

		self.view.frame.origin.y = 0

		self.view.backgroundColor = .white

		self.registerObservers()

		Core.shared().changeViewController(name: "libraryController")
//		Core.shared().changeViewController(name: "settingsController")
//		Core.shared().changeViewController(name: "signatureController")


		for sub in Core.shared().productIds
		{
			let m = SubscriptionModel()
			m.PurchaseId = sub

			try! Core.shared().getRealm.write {
				Core.shared().getRealm.add(m, update: true)
			}
		}

		registerProducts()

//		var sign = SignatureSettings()
//		self.view.addSubview(sign)

		//----

		let headers: HTTPHeaders = [
			"userId": Core.shared().DeviceId,
			"token": (Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String).md5()
		]

		print(headers)

		let parameters: Parameters = ["recipe": ""]

		Alamofire.request(Constants.SERVICE_API + "auth", method: .post, parameters: parameters, headers: headers).responseJSON { response in
			print("AUTH RESPONSE")
			print(response)

			if let json = response.result.value
			{
				var jsonData = JSON(json)

				var success = jsonData["success"].intValue

				if(success == 1)
				{
					var userData = jsonData["user"].dictionaryValue ?? [String: JSON]()
					var userToken = userData["token"]?.stringValue

					var userModel = UserModel()
					userModel.Id = 1
					userModel.Token = userToken!

					try! Core.shared().getRealm.write {
						Core.shared().getRealm.add(userModel, update: true)
					}

//					var subs = jsonData["subscriptions"].arrayValue
//
//					for sub in subs
//					{
//						var store_id = sub.dictionaryValue["store_id"]?.stringValue
//
//						var m = SubscriptionModel()
//						m.PurchaseId = store_id!
//
//						try! Core.shared().getRealm.write {
//							Core.shared().getRealm.add(m, update: true)
//						}
//					}

					self.registerProducts()
				}
			}
			else
			{

			}
		}

		Core.shared().ValidateRecept { (success) in
			
		}

		Timer.schedule(repeatInterval: 60 * 10) { (timer) in
			Core.shared().ValidateRecept { (success) in

			}
		}
	}

	var waitingView: UIView!

	func showProcessing()
	{
		if(waitingView != nil)
		{
			waitingView.removeFromSuperview()
		}

		waitingView = UIView()
		waitingView.backgroundColor = UIColor.black.withAlphaComponent(0.75)
		waitingView.setX(0)
		waitingView.setY(0)
		waitingView.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
		self.view.addSubview(waitingView)

		var waitingLoader = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
		waitingLoader.setSize(50, 50)
		waitingLoader.toCenterX(waitingView)
		waitingLoader.toCenterY(waitingView)
		waitingLoader.startAnimating()
		waitingView.addSubview(waitingLoader)

		var waitingTitle = UILabel()
		waitingTitle.textAlignment = .center
		waitingTitle.setSize(ScreenSize.SCREEN_WIDTH, 20)
		waitingTitle.text = s("processing")
		waitingTitle.setX(0)
		waitingTitle.setY(10, relative: waitingLoader)
		waitingTitle.textColor = UIColor.white
		waitingView.addSubview(waitingTitle)

		self.view.bringSubviewToFront(waitingView)
	}

	func hideProcessing()
	{
		if(waitingView != nil)
		{
			waitingView.removeFromSuperview()
		}
	}

	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)
	}

	@objc func observerShowProcessing(_ notification: NSNotification)
	{
		DispatchQueue.main.async(execute: {
			self.showProcessing()
		})
	}

	@objc func observerHideProcessing(_ notification: NSNotification)
	{
		DispatchQueue.main.async(execute: {
			self.hideProcessing()
		})
	}

	func registerObservers()
	{
		NotificationCenter.default.addObserver(self, selector: #selector(self.observerChangeController(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" ,Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_CHANGE_CONTROLLER)), object: nil)

		NotificationCenter.default.addObserver(self, selector: #selector(self.observerRemoveController(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_REMOVE_CONTROLLER)), object: nil)


		NotificationCenter.default.addObserver(self, selector: #selector(self.observerDoPurchase(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_DO_PURCHASE)), object: nil)

		NotificationCenter.default.addObserver(self, selector: #selector(self.observerDoPurchaseResore(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_DO_RESTORE_PURCHASE)), object: nil)

		NotificationCenter.default.addObserver(self, selector: #selector(self.observerOpenDoc(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_OPEN_DOC)), object: nil)

		//EVENT_DO_SHARE

		NotificationCenter.default.addObserver(self, selector: #selector(self.observerShare(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_DO_SHARE)), object: nil)

		//EVENT_SHOW_LEFT_MENU

		NotificationCenter.default.addObserver(self, selector: #selector(self.observerShowLeftMenu(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_SHOW_LEFT_MENU)), object: nil)

		//EVENT_SHOW_SUBSRIBTIONS
		NotificationCenter.default.addObserver(self, selector: #selector(self.observerSubsciptions(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_SHOW_SUBSRIBTIONS)), object: nil)

		//EVENT_SHOW_SETTINGS
		NotificationCenter.default.addObserver(self, selector: #selector(self.observerSettings(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_SHOW_SETTINGS)), object: nil)

		NotificationCenter.default.addObserver(self, selector: #selector(self.observerShowProcessing(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_SHOW_PROCESSING)), object: nil)

		NotificationCenter.default.addObserver(self, selector: #selector(self.observerHideProcessing(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_HIDE_PROCESSING)), object: nil)

		//EVENT_ADD_IMAGE_TO_DOC

		NotificationCenter.default.addObserver(self, selector: #selector(self.observerEventOpenPhotoLibrary(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_ADD_IMAGE_TO_DOC)), object: nil)

		NotificationCenter.default.addObserver(self, selector: #selector(self.observerEventPremium(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_PREMIUM)), object: nil)

		NotificationCenter.default.addObserver(self, selector: #selector(self.observerEventPremiumHide(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_PREMIUM_HIDE)), object: nil)

		NotificationCenter.default.addObserver(self, selector: #selector(self.observerAppForeground(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" ,Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_APP_FOREGROUND)), object: nil)
	}

	var imagePicker = UIImagePickerController(navigationBarStyle: UIBarStyle.black)

	var selectedDocID: String = ""

	@objc func observerAppForeground(_ notification: NSNotification)
	{
		DispatchQueue.main.async(execute: {
			Core.shared().ValidateRecept(completion: { (success) in
				
			})
		})
	}

	@objc func observerEventPremium(_ notification: NSNotification)
	{
		DispatchQueue.main.async(execute: {
			if(self.premium != nil)
			{
				self.premium.removeFromSuperview()
			}

			self.premium = Premium()
			self.view.addSubview(self.premium)
			self.view.bringSubviewToFront(self.premium)
		})
	}

	@objc func observerEventPremiumHide(_ notification: NSNotification)
	{
		DispatchQueue.main.async(execute: {
			if(self.premium != nil)
			{
				self.premium.removeFromSuperview()
			}
		})
	}

	@objc func observerEventOpenPhotoLibrary(_ notification: NSNotification)
	{
		DispatchQueue.main.async(execute: {
			var info = notification.userInfo as! Dictionary<String, AnyObject>

			var docId = info["doc_id"] as! String

			self.selectedDocID = docId

			if UIImagePickerController.isSourceTypeAvailable(.photoLibrary)
			{
				print("Button capture")

				self.imagePicker.delegate = self
				self.imagePicker.sourceType = .savedPhotosAlbum;
				self.imagePicker.allowsEditing = false

				self.present(self.imagePicker, animated: true, completion: nil)
			}
		})
	}

	@objc func observerSettings(_ notification: NSNotification)
	{
		DispatchQueue.main.async(execute: {
			var sub = Settings()
			self.view.addSubview(sub)
			self.view.bringSubviewToFront(sub)
		})
	}

	@objc func observerSubsciptions(_ notification: NSNotification)
	{
		DispatchQueue.main.async(execute: {
			var sub = Subscription()
			self.view.addSubview(sub)
			self.view.bringSubviewToFront(sub)

		})
	}

	@objc func observerShowLeftMenu(_ notification: NSNotification)
	{
		DispatchQueue.main.async(execute: {
			var menu = LeftMenu()
			self.view.addSubview(menu)

			self.view.bringSubviewToFront(menu)
			menu.Show()

		})
	}

	@objc func observerRemoveController(_ notification: NSNotification)
	{
		DispatchQueue.main.async(execute: {

			var info = notification.userInfo as! Dictionary<String, AnyObject>

			let controllerName = info["controller"] as! String

			if let controllerModel = self._controllers.filter({ $0.Name == controllerName }).first
			{
				controllerModel.Controller.view.removeFromSuperview()
			}

			if let ii = self._controllers.index(where: { $0.Name == controllerName })
			{
				self._controllers.remove(at: ii)
			}
		})
	}

	@objc func observerChangeController(_ notification: NSNotification)
	{
		DispatchQueue.main.async(execute: {

			Core.shared().RootWindow.rootViewController!.view.bringSubviewToFront(self.view)

			print("*** observerChangeController 1")

			var info = notification.userInfo as! Dictionary<String, AnyObject>

			let controllerName = info["controller"] as! String
			let controllerParams = info["params"] as? [String: Any] ?? [String: Any]()

			if(controllerName.isEmpty)
			{
				return
			}

			if(self.currentControllerName == controllerName)
			{
				let controllerModel = self._controllers.filter({ $0.Name == controllerName }).first
				(controllerModel?.Controller as? ViewControllerDelegate)?.setParams(params: controllerParams)
				(controllerModel?.Controller as? ViewControllerDelegate)?.willBeAppear()
				return
			}

			print("*** observerChangeController 2")

			self.currentControllerName = controllerName

			for i in Core.shared().RootWindow.rootViewController!.view.subviews
			{
				UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {

					i.frame.origin.x = ScreenSize.SCREEN_WIDTH
				}, completion: nil)
			}

			for i in self._controllers
			{
				if(i.Name != self.currentControllerName)
				{
					(i.Controller as? ViewControllerDelegate)?.willBeDisappear()
				}
			}

			if(self._controllers.filter({ $0.Name == controllerName }).count == 0)
			{

				print("*** observerChangeController 3")
				
				let controller = self.getControllerByName(name: controllerName, params: controllerParams)
				let controllerModel = ControllerModel(name: controllerName, controller: controller)

				(controllerModel.Controller as? ViewControllerDelegate)?.setParams(params: controllerParams)

				self._controllers.append(controllerModel)
				//				self.view.addSubview(controllerModel.Controller.view)
				Core.shared().RootWindow.rootViewController!.view.addSubview(controllerModel.Controller.view)

				(controllerModel.Controller as? ViewControllerDelegate)?.willBeAppear()

				//				self.view.bringSubview(toFront: controllerModel.Controller.view)

				Core.shared().RootWindow.rootViewController!.view.bringSubviewToFront(controllerModel.Controller.view)

				controllerModel.Controller.view.frame.origin.x = 0

			}
			else
			{

				print("*** observerChangeController 4")
				let controllerModel = self._controllers.filter({ $0.Name == controllerName }).first

				(controllerModel?.Controller as? ViewControllerDelegate)?.setParams(params: controllerParams)

				//				self.view.bringSubview(toFront: (controllerModel?.Controller.view)!)

				Core.shared().RootWindow.rootViewController!.view.bringSubviewToFront((controllerModel?.Controller.view)!)

				(controllerModel?.Controller as? ViewControllerDelegate)?.willBeAppear()

				controllerModel?.Controller.view.frame.origin.x = 0

			}
		})
	}

	func getControllerByName(name: String, params: Dictionary<String, Any>) -> UIViewController
	{
		print("*** getControllerByName \(name)")
		switch(name)
		{
		case "scanController":
			return ScanController()
		case "settingsController":
			return SettingsController()
		case "libraryController":
			return LibraryController()
		case "signatureController":
			return SignatureController()
		default:
			return LibraryController()
		}
	}

	//---

	func PhotoPreviewOpenLibrary()
	{
		Core.shared().changeViewController(name: "libraryController")
	}

	func PhotoPreviewOCR(id: String)
	{
		print("OPEN OCR \(id)")
		var ocr = PhotoScanner(id: id)
		self.view.addSubview(ocr)
	}

	func PhotoPreviewRetakeImage()
	{
		Core.shared().changeViewController(name: "scanController")
	}
}

extension MainController: UINavigationControllerDelegate, UIImagePickerControllerDelegate
{
	func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!)
	{
		print("didFinishPickingImage 1")
	}

	func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
	{
		print("** imagePickerControllerDidCancel 1")

		self.dismiss(animated: true, completion: { () -> Void in

		})
	}

	func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool)
	{
		print("*** willShow UIImagePickerController 1")

		UIApplication.shared.statusBarStyle = .default
	}

	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
	{
		print("didFinishPickingImage 1")

		DispatchQueue.main.async(execute: {

			EventsManager.shared().sendNotify(Constants.EVENT_SHOW_PROCESSING)

			print("didFinishPickingImage 11")

			self.dismiss(animated: true, completion: { () -> Void in

//				print("didFinishPickingImage 111")
//
				var image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage

				EventsManager.shared().sendNotify(Constants.EVENT_ADDED_IMAGE, data: ["image": image, "doc_id": self.selectedDocID])
//				//

			})

		})
	}
}
