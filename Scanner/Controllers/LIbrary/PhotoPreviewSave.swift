//
//  PhotoPreviewSave.swift
//  Scanner
//
//  Created by Artem  on 26/09/2018.
//  Copyright © 2018 VladimirKuzmin. All rights reserved.
//

import Foundation
import UIKit

protocol DocEditDelegate
{
	func PhotoPreviewRetakeImage()

	func PhotoPreviewOCR(id: String)

	func PhotoPreviewOpenLibrary()
}

class DocEdit: UIView, UIScrollViewDelegate, ActionsMenuDelegate
{
	var infoPlaceholder: UIView!

	var scrollStrip: UIScrollView!

	var delegate: DocEditDelegate!

	var bottom: UIView!

	var startOffset: CGFloat = 0

	var top: UIView!

	var docName = ""

	var scroll: UIView!

	private var _image: UIImage!

	private var _id: String = ""

	var imageView: UIImageView!

	var files = [DocModelFile]()

	var infoText: UILabel!

	var currentFile = 0

	var tools: PhotoPreviewTool!

	var buttonScan: UIView!

	var newPhoto: UIView!

	var editLayout: UIView!

	var shareIcon: UIImageView!

	var isEdit = false

	var factor2: CGFloat = 0

	var buttonScanTitle: UILabel!

	init(id: String)
	{
		super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))

		NotificationCenter.default.addObserver(self, selector: #selector(self.observerEventAddedImage(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_ADDED_IMAGE)), object: nil)

		self.backgroundColor = UIColor.black

		startOffset = UIApplication.shared.statusBarFrame.size.height

		if let doc = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(id)'").first
		{
			print(doc)

			self._id = doc.Id

			self.docName = doc.Name

			self.initTopControls()

			self.initBottomControls()

			//-----

			currentFile = 0

			self.files = [DocModelFile]()

			for i in Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(doc.Id)'")
			{
				files.append(i)
			}

			self.scroll = UIView()
//			scroll.backgroundColor = .red
//			self.scroll.delegate = self
			self.scroll.setX(10)
			self.scroll.setY(10, relative: top)
			self.scroll.setSize(ScreenSize.SCREEN_WIDTH - 20, ScreenSize.SCREEN_HEIGHT - top.getHeight() - bottom.getHeight() - 20)
//			self.scroll.frame = CGRect(0, 0, ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
			self.scroll.backgroundColor = UIColor.black
//			self.scroll.alwaysBounceVertical = false
//			self.scroll.alwaysBounceHorizontal = false
//			self.scroll.showsVerticalScrollIndicator = true
//			self.scroll.flashScrollIndicators()

//			self.scroll.minimumZoomScale = 1.0
//			self.scroll.maximumZoomScale = 10.0

			self.addSubview(scroll)

			if(files.count > 0)
			{
				self.initImage(image: files[self.currentFile])
			}

			print("** FILES COINT \(files.count)")

			self.reinitFilesStrip()

			//---
			var buttonScanText = ""

			if(files.count > 0 && files[0].ORCText.count > 0)
			{
				buttonScanText = s("button_edit_text")
			}
			else
			{
				buttonScanText = s("button_start_recognize")
			}


			var buttonScanTextSize = getLabelSize(str: buttonScanText, size: 20)

			buttonScan = UIView()
			buttonScan.layer.cornerRadius = 30
			buttonScan.setSize(buttonScanTextSize.width + 40, 60)
			buttonScan.backgroundColor = UIColor.init(red: 0.21, green: 0.42, blue: 0.69, alpha: 1)
			buttonScan.setX(ScreenSize.SCREEN_WIDTH - buttonScan.getWidth() - 20)
			buttonScan.setY(ScreenSize.SCREEN_HEIGHT - bottom.getHeight() - (buttonScan.getHeight() / 2) - 20)
			buttonScan.clipsToBounds = true
//			buttonScan.backgroundColor = UIColor(patternImage: UIImage(named: "button_scan")!)
			self.addSubview(buttonScan)

			buttonScanTitle = UILabel()
			buttonScanTitle.textAlignment = .center
			buttonScanTitle.text = buttonScanText
			buttonScanTitle.textColor = .white
			buttonScanTitle.font = UIFont.systemFont(ofSize: 20)
			buttonScanTitle.setX(0)
			buttonScanTitle.setY(0)
			buttonScanTitle.setSize(buttonScan.getWidth(), buttonScan.getHeight())
			buttonScan.addSubview(buttonScanTitle)

			var buttonScanAction = UIButton()
			buttonScanAction.addTarget(self, action: #selector(self.recognizeAction(_:)), for: .touchUpInside)
			buttonScanAction.setX(0)
			buttonScanAction.setY(0)
			buttonScanAction.setSize(buttonScan.getWidth(), buttonScan.getHeight())
			buttonScan.addSubview(buttonScanAction)

			//----

			newPhoto = UIView()
			newPhoto.backgroundColor = UIColor.init(red: 0.21, green: 0.42, blue: 0.69, alpha: 1)
			newPhoto.setX(buttonScan.getX() - 10 - 60)
			newPhoto.setY(buttonScan.getY())
			newPhoto.setSize(60, 60)
			newPhoto.layer.cornerRadius = newPhoto.getWidth() / 2
			self.addSubview(newPhoto)

			var newPhotoImagePlus = UIImageView()
			newPhotoImagePlus.setY(15)
			newPhotoImagePlus.setX(15)
			newPhotoImagePlus.setSize(newPhoto.getWidth() - 30, newPhoto.getHeight() - 30)
			newPhotoImagePlus.image = UIImage(named: "new_photo_plus")
			newPhoto.addSubview(newPhotoImagePlus)

			var newPhotoButton = UIButton()
			newPhotoButton.addTarget(self, action: #selector(self.openScan(_:)), for: .touchUpInside)
			newPhotoButton.setY(0)
			newPhotoButton.setX(0)
			newPhotoButton.setSize(newPhoto.getWidth(), newPhoto.getHeight())			
			newPhoto.addSubview(newPhotoButton)
		}
		else
		{

		}

//		var waitingView = UIView()
//		waitingView.backgroundColor = UIColor.red
//		waitingView.setX(0)
//		waitingView.setY(0)
//		waitingView.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
//		self.addSubview(waitingView)
//
//		var waitingLoader = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
//		waitingLoader.setSize(50, 50)
//		waitingLoader.toCenterX(waitingView)
//		waitingLoader.toCenterY(waitingView)
//		waitingLoader.startAnimating()
//		waitingView.addSubview(waitingLoader)
//
//		var waitingTitle = UILabel()
//		waitingTitle.textAlignment = .center
//		waitingTitle.setSize(ScreenSize.SCREEN_WIDTH, 20)
//		waitingTitle.text = "Processing"
//		waitingTitle.setX(0)
//		waitingTitle.setY(10, relative: waitingLoader)
//		waitingTitle.textColor = UIColor.white
//		waitingView.addSubview(waitingTitle)
//
//		self.bringSubviewToFront(waitingView)
	}

	@objc func openEditTools(_ sender: AnyObject)
	{
		buttonScan.isHidden = true

		newPhoto.isHidden = true

		editLayout.isHidden = true

		shareIcon.image = UIImage(named: "image_save")

		isEdit = true

		tools = PhotoPreviewTool()
		tools.delegate = self
		self.addSubview(tools)
		tools.Show()
	}

	@objc func backToLibrary(_ sender: AnyObject)
	{
		if(isEdit)
		{
			if(tools != nil)
			{
				tools.Hide(finish: {})

				buttonScan.isHidden = false

				newPhoto.isHidden = false

				editLayout.isHidden = false

				shareIcon.image = UIImage(named: "share_icon")
			}

			isEdit = false

			renderImage(image: self._image)
		}
		else
		{
			self.removeFromSuperview()

			Core.shared().changeViewController(name: "libraryController")
		}
	}


	@objc func recognizeAction(_ sender: AnyObject)
	{
		if(Core.shared().NetworkStatus.connection == .none)
		{
			return
		}

		Core.shared().checkTrial() { status in
			if(status == .Premium || Constants.IS_DEV)
			{
				self.buttonScanTitle.text = s("button_edit_text")
				self.delegate.PhotoPreviewOCR(id: self._id)
			}
			else
			{
				Core.shared().showPremium()
			}
		}


	}

	func reinitFilesStrip()
	{
		if(scrollStrip != nil)
		{
			scrollStrip.removeFromSuperview()
		}

		scrollStrip = UIScrollView()
		scrollStrip.setX(20)
		scrollStrip.setY(20)
		scrollStrip.setSize(bottom.getWidth() - 40, bottom.getHeight() - 40)
		bottom.addSubview(scrollStrip)

		for i in scrollStrip.subviews
		{
			i.removeFromSuperview()
		}

		var offset: CGFloat = 0

		var blockSize: CGFloat = bottom.getHeight() - 40

		var ii = 0

		for i in files
		{
			var item = UIView()
			item.tag = 10000 + ii
			item.setX(offset)
			item.setY(0)
			item.setSize(blockSize, blockSize)
			scrollStrip.addSubview(item)

			if(ii == currentFile)
			{
				item.layer.borderWidth = 2
				item.layer.borderColor = UIColor(red: 0.13, green: 0.73, blue: 0.81, alpha: 1).cgColor
			}

			offset += 10 + blockSize

			var imageView = UIImageView()
			imageView.setY(0)
			imageView.setX(0)
			imageView.setSize(item.getWidth(), item.getHeight())
			item.addSubview(imageView)

			let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)

			let path = docDirURL.appendingPathComponent(i.FileName)

			let data = try? Data(contentsOf: path)

			imageView.image = UIImage(data: data!)!
			imageView.layer.contentsGravity = CALayerContentsGravity.resizeAspectFill
			imageView.layer.masksToBounds = true

			var imageButton = UIButton()
			imageButton.addTarget(self, action: "buttonItemStripeAction:", for: .touchUpInside)
			imageButton.tag = 20000 + ii
			imageButton.setX(0)
			imageButton.setY(0)
			imageButton.setSize(item.getWidth(), item.getHeight())
			item.addSubview(imageButton)

			ii += 1
		}

		scrollStrip.contentSize = CGSize(width: offset, height: bottom.getHeight() - 40)
	}

	func renderImage(image: UIImage)
	{
		self._image = image

		let imageWidthOrigin = self._image.size.width
		let imageHeightOrigin = self._image.size.height

		print("** IMAGE \(imageWidthOrigin) \(imageHeightOrigin)")

		let maxImageWidth: CGFloat = ScreenSize.SCREEN_WIDTH - 40

		let maxImageHeight: CGFloat = ScreenSize.SCREEN_HEIGHT - 40 - top.getHeight() - bottom.getHeight()

		var imageWidth: CGFloat = 0
		var imageHeight: CGFloat = 0

		var factor: CGFloat = 0

		imageHeight = maxImageHeight

		factor = CGFloat(imageHeight) / CGFloat(imageHeightOrigin)

		print("** FACTOR \(CGFloat(imageHeightOrigin) / CGFloat(imageHeight) )")

		imageWidth = CGFloat(imageWidthOrigin) * factor

		if(imageWidth > maxImageWidth)
		{
			imageWidth = maxImageWidth

			factor = CGFloat(imageWidth) / CGFloat(imageWidthOrigin)

			imageHeight = CGFloat(imageHeightOrigin) * factor
		}

		factor2 = imageWidthOrigin / imageWidth

		print("** ORIGINAL SIZE \(imageWidthOrigin) \(imageHeightOrigin)")
		print("** NEW SIZE \(imageWidth) \(imageHeight)")

		if(self.imageView != nil)
		{
			self.imageView.removeFromSuperview()
		}

		imageView = UIImageView()
		imageView.isUserInteractionEnabled = true
		imageView.setSize(imageWidth, imageHeight)
		imageView.toCenterX(self.scroll)
		imageView.toCenterY(self.scroll)
		imageView.image = self._image

		self.scroll.addSubview(imageView)

//		let upSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeUp(_:)))
//		upSwipe.direction = UISwipeGestureRecognizer.Direction.up
//		imageView.addGestureRecognizer(upSwipe)

		//---

		let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.leftSwipe(_:)))
		leftSwipe.direction = UISwipeGestureRecognizer.Direction.left
		imageView.addGestureRecognizer(leftSwipe)

		let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.rightSwipe(_:)))
		rightSwipe.direction = UISwipeGestureRecognizer.Direction.right
		imageView.addGestureRecognizer(rightSwipe)
	}

	func initImage(image: DocModelFile)
	{
		let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)

		let path = docDirURL.appendingPathComponent(image.FileName)

		let data = try? Data(contentsOf: path)

		renderImage(image: UIImage(data: data!)!)
	}

	func actionLeft()
	{

	}

	func actionRight()
	{

	}

	@objc func buttonItemStripeAction(_ sender: AnyObject)
	{
		print("*** buttonItemStripeAction CURRENT \(self.currentFile) TAG \(sender.tag)")

		var id = sender.tag - 20000

		if let item = scrollStrip.viewWithTag(10000 + self.currentFile)
		{
			item.layer.borderWidth = 0
			item.layer.borderColor = UIColor.clear.cgColor
		}

		self.currentFile = id

		if let item = scrollStrip.viewWithTag(10000 + self.currentFile)
		{
			item.layer.borderWidth = 2
			item.layer.borderColor = UIColor(red: 0.13, green: 0.73, blue: 0.81, alpha: 1).cgColor
		}

		//			infoText.text = "Files: \(self.currentFile + 1) / \(files.count)"

		initImage(image: self.files[self.currentFile])
	}

	@objc func leftSwipe(_ sender: AnyObject)
	{
		print("** current file \(self.currentFile) files \(self.files.count)")

		if(self.currentFile + 1 < self.files.count)
		{
			if let item = scrollStrip.viewWithTag(10000 + self.currentFile)
			{
				item.layer.borderWidth = 0
				item.layer.borderColor = UIColor.clear.cgColor
			}

			self.currentFile += 1

			if let item = scrollStrip.viewWithTag(10000 + self.currentFile)
			{
				item.layer.borderWidth = 2
				item.layer.borderColor = UIColor(red: 0.13, green: 0.73, blue: 0.81, alpha: 1).cgColor
			}

//			infoText.text = "Files: \(self.currentFile + 1) / \(files.count)"

			initImage(image: self.files[self.currentFile])
		}
	}

	@objc func rightSwipe(_ sender: AnyObject)
	{
		print("** current file \(self.currentFile) files \(self.files.count)")

		if(self.currentFile > 0)
		{
			if let item = scrollStrip.viewWithTag(10000 + self.currentFile)
			{
				item.layer.borderWidth = 0
				item.layer.borderColor = UIColor.clear.cgColor
			}

			self.currentFile -= 1

			if let item = scrollStrip.viewWithTag(10000 + self.currentFile)
			{
				item.layer.borderWidth = 2
				item.layer.borderColor = UIColor(red: 0.13, green: 0.73, blue: 0.81, alpha: 1).cgColor
			}

//			infoText.text = "Files: \(self.currentFile + 1) / \(files.count)"

			initImage(image: self.files[self.currentFile])
		}
	}

	@objc func swipeUp(_ sender: UISwipeGestureRecognizer)
	{
		UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveEaseInOut, animations:
			{
				self.frame.origin.y = 0 - ScreenSize.SCREEN_HEIGHT
		}, completion: { (finished: Bool) -> Void in

			self.delegate.PhotoPreviewOpenLibrary()
			self.removeFromSuperview()
		})
	}

	func viewForZooming(in scrollView: UIScrollView) -> UIView?
	{
		return self.imageView
	}

	func scrollViewDidZoom(_ scrollView: UIScrollView)
	{
		let width = self.imageView.frame.width
		let height = self.imageView.frame.height

		self.imageView.toCenterX(self.scroll)
		self.imageView.toCenterY(self.scroll)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	@objc func openLeftMenuAction(_ sender: AnyObject)
	{
		EventsManager.shared().sendNotify(Constants.EVENT_SHOW_LEFT_MENU)
	}

	func initTopControls()
	{
		top = UIView()
		top.backgroundColor = UIColor.init(red: 0.21, green: 0.42, blue: 0.69, alpha: 1)
		top.setX(0)
		top.setY(0)
		top.setSize(ScreenSize.SCREEN_WIDTH, 60 + startOffset)
		top.clipsToBounds = true
		self.addSubview(top)

		///------

		let iconSize: CGFloat = 50 / 2.5

		editLayout = UIView()
//		editLayout.backgroundColor = .red
		editLayout.setX(top.getWidth() - 50 - 50 - 10)
		editLayout.setY(startOffset + 20)
		editLayout.setSize(50, 30)
		top.addSubview(editLayout)

		var editIcon = UIImageView()
		editIcon.setSize(iconSize, iconSize)
		editIcon.toCenterX(editLayout)
		editIcon.toCenterY(editLayout)
		editIcon.image = UIImage(named: "lib_icon_edit")
		editLayout.addSubview(editIcon)

		let editAction = UIButton()
		editAction.addTarget(self, action: #selector(self.openEditTools(_:)), for: .touchUpInside)
		editAction.setX(0)
		editAction.setY(0)
		editAction.setSize(editLayout.getWidth(), editLayout.getHeight())
		editLayout.addSubview(editAction)

		var shareLayout = UIView()
		shareLayout.setX(0, relative: editLayout)
		shareLayout.setY(startOffset + 20)
		shareLayout.setSize(50, 30)
		top.addSubview(shareLayout)

		shareIcon = UIImageView()
		shareIcon.setSize(iconSize, iconSize)
		shareIcon.toCenterX(editLayout)
		shareIcon.toCenterY(editLayout)
		shareIcon.image = UIImage(named: "share_icon")
		shareLayout.addSubview(shareIcon)

		let shareAction = UIButton()
		shareAction.addTarget(self, action: #selector(self.actionShare(_:)), for: .touchUpInside)
		shareAction.setX(0)
		shareAction.setY(0)
		shareAction.setSize(shareLayout.getWidth(), shareLayout.getHeight())
		shareLayout.addSubview(shareAction)

		//----

		//----

		let libraryLayout = UIView()
		libraryLayout.setX(0)
		libraryLayout.setY(startOffset + 20)
		libraryLayout.setSize(50, 30)
		top.addSubview(libraryLayout)

		let libraryIcon = UIImageView()
		libraryIcon.setSize(12, iconSize)
		libraryIcon.toCenterX(libraryLayout)
		libraryIcon.toCenterY(libraryLayout)
		libraryIcon.image = UIImage(named: "left_menu_back")
		libraryLayout.addSubview(libraryIcon)

		let libraryAction = UIButton()
		libraryAction.addTarget(self, action: #selector(self.backToLibrary(_:)), for: .touchUpInside)
		libraryAction.setX(0)
		libraryAction.setY(0)
		libraryAction.setSize(libraryLayout.getWidth(), libraryLayout.getHeight())
		libraryLayout.addSubview(libraryAction)

		//------

		var libTitle = UILabel()
		//		libTitle.backgroundColor = .yellow
		libTitle.setX(0, relative: libraryLayout)
		libTitle.setY(startOffset + 20)
		libTitle.setSize(ScreenSize.SCREEN_WIDTH - 140, 30)
		libTitle.textColor = UIColor.white
		libTitle.textAlignment = .center
		libTitle.text = self.docName
		libTitle.font = UIFont.systemFont(ofSize: 16)
		top.addSubview(libTitle)
	}

	func initBottomControls()
	{
		bottom = UIView()
		bottom.backgroundColor = UIColor.black
		bottom.setX(0)
		bottom.setY(ScreenSize.SCREEN_HEIGHT - 120 - startOffset)
		bottom.setSize(ScreenSize.SCREEN_WIDTH, 120 + startOffset)
		self.addSubview(bottom)

	}

//	func saveToLibrary(image: UIImage) -> DocModel
//	{
//		let doc = DocModel()
//		doc.Id = UUID().uuidString
//		doc.CreatedAt = Int64(NSDate().timeIntervalSince1970)
//		doc.Name = self.fileName
//		doc.Width = Float(image.size.width)
//		doc.Height = Float(image.size.height)
//		doc.FileName = UUID().uuidString + ".jpg"
//
//		try! Core.shared().getRealm.write {
//			Core.shared().getRealm.add(doc)
//		}
//
//		return doc
//	}
//
	func saveToPath(image: UIImage, fileName: String)
	{
//		let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
//		let filePath = "\(paths[0])/\(fileName).jpg"

//		UIImageJ

		let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)

		let path = docDirURL.appendingPathComponent(fileName)

		//try FileManager.default.copyItem(atPath: (documentsURL1?.path)!, toPath: docDirURL.appendingPathComponent(i).path)
		try! image.jpegData(compressionQuality: 1)?.write(to: path, options: [.atomic])

//		UIImage.jpegData(compressionQuality: 1).write(to: URL(fileURLWithPath: path.absoluteString), options: [.atomic])

//		try? UIImageJPEGRepresentation(image, 1.0)!.write(to: URL(fileURLWithPath: path.absoluteString), options: [.atomic])
//		UIImagePNGRepresentation(image)?.writeToFile(filePath, atomically: true)
	}

	@objc func actionShare(_ sender: AnyObject)
	{
		if(isEdit) // then do save
		{
			if(tools != nil)
			{
				tools.Hide(finish: {

					let imageName = self.files[self.currentFile].FileName

					var image = self.imageView.image!

					self._image = image

					self.saveToPath(image: image, fileName: imageName)

					self.buttonScan.isHidden = false

					self.newPhoto.isHidden = false

					self.editLayout.isHidden = false

					self.shareIcon.image = UIImage(named: "share_icon")

					self.isEdit = false

					if(self.editMode == .Crop)
					{
						EventsManager.shared().sendNotify(Constants.EVENT_SHOW_PROCESSING)

						let alertController = UIAlertController(title: s("crop_title"), message: s("crop_subtitle"), preferredStyle: .alert)

						let okAction = UIAlertAction(title: s("crop_button"), style: .default, handler: {
							(action : UIAlertAction!) -> Void in

//							DispatchQueue.main.async(execute: {
								self.saveCrop()
//							})
							EventsManager.shared().sendNotify(Constants.EVENT_HIDE_PROCESSING)
						})

						alertController.addAction(okAction)

						let cancelAction = UIAlertAction(title: s("cancel_title"), style: .cancel, handler: {
							(action : UIAlertAction!) -> Void in

							DispatchQueue.main.async(execute: {

								EventsManager.shared().sendNotify(Constants.EVENT_HIDE_PROCESSING)

								if(self.crop != nil)
								{
									self.crop.removeFromSuperview()
								}
							})
						})

						alertController.addAction(cancelAction)

						self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
					}
				})
			}
		}
		else
		{
			if let doc = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(self._id)'").first
			{
				var items = [
					ActionMenuItem(title: s("share_image"), itemId: 1),
					ActionMenuItem(title: s("share_pdf"), itemId: 2),
					ActionMenuItem(title: s("save_to_library"), itemId: 3)
				]

				if(doc.ORCText.count > 0)
				{
					items.append(ActionMenuItem(title: s("share_recognized_pdf"), itemId: 4))
					items.append(ActionMenuItem(title: s("share_recognized_plain_text"), itemId: 5))
				}

				let menu = ActionsMenu(menuId: 1, items: items)

				menu.delegate = self

				self.addSubview(menu)

				menu.Show()
			}
		}
	}

	func ActionsMenuAction(menuId: Int, itemId: Int)
	{
		switch(itemId)
		{
		case 1:
			EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._id, "type": "image"])
			break;
		case 2:
			EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._id, "type": "pdf"])
			break;
		case 3:
			EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._id, "type": "image_save"])
			break;
		case 4:
			EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._id, "type": "ocrpdf"])
			break;
		case 5:
			EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._id, "type": "text"])
			break;
		default:
			break;
		}
		//
	}

	func ActionsMenuCancel()
	{

	}

	@objc func actionOCR(_ sender: AnyObject)
	{
//		var doc = self.saveToLibrary(image: self._image)
//
//		self.saveToPath(image: self._image, fileName: doc.FileName)
//
		self.delegate.PhotoPreviewOCR(id: self._id)
	}

	@objc func saveImageAction(_ sender: AnyObject)
	{
//		var doc = self.saveToLibrary(image: self._image)
//
//		self.saveToPath(image: self._image, fileName: doc.FileName)
//
//		self.delegate.PhotoPreviewOpenLibrary()
	}

	@objc func retakeImageAction(_ sender: AnyObject)
	{
		self.delegate.PhotoPreviewRetakeImage()

		self.removeFromSuperview()
	}

	@objc func renameFileAction(_ sender: AnyObject)
	{

	}

	@objc func openLibraryAction(_ sender: AnyObject)
	{
		self.delegate.PhotoPreviewOpenLibrary()

		self.removeFromSuperview()
	}

	var crop: CropPolygon!

	var editMode = PhotoPreviewToolMode.Bright
}



extension DocEdit: CropPolygonDelegate
{
	func CropPolygonChangeSize(points: [CGPoint])
	{

	}

	func saveCrop()
	{
		print("** SAVE CROP ORIENTATION \(self._image.imageOrientation.rawValue)")

		DispatchQueue.main.async(execute: {
			var points = self.crop.getPoints()

			var originalImage = self.imageView.image

			let updatedImage = originalImage?.cropByPoints(points: points, imageCropFactor: self.factor2)

			self.saveToPath(image: updatedImage!, fileName: self.files[self.currentFile].FileName)

			self.renderImage(image: updatedImage!)
		})
	}
}

extension DocEdit: PhotoPreviewToolDelegate
{
	func changeTool(newMode: PhotoPreviewToolMode)
	{
		if(crop != nil)
		{
			crop.removeFromSuperview()
		}

		editMode = newMode
	}

	func showCropTool()
	{
		print("** SHOW CROP TOOL ORIENTATION \(imageView.image?.imageOrientation.rawValue) SCALE \(imageView.image?.scale)")
		//imageView

		if(crop != nil)
		{
			crop.removeFromSuperview()
		}

		crop = CropPolygon(width: imageView.getWidth(), height: imageView.getHeight(), points: [
			CGPoint(x: 10, y: 10),
			CGPoint(x: imageView.getWidth() - 10, y: 10),
			CGPoint(x: imageView.getWidth() - 10, y: imageView.getHeight() - 10),
			CGPoint(x: 10, y: imageView.getHeight() - 10)
			])
		crop.delegate = self
		imageView.addSubview(crop)


	}

	func rotateImageLeft()
	{
		var image = imageView.image!.imageRotatedByDegrees(degrees: 90, flip: true)

		renderImage(image: image)
	}

	func rotateImageRight()
	{
		var image = imageView.image!.imageRotatedByDegrees(degrees: -90, flip: true)

		renderImage(image: image)
	}

	func getOriginImage() -> UIImage
	{
		return imageView.image!
	}

	func passNewImage(image: UIImage)
	{
		imageView.image = image
	}

	func removeCurrentSlide()
	{
		var text = ""

//		if(files.count > 1)
//		{
//			text = s("remove_image_slide")
//		}
//		else
//		{
			text = s("remove_whole_image")
//		}

		let alertController = UIAlertController(title: s("remove_image_title"), message: text, preferredStyle: .alert)

		let okAction = UIAlertAction(title: s("remove_title"), style: .default, handler: {
			(action : UIAlertAction!) -> Void in

			DispatchQueue.main.async(execute: {
//				if(self.files.count > 1)
//				{
//					self.removeCurrentSlideAction()
//				}
//				else
//				{
					self.removeWholeDoc()
//				}
			})
		})

		alertController.addAction(okAction)

		let cancelAction = UIAlertAction(title: s("cancel_title"), style: .cancel, handler: {
			(action : UIAlertAction!) -> Void in
		})

		alertController.addAction(cancelAction)

		self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
	}

	func removeWholeDoc()
	{
		if let doc = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(self._id)'").first
		{
			try! Core.shared().getRealm.write {
				Core.shared().getRealm.delete(doc)
			}
		}

		EventsManager.shared().sendNotify("EVENT_RELOAD_LIB")

		self.removeFromSuperview()
	}

	func removeCurrentSlideAction()
	{

	}

	func observerEventAddedImage()
	{

	}

	@objc func openScan(_ sender: AnyObject)
	{
		EventsManager.shared().sendNotify(Constants.EVENT_ADD_IMAGE_TO_DOC, data: ["doc_id": self._id])
	}

	@objc func observerEventAddedImage(_ notification: NSNotification)
	{
		DispatchQueue.main.async(execute: {
			var info = notification.userInfo as! Dictionary<String, AnyObject>

			var doc_id = info["doc_id"] as! String

			var image = info["image"] as! UIImage

			var isRotate = false

			var degrees: CGFloat = 0

			switch(image.imageOrientation)
			{
			case .down:
				isRotate = true
				degrees = 180
				break
			case .left:
				isRotate = true
				degrees = 90
				break
			case .right:
				isRotate = true
				degrees = -90
				break
			case .up:
				isRotate = false
				degrees = 0
				break
			default: break
			}

			var newImage = image.toNoir(isRotate, degrees: degrees)
			//
			//				var newImage = self.imagePostProcessing(input: i2)

			var fileName = UUID().uuidString + ".jpg"

			let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)

			let fileURL = docDirURL.appendingPathComponent(fileName)

			do
			{
				try newImage.jpegData(compressionQuality: 1)!.write(to: fileURL, options: [.atomic])
			}
			catch(let error)
			{
				print(error)
			}

			let file = DocModelFile()
			file.Width = Float(image.size.width)
			file.Height = Float(image.size.height)
			file.FileName = fileName
			file.Id = UUID().uuidString
			file.DocId = self._id
			file.TypeFile = "image"

			try! Core.shared().getRealm.write {
				Core.shared().getRealm.add(file)
			}

			print("*** OLD FILES \(self.files.count)")

			self.files.append(file)

			self.currentFile = self.files.count - 1

			print("*** NEW FILES \(self.files.count)")

			self.reinitFilesStrip()

			self.initImage(image: self.files[self.currentFile])

//			waitingView.removeFromSuperview()

			EventsManager.shared().sendNotify(Constants.EVENT_HIDE_PROCESSING)
		})
	}
}
