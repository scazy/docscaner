//
//  LibrarySubtop.swift
//  Scanner
//
//  Created by Artem  on 21/01/2019.
//  Copyright © 2019 VladimirKuzmin. All rights reserved.
//

import Foundation
import UIKit

protocol LibrarySubtopDelegate
{

}

class LibrarySubtop: UIView
{
	private var height: CGFloat = 42

	var delegate: LibrarySubtopDelegate!

	init()
	{
		super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 42))

		self.backgroundColor = UIColor.white.withAlphaComponent(0.75)

		var topPart = self.getWidth() / 7

		//----

		var sortLayout = UIView()
		sortLayout.setX(5)
		sortLayout.setY(0)
		sortLayout.setSize(self.height, self.height)
		self.addSubview(sortLayout)

		var lib_i_3 = UIImage(named: "lib_i_3")
//		var lib_i_3Size = getImageSize(image: lib_i_3!, width: 0, height: sortLayout.getHeight() - 20)

		var sortIcon = UIImageView()
		sortIcon.image = lib_i_3
		sortIcon.setSize(sortLayout.getWidth() - 14, sortLayout.getWidth() - 14)
		sortIcon.setX(7)
		sortIcon.setY(7)
		sortLayout.addSubview(sortIcon)

		//----

		var filterLayout = UIView()
		filterLayout.setX(0, relative: sortLayout)
		filterLayout.setY(0)
		filterLayout.setSize(self.height, self.height)
		self.addSubview(filterLayout)

		var lib_i_2 = UIImage(named: "lib_i_2")

		var filterIcon = UIImageView()
		filterIcon.image = lib_i_2
		filterIcon.setSize(filterLayout.getWidth() - 14, filterLayout.getWidth() - 14)
		filterIcon.setX(7)
		filterIcon.setY(7)
		filterLayout.addSubview(filterIcon)

		//---

		var listLayout = UIView()
		listLayout.setX(self.getWidth() - self.height - 5)
		listLayout.setY(0)
		listLayout.setSize(self.height, self.height)
		self.addSubview(listLayout)

		var lib_i_1 = UIImage(named: "lib_i_1")
//		var lib_i_4Size = getImageSize(image: lib_i_4!, width: 0, height: listLayout.getHeight() - 20)

		var listIcon = UIImageView()
		listIcon.image = lib_i_1
		listIcon.setSize(listLayout.getWidth() - 14, listLayout.getWidth() - 14)
		listIcon.setX(7)
		listIcon.setY(7)
		listLayout.addSubview(listIcon)

		//---

		var tileLayout = UIView()
		//		newLayout.backgroundColor = .green
		tileLayout.setX(self.getWidth() - listLayout.getWidth() - 5  - self.height)
		tileLayout.setY(0)
		tileLayout.setSize(self.height, self.height)
		self.addSubview(tileLayout)

		var lib_i_8 = UIImage(named: "lib_i_8")
//		var lib_i_6Size = getImageSize(image: lib_i_6!, width: 0, height: tileLayout.getHeight() - 20)

		var newIcon = UIImageView()
		newIcon.image = lib_i_8
		newIcon.setSize(tileLayout.getWidth() - 14, tileLayout.getWidth() - 14)
		newIcon.setX(7)
		newIcon.setY(7)
		tileLayout.addSubview(newIcon)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
