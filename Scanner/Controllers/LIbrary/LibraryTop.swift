//
//  LibraryTop.swift
//  Scanner
//
//  Created by Artem  on 17/01/2019.
//  Copyright © 2019 VladimirKuzmin. All rights reserved.
//

import Foundation
import UIKit

protocol LibraryTopDelegate
{
	func LibraryTopSettingsAction()

	func LibraryTopNewAction()

	func LibraryTopSelectAction()
}

class LibraryTop: UIView
{
	private var height: CGFloat = 42

	var delegate: LibraryTopDelegate!

	var libTitle: UILabel!

	func setTopTitle(title: String)
	{
		libTitle.text = title
	}

	init()
	{
		var startOffset = UIApplication.shared.statusBarFrame.size.height
		
		super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 42 + startOffset))

		self.backgroundColor = UIColor.white

		libTitle = UILabel()
		//		libTitle.backgroundColor = .green
		libTitle.setX(0)
		libTitle.setY(startOffset)
		libTitle.setSize(ScreenSize.SCREEN_WIDTH, height)
		libTitle.textColor = UIColor.black
		libTitle.textAlignment = .center
		libTitle.text = s("library_title")
		libTitle.font = UIFont.boldSystemFont(ofSize: 17)
		self.addSubview(libTitle)

		var topPart = self.getWidth() / 7

		//----

		var settingsLayout = UIView()
		settingsLayout.setX(5)
		settingsLayout.setY(startOffset)
		settingsLayout.setSize(self.height, self.height)
		self.addSubview(settingsLayout)

		var lib_i_5 = UIImage(named: "lib_i_5")
		var lib_i_5Size = getImageSize(image: lib_i_5!, width: 0, height: settingsLayout.getHeight() - 20)

		var settingsIcon = UIImageView()
		settingsIcon.image = lib_i_5
		settingsIcon.setSize(settingsLayout.getWidth() - 14, settingsLayout.getWidth() - 14)
		settingsIcon.setX(7)
		settingsIcon.setY(7)
		settingsLayout.addSubview(settingsIcon)

		var settingsButton = UIButton()
		settingsButton.setX(0)
		settingsButton.setY(0)
		settingsButton.setSize(settingsLayout.getWidth(), settingsLayout.getHeight())
		settingsButton.addTarget(self, action: "settingsAction:", for: .touchUpInside)
		settingsLayout.addSubview(settingsButton)

		//----

		var selectLayout = UIView()
//		selectLayout.backgroundColor = .red
		selectLayout.setX(self.getWidth() - self.height - 5)
		selectLayout.setY(startOffset)
		selectLayout.setSize(self.height, self.height)
		self.addSubview(selectLayout)

		var lib_i_4 = UIImage(named: "lib_i_4")
		var lib_i_4Size = getImageSize(image: lib_i_4!, width: 0, height: selectLayout.getHeight() - 20)

		var selectIcon = UIImageView()
		selectIcon.image = lib_i_4
		selectIcon.setSize(selectLayout.getWidth() - 14, selectLayout.getWidth() - 14)
		selectIcon.setX(7)
		selectIcon.setY(7)
		selectLayout.addSubview(selectIcon)

		var selectButton = UIButton()
		selectButton.setX(0)
		selectButton.setY(0)
		selectButton.setSize(selectLayout.getWidth(), selectLayout.getHeight())
		selectButton.addTarget(self, action: "selectAction:", for: .touchUpInside)
		selectLayout.addSubview(selectButton)

		//---

		var newLayout = UIView()
//		newLayout.backgroundColor = .green
		newLayout.setX(self.getWidth() - selectLayout.getWidth() - 5  - self.height)
		newLayout.setY(startOffset)
		newLayout.setSize(self.height, self.height)
		self.addSubview(newLayout)

		var lib_i_6 = UIImage(named: "lib_i_6")
		var lib_i_6Size = getImageSize(image: lib_i_6!, width: 0, height: selectLayout.getHeight() - 20)

		newIcon = UIImageView()
		newIcon.image = lib_i_6
		newIcon.setSize(newLayout.getWidth() - 14, newLayout.getWidth() - 14)
		newIcon.setX(7)
		newIcon.setY(7)
		newLayout.addSubview(newIcon)

		var newButton = UIButton()
		newButton.setX(0)
		newButton.setY(0)
		newButton.setSize(newLayout.getWidth(), newLayout.getHeight())
		newButton.addTarget(self, action: "newAction:", for: .touchUpInside)
		newLayout.addSubview(newButton)
	}

	var newIcon: UIImageView!

	var isNewEnable = true

	func setNewEnable(_ isEnable: Bool)
	{
		isNewEnable = isEnable

		newIcon.isHidden = !isEnable
	}

	required init?(coder aDecoder: NSCoder)
	{
		fatalError("init(coder:) has not been implemented")
	}

	@objc func settingsAction(_ sender: AnyObject)
	{
		self.delegate.LibraryTopSettingsAction()
	}

	@objc func selectAction(_ sender: AnyObject)
	{
		self.delegate.LibraryTopSelectAction()
	}

	@objc func newAction(_ sender: AnyObject)
	{
		if(!isNewEnable)
		{
			return
		}

		self.delegate.LibraryTopNewAction()
	}
}
