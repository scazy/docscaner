//
//  LibraryController.swift
//  Scanner
//
//  Created by Artem  on 27/09/2018.
//  Copyright © 2018 VladimirKuzmin. All rights reserved.
//

import Foundation
import UIKit

extension UIImagePickerController
{
	convenience init(navigationBarStyle: UIBarStyle)
	{
		self.init()
		self.navigationBar.barStyle = navigationBarStyle
	}
}

class LibraryController: UIViewController, ViewControllerDelegate, LibrarySubtopDelegate, LibraryTopDelegate
{


	var isActionMenu = false

	var startOffset: CGFloat = 0

	var top: LibraryTop!

	var subtop: LibrarySubtop!

	var docsScroll: UIScrollView!

	var _docs = [DocModel]()

	var takePhotoLibTitle: UILabel!

	var openLibTitle: UILabel!

	var imagePicker = UIImagePickerController(navigationBarStyle: UIBarStyle.black)


	var editIcon: UIImageView!

	//	var isEdit = 1

	var buttonState = 1

	var buttonOpenLib: UIView!

	var buttonTakePhotoLib: UIView!


	var layoutTakePhotoImage: UILabel!

	//	var hintLayout: UIView!

	var buttonTakePhoto: UIButton!

	var layoutTakePhoto: UIView!

	var selectedItems = [Int]()

	func setParams(params: [String : Any])
	{

	}

	var zeroFiles: UIView!

	var parentId = "";

	var firstLayout: UIView!

	func renderScroll()
	{
		_docs = [DocModel]()

		docsScroll.contentSize = CGSize(width: ScreenSize.SCREEN_WIDTH, height: 0)

		if(zeroFiles != nil)
		{
			zeroFiles.removeFromSuperview()
		}

		for i in docsScroll.subviews
		{
			i.removeFromSuperview()
		}

		for i in Core.shared().getRealm.objects(DocModel.self).filter("ParentId = '\(self.parentId)'").sorted(byKeyPath: "CreatedAt", ascending: false)
		{
			self._docs.append(i)
		}

		print("** DOCS COUNT \(self._docs.count)")

		var lineItemsCount = 3

		var offsetY: CGFloat = 10
		var offsetX: CGFloat = 0

		var lineItems = 0

		var blockWidth = docsScroll.getWidth()  / CGFloat(lineItemsCount)
		var blockHeight = blockWidth + 24

		//		offsetY += 0
		//
		//		offsetX += 10

		let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)

		var ii = 0

		if(!self.parentId.isEmpty)
		{
			var backLayout = UIView()
			backLayout.setSize(docsScroll.getWidth(), 32)
			backLayout.setX(0)
			backLayout.setY(offsetY)
			backLayout.layer.cornerRadius = 5
			backLayout.backgroundColor = .white
			docsScroll.addSubview(backLayout)

			var backLabel = UILabel()
			backLabel.setSize(backLayout.getWidth(), backLayout.getHeight())
			backLabel.setX(0)
			backLabel.setY(0)
			backLabel.textAlignment = .center
			backLabel.text = "← back to library"
			backLabel.font = UIFont.systemFont(ofSize: 14)
			backLayout.addSubview(backLabel)

			var backButton = UIButton()
			backButton.setSize(backLayout.getWidth(), backLayout.getHeight())
			backButton.setX(0)
			backButton.setY(0)
			backButton.addTarget(self, action: "backToLibaray:", for: .touchUpInside)
			backLayout.addSubview(backButton)

			offsetY += 32
		}

		if(firstLayout != nil)
		{
			firstLayout.removeFromSuperview()
		}

		if(_docs.count == 0)
		{
			var leftArrow = UIImage(named: "first_scan_left")

			var part = ScreenSize.SCREEN_WIDTH / 4

			var imageSize = getImageSize(image: leftArrow!, width: part - 20, height: 0)

			firstLayout = UIView()
			firstLayout.setSize(part * 2, imageSize[1] + 20)
			firstLayout.toCenterX(self.view)
			firstLayout.setY(ScreenSize.SCREEN_HEIGHT - firstLayout.getHeight() - (part / 2))
			self.view.addSubview(firstLayout)

			var firsttitle = UILabel()
			firsttitle.text = "Add your first scan"
			firsttitle.setX(0)
			firsttitle.setY(0)
			firsttitle.setSize(firstLayout.getWidth(), 20)
			firsttitle.textAlignment = .center
			firsttitle.font = UIFont.boldSystemFont(ofSize: 18)
			firsttitle.textColor = .white
			firstLayout.addSubview(firsttitle)

			var leftArrowView = UIImageView()
			leftArrowView.image = leftArrow
			leftArrowView.setX(0)
			leftArrowView.setY(2, relative: firsttitle)
			leftArrowView.setSize(imageSize[0], imageSize[1])
			firstLayout.addSubview(leftArrowView)

			var rightArrowView = UIImageView()
			rightArrowView.image = UIImage(named: "first_scan_right")
			rightArrowView.setX(firstLayout.getWidth() - imageSize[0])
			rightArrowView.setY(2, relative: firsttitle)
			rightArrowView.setSize(imageSize[0], imageSize[1])
			firstLayout.addSubview(rightArrowView)

		}
		else
		{
			offsetY += 10

			for i in _docs
			{


				if(lineItems == lineItemsCount)
				{
					lineItems = 0
					offsetY += blockHeight
					offsetX = 0
				}

				var layout = UIView()
				layout.setSize(blockWidth, blockHeight)
				layout.setX(offsetX)
				layout.setY(offsetY)
				docsScroll.addSubview(layout)

				let item = UIView()
				item.layer.cornerRadius = 10
				item.setSize(blockWidth - 20, blockWidth - 20)
				item.setX(10)
				item.setY(10)
				layout.addSubview(item)

				var itemLayout = UIView()
//				itemLayout.layer.cornerRadius = 10
				itemLayout.setX(0)
				itemLayout.setY(0)
				itemLayout.setSize(item.getWidth(), item.getHeight())
//				itemLayout.backgroundColor = UIColor.white
				item.addSubview(itemLayout)

				itemLayout.tag = 30000 + ii

				var title = UILabel()
				title.setX(5)
				title.setY(3, relative: item)
				title.font = UIFont.boldSystemFont(ofSize: 12)
				title.setSize(layout.getWidth() - 10, 14)
				title.textColor = UIColor.white
				title.textAlignment = .center
				layout.addSubview(title)

				var subtitle = UILabel()
				subtitle.setX(5)
				subtitle.textAlignment = .center
				subtitle.setY(0, relative: title)
				subtitle.font = UIFont.systemFont(ofSize: 11)
				subtitle.setSize(layout.getWidth() - 10, 14)
				subtitle.textColor = UIColor.white.withAlphaComponent(0.7)
				layout.addSubview(subtitle)

				if(i.IsFolder)
				{
					title.text = i.Name

					subtitle.text = "0 scans"

					var folderLayout = UIView()
					folderLayout.clipsToBounds = true
					folderLayout.layer.masksToBounds = true
					folderLayout.layer.contentsGravity = CALayerContentsGravity.resizeAspectFill
					folderLayout.setY(0)
					folderLayout.setX(0)
					folderLayout.setSize(itemLayout.getWidth(), itemLayout.getHeight())
					folderLayout.backgroundColor = UIColor(red: 0.36, green: 0.36, blue: 0.36, alpha: 1)
					folderLayout.layer.cornerRadius = 3
					folderLayout.layer.borderWidth = 1
					folderLayout.layer.borderColor = UIColor(red: 0.69, green: 0.69, blue: 0.69, alpha: 1).cgColor
					itemLayout.addSubview(folderLayout)

					//---

					var folderFilesCount = Core.shared().getRealm.objects(DocModelFile.self).filter("ParentId = '\(i.Id)'").count

					subtitle.text = "\(folderFilesCount) scans"

					if(folderFilesCount > 0)
					{
						var ii = 0
						for image in Core.shared().getRealm.objects(DocModelFile.self).filter("ParentId = '\(i.Id)'")
						{
							if(ii >= 3)
							{
								break
							}

							let path = docDirURL.appendingPathComponent(image.FileName)
							let data = try? Data(contentsOf: path)
							let imageData = UIImage(data: data!)!

							var imageSize = getImageSize(image: imageData, width: 0, height: folderLayout.getHeight() - 25)

							var preview = UIImageView()

							preview.setX(0)
							preview.setY(0)
							preview.image = imageData
							preview.setSize(imageSize[0], imageSize[1])
							folderLayout.addSubview(preview)

							if(imageSize[1] > imageSize[0])
							{
								ii += 1
							}
						}
					}

					//---

					var coverFolderImage = UIImage(named: "folder_cover")

					var coverFolderImageSize = getImageSize(image: coverFolderImage!, width: folderLayout.getWidth(), height: 0)

					var coverFolder = UIImageView()
					coverFolder.image = coverFolderImage
					coverFolder.setSize(coverFolderImageSize[0], coverFolderImageSize[1])
					coverFolder.setX(0)
					coverFolder.setY(folderLayout.getHeight() - coverFolderImageSize[1])
					itemLayout.addSubview(coverFolder)
				}
				else
				{
					var countOfFiles = Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(i.Id)'").count

					title.text = i.Name

					if(countOfFiles == 1)
					{
						let iv = UIImageView()
						//					iv.layer.cornerRadius = 10

						if let image = Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(i.Id)'").first
						{
							let path = docDirURL.appendingPathComponent(image.FileName)

							let data = try? Data(contentsOf: path)

							let image = UIImage(data: data!)!

							print("** LIB IMAGE OR \(image.imageOrientation.rawValue)")

							var imageSize = getImageSize(image: image, width: 0, height: itemLayout.getHeight())

							if(imageSize[0] > imageSize[1])
							{
								var imageSize1 = getImageSize(image: image, width: itemLayout.getWidth(), height: 0)

								iv.setSize(imageSize1[0], imageSize1[1])
							}
							else
							{
								iv.setSize(imageSize[0], imageSize[1])
							}

							iv.toCenterX(itemLayout)
							iv.toCenterY(itemLayout)
							iv.image = image

							//---

							let date = Date(timeIntervalSince1970: TimeInterval(i.CreatedAt))
							let dateFormatter = DateFormatter()
							dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
							dateFormatter.locale = NSLocale.current
							dateFormatter.dateFormat = "yyyy-MM-dd"

							let countBytes = ByteCountFormatter()
							countBytes.allowedUnits = [.useAll]
							countBytes.countStyle = .file
							let fileSize = countBytes.string(fromByteCount: Int64(data!.count))

							print("File size: \(fileSize)")

							subtitle.text = String(format: "%@, %@", dateFormatter.string(from: date), fileSize)
						}

						itemLayout.addSubview(iv)
					}
					else
					{
						var ii2 = 0

						subtitle.text = "\(countOfFiles) scans"

						print("*** MULTI")
						for image in Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(i.Id)'")
						{
							//						print("*** MULTI 1")
							if(ii2 > 3)
							{
								break
							}

							var iv = UIImageView()
							let path = docDirURL.appendingPathComponent(image.FileName)
							let data = try? Data(contentsOf: path)
							let imageData = UIImage(data: data!)!

							var imageSize = getImageSize(image: imageData, width: 0, height: itemLayout.getHeight() - 25)

							if(imageSize[1] > imageSize[0])
							{
								print("** LIB2 IMAGE OR \(imageData.imageOrientation.rawValue)")
								iv.image = imageData
								iv.setSize(imageSize[0], imageSize[1])
								iv.toCenterY(itemLayout)
								iv.toCenterX(itemLayout)
								itemLayout.addSubview(iv)

								if(ii2 == 0 || ii2 == 2)
								{
									var degrees: Float = 0

									var offset: CGFloat = 0

									if(ii2 == 0)
									{
										degrees = -10
										offset = iv.getX() - 25
									}
									else
									{
										degrees = 10
										offset = iv.getX() + 25
									}

									iv.setX(offset)

									iv.transform = CGAffineTransform(rotationAngle: CGFloat(degrees * Float.pi / 180))
								}

								ii2 += 1
							}
						}
					}
				}

				//
				var button = UIButton()
				button.tag = 10000 + ii
				button.setX(0)
				button.setY(0)
				button.setSize(item.getWidth(), item.getHeight())
				item.addSubview(button)

				let tapLongGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.fileLongTap(_:)))
				button.addGestureRecognizer(tapLongGesture)

				let tapGesture = UITapGestureRecognizer(target:self, action: #selector(self.fileTap(_:)))
				button.addGestureRecognizer(tapGesture)

				offsetX += blockWidth

				lineItems += 1

				ii += 1
			}
		}

		offsetY += blockHeight

		docsScroll.contentSize = CGSize(width: ScreenSize.SCREEN_WIDTH, height: offsetY)
	}

	func willBeAppear()
	{
		print("*** renderScroll 1")
		renderScroll()
	}

	func willBeDisappear()
	{

	}

	var height: CGFloat = 42

	func initTopControls()
	{
		top = LibraryTop()
		top.delegate = self
		top.setX(0)
		top.setY(0)
		self.view.addSubview(top)

		var start = UIView()
//		start.backgroundColor = UIColor.red
		start.setX(0)
		start.setY(0)
		start.setSize(ScreenSize.SCREEN_WIDTH, startOffset)
		top.addSubview(start)

		subtop = LibrarySubtop()
		subtop.delegate = self
		subtop.setX(0)
		subtop.setY(0, relative: self.top)
		self.view.addSubview(subtop)

	}

	@objc func openLeftMenuAction(_ sender: AnyObject)
	{
		EventsManager.shared().sendNotify(Constants.EVENT_SHOW_LEFT_MENU)
	}

	override func viewDidLoad()
	{
		super.viewDidLoad()

		startOffset = UIApplication.shared.statusBarFrame.size.height

		initTopControls()

		var bgImage = UIImage(named: "lib_bg_image")

		var minHeight = ScreenSize.SCREEN_HEIGHT - self.top.getHeight()
		var minWidth = ScreenSize.SCREEN_WIDTH

		var originalSize = getImageSize(image: bgImage!, width: minWidth, height: 0)

		var newWidth: CGFloat = 0
		var newHeight: CGFloat = 0

		if(originalSize[1] < minHeight)
		{
			newHeight = minHeight
			var scale = minHeight / originalSize[1];

			newWidth = originalSize[0] * scale
		}
		else
		{
			newWidth = minWidth
			newHeight = originalSize[1]
		}

		var bg = UIImageView()
		bg.layer.contentsGravity = CALayerContentsGravity.resizeAspectFill
		bg.layer.masksToBounds = true
		bg.image = bgImage
		bg.setX(0)
		bg.setY(0, relative: self.top)
		bg.setSize(minWidth, minHeight)
		self.view.addSubview(bg)
		self.view.sendSubviewToBack(bg)

		//---

		print("** BG \(originalSize) \(newWidth) \(newHeight)")

		docsScroll = UIScrollView()
		docsScroll.backgroundColor = UIColor.clear
		docsScroll.delegate = self
		docsScroll.tag = -918789
		docsScroll.setX(0)
		docsScroll.setY(0, relative: subtop)
		docsScroll.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT - top.getHeight() - self.subtop.getHeight())
		docsScroll.contentSize = CGSize(width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT - top.getHeight())
		self.view.addSubview(docsScroll)

		//---

		addButtonTakeDone()

		NotificationCenter.default.addObserver(self, selector: #selector(self.observerReloadLib(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_RELOAD_LIB)), object: nil)
	}

	@objc func observerReloadLib(_ notification: NSNotification)
	{
		DispatchQueue.main.async(execute: {
			print("*** renderScroll 2")
			self.renderScroll()
		})
	}

	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)
	}

	func LibraryTopSettingsAction()
	{
		Core.shared().changeViewController(name: "settingsController")
	}

	func LibraryTopNewAction()
	{
		print("*** LibraryTopNewAction")

		var folderName = ""

		let alert = UIAlertController(title: "Enter new folder name", message: nil, preferredStyle: .alert)

		alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in}))

		let saveAction = UIAlertAction(title: "Add", style: .default, handler: { (action) -> Void in
			NotificationCenter.default.removeObserver(self, name: UITextField.textDidChangeNotification, object: self)

			self.addNewFolder(folderName)
		})

		alert.addAction(saveAction)

		alert.addTextField(configurationHandler: { (textField) in
			textField.text = ""
			textField.placeholder = "Folder name"
			saveAction.isEnabled = false
			NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main) { (notification) in
					saveAction.isEnabled = textField.text!.count > 0

				folderName = textField.text!
			}
		})

		self.present(alert, animated: true, completion: nil)
	}

	private func addNewFolder(_ name: String)
	{
		let doc2 = DocModel()
		doc2.Id = UUID().uuidString
		doc2.CreatedAt = Int64(NSDate().timeIntervalSince1970)
		doc2.Name = name
		doc2.IsFolder = true

		try! Core.shared().getRealm.write
		{
			Core.shared().getRealm.add(doc2, update: true)
		}

		goToFolder(id: doc2.Id)
	}

	func goToFolder(id: String)
	{
		if(id.isEmpty)
		{
			goToLibrary()
		}

		if let folder = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(id)'").first
		{
			self.parentId = folder.Id
			self.top.setTopTitle(title: folder.Name)
			self.top.setNewEnable(false)

			print("*** renderScroll 3")
			renderScroll()
		}
	}

	func goToLibrary()
	{
		self.parentId = ""
		self.top.setTopTitle(title: s("library_title"))
		self.top.setNewEnable(true)

		print("*** renderScroll 4")
		renderScroll()
	}

	func LibraryTopSelectAction()
	{

	}

	@objc func backToLibaray(_ sender: AnyObject)
	{
		goToLibrary()
	}
}

extension LibraryController: UIScrollViewDelegate
{

}
