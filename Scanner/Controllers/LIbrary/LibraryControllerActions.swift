//
//  LibraryControllerActions.swift
//  Scanner
//
//  Created by Artem  on 06/11/2018.
//  Copyright © 2018 VladimirKuzmin. All rights reserved.
//

import Foundation
import UIKit


extension LibraryController: UINavigationControllerDelegate, UIImagePickerControllerDelegate, ActionsMenuDelegate
{
	@objc func enableEdit(_ sender: AnyObject)
	{
		if(buttonState == 2)
		{
			for i in selectedItems
			{
				try! Core.shared().getRealm.write {
					Core.shared().getRealm.delete(self._docs[i])
				}
			}

			selectedItems = [Int]()

			buttonState = 1

			addButtonTakeDone()

			editIcon.image = UIImage(named: "lib_icon_edit")

			print("*** renderScroll 5")
			renderScroll()
		}
		else
		{
			for id in selectedItems
			{
				if let item = docsScroll.viewWithTag(30000 + id)
				{
					if let shot = item.viewWithTag(40000 + id) as? UIImageView
					{
						shot.removeFromSuperview()
					}
				}
			}


			selectedItems = [Int]()

			buttonState = 2

			editIcon.image = UIImage(named: "library_delete")

			addButtonTakeDone()
		}
	}

	@objc func openTakePhotoAction(_ sender: AnyObject)
	{
		print("*** openTakePhotoAction STTE \(buttonState)")

//		selectedItems = [Int]()



		if(buttonState == 2)
		{
			buttonState = 1

			editIcon.image = UIImage(named: "lib_icon_edit")

			addButtonTakeDone()

			for id in selectedItems
			{
				if let item = docsScroll.viewWithTag(30000 + id)
				{
					if let shot = item.viewWithTag(40000 + id) as? UIImageView
					{
						shot.removeFromSuperview()
					}
				}
			}

			selectedItems = [Int]()

			if(zeroFiles != nil)
			{
				zeroFiles.isHidden = true
			}
		}
		else if(buttonState == 3)
		{
			buttonState = 1
			layoutTakePhotoImage.text = "+"

//			hideAdditionalButtons()

			if(zeroFiles != nil)
			{
				zeroFiles.isHidden = false
			}
		}
		else
		{
//			Core.shared().changeViewController(name: "scanController")
			buttonState = 3
			layoutTakePhotoImage.text = "✕"

//			addAdditionalButtons()

			if(zeroFiles != nil)
			{
				zeroFiles.isHidden = true
			}
		}
	}

//	func hideAdditionalButtons()
//	{
//		UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveEaseInOut, animations:
//		{
//			self.buttonOpenLib.center = self.layoutTakePhoto.center
//			self.buttonTakePhotoLib.center = self.layoutTakePhoto.center
//
//			self.takePhotoLibTitle.alpha = 0
//			self.takePhotoLibTitle.setX(self.buttonTakePhotoLib.getX() - 10 - self.takePhotoLibTitle.getWidth())
//			self.takePhotoLibTitle.center.y = self.buttonTakePhotoLib.center.y
//
//			self.openLibTitle.alpha = 0
//			self.openLibTitle.setX(self.buttonOpenLib.getX() - 10 - self.openLibTitle.getWidth())
//			self.openLibTitle.center.y = self.buttonOpenLib.center.y
//		}, completion: { (finished: Bool) -> Void in
////			self.hintLayout.isHidden = false
//
//			self.buttonOpenLib.removeFromSuperview()
//			self.buttonTakePhotoLib.removeFromSuperview()
//
//			self.takePhotoLibTitle.removeFromSuperview()
//			self.openLibTitle.removeFromSuperview()
//
//			self.layoutTakePhotoImage.text = "+"
//		})
//	}

//	func addAdditionalButtons()
//	{
//		//buttonTakePhoto
//
////		self.hintLayout.isHidden = true
//
//		buttonOpenLib = UIView()
//		buttonOpenLib.setSize(layoutTakePhoto.getWidth() - 10, layoutTakePhoto.getWidth() - 10)
//		buttonOpenLib.layer.cornerRadius = buttonOpenLib.getWidth() / 2
//		buttonOpenLib.backgroundColor = UIColor(red: 0.21, green: 0.42, blue: 0.69, alpha: 1)
//		buttonOpenLib.center = layoutTakePhoto.center
//		self.view.addSubview(buttonOpenLib)
//
//		var buttonOpenLibImage = UIImageView()
//		buttonOpenLibImage.setSize(buttonOpenLib.getWidth() - 35, buttonOpenLib.getWidth() - 35)
//		buttonOpenLibImage.image = UIImage(named: "open_lib")
//		buttonOpenLibImage.toCenterX(buttonOpenLib)
//		buttonOpenLibImage.toCenterY(buttonOpenLib)
//		buttonOpenLib.addSubview(buttonOpenLibImage)
//
//		var buttonOpenLibAction = UIButton()
//		buttonOpenLibAction.setSize(buttonOpenLib.getWidth() - 35, buttonOpenLib.getWidth() - 35)
//		buttonOpenLibAction.toCenterX(buttonOpenLib)
//		buttonOpenLibAction.toCenterY(buttonOpenLib)
//		buttonOpenLibAction.addTarget(self, action: #selector(self.actionOpenLib(_:)), for: .touchUpInside)
//		buttonOpenLib.addSubview(buttonOpenLibAction)
//
//		//----
//
//		buttonTakePhotoLib = UIView()
//		buttonTakePhotoLib.setSize(layoutTakePhoto.getWidth() - 10, layoutTakePhoto.getWidth() - 10)
//		buttonTakePhotoLib.layer.cornerRadius = buttonTakePhotoLib.getWidth() / 2
//		buttonTakePhotoLib.backgroundColor = UIColor(red: 0.21, green: 0.42, blue: 0.69, alpha: 1)
//		buttonTakePhotoLib.center = layoutTakePhoto.center
//		self.view.addSubview(buttonTakePhotoLib)
//
//		let buttonTakePhotoLibImage = UIImageView()
//		buttonTakePhotoLibImage.setSize(buttonTakePhotoLib.getWidth() - 35, buttonTakePhotoLib.getWidth() - 35)
//		buttonTakePhotoLibImage.image = UIImage(named: "open_scanner")
//		buttonTakePhotoLibImage.toCenterX(buttonOpenLib)
//		buttonTakePhotoLibImage.toCenterY(buttonOpenLib)
//		buttonTakePhotoLib.addSubview(buttonTakePhotoLibImage)
//
//		let buttonTakePhotoLibAction = UIButton()
//		buttonTakePhotoLibAction.setSize(buttonTakePhotoLib.getWidth() - 35, buttonTakePhotoLib.getWidth() - 35)
//		buttonTakePhotoLibAction.toCenterX(buttonTakePhotoLib)
//		buttonTakePhotoLibAction.toCenterY(buttonTakePhotoLib)
//		buttonTakePhotoLibAction.addTarget(self, action: #selector(self.actionOpenScan(_:)), for: .touchUpInside)
//		buttonTakePhotoLib.addSubview(buttonTakePhotoLibAction)
//
//		self.view.bringSubviewToFront(layoutTakePhoto)
//
//		self.takePhotoLibTitle = UILabel()
//		self.takePhotoLibTitle.alpha = 0
//		self.takePhotoLibTitle.text = s("button_scanner")
//		self.takePhotoLibTitle.font = UIFont.systemFont(ofSize: 16)
//		self.takePhotoLibTitle.textColor = .white
//		self.takePhotoLibTitle.textAlignment = .center
//		self.takePhotoLibTitle.setSize(100, 20)
//		self.takePhotoLibTitle.setX(self.buttonTakePhotoLib.getX() - 10 - self.takePhotoLibTitle.getWidth())
//		self.takePhotoLibTitle.center.y = self.buttonTakePhotoLib.center.y
//		self.view.addSubview(self.takePhotoLibTitle)
//
//		self.openLibTitle = UILabel()
//		self.openLibTitle.alpha = 0
//		self.openLibTitle.text = s("button_library")
//		self.openLibTitle.font = UIFont.systemFont(ofSize: 16)
//		self.openLibTitle.textColor = .white
//		self.openLibTitle.textAlignment = .center
//		self.openLibTitle.setSize(100, 20)
//		self.openLibTitle.setX(self.buttonOpenLib.getX() - 10 - self.openLibTitle.getWidth())
//		self.openLibTitle.center.y = self.buttonOpenLib.center.y
//		self.view.addSubview(self.openLibTitle)
//
//		let buttonOpenLibY = self.layoutTakePhoto.getY() - 10 - buttonOpenLib.getHeight()
//		let buttonTakePhotoY = buttonOpenLibY - 5 - buttonTakePhotoLib.getHeight()
//
//		UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveEaseInOut, animations:
//		{
//			self.buttonOpenLib.setY(buttonOpenLibY)
//			self.buttonTakePhotoLib.setY(buttonTakePhotoY)
//
//			self.takePhotoLibTitle.alpha = 1
//			self.takePhotoLibTitle.setX(self.buttonTakePhotoLib.getX() - 10 - self.takePhotoLibTitle.getWidth())
//			self.takePhotoLibTitle.center.y = self.buttonTakePhotoLib.center.y
//
//			self.openLibTitle.alpha = 1
//			self.openLibTitle.setX(self.buttonOpenLib.getX() - 10 - self.openLibTitle.getWidth())
//			self.openLibTitle.center.y = self.buttonOpenLib.center.y
//		}, completion: { (finished: Bool) -> Void in
//
//		})
//	}

	@objc func actionOpenLib(_ sender: AnyObject)
	{
//		hideAdditionalButtons()

		buttonState = 1

		var docsCount = Core.shared().getRealm.objects(DocModel.self).count

		Core.shared().checkTrial() { status in
			switch(status)
			{
			case .Free, .Trial:
				if(docsCount >= 3 && !Constants.IS_DEV)
				{
					Core.shared().showPremium()
					return
				}
				break
			case .Premium:
				break
			}

			if UIImagePickerController.isSourceTypeAvailable(.photoLibrary)
			{
				print("Button capture")

				self.imagePicker.delegate = self
				self.imagePicker.sourceType = .savedPhotosAlbum;
				self.imagePicker.allowsEditing = false

				self.present(self.imagePicker, animated: true, completion: nil)
			}
		}
	}

	func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!)
	{
		print("didFinishPickingImage")


//		imageView.image = image
	}

	func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
	{
		print("** imagePickerControllerDidCancel")

		self.dismiss(animated: true, completion: { () -> Void in

		})
	}

	func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool)
	{
		print("*** willShow UIImagePickerController")

		UIApplication.shared.statusBarStyle = .default
	}

	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
	{
		print("didFinishPickingImage")

		DispatchQueue.main.async(execute: {

			var waitingView = UIView()
			waitingView.backgroundColor = UIColor.black.withAlphaComponent(0.75)
			waitingView.setX(0)
			waitingView.setY(0)
			waitingView.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
			self.view.addSubview(waitingView)

			var waitingLoader = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
			waitingLoader.setSize(50, 50)
			waitingLoader.toCenterX(waitingView)
			waitingLoader.toCenterY(waitingView)
			waitingLoader.startAnimating()
			waitingView.addSubview(waitingLoader)

			var waitingTitle = UILabel()
			waitingTitle.textAlignment = .center
			waitingTitle.setSize(ScreenSize.SCREEN_WIDTH, 20)
			waitingTitle.text = s("importing")
			waitingTitle.setX(0)
			waitingTitle.setY(10, relative: waitingLoader)
			waitingTitle.textColor = UIColor.white
			waitingView.addSubview(waitingTitle)

			self.view.bringSubviewToFront(waitingView)

			self.dismiss(animated: true, completion: { () -> Void in

				var image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage

				var isRotate = false

				var degrees: CGFloat = 0

				var isFlip = true

				switch(image.imageOrientation)
				{
				case .down:
					isRotate = true
					degrees = 90
					isFlip = false
					break
				case .left:
					isRotate = true
					degrees = 90
					break
				case .right:
					isRotate = true
					degrees = -90
					break
				case .up:
					isRotate = false
					degrees = 0
					break
				default: break
				}

				var newImage = image.toNoir(isRotate, degrees: degrees, flip: isFlip)

				if(image.imageOrientation == .down)
				{
					newImage = newImage.imageRotatedByDegrees(degrees: 90, flip: true)
				}

//				var final = newImage.fixed()

				var fileName = UUID().uuidString + ".jpg"

				let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)

				let fileURL = docDirURL.appendingPathComponent(fileName)

				print("** IMPORT IMAGE OR \(image.imageOrientation.rawValue)  \(newImage.imageOrientation.rawValue)")

				do
				{
					try newImage.jpegData(compressionQuality: 1)!.write(to: fileURL, options: [.atomic])
				}
				catch(let error)
				{
					print(error)
				}

				//---

				let doc = DocModel()
				doc.ParentId = self.parentId
				doc.Id = UUID().uuidString
				doc.CreatedAt = Int64(NSDate().timeIntervalSince1970)
				doc.Name = self.getFilename()

				let file = DocModelFile()
				file.ParentId = self.parentId
				file.Width = Float(image.size.width)
				file.Height = Float(image.size.height)
				file.FileName = fileName
				file.Id = UUID().uuidString
				file.DocId = doc.Id
				file.TypeFile = "image"

				try! Core.shared().getRealm.write {
					Core.shared().getRealm.add(doc)
					Core.shared().getRealm.add(file)
				}

				self.goToFolder(id: self.parentId)

//				EventsManager.shared().sendNotify(Constants.EVENT_HIDE_PROCESSING)

				waitingView.removeFromSuperview()

				EventsManager.shared().sendNotify(Constants.EVENT_OPEN_DOC, data: ["doc_id": doc.Id])
			})

		})
	}

	func getFilename() -> String
	{
		let date = NSDate()

		let df1 = DateFormatter()
		let locale = Locale(identifier: s("locale_id"))
		df1.locale = locale

		df1.dateFormat = s("date_format")
		let stringDate =  df1.string(from: date as Date)

		return String(format: s("file_name_format"), stringDate)

	}

	@objc func actionOpenScan(_ sender: AnyObject)
	{
		buttonState = 1
//		hideAdditionalButtons()

		var docsCount = Core.shared().getRealm.objects(DocModel.self).count

		Core.shared().checkTrial() { premStatus in
			switch(premStatus)
			{
			case .Free, .Trial:
				if(docsCount >= 3 && !Constants.IS_DEV)
				{
					Core.shared().showPremium()
					return
				}
				break
			case .Premium:
				break
			}

			Core.shared().changeViewController(name: "scanController")
		}


	}

	func addButtonTakeDone()
	{
		var part = ScreenSize.SCREEN_WIDTH / 4

		var takeLayoutLib = UIImage(named: "take_from_lib")

		var takeLayoutLibSize = getImageSize(image: takeLayoutLib!, width: part, height: 0)

		var takeImageLayout = UIView()
//		takeImageLayout.alpha = 0.7
		takeImageLayout.setSize(takeLayoutLibSize[0], takeLayoutLibSize[1])
		takeImageLayout.setX(0)
		takeImageLayout.setY(ScreenSize.SCREEN_HEIGHT - takeImageLayout.getHeight())
		self.view.addSubview(takeImageLayout)

		var takeImageView = UIImageView()
		takeImageView.alpha = 0.7
		takeImageView.image = takeLayoutLib
		takeImageView.setX(0)
		takeImageView.setY(0)
		takeImageView.setSize(takeImageLayout.getWidth(), takeImageLayout.getHeight())
		takeImageLayout.addSubview(takeImageView)

		var takeImageIconImage = UIImage(named: "take_from_lib_icon")
		var takeImageIconImageSize = getImageSize(image: takeImageIconImage!, width: takeImageLayout.getWidth() / 2, height: 0)

		var takeImageIcon = UIImageView()
		takeImageIcon.image = takeImageIconImage
		takeImageIcon.setSize(takeImageIconImageSize[0], takeImageIconImageSize[1])
		takeImageIcon.setX(takeImageLayout.getWidth() - takeImageIconImageSize[0] - 30)
		takeImageIcon.setY(30)
		takeImageLayout.addSubview(takeImageIcon)

		var takeLibButton = UIButton()
		takeLibButton.addTarget(self, action: #selector(self.actionOpenLib(_:)), for: .touchUpInside)
		takeLibButton.setX(0)
		takeLibButton.setY(0)
		takeLibButton.setSize(takeImageLayout.getWidth(), takeImageLayout.getHeight())
		takeImageLayout.addSubview(takeLibButton)

		//---- take_camera

		var takeCameraLayout = UIView()
		takeCameraLayout.setSize(takeLayoutLibSize[0], takeLayoutLibSize[1])
		takeCameraLayout.setX(ScreenSize.SCREEN_WIDTH - takeCameraLayout.getHeight())
		takeCameraLayout.setY(ScreenSize.SCREEN_HEIGHT - takeCameraLayout.getHeight())
		self.view.addSubview(takeCameraLayout)

		var cameraImageView = UIImageView()
		cameraImageView.alpha = 0.7
		cameraImageView.image = UIImage(named: "take_camera")
		cameraImageView.setX(0)
		cameraImageView.setY(0)
		cameraImageView.setSize(takeCameraLayout.getWidth(), takeCameraLayout.getHeight())
		takeCameraLayout.addSubview(cameraImageView)

		//take_camera_icon
		var takeCameraIconImage = UIImage(named: "take_camera_icon")
		var takeCameraIconImageSize = getImageSize(image: takeCameraIconImage!, width: takeCameraLayout.getWidth() / 2, height: 0)

		var takeCameraIcon = UIImageView()
		takeCameraIcon.image = takeCameraIconImage
		takeCameraIcon.setSize(takeCameraIconImageSize[0], takeCameraIconImageSize[1])
		takeCameraIcon.setX(takeCameraLayout.getWidth() - takeCameraIconImageSize[0] - 25)
		takeCameraIcon.setY(25)
		takeCameraLayout.addSubview(takeCameraIcon)

		var takeCameraButton = UIButton()
		takeCameraButton.addTarget(self, action: "actionOpenScan:", for: .touchUpInside)
		takeCameraButton.setX(0)
		takeCameraButton.setY(0)
		takeCameraButton.setSize(takeCameraLayout.getWidth(), takeCameraLayout.getHeight())
		takeCameraLayout.addSubview(takeCameraButton)


//		var addText = ""
//
//		if(layoutTakePhoto == nil)
//		{
//			layoutTakePhoto = UIView()
//			layoutTakePhoto.backgroundColor = UIColor(red: 0.21, green: 0.42, blue: 0.69, alpha: 1)
//			self.view.addSubview(layoutTakePhoto)
//			self.view.bringSubviewToFront(layoutTakePhoto)
//
//			if(buttonState == 2)
//			{
//				layoutTakePhoto.setSize(170, 75)
//			}
//			else
//			{
//				layoutTakePhoto.setSize(75, 75)
//			}
//
//			layoutTakePhoto.layer.cornerRadius = layoutTakePhoto.getHeight() / 2
//
//			layoutTakePhoto.setX(ScreenSize.SCREEN_WIDTH - layoutTakePhoto.getWidth() - 20)
//			layoutTakePhoto.setY(ScreenSize.SCREEN_HEIGHT - layoutTakePhoto.getHeight() - 20)
//
//			layoutTakePhotoImage = UILabel()
//			layoutTakePhotoImage.textAlignment = .center
//			layoutTakePhotoImage.textColor = UIColor.white
//			layoutTakePhotoImage.font = UIFont.systemFont(ofSize: 24)
//			layoutTakePhotoImage.setX(0)
//			layoutTakePhotoImage.setY(0)
//			layoutTakePhotoImage.setSize(layoutTakePhoto.getWidth(), layoutTakePhoto.getHeight())
//			layoutTakePhoto.addSubview(layoutTakePhotoImage)
//
//			buttonTakePhoto = UIButton()
//			buttonTakePhoto.setX(0)
//			buttonTakePhoto.setY(0)
//			buttonTakePhoto.setSize(layoutTakePhoto.getWidth(), layoutTakePhoto.getHeight())
//			buttonTakePhoto.addTarget(self, action: #selector(self.openTakePhotoAction(_:)), for: .touchUpInside)
//			layoutTakePhoto.addSubview(buttonTakePhoto)
//		}
//
//		if(buttonState == 2)
//		{
//			layoutTakePhoto.setSize(170, 75)
//			layoutTakePhotoImage.text = "Done"
////			hintLayout.isHidden = true
//		}
//		else
//		{
////			hintLayout.isHidden = false
//			layoutTakePhoto.setSize(75, 75)
//			layoutTakePhotoImage.text = "+"
//		}
//
//		layoutTakePhoto.setX(ScreenSize.SCREEN_WIDTH - layoutTakePhoto.getWidth() - 20)
//		layoutTakePhoto.setY(ScreenSize.SCREEN_HEIGHT - layoutTakePhoto.getHeight() - 20)
//
//		layoutTakePhotoImage.setSize(layoutTakePhoto.getWidth(), layoutTakePhoto.getHeight())
//		buttonTakePhoto.setSize(layoutTakePhoto.getWidth(), layoutTakePhoto.getHeight())
	}

	@objc func fileLongTap(_ sender: AnyObject)
	{
		var id = sender.view.tag - 10000

		print("*** LONG TAP \(sender.view.tag)")

		if(isActionMenu)
		{
			return
		}

		isActionMenu = true

		var doc = self._docs[id]
//
		var items = [
			ActionMenuItem(title: s("share_image"), itemId: 1),
			ActionMenuItem(title: s("share_pdf"), itemId: 2),
			ActionMenuItem(title: s("save_to_library"), itemId: 3)
		]

		if(doc.ORCText.count > 0)
		{
			items.append(ActionMenuItem(title: s("share_recognized_pdf"), itemId: 5))
			items.append(ActionMenuItem(title: s("share_recognized_plain_text"), itemId: 6))
		}

		items.append(ActionMenuItem(title: s("remove_title"), itemId: 4))

		let menu = ActionsMenu(menuId: id, items: items)
		menu.delegate = self

		self.view.addSubview(menu)

		menu.Show()
	}

	@objc func fileTap(_ sender: AnyObject)
	{
		print("** TAP \(sender.view.tag)")
		var id = sender.view.tag - 10000
//
		if(buttonState == 2)
		{
			if let item = docsScroll.viewWithTag(30000 + id)
			{
				if(selectedItems.contains(id))
				{
					if let shot = item.viewWithTag(40000 + id) as? UIImageView
					{
						shot.removeFromSuperview()
					}
				}
				else
				{
					selectedItems.append(id)

					var shot = UIImageView()
					shot.tag = 40000 + id
					shot.setSize(20, 20)
					shot.setY(10)
					shot.setX(item.getWidth() - 30)
					shot.image = UIImage(named: "selected_image")
					item.addSubview(shot)
				}
			}
		}
		else
		{
			var doc = self._docs[id]

			if(doc.IsFolder)
			{
				goToFolder(id: doc.Id)
			}
			else
			{
				EventsManager.shared().sendNotify(Constants.EVENT_OPEN_DOC, data: ["doc_id": doc.Id])
			}
		}
	}

	func ActionsMenuAction(menuId: Int, itemId: Int)
	{
		isActionMenu = false

		switch(itemId)
		{
		case 1:
			EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._docs[menuId].Id, "type": "image"])
			break;
		case 2:
			EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._docs[menuId].Id, "type": "pdf"])
			break;
		case 3:
			EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._docs[menuId].Id, "type": "image_save"])
			break;
		case 5:
			EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._docs[menuId].Id, "type": "ocrpdf"])
			break;
		case 6:
			EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._docs[menuId].Id, "type": "text"])
			break;
		case 4:

			try! Core.shared().getRealm.write {
				Core.shared().getRealm.delete(self._docs[menuId])
			}

			selectedItems = [Int]()

			print("*** renderScroll 6")
			renderScroll()
			break
		default:
			break;
		}
	}

	func ActionsMenuCancel()
	{
		isActionMenu = false
	}
	
	
}
