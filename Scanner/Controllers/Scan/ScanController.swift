//
//  ScanController.swift
//  Scanner
//
//  Created by Artem  on 24.09.2018.
//  Copyright © 2018 VladimirKuzmin. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Photos
import GLKit
import CoreMedia
import CoreImage
import OpenGLES
import QuartzCore


class ScanController: UIViewController, ViewControllerDelegate
{
	var autoScanImage: UIImage!

	var applyFilter: ((CIImage) -> CIImage?)?

	var detector: CIDetector?

	var ticks = 0

	var prevResult = CGRect.zero

	var timer = Timer()

	var timerCounter = 3

	var previewLayer : AVCaptureVideoPreviewLayer?

	var captureDevice : AVCaptureDevice?

	var captureSession = AVCaptureSession()

	var captureSessionVideo = AVCaptureSession()

	var top: UIView!

	var bottomBar: UIView!

	let photoOutput = AVCapturePhotoOutput()

	var IsPosibleTakeImage = true

	var preview: DocEdit!

	var startOffset: CGFloat = 0

	var ph: UIView!

	var isFlashEnable = 1 // 1 - auto; 2 - on; 3 - off

	var flashIcon: UIImageView!

	var videoDisplayView: GLKView!

	var videoDisplayViewBounds: CGRect!

	var renderContext: CIContext!

	var switchTitle: UILabel!

	var switchAction: UISwitch!

	var isBatch = false

	var batchPhoto = 10

	var images = [UIImage]()

	var batchCount: UILabel!

	var completeTitle: UILabel!

	var completeButton: UIButton!

	var isDetector = false

	var detectorTitle: UILabel!

	var sessionQueue: DispatchQueue!

	var videoPreviewLayer: LiveScanner!

	var videoOutput: AVCaptureVideoDataOutput!

	var videoInput: AVCaptureDeviceInput!

	var imageInput: AVCaptureDeviceInput!

	var previewLayerLayout: UIView!

	var buttonsStrip: UIView!

	//buttonsStrip.setX(bottomSwitcherButtomCenter.getX())

	var bottomStripCurrent = 1

	var bottomSwitcherButtomCenter: UIButton!

	var stripTitleSingle: UILabel!

	var stripTitleBatch: UILabel!

	//	var stripTitleGallery: UILabel!

	var photoPreview: UIView!

	var photoPreviewButton: UIButton!

	func setParams(params: [String : Any])
	{

	}

	func willBeAppear()
	{
		print("*** SCAN willBeAppear")

		self.IsPosibleTakeImage = true

		self.captureSession.startRunning()
	}

	func willBeDisappear()
	{
		print("*** SCAN willBeDisappear")
	}

	func checkAuthorization(finish: @escaping (_ success: Bool) -> ())
	{
		switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
		case .authorized:
			finish(true)
			break
		case .notDetermined:
			AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { [unowned self] granted in
				if !granted
				{
					finish(false)
				}
				else
				{
					finish(true)
				}
			})
		default:
			finish(false)
		}
	}

	@objc func backAction(_ sender: AnyObject)
	{
		Core.shared().changeViewController(name: "libraryController")
	}

	func initTopControls()
	{
		top = UIView()
		top.backgroundColor = UIColor.white
		top.setX(0)
		top.setY(0)
		top.setSize(ScreenSize.SCREEN_WIDTH, 42 + startOffset)
		self.view.addSubview(top)

		var topPart = top.getWidth() / 7

		//----

		var backLayout = UIView()
		backLayout.setX(5)
		backLayout.setY(startOffset)
		backLayout.setSize(42, 42)
		top.addSubview(backLayout)

		var lib_i_5 = UIImage(named: "edit_back")
		var lib_i_5Size = getImageSize(image: lib_i_5!, width: 0, height: backLayout.getHeight() - 20)

		var backIcon = UIImageView()
		backIcon.image = lib_i_5
		backIcon.setSize(backLayout.getWidth() - 14, backLayout.getWidth() - 14)
		backIcon.setX(7)
		backIcon.setY(7)
		backLayout.addSubview(backIcon)

		var backButton = UIButton()
		backButton.setX(0)
		backButton.setY(0)
		backButton.setSize(backLayout.getWidth(), backLayout.getHeight())
		backButton.addTarget(self, action: "backAction:", for: .touchUpInside)
		backLayout.addSubview(backButton)

//		var libTitle = UILabel()
//		//		libTitle.backgroundColor = .green
//		libTitle.setX(0, relative: backLayout)
//		libTitle.setY(startOffset)
//		libTitle.setSize(ScreenSize.SCREEN_WIDTH - backLayout.getWidth() - backLayout.getWidth(), 42)
//		libTitle.textColor = UIColor.black
//		libTitle.textAlignment = .center
//		libTitle.text = self.docName
//		libTitle.font = UIFont.boldSystemFont(ofSize: 17)
//		top.addSubview(libTitle)

//		var imageName = "flash_auto"
//
//		switch(flashEnable)
//		{
//		case 1: // // 1 - auto; 2 - on; 3 - off
//			self.isFlashEnable = 2
//			flashIcon.image = UIImage(named: "flash_on")
//			break
//		case 2:
//			self.isFlashEnable = 3
//			flashIcon.image = UIImage(named: "flash_off")
//			break
//		case 3:
//			self.isFlashEnable = 1
//			flashIcon.image = UIImage(named: "flash_auto")
//			break
//		default:
//			break
//		}



	}

	func initVideoCapture()
	{
		captureSession.stopRunning()
//		captureSession.removeOutput(photoOutput)
//		captureSession.removeInput(imageInput)

		videoPreviewLayer = LiveScanner()
		videoPreviewLayer.delegate = self
		self.view.addSubview(videoPreviewLayer)
		self.view.sendSubviewToBack(videoPreviewLayer)

		timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: "timerAction", userInfo: nil, repeats: true)

		//---

		_ = CGRect(0, self.top.getHeight(), ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT - self.top.getHeight() - self.bottomBar.getHeight())

		videoDisplayView = GLKView(frame: CGRect.zero, context: EAGLContext(api: .openGLES2)!)

		videoDisplayView.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI_2))

		videoDisplayView.frame = videoPreviewLayer.VideoLayer.bounds

		videoPreviewLayer.VideoLayer.addSubview(videoDisplayView)

//		self.view.sendSubview(toBack: videoDisplayView)

		renderContext = CIContext(eaglContext: videoDisplayView.context)

		sessionQueue = DispatchQueue(label: "AVSessionQueue", attributes: [])

		videoDisplayView.bindDrawable()

		videoDisplayViewBounds = CGRect(x: 0, y: 0, width: videoDisplayView.drawableWidth, height: videoDisplayView.drawableHeight)

		//-----


			do {
				let device = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .back)
				videoInput = try AVCaptureDeviceInput(device: device!)


				try! device?.lockForConfiguration()
				device?.focusMode = .continuousAutoFocus
				device?.unlockForConfiguration()
				// Start out with low quality

				sessionQueue = DispatchQueue(label: "AVSessionQueue", attributes: [])

				//		captureSessionVideo = AVCaptureSession()
				captureSessionVideo.sessionPreset = AVCaptureSession.Preset.high

				// Output
				videoOutput = AVCaptureVideoDataOutput()
				videoOutput.videoSettings = [ kCVPixelBufferPixelFormatTypeKey as AnyHashable: kCVPixelFormatType_32BGRA] as? [String : Any]
				videoOutput.alwaysDiscardsLateVideoFrames = true
				videoOutput.setSampleBufferDelegate(self, queue: sessionQueue)

				// Join it all together
				captureSessionVideo.addInput(videoInput)
				captureSessionVideo.addOutput(videoOutput)

				detector = prepareRectangleDetector()

				captureSessionVideo.startRunning()
			} catch {

			}
		


	}

	func initCamera()
	{
		print("** INIT CAMERA 1")

//		captureSession = AVCaptureSession()

		captureSession.sessionPreset = AVCaptureSession.Preset.high

		let deviceDiscoverySession = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .back)

		guard let captureDevice = deviceDiscoverySession else {
			print("Failed to get the camera device")
			self.IsPosibleTakeImage = false
			createCameraNotAccess(title: s("access_denied"))
			return
		}

		try! captureDevice.lockForConfiguration()
		captureDevice.focusMode = .continuousAutoFocus
		captureDevice.unlockForConfiguration()

		do
		{
			imageInput = try AVCaptureDeviceInput(device: captureDevice)
			captureSession.addInput(imageInput)
		}
		catch
		{
			print(error)
			return
		}

		previewLayerLayout = UIView()
		previewLayerLayout.setX(0)
		previewLayerLayout.setY(0)
		previewLayerLayout.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
		self.view.addSubview(previewLayerLayout)
		self.view.sendSubviewToBack(self.previewLayerLayout)

		let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.leftSwipe(_:)))
		leftSwipe.direction = UISwipeGestureRecognizer.Direction.left
		previewLayerLayout.addGestureRecognizer(leftSwipe)

		let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.rightSwipe(_:)))
		rightSwipe.direction = UISwipeGestureRecognizer.Direction.right
		previewLayerLayout.addGestureRecognizer(rightSwipe)

		self.previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)

		self.previewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill

		self.previewLayer?.frame = CGRect(0, 0, ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)

		previewLayerLayout.layer.addSublayer(self.previewLayer!)

//		self.view.sendSubviewToBack(self.previewLayer)

		if captureSession.canAddOutput(photoOutput)
		{
			captureSession.addOutput(photoOutput)

			photoOutput.isHighResolutionCaptureEnabled = true
			photoOutput.isLivePhotoCaptureEnabled = photoOutput.isLivePhotoCaptureSupported
		}
		else
		{
			captureSession.commitConfiguration()
			return
		}

		captureSession.commitConfiguration()

		captureSession.startRunning()
	}

	@objc func captureAction(_ sender: AnyObject)
	{
		if(videoPreviewLayer != nil )
		{
			captureSession.stopRunning()
			captureSessionVideo.startRunning()
			videoPreviewLayer.isHidden = false
		}
		else
		{
			initVideoCapture()
		}
	}

	@objc func openLibraryAction(_ sender: AnyObject)
	{
		Core.shared().changeViewController(name: "libraryController")
	}

	@objc func enableFlash(_ sender: AnyObject)
	{
		switch(self.isFlashEnable)
		{
		case 1: // // 1 - auto; 2 - on; 3 - off
			self.isFlashEnable = 2
			flashIcon.image = UIImage(named: "flash_on")
			break
		case 2:
			self.isFlashEnable = 3
			flashIcon.image = UIImage(named: "flash_off")
			break
		case 3:
			self.isFlashEnable = 1
			flashIcon.image = UIImage(named: "flash_auto")
			break
		default:
			break
		}
//		if(self.isFlashEnable)
//		{
//			self.isFlashEnable = false
//			flashIcon.image = UIImage(named: "flash_on")
//
//			Core.shared().defaults.set(0, forKey: "flash_enable")
//		}
//		else
//		{
//			self.isFlashEnable = true
//			flashIcon.image = UIImage(named: "flash_off")
//
////			Core.shared().defaults.integer(forKey: "flash_enable") ?? 0
//
//			Core.shared().defaults.set(1, forKey: "flash_enable")
//		}
		//
	}

	@objc func bottomSwitcherCenter(_ sender: AnyObject)
	{

	}

	@objc func leftSwipe(_ sender: AnyObject)
	{
		print("** SWIPE left")
		actionSwitcherRight()
	}

	@objc func rightSwipe(_ sender: AnyObject)
	{
		print("** SWIPE right")
		actionSwitcherLeft()
	}

	func actionSwitcherLeft()
	{
//		if(cameraAutoScan)
//		{
//			return
//		}

		var offsetX: CGFloat = 0

		print("*** bottomSwitcherLeft \(bottomStripCurrent)")

		var isSwitch = false

//		self.isBatch = false

		switch(bottomStripCurrent)
		{
		case 2:
			stripTitleSingle.textColor = UIColor.white
			stripTitleBatch.textColor = UIColor.white.withAlphaComponent(0.5)

			offsetX = bottomSwitcherButtomCenter.getX()
			bottomStripCurrent = 1

			isSwitch = true
			break
		default:
			break
		}

		if(!isSwitch)
		{
			return
		}

		UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations:
		{
			self.buttonsStrip.setX(offsetX)
		}, completion: { (result) -> () in
			self.switchCameraMode()
		})
	}

	func actionSwitcherRight()
	{
		print("*** bottomSwitcherRight \(bottomStripCurrent)")

		if(cameraAutoScan)
		{
			return
		}

		var offsetX: CGFloat = 0

		var isSwitch = false

//		self.isBatch = false

		images = [UIImage]()

//		batchCount.text = ""

		for i in photoPreview.subviews {
			i.removeFromSuperview()
		}

		photoPreview.isHidden = true
		photoPreviewButton.isHidden = true

		switch(bottomStripCurrent)
		{
		case 1:
			stripTitleSingle.textColor = UIColor.white.withAlphaComponent(0.5)
			stripTitleBatch.textColor = UIColor.white
			//			stripTitleGallery.textColor = UIColor.lightGray

			offsetX = bottomSwitcherButtomCenter.getX() - bottomSwitcherButtomCenter.getWidth()
			bottomStripCurrent = 2

//			print("*** bottomSwitcherRight1 \(offsetX) \(bottomStripCurrent)")

			isSwitch = true

//			self.isBatch = true
			break
		default:
			break
		}

		if(!isSwitch)
		{
			return
		}

		print("*** bottomSwitcherRight OFFSET \(offsetX)")

		UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations:
		{
			self.buttonsStrip.setX(offsetX)
		}, completion: { (result) -> () in
			self.switchCameraMode()
		})
	}

	@objc func bottomSwitcherLeft(_ sender: AnyObject)
	{
		actionSwitcherLeft()
	}

	@objc func bottomSwitcherRight(_ sender: AnyObject)
	{
		actionSwitcherRight()
	}

	override func viewDidLoad()
	{
		super.viewDidLoad()

		print("*** SCAN DID LOAD")

		self.view.backgroundColor = UIColor.white

		startOffset = UIApplication.shared.statusBarFrame.size.height

		initTopControls()

		bottomBar = UIView()
		bottomBar.backgroundColor = UIColor.black
		bottomBar.setX(0)
		bottomBar.setY(ScreenSize.SCREEN_HEIGHT - 160)
		bottomBar.setSize(ScreenSize.SCREEN_WIDTH, 160)
		self.view.addSubview(bottomBar)
		self.view.bringSubviewToFront(bottomBar)

		//---

		let bottomSwitcher = UIView()
		bottomSwitcher.setX(0)
		bottomSwitcher.setY(0)
		bottomSwitcher.setSize(bottomBar.getWidth(), 40)
		bottomBar.addSubview(bottomSwitcher)

		let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.leftSwipe(_:)))
		leftSwipe.direction = UISwipeGestureRecognizer.Direction.left
		bottomSwitcher.addGestureRecognizer(leftSwipe)

		let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.rightSwipe(_:)))
		rightSwipe.direction = UISwipeGestureRecognizer.Direction.right
		bottomSwitcher.addGestureRecognizer(rightSwipe)

		let buttonSwitcherWidth: CGFloat = bottomSwitcher.getWidth() / 5

		bottomSwitcherButtomCenter = UIButton()
		bottomSwitcherButtomCenter.addTarget(self, action: #selector(self.bottomSwitcherCenter(_:)), for: .touchUpInside)
//		bottomSwitcherButtomCenter.backgroundColor = UIColor.red
		bottomSwitcherButtomCenter.setSize(buttonSwitcherWidth, bottomSwitcher.getHeight())
		bottomSwitcherButtomCenter.setY(0)
		bottomSwitcherButtomCenter.toCenterX(bottomSwitcher)
		bottomSwitcher.addSubview(bottomSwitcherButtomCenter)

		let bottomSwitcherButtomRight = UIButton()
		bottomSwitcherButtomRight.addTarget(self, action: #selector(self.bottomSwitcherRight(_:)), for: .touchUpInside)
//		bottomSwitcherButtomRight.backgroundColor = UIColor.green
		bottomSwitcherButtomRight.setSize(buttonSwitcherWidth, bottomSwitcher.getHeight())
		bottomSwitcherButtomRight.setY(0)
		bottomSwitcherButtomRight.setX(0, relative: bottomSwitcherButtomCenter)
		bottomSwitcher.addSubview(bottomSwitcherButtomRight)

		let bottomSwitcherButtomLeft = UIButton()
		bottomSwitcherButtomLeft.addTarget(self, action: #selector(self.bottomSwitcherLeft(_:)), for: .touchUpInside)
//		bottomSwitcherButtomLeft.backgroundColor = UIColor.green
		bottomSwitcherButtomLeft.setSize(buttonSwitcherWidth, bottomSwitcher.getHeight())
		bottomSwitcherButtomLeft.setY(0)
		bottomSwitcherButtomLeft.setX(bottomSwitcherButtomCenter.getX() - buttonSwitcherWidth)
		bottomSwitcher.addSubview(bottomSwitcherButtomLeft)

		//---

		buttonsStrip = UIView()
		buttonsStrip.setY(0)
		buttonsStrip.setX(bottomSwitcherButtomCenter.getX())
		buttonsStrip.setSize(buttonSwitcherWidth * 3, bottomSwitcher.getHeight())
		bottomSwitcher.addSubview(buttonsStrip)

		stripTitleSingle = UILabel()
		stripTitleSingle.textColor = UIColor.white
		stripTitleSingle.setX(0)
		stripTitleSingle.setY(0)
		stripTitleSingle.setSize(buttonSwitcherWidth, bottomSwitcher.getHeight())
		stripTitleSingle.text = "MANUAL"
		stripTitleSingle.textAlignment = .center
		stripTitleSingle.font = UIFont.systemFont(ofSize: 12)
		buttonsStrip.addSubview(stripTitleSingle)

		stripTitleBatch = UILabel()
		stripTitleBatch.textColor = UIColor.white.withAlphaComponent(0.5)
		stripTitleBatch.setX(0, relative: stripTitleSingle)
		stripTitleBatch.setY(0)
		stripTitleBatch.setSize(buttonSwitcherWidth, bottomSwitcher.getHeight())
		stripTitleBatch.text = "AUTOSCAN"
		stripTitleBatch.textAlignment = .center
		stripTitleBatch.font = UIFont.systemFont(ofSize: 12)
		buttonsStrip.addSubview(stripTitleBatch)

		bottomSwitcher.bringSubviewToFront(bottomSwitcherButtomRight)
		bottomSwitcher.bringSubviewToFront(bottomSwitcherButtomLeft)

		liveButton = UILabel()
		liveButton.textColor = .white
		liveButton.setSize((ScreenSize.SCREEN_WIDTH / 3) - 20, 20)
		liveButton.setX(10)
		liveButton.setY(20 + (bottomBar.getHeight() / 2 - 20))
		liveButton.textAlignment = .center
		liveButton.text = "SETTINGS"
		liveButton.font = UIFont.systemFont(ofSize: 16)
		bottomBar.addSubview(liveButton)

		var buttonSwitchLive = UIButton()
//		buttonSwitchLive.backgroundColor = .red
		buttonSwitchLive.setSize((ScreenSize.SCREEN_WIDTH / 3) - 20, 20 + 20)
		buttonSwitchLive.setX(10)
		buttonSwitchLive.setY(liveButton.getY())
		buttonSwitchLive.addTarget(self, action: "openSettings:", for: .touchUpInside)
		bottomBar.addSubview(buttonSwitchLive)
		//----

		let takePhotoView = UIView()
		takePhotoView.backgroundColor = UIColor.clear
		takePhotoView.layer.borderWidth = 2
		takePhotoView.layer.borderColor = UIColor.white.cgColor
		takePhotoView.setSize(80, 80)
		takePhotoView.setY(20, relative: bottomSwitcher)
		takePhotoView.toCenterX(self.bottomBar)
		takePhotoView.layer.cornerRadius = takePhotoView.getWidth() / 2
		self.bottomBar.addSubview(takePhotoView)

		let takePhotoView2 = UIView()
		takePhotoView2.backgroundColor = UIColor.white
		takePhotoView2.setSize(takePhotoView.getHeight() - 9, takePhotoView.getHeight() - 9)
		takePhotoView2.toCenterY(takePhotoView)
		takePhotoView2.toCenterX(takePhotoView)
		takePhotoView2.layer.cornerRadius = takePhotoView2.getWidth() / 2
//		takePhotoView2.image = UIImage(named: "take_photo")
		takePhotoView.addSubview(takePhotoView2)


		batchCount = UILabel()
		batchCount.setX(0)
		batchCount.setY(0)
		batchCount.setSize(takePhotoView.getWidth(), takePhotoView.getHeight())
		batchCount.textAlignment = .center
		batchCount.textColor = .black
		batchCount.text = ""
		batchCount.font = UIFont.systemFont(ofSize: 18)
		takePhotoView.addSubview(batchCount)

		let takePhotoButton = UIButton()
		takePhotoButton.addTarget(self, action: #selector(self.takePhotoAction(_:)), for: .touchUpInside)
		takePhotoButton.setX(0)
		takePhotoButton.setY(0)
		takePhotoButton.setSize(takePhotoView.getWidth(), takePhotoView.getHeight())
		takePhotoView.addSubview(takePhotoButton)

		self.checkAuthorization { (success) in
			if(success)
			{
				DispatchQueue.main.async(execute: {
					self.initCamera()
				})
			}
			else
			{
				DispatchQueue.main.async(execute: {
					self.IsPosibleTakeImage = false
					self.createCameraNotAccess(title: s("access_denied"))
				})
			}
		}

		photoPreview = UIView()
		photoPreview.isHidden = true
		photoPreview.backgroundColor = .clear
		photoPreview.setSize(70, 70)
		photoPreview.setY(20 + (bottomBar.getHeight() / 2 - 35))
		photoPreview.setX(ScreenSize.SCREEN_HALF_WIDTH + ScreenSize.SCREEN_QUARTER_WIDTH - (photoPreview.getWidth() / 2) + 20)
		photoPreview.layer.cornerRadius = photoPreview.getWidth() / 2
		self.bottomBar.addSubview(photoPreview)

		photoPreviewButton = UIButton()
		photoPreviewButton.isHidden = true
		photoPreviewButton.setX(photoPreview.getX())
		photoPreviewButton.setY(photoPreview.getY())
		photoPreviewButton.setSize(photoPreview.getWidth(), photoPreview.getHeight())
		photoPreviewButton.addTarget(self, action: #selector(self.openDoc(_:)), for: .touchUpInside)
		self.bottomBar.addSubview(photoPreviewButton)


//		EventsManager.shared().sendNotify(Constants.EVENT_PREMIUM)
	}

	var liveButton: UILabel!

	var cameraAutoScan = false

	func switchCameraMode()
	{
		if(!self.cameraAutoScan)
		{
			print("*** switchCameraMode 1")
//			self.actionSwitcherLeft()

			self.cameraAutoScan = true

			self.captureSession.stopRunning()

			if(self.previewLayerLayout != nil )
			{
				self.previewLayerLayout.isHidden = true
			}

			if(self.videoPreviewLayer != nil )
			{
				self.captureSessionVideo.startRunning()
				self.videoPreviewLayer.isHidden = false
			}
			else
			{
				self.initVideoCapture()
			}

			self.initTimer()
		}
		else
		{
			print("*** switchCameraMode 2")
			self.cameraAutoScan = false

			if(self.previewLayerLayout != nil )
			{
				self.previewLayerLayout.isHidden = false
			}

			self.LiveScannerClosed()
		}
	}

	func saveImageAndOpen(image: UIImage)
	{
		print("*** saveImageAndOpen 1")

		var shutterView = UIView()
		shutterView.alpha = 0
		shutterView.backgroundColor = UIColor.white
		shutterView.setX(0)
		shutterView.setY(0)
		shutterView.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
		self.view.addSubview(shutterView)

		UIView.animate(withDuration: 0.075, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveEaseInOut, animations:
		{
			shutterView.alpha = 1
		}, completion: { (finished: Bool) -> Void in
			UIView.animate(withDuration: 0.075, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveEaseInOut, animations:
			{
				shutterView.alpha = 0
			}, completion: { (finished: Bool) -> Void in

			})
		})

		if(self.isBatch)
		{
			print("*** saveImageAndOpen 2")

			if(self.images.count < self.batchPhoto)
			{
				self.images.append(image)

				batchCount.text = "\(self.images.count)"

//				completeButton.isHidden = false

//				completeTitle.isHidden = false
			}
			else
			{
				self.stopCature()

				self.IsPosibleTakeImage = false
			}

			for i in photoPreview.subviews {
				i.removeFromSuperview()
			}

			var imagePreview = UIImageView()
			imagePreview.layer.cornerRadius = photoPreview.getWidth() / 2
			imagePreview.image = image
			imagePreview.setX(0)
			imagePreview.setY(0)
			imagePreview.setSize(photoPreview.getWidth(), photoPreview.getHeight())
			imagePreview.layer.contentsGravity = CALayerContentsGravity.resizeAspectFill
			imagePreview.layer.masksToBounds = true
			photoPreview.addSubview(imagePreview)

			photoPreview.isHidden = false
			photoPreviewButton.isHidden = false
		}
		else
		{
//			self.stopCature()

			print("*** saveImageAndOpen 3")

			batchCount.text = ""

			self.images = [UIImage]()

			self.images.append(image)

			//----

			var fileName = UUID().uuidString + ".jpg"
			//
			//---

			let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)

			let fileURL = docDirURL.appendingPathComponent(fileName)

			do
			{
				try image.jpegData(compressionQuality: 1)!.write(to: fileURL, options: [.atomic])
			}
			catch(let error)
			{
				print(error)
			}

			//---

			let doc = DocModel()
			doc.Id = UUID().uuidString
			doc.CreatedAt = Int64(NSDate().timeIntervalSince1970)
			doc.Name = getFilename()

			let file = DocModelFile()
			file.Width = Float(image.size.width)
			file.Height = Float(image.size.height)
			file.FileName = fileName
			file.Id = UUID().uuidString
			file.DocId = doc.Id
			file.TypeFile = "image"

			try! Core.shared().getRealm.write {
				Core.shared().getRealm.add(doc)
				Core.shared().getRealm.add(file)
			}

			self.IsPosibleTakeImage = true

//			photoPreview

			for i in photoPreview.subviews {
				i.removeFromSuperview()
			}

			var imagePreview = UIImageView()
			imagePreview.layer.cornerRadius = photoPreview.getWidth() / 2
			imagePreview.image = image
			imagePreview.setX(0)
			imagePreview.setY(0)
			imagePreview.setSize(photoPreview.getWidth(), photoPreview.getHeight())
			imagePreview.layer.contentsGravity = CALayerContentsGravity.resizeAspectFill
			imagePreview.layer.masksToBounds = true
			photoPreview.addSubview(imagePreview)

			photoPreview.isHidden = false
			photoPreviewButton.isHidden = false

			lastDocId = doc.Id
//

			var crop = PhotoCrop(id: lastDocId)
			self.view.addSubview(crop)
//
//			EventsManager.shared().sendNotify(Constants.EVENT_OPEN_DOC, data: ["doc_id": doc.Id])
		}
	}

	@objc func openDoc(_ sender: AnyObject)
	{
		if(self.isBatch)
		{
			self.stopCature()

			self.IsPosibleTakeImage = false


			batchCount.text = ""

			//-----

			let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)

			var files = [DocModelFile]()

			let doc = DocModel()
			doc.Id = UUID().uuidString
			doc.CreatedAt = Int64(NSDate().timeIntervalSince1970)
			doc.Name = getFilename()

			for image in self.images
			{
				let fileName = UUID().uuidString + ".jpg"

				let fileURL = docDirURL.appendingPathComponent(fileName)

				do
				{
					try image.jpegData(compressionQuality: 1)!.write(to: fileURL, options: [.atomic])
				}
				catch(let error)
				{
					print(error)
				}

				let file = DocModelFile()
				file.Width = Float(image.size.width)
				file.Height = Float(image.size.height)
				file.FileName = fileName
				file.Id = UUID().uuidString
				file.DocId = doc.Id
				file.TypeFile = "image"

				files.append(file)
			}

			self.images = [UIImage]()

			try! Core.shared().getRealm.write {
				Core.shared().getRealm.add(doc)
				Core.shared().getRealm.add(files)
			}

			EventsManager.shared().sendNotify(Constants.EVENT_OPEN_DOC, data: ["doc_id": doc.Id])
		}
		else
		{
			if(lastDocId.isEmpty)
			{
				return
			}

			self.stopCature()

			photoPreview.isHidden = true
			photoPreviewButton.isHidden = true

			EventsManager.shared().sendNotify(Constants.EVENT_OPEN_DOC, data: ["doc_id": lastDocId])
		}

		self.images = [UIImage]()

		for i in photoPreview.subviews {
			i.removeFromSuperview()
		}

		for i in photoPreview.subviews {
			i.removeFromSuperview()
		}

		photoPreview.isHidden = true
		photoPreviewButton.isHidden = true

	}

	var lastDocId = ""

//	@objc func batchChange(_ sender: UISwitch)
//	{
//		var docsCount = Core.shared().getRealm.objects(DocModel.self).count
//
//		Core.shared().checkTrial() { premStatus in
//			switch(premStatus)
//			{
//			case .Free, .Trial:
//				if(docsCount >= 3 && !Constants.IS_DEV)
//				{
//					Core.shared().showPremium()
//					return
//				}
//				break
//			case .Premium:
//				break
//			}
//
//			self.images = [UIImage]()
//
//			self.batchCount.text = ""
//
//			self.completeTitle.isHidden = true
//
//			self.completeButton.isHidden = true
//
//			if(sender.isOn)
//			{
//				self.switchTitle.text = s("batch_on")
//				self.switchTitle.textColor = UIColor.white
//				self.isBatch = true
//			}
//			else
//			{
//				self.switchTitle.text = s("batch_off")
//				self.switchTitle.textColor = UIColor.lightGray
//				self.isBatch = false
//			}
//		}
//
//
//	}

	func createCameraNotAccess(title: String)
	{
		if(ph != nil)
		{
			ph.removeFromSuperview()
		}

		ph = UIView()
		ph.frame = CGRect(0, self.top.getHeight(), ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT - self.top.getHeight() - self.bottomBar.getHeight())
		self.view.addSubview(ph)

		let label = UILabel()
		label.textAlignment = .center
		label.textColor = UIColor.black
		label.text = title
		label.setX(10)
		label.setY(10)
		label.setSize(ph.getWidth() - 20, ph.getHeight() - 20)
		ph.addSubview(label)
	}

	@objc func takePhotoAction(_ sender: AnyObject)
	{
		print("** takePhotoAction")

		if(cameraAutoScan)
		{
			return
		}

		if(!self.IsPosibleTakeImage)
		{
			return
		}

		var docsCount = Core.shared().getRealm.objects(DocModel.self).count

		Core.shared().checkTrial() { premStatus in
			switch(premStatus)
			{
			case .Free, .Trial:
				if(docsCount >= 3 && !Constants.IS_DEV)
				{
					Core.shared().showPremium()
					return
				}
				break
			case .Premium:
				break
			}

			let photoSettings = AVCapturePhotoSettings()

			photoSettings.isHighResolutionPhotoEnabled = true

			switch(self.isFlashEnable)
			{
			case 1:
				photoSettings.flashMode = .auto
				break
			case 2:
				photoSettings.flashMode = .on
				break;
			case 3:
				photoSettings.flashMode = .off
				break
			default:
				break
			}

			if let firstAvailablePreviewPhotoPixelFormatTypes = photoSettings.availablePreviewPhotoPixelFormatTypes.first
			{
				photoSettings.previewPhotoFormat = [kCVPixelBufferPixelFormatTypeKey as String: firstAvailablePreviewPhotoPixelFormatTypes]
			}

			self.photoOutput.capturePhoto(with: photoSettings, delegate: self)
		}


	}

	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)

		print("*** SCAN WILL APPEAR")
	}

	func stopCature()
	{
		self.captureSession.stopRunning()
	}

	var previewLiveLayer: UIView!

	var timerView: UILabel!
}

extension ScanController: AVCapturePhotoCaptureDelegate
{
	@available(iOS 11.0, *)
	func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?)
	{
		print("*** didFinishProcessingPhoto 1")

		let data = photo.fileDataRepresentation()
		let image =  UIImage(data: data!)

		var newImage = image?.toNoir(true)

		saveImageAndOpen(image: newImage!)
	}

	func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?)
	{
		if let error = error
		{
			print("Error capturing photo: \(error)")
		}
		else
		{
			if let sampleBuffer = photoSampleBuffer, let previewBuffer = previewPhotoSampleBuffer, let dataImage = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: sampleBuffer, previewPhotoSampleBuffer: previewBuffer)
			{
				if let image = UIImage(data: dataImage)
				{
					print("** TAKE PHOTO SUCCESS")
					self.stopCature()

					self.IsPosibleTakeImage = false

					var newImage = image.toNoir(true)

					saveImageAndOpen(image: newImage)
				}
			}
		}
	}

	func getFilename() -> String
	{
		let date = NSDate()

		let df1 = DateFormatter()
		let locale = Locale(identifier: s("locale_id"))
		df1.locale = locale

		df1.dateFormat = s("date_format")
		let stringDate =  df1.string(from: date as Date)

		return String(format: s("file_name_format"), stringDate)

	}
}
