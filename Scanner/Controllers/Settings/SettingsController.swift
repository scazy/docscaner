//
//  SettingsController.swift
//  Scanner
//
//  Created by Artem  on 05/02/2019.
//  Copyright © 2019 VladimirKuzmin. All rights reserved.
//

import Foundation
import UIKit

class SettingsController: UIViewController, ViewControllerDelegate, SettingsTopDelegate
{


	var top: SettingsTop!

	var docsScroll: UIScrollView!

	func setParams(params: [String : Any])
	{

	}

	func willBeAppear()
	{

	}

	func willBeDisappear()
	{
		
	}

	override func viewDidLoad()
	{
		self.top = SettingsTop()
		self.top.delegate = self
		self.view.addSubview(self.top)

		var startOffset = UIApplication.shared.statusBarFrame.size.height


		var bgImage = UIImage(named: "lib_bg_image")

		var minHeight = ScreenSize.SCREEN_HEIGHT - self.top.getHeight()
		var minWidth = ScreenSize.SCREEN_WIDTH

		var originalSize = getImageSize(image: bgImage!, width: minWidth, height: 0)

		var newWidth: CGFloat = 0
		var newHeight: CGFloat = 0

		if(originalSize[1] < minHeight)
		{
			newHeight = minHeight
			var scale = minHeight / originalSize[1];

			newWidth = originalSize[0] * scale
		}
		else
		{
			newWidth = minWidth
			newHeight = originalSize[1]
		}

		var bg = UIImageView()
		bg.layer.contentsGravity = CALayerContentsGravity.resizeAspectFill
		bg.layer.masksToBounds = true
		bg.image = bgImage
		bg.setX(0)
		bg.setY(0, relative: self.top)
		bg.setSize(minWidth, minHeight)
		self.view.addSubview(bg)
		self.view.sendSubviewToBack(bg)

		//---

		print("** BG \(originalSize) \(newWidth) \(newHeight)")

		docsScroll = UIScrollView()
		docsScroll.backgroundColor = UIColor.clear
//		docsScroll.delegate = self
		docsScroll.tag = -918789
		docsScroll.setX(0)
		docsScroll.setY(0, relative: self.top)
		docsScroll.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT - top.getHeight())
		docsScroll.contentSize = CGSize(width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT - top.getHeight())
		self.view.addSubview(docsScroll)

		initBlocks()
	}

	func initBlocks()
	{
		var blockHeight: CGFloat = 50

		var blockName = UIView()
		blockName.backgroundColor = UIColor.white
		blockName.setSize(ScreenSize.SCREEN_WIDTH, 84)
		blockName.setX(0)
		blockName.setY(30)
		docsScroll.addSubview(blockName)

		//----

		var subsBlock = UIView()
		subsBlock.backgroundColor = UIColor.white
		subsBlock.setSize(ScreenSize.SCREEN_WIDTH, blockHeight)
		subsBlock.setX(0)
		subsBlock.setY(30, relative: blockName)
		docsScroll.addSubview(subsBlock)

		var subsLabel = UILabel()
		subsLabel.text = "Subscription"
		subsLabel.font = UIFont.systemFont(ofSize: 17)
		subsLabel.setX(20)
		subsLabel.setY(0)
		subsLabel.setSize(ScreenSize.SCREEN_WIDTH - 20, blockHeight)
		subsBlock.addSubview(subsLabel)

		var subsNext = UIImageView()
		subsNext.alpha = 0.5
		subsNext.setSize(blockHeight - 15, blockHeight - 15)
		subsNext.setX(ScreenSize.SCREEN_WIDTH - blockHeight + 15)
		subsNext.setY(7.5)
		subsNext.image = UIImage(named: "settings_next")
		subsBlock.addSubview(subsNext)

		//----

		var signBlock = UIView()
		signBlock.backgroundColor = UIColor.white
		signBlock.setSize(ScreenSize.SCREEN_WIDTH, blockHeight)
		signBlock.setX(0)
		signBlock.setY(30, relative: subsBlock)
		docsScroll.addSubview(signBlock)

		var signLabel = UILabel()
		signLabel.text = "Signature"
		signLabel.font = UIFont.systemFont(ofSize: 17)
		signLabel.setX(20)
		signLabel.setY(0)
		signLabel.setSize(ScreenSize.SCREEN_WIDTH - 20, blockHeight)
		signBlock.addSubview(signLabel)

		var signNext = UIImageView()
		signNext.alpha = 0.5
		signNext.setSize(blockHeight - 15, blockHeight - 15)
		signNext.setX(ScreenSize.SCREEN_WIDTH - blockHeight + 15)
		signNext.setY(7.5)
		signNext.image = UIImage(named: "settings_next")
		signBlock.addSubview(signNext)

		var signButton = UIButton()
		signButton.setX(0)
		signButton.setY(0)
		signButton.setSize(ScreenSize.SCREEN_WIDTH, blockHeight)
		signButton.addTarget(self, action: "signatureAction:", for: .touchUpInside)
		signBlock.addSubview(signButton)

		//---

		var pinBlock = UIView()
		pinBlock.backgroundColor = UIColor.white
		pinBlock.setSize(ScreenSize.SCREEN_WIDTH, blockHeight)
		pinBlock.setX(0)
		pinBlock.setY(30, relative: signBlock)
		docsScroll.addSubview(pinBlock)

		var pinLabel = UILabel()
		pinLabel.text = "Use pin-code"
		pinLabel.font = UIFont.systemFont(ofSize: 17)
		pinLabel.setX(20)
		pinLabel.setY(0)
		pinLabel.setSize(ScreenSize.SCREEN_WIDTH - 20, blockHeight)
		pinBlock.addSubview(pinLabel)

		//---

		var launchBlock = UIView()
		launchBlock.backgroundColor = UIColor.white
		launchBlock.setSize(ScreenSize.SCREEN_WIDTH, blockHeight)
		launchBlock.setX(0)
		launchBlock.setY(30, relative: pinBlock)
		docsScroll.addSubview(launchBlock)

		var launchSep = UIView()
		launchSep.backgroundColor = UIColor(red: 0.78, green: 0.78, blue: 0.8, alpha: 0.5)
		launchSep.setSize(ScreenSize.SCREEN_WIDTH - 20, 1)
		launchSep.setX(20)
		launchSep.setY(launchBlock.getHeight() - 1)
		launchBlock.addSubview(launchSep)

		var launchLabel = UILabel()
		launchLabel.text = "On app launch"
		launchLabel.font = UIFont.systemFont(ofSize: 17)
		launchLabel.setX(20)
		launchLabel.setY(0)
		launchLabel.setSize(ScreenSize.SCREEN_WIDTH - 20, blockHeight)
		launchBlock.addSubview(launchLabel)

		var launchNext = UIImageView()
		launchNext.alpha = 0.5
		launchNext.setSize(blockHeight - 15, blockHeight - 15)
		launchNext.setX(ScreenSize.SCREEN_WIDTH - blockHeight + 15)
		launchNext.setY(7.5)
		launchNext.image = UIImage(named: "settings_next")
		launchBlock.addSubview(launchNext)

		//----

		var flashBlock = UIView()
		flashBlock.backgroundColor = UIColor.white
		flashBlock.setSize(ScreenSize.SCREEN_WIDTH, blockHeight)
		flashBlock.setX(0)
		flashBlock.setY(0, relative: launchBlock)
		docsScroll.addSubview(flashBlock)

		var flashSep = UIView()
		flashSep.backgroundColor = UIColor(red: 0.78, green: 0.78, blue: 0.8, alpha: 0.5)
		flashSep.setSize(ScreenSize.SCREEN_WIDTH - 20, 1)
		flashSep.setX(20)
		flashSep.setY(flashBlock.getHeight() - 1)
		flashBlock.addSubview(flashSep)

		var flashLabel = UILabel()
		flashLabel.text = "Flashlight"
		flashLabel.font = UIFont.systemFont(ofSize: 17)
		flashLabel.setX(20)
		flashLabel.setY(0)
		flashLabel.setSize(ScreenSize.SCREEN_WIDTH - 20, blockHeight)
		flashBlock.addSubview(flashLabel)

		var flashNext = UIImageView()
		flashNext.alpha = 0.5
		flashNext.setSize(blockHeight - 15, blockHeight - 15)
		flashNext.setX(ScreenSize.SCREEN_WIDTH - blockHeight + 15)
		flashNext.setY(7.5)
		flashNext.image = UIImage(named: "settings_next")
		flashBlock.addSubview(flashNext)

		//----

		var filterBlock = UIView()
		filterBlock.backgroundColor = UIColor.white
		filterBlock.setSize(ScreenSize.SCREEN_WIDTH, blockHeight)
		filterBlock.setX(0)
		filterBlock.setY(0, relative: flashBlock)
		docsScroll.addSubview(filterBlock)

		var filterSep = UIView()
		filterSep.backgroundColor = UIColor(red: 0.78, green: 0.78, blue: 0.8, alpha: 0.5)
		filterSep.setSize(ScreenSize.SCREEN_WIDTH - 20, 1)
		filterSep.setX(20)
		filterSep.setY(filterBlock.getHeight() - 1)
		filterBlock.addSubview(filterSep)

		var filterLabel = UILabel()
		filterLabel.text = "Filter"
		filterLabel.font = UIFont.systemFont(ofSize: 17)
		filterLabel.setX(20)
		filterLabel.setY(0)
		filterLabel.setSize(ScreenSize.SCREEN_WIDTH - 20, blockHeight)
		filterBlock.addSubview(filterLabel)

		var filterNext = UIImageView()
		filterNext.alpha = 0.5
		filterNext.setSize(blockHeight - 15, blockHeight - 15)
		filterNext.setX(ScreenSize.SCREEN_WIDTH - blockHeight + 15)
		filterNext.setY(7.5)
		filterNext.image = UIImage(named: "settings_next")
		filterBlock.addSubview(filterNext)

		//----

		var typeBlock = UIView()
		typeBlock.backgroundColor = UIColor.white
		typeBlock.setSize(ScreenSize.SCREEN_WIDTH, blockHeight)
		typeBlock.setX(0)
		typeBlock.setY(0, relative: filterBlock)
		docsScroll.addSubview(typeBlock)

		var typeSep = UIView()
		typeSep.backgroundColor = UIColor(red: 0.78, green: 0.78, blue: 0.8, alpha: 0.5)
		typeSep.setSize(ScreenSize.SCREEN_WIDTH - 20, 1)
		typeSep.setX(20)
		typeSep.setY(typeBlock.getHeight() - 1)
		typeBlock.addSubview(typeSep)

		var typeLabel = UILabel()
		typeLabel.text = "Scan type"
		typeLabel.font = UIFont.systemFont(ofSize: 17)
		typeLabel.setX(20)
		typeLabel.setY(0)
		typeLabel.setSize(ScreenSize.SCREEN_WIDTH - 20, blockHeight)
		typeBlock.addSubview(typeLabel)

		var typeNext = UIImageView()
		typeNext.alpha = 0.5
		typeNext.setSize(blockHeight - 15, blockHeight - 15)
		typeNext.setX(ScreenSize.SCREEN_WIDTH - blockHeight + 15)
		typeNext.setY(7.5)
		typeNext.image = UIImage(named: "settings_next")
		typeBlock.addSubview(typeNext)


		//----

		var saveBlock = UIView()
		saveBlock.backgroundColor = UIColor.white
		saveBlock.setSize(ScreenSize.SCREEN_WIDTH, blockHeight)
		saveBlock.setX(0)
		saveBlock.setY(0, relative: typeBlock)
		docsScroll.addSubview(saveBlock)

		var saveSep = UIView()
		saveSep.backgroundColor = UIColor(red: 0.78, green: 0.78, blue: 0.8, alpha: 0.5)
		saveSep.setSize(ScreenSize.SCREEN_WIDTH - 20, 1)
		saveSep.setX(20)
		saveSep.setY(saveBlock.getHeight() - 1)
		saveBlock.addSubview(saveSep)

		var saveLabel = UILabel()
		saveLabel.text = "Save original to Library"
		saveLabel.font = UIFont.systemFont(ofSize: 17)
		saveLabel.setX(20)
		saveLabel.setY(0)
		saveLabel.setSize(ScreenSize.SCREEN_WIDTH - 20, blockHeight)
		saveBlock.addSubview(saveLabel)

		//----

		var contactBlock = UIView()
		contactBlock.backgroundColor = UIColor.white
		contactBlock.setSize(ScreenSize.SCREEN_WIDTH, blockHeight)
		contactBlock.setX(0)
		contactBlock.setY(30, relative: saveBlock)
		docsScroll.addSubview(contactBlock)

		var contactLabel = UILabel()
		contactLabel.text = "Contact support"
		contactLabel.font = UIFont.systemFont(ofSize: 17)
		contactLabel.setX(20)
		contactLabel.setY(0)
		contactLabel.setSize(ScreenSize.SCREEN_WIDTH - 20, blockHeight)
		contactBlock.addSubview(contactLabel)

		var contactNext = UIImageView()
		contactNext.alpha = 0.5
		contactNext.setSize(blockHeight - 15, blockHeight - 15)
		contactNext.setX(ScreenSize.SCREEN_WIDTH - blockHeight + 15)
		contactNext.setY(7.5)
		contactNext.image = UIImage(named: "settings_next")
		contactBlock.addSubview(contactNext)

		docsScroll.contentSize = CGSize(width: ScreenSize.SCREEN_WIDTH, height: contactBlock.getHeight() + contactBlock.getY() + 30)
	}

	func SettingsTopBack()
	{
		Core.shared().changeViewController(name: "libraryController")
	}

	@objc func signatureAction(_ sender: AnyObject)
	{
		Core.shared().changeViewController(name: "signatureController")
	}

	func SignatureSettingsBack()
	{

	}
}
