//
//  SettingsTop.swift
//  Scanner
//
//  Created by Artem  on 05/02/2019.
//  Copyright © 2019 VladimirKuzmin. All rights reserved.
//

import Foundation
import UIKit

protocol SettingsTopDelegate
{
	func SettingsTopBack()
}

class SettingsTop: UIView
{
	private var height: CGFloat = 42

	var delegate: SettingsTopDelegate!

	var libTitle: UILabel!

	func setTopTitle(title: String)
	{
		libTitle.text = title
	}

	init()
	{
		var startOffset = UIApplication.shared.statusBarFrame.size.height

		super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 42 + startOffset))

		self.backgroundColor = UIColor.white

		libTitle = UILabel()
		//		libTitle.backgroundColor = .green
		libTitle.setX(0)
		libTitle.setY(startOffset)
		libTitle.setSize(ScreenSize.SCREEN_WIDTH, height)
		libTitle.textColor = UIColor.black
		libTitle.textAlignment = .center
		libTitle.text = "Settings"
		libTitle.font = UIFont.boldSystemFont(ofSize: 17)
		self.addSubview(libTitle)

		var topPart = self.getWidth() / 7

		//----

		var settingsLayout = UIView()
		settingsLayout.setX(5)
		settingsLayout.setY(startOffset)
		settingsLayout.setSize(self.height, self.height)
		self.addSubview(settingsLayout)

		var lib_i_5 = UIImage(named: "edit_back")
		var lib_i_5Size = getImageSize(image: lib_i_5!, width: 0, height: settingsLayout.getHeight() - 20)

		var settingsIcon = UIImageView()
		settingsIcon.image = lib_i_5
		settingsIcon.setSize(settingsLayout.getWidth() - 14, settingsLayout.getWidth() - 14)
		settingsIcon.setX(7)
		settingsIcon.setY(7)
		settingsLayout.addSubview(settingsIcon)

		var settingsButton = UIButton()
		settingsButton.setX(0)
		settingsButton.setY(0)
		settingsButton.setSize(settingsLayout.getWidth(), settingsLayout.getHeight())
		settingsButton.addTarget(self, action: "backAction:", for: .touchUpInside)
		settingsLayout.addSubview(settingsButton)
	}

	required init?(coder aDecoder: NSCoder)
	{
		fatalError("init(coder:) has not been implemented")
	}

	@objc func backAction(_ sender: AnyObject)
	{
		self.delegate.SettingsTopBack()
	}

}
