//
//  SignatureSettings.swift
//  Scanner
//
//  Created by Artem  on 05/02/2019.
//  Copyright © 2019 VladimirKuzmin. All rights reserved.
//

import Foundation
import UIKit

class SignatureController: UIViewController, ViewControllerDelegate
{
	var top: UIView!

	var tempImageView: UIImageView!

	var mainImageView: UIImageView!

	var lastPoint = CGPoint.zero

	var color = UIColor.black

	var brushWidth: CGFloat = 5

	var opacity: CGFloat = 1.0

	var swiped = false

	func setParams(params: [String : Any])
	{

	}

	func willBeAppear()
	{

	}

	func willBeDisappear()
	{

	}

	override func viewDidLoad()
	{
		self.view.backgroundColor = UIColor.white

		tempImageView = UIImageView()
		tempImageView.setX(0)
		tempImageView.setY(0)
		tempImageView.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
		self.view.addSubview(tempImageView)

		mainImageView = UIImageView()
		mainImageView.setX(0)
		mainImageView.setY(0)
		mainImageView.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
		self.view.addSubview(mainImageView)

		var layout = UIView()
		layout.backgroundColor = UIColor.clear
		layout.setX(15)
		layout.setSize(ScreenSize.SCREEN_WIDTH - 30, ScreenSize.SCREEN_HEIGHT / 3)
		layout.toCenterY(self.view)
		layout.layer.borderColor = UIColor.black.withAlphaComponent(0.5).cgColor
		layout.layer.borderWidth = 1
		self.view.addSubview(layout)

		var hideTop = UIView()
		hideTop.backgroundColor = UIColor.white
		hideTop.setX(0)
		hideTop.setY(0)
		hideTop.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT / 3)
		self.view.addSubview(hideTop)

		var hideBottom = UIView()
		hideBottom.backgroundColor = UIColor.white
		hideBottom.setX(0)
		hideBottom.setY(0, relative: layout)
		hideBottom.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT / 3)
		self.view.addSubview(hideBottom)

		var hideLeft = UIView()
		hideLeft.backgroundColor = UIColor.white
		hideLeft.setX(0)
		hideLeft.setSize(15, ScreenSize.SCREEN_HEIGHT / 3)
		hideLeft.toCenterY(self.view)
		self.view.addSubview(hideLeft)

		var hideRight = UIView()
		hideRight.backgroundColor = UIColor.white
		hideRight.setX(0, relative: layout)
		hideRight.setSize(15, ScreenSize.SCREEN_HEIGHT / 3)
		hideRight.toCenterY(self.view)
		self.view.addSubview(hideRight)


		//----

		initTopControls()

		var bottomLayout = UIView()
		bottomLayout.backgroundColor = UIColor.white
		bottomLayout.setSize(ScreenSize.SCREEN_WIDTH, 65 + UIApplication.shared.statusBarFrame.size.height)
		bottomLayout.setX(0)
		bottomLayout.setY(ScreenSize.SCREEN_HEIGHT - bottomLayout.getHeight())
		self.view.addSubview(bottomLayout)

		var buttonsOffset: CGFloat = ScreenSize.SCREEN_WIDTH / 2 - 10 - 55


		buttonBlackLayout = UIView()
		buttonBlackLayout.setX(buttonsOffset)
		buttonBlackLayout.setY(5)
		buttonBlackLayout.setSize(55, 55)
		buttonBlackLayout.layer.borderWidth = 1
		buttonBlackLayout.layer.borderColor = UIColor.black.cgColor
		buttonBlackLayout.layer.cornerRadius = buttonBlackLayout.getWidth() / 2
		bottomLayout.addSubview(buttonBlackLayout)

		var buttonBlack = UIButton()
		buttonBlack.backgroundColor = UIColor.black
		buttonBlack.tag = 1
		buttonBlack.addTarget(self, action: "changeColor:", for: .touchUpInside)
		buttonBlack.setX(3)
		buttonBlack.setY(3)
		buttonBlack.setSize(buttonBlackLayout.getWidth() - 6, buttonBlackLayout.getHeight() - 6)
		buttonBlack.layer.cornerRadius = buttonBlack.getWidth() / 2
		buttonBlackLayout.addSubview(buttonBlack)

		buttonBlueLayout = UIView()
		buttonBlueLayout.setX(20, relative: buttonBlackLayout)
		buttonBlueLayout.setY(5)
		buttonBlueLayout.setSize(55, 55)
//		buttonBlueLayout.layer.borderWidth = 1
//		buttonBlueLayout.layer.borderColor = UIColor.blue.cgColor
		buttonBlueLayout.layer.cornerRadius = buttonBlackLayout.getWidth() / 2
		bottomLayout.addSubview(buttonBlueLayout)

		var buttonBlue = UIButton()
		buttonBlue.tag = 2
		buttonBlue.addTarget(self, action: "changeColor:", for: .touchUpInside)
		buttonBlue.backgroundColor = UIColor.blue
		buttonBlue.setX(3)
		buttonBlue.setY(3)
		buttonBlue.setSize(buttonBlueLayout.getWidth() - 6, buttonBlueLayout.getHeight() - 6)
		buttonBlue.layer.cornerRadius = buttonBlue.getWidth() / 2
		buttonBlueLayout.addSubview(buttonBlue)

		var buttonClear = UIButton()
		buttonClear.setSize(100, 55)
		buttonClear.setX(bottomLayout.getWidth() - buttonClear.getWidth())
		buttonClear.setY(5)
		buttonClear.setTitle("Clear", for: .normal)
		buttonClear.setTitleColor(.blue, for: .normal)
//		button.setTitleColor(.lightGray, for: .selected)
		buttonClear.titleLabel?.font = UIFont.systemFont(ofSize: 18)
		buttonClear.addTarget(self, action: "clear:", for: .touchUpInside)
		bottomLayout.addSubview(buttonClear)
	}

	@objc func clear(_ sender: AnyObject)
	{
		tempImageView.image = nil

		mainImageView.image = nil
	}

	var buttonBlackLayout: UIView!

	var buttonBlueLayout: UIView!

	@objc func changeColor(_ sender: AnyObject)
	{
		if(sender.tag == 1)
		{
			buttonBlackLayout.layer.borderWidth = 1
			buttonBlackLayout.layer.borderColor = UIColor.black.cgColor

			buttonBlueLayout.layer.borderWidth = 0
			buttonBlueLayout.layer.borderColor = UIColor.clear.cgColor

			self.color = UIColor.black
		}
		else
		{
			buttonBlueLayout.layer.borderWidth = 1
			buttonBlueLayout.layer.borderColor = UIColor.blue.cgColor

			buttonBlackLayout.layer.borderWidth = 0
			buttonBlackLayout.layer.borderColor = UIColor.clear.cgColor

			self.color = UIColor.blue
		}
	}

	func initTopControls()
	{
		var startOffset = UIApplication.shared.statusBarFrame.size.height



		top = UIView()
		top.backgroundColor = UIColor.white
		top.setX(0)
		top.setY(0)
		top.setSize(ScreenSize.SCREEN_WIDTH, 42 + startOffset)
		top.clipsToBounds = true
		self.view.addSubview(top)

		///------

		let iconSize: CGFloat = 50 / 2.5

		var topPart = ScreenSize.SCREEN_WIDTH / 7

		//----

		var backLayout = UIView()
		backLayout.setX(5)
		backLayout.setY(startOffset)
		backLayout.setSize(42, 42)
		top.addSubview(backLayout)

		var lib_i_5 = UIImage(named: "edit_back")
		var lib_i_5Size = getImageSize(image: lib_i_5!, width: 0, height: backLayout.getHeight() - 20)

		var backIcon = UIImageView()
		backIcon.image = lib_i_5
		backIcon.setSize(backLayout.getWidth() - 14, backLayout.getWidth() - 14)
		backIcon.setX(7)
		backIcon.setY(7)
		backLayout.addSubview(backIcon)

		var backButton = UIButton()
		backButton.setX(0)
		backButton.setY(0)
		backButton.setSize(backLayout.getWidth(), backLayout.getHeight())
		backButton.addTarget(self, action: "backAction:", for: .touchUpInside)
		backLayout.addSubview(backButton)

		var libTitle = UILabel()
		libTitle.setX(0, relative: backLayout)
		libTitle.setY(startOffset)
		libTitle.setSize(ScreenSize.SCREEN_WIDTH - backLayout.getWidth() - backLayout.getWidth(), 42)
		libTitle.textColor = UIColor.black
		libTitle.textAlignment = .center
		libTitle.text = "Signature"
		libTitle.font = UIFont.boldSystemFont(ofSize: 17)
		top.addSubview(libTitle)
	}

	@objc func backAction(_ sender: AnyObject)
	{
		Core.shared().changeViewController(name: "settingsController")
	}

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		guard let touch = touches.first else {
			return
		}

		swiped = false
		lastPoint = touch.location(in: view)
	}

	override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		guard let touch = touches.first else {
			return
		}

		swiped = true
		let currentPoint = touch.location(in: view)
		drawLine(from: lastPoint, to: currentPoint)

		lastPoint = currentPoint
	}

	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		if !swiped
		{
			// draw a single point
			drawLine(from: lastPoint, to: lastPoint)
		}

		// Merge tempImageView into mainImageView
		UIGraphicsBeginImageContext(mainImageView.frame.size)

		mainImageView.image?.draw(in: view.bounds, blendMode: .normal, alpha: 1.0)

		tempImageView?.image?.draw(in: view.bounds, blendMode: .normal, alpha: opacity)

		mainImageView.image = UIGraphicsGetImageFromCurrentImageContext()

		UIGraphicsEndImageContext()

		tempImageView.image = nil
	}



	func drawLine(from fromPoint: CGPoint, to toPoint: CGPoint)
	{
		UIGraphicsBeginImageContext(view.frame.size)

		guard let context = UIGraphicsGetCurrentContext() else {
			return
		}

		tempImageView.image?.draw(in: view.bounds)

		context.move(to: fromPoint)
		context.addLine(to: toPoint)

		context.setLineCap(.round)
		context.setBlendMode(.normal)
		context.setLineWidth(brushWidth)
		context.setStrokeColor(color.cgColor)

		context.strokePath()

		tempImageView.image = UIGraphicsGetImageFromCurrentImageContext()
		tempImageView.alpha = opacity

		UIGraphicsEndImageContext()
	}
}
