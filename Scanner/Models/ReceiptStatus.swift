//
//  ReceiptStatus.swift
//  Scanner
//
//  Created by Artem  on 28/11/2018.
//  Copyright © 2018 VladimirKuzmin. All rights reserved.
//

import Foundation
import RealmSwift

class ReceiptStatus: Object
{
	override static func primaryKey() -> String?
	{
		return "Id"
	}

	@objc dynamic var Status = 0 // 0 - trial, 2 - full

	@objc dynamic var Id = 0
}
