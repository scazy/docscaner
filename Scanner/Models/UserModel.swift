//
//  UserModel.swift
//  Scanner
//
//  Created by Artem  on 14/11/2018.
//  Copyright © 2018 VladimirKuzmin. All rights reserved.
//

import Foundation
import RealmSwift

class UserModel: Object
{
	override static func primaryKey() -> String?
	{
		return "Id"
	}

	@objc dynamic var Token = ""

	@objc dynamic var Id: Int = 0
}
