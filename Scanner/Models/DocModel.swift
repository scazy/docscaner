//
//  DocModel.swift
//  Scanner
//
//  Created by Artem  on 26/09/2018.
//  Copyright © 2018 VladimirKuzmin. All rights reserved.
//

import Foundation
import RealmSwift

class DocModel: Object
{
	@objc dynamic var Id: String = ""

	@objc dynamic var Name: String = ""

	@objc dynamic var CreatedAt: Int64 = 0

	override static func primaryKey() -> String?
	{
		return "Id"
	}

	@objc dynamic var IsTest: Bool = false

	@objc dynamic var ORCText: String = ""

	@objc dynamic var OCRFileName: String = ""

	@objc dynamic var PDFFileName: String = ""

	@objc dynamic var IsFolder: Bool = false

	@objc dynamic var ParentId = ""
}

class DocModelFile: Object
{
	@objc dynamic var Id: String = ""

	@objc dynamic var DocId: String = ""

	override static func primaryKey() -> String?
	{
		return "Id"
	}

	@objc dynamic var FileName: String = ""

	@objc dynamic var TypeFile: String = ""

	@objc dynamic var Width: Float = 0

	@objc dynamic var Height: Float = 0

	@objc dynamic var ORCText: String = ""

	@objc dynamic var Size: Int = 0

	@objc dynamic var ParentId = ""
}
