//
//  LeftMenu.swift
//  Scanner
//
//  Created by Artem  on 15/10/2018.
//  Copyright © 2018 VladimirKuzmin. All rights reserved.
//

import Foundation
import UIKit

class LeftMenuItem
{
	var Id = 0
	var Title = ""

	init(id: Int, title: String)
	{
		self.Id = id
		self.Title = title
	}
}

class LeftMenu: UIView
{
	var items = [LeftMenuItem]()

	var bg: UIView!

	var menu: UIView!

	init()
	{
		super.init(frame: CGRect(0, 0, ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT))

		let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.leftSwipe(_:)))
		leftSwipe.direction = UISwipeGestureRecognizer.Direction.left
		self.addGestureRecognizer(leftSwipe)

		self.items = [
			LeftMenuItem(id: 1, title: s("menu_settings")),
			LeftMenuItem(id: 2, title: s("menu_subscribe")),
			LeftMenuItem(id: 3, title: s("menu_about"))
		]

		bg = UIView()
		bg.backgroundColor = UIColor.black.withAlphaComponent(0)
		bg.setX(0)
		bg.setY(0)
		bg.setSize(self.getWidth(), self.getHeight())
		self.addSubview(bg)

		let tapGesture = UITapGestureRecognizer(target:self, action: #selector(self.closeMenu(_:)))
		bg.addGestureRecognizer(tapGesture)


		menu = UIView()
		menu.backgroundColor = UIColor.init(red: 0.21, green: 0.42, blue: 0.69, alpha: 1)
		menu.setX((ScreenSize.SCREEN_WIDTH * 0.75) * -1)
		menu.setY(0)
		menu.setSize(ScreenSize.SCREEN_WIDTH * 0.75, ScreenSize.SCREEN_HEIGHT)
		menu.clipsToBounds = true
		self.addSubview(menu)

//		let layer0 = CAGradientLayer()
//		layer0.colors = [
//			UIColor(red: 0.13, green: 0.64, blue: 0.85, alpha: 1).cgColor,
//			UIColor(red: 0.13, green: 0.85, blue: 0.76, alpha: 1).cgColor
//		]
//		layer0.locations = [0, 1]
//		layer0.startPoint = CGPoint(x: 0.25, y: 0.5)
//		layer0.endPoint = CGPoint(x: 0.75, y: 0.5)
//		layer0.frame = CGRect(0, 0, ScreenSize.SCREEN_WIDTH, menu.getHeight())
//		menu.layer.addSublayer(layer0)

		var layoutBack = UIView()
		layoutBack.setX(0)
		layoutBack.setY(50)
		layoutBack.setSize(menu.getWidth(), 40)
		menu.addSubview(layoutBack)

		var imageBack = UIImageView()
		imageBack.image = UIImage(named: "left_menu_back")
		imageBack.setSize(10, 15)
		imageBack.setX(20)
		imageBack.toCenterY(layoutBack)
		layoutBack.addSubview(imageBack)

		//----

		var menuTitle = UILabel()
		menuTitle.text = s("menu_title")
		menuTitle.textColor = UIColor.white
		menuTitle.setSize(100, 20)
		menuTitle.setX(20, relative: imageBack)
		menuTitle.toCenterY(layoutBack)
		menuTitle.font = UIFont.systemFont(ofSize: 18)
		layoutBack.addSubview(menuTitle)

		var buttonBack = UIButton()
		buttonBack.addTarget(self, action: #selector(self.closeMenu(_:)), for: .touchUpInside)
		buttonBack.setX(0)
		buttonBack.setY(0)
		buttonBack.setSize(layoutBack.getWidth(), layoutBack.getHeight())
		layoutBack.addSubview(buttonBack)

		var itemsOffset: CGFloat = layoutBack.getY() + layoutBack.getHeight() + 100

		for i in self.items
		{
			var menuItem = UIView()
			menuItem.setX(0)
			menuItem.setY(itemsOffset)
			menuItem.setSize(menu.getWidth(), 40)
			menu.addSubview(menuItem)

			var menuItemTitle = UILabel()
			menuItemTitle.text = i.Title
			menuItemTitle.textColor = UIColor.white
			menuItemTitle.setX(50)
			menuItemTitle.setY(0)
			menuItemTitle.setSize(150, menuItem.getHeight())
			menuItemTitle.font = UIFont.systemFont(ofSize: 18)
			menuItem.addSubview(menuItemTitle)

			var menuAction = UIButton()
			menuAction.tag = i.Id
			menuAction.setX(0)
			menuAction.setY(0)
			menuAction.setSize(menuItem.getWidth(), menuItem.getHeight())
			menuAction.addTarget(self, action: "menuAction:", for: .touchUpInside)
			menuItem.addSubview(menuAction)

			itemsOffset += menuItem.getHeight() + 5
		}
	}

	@objc func menuAction(_ sender: AnyObject)
	{
		var id = sender.tag

		switch(id)
		{
		case 1:
			self.Hide {
				EventsManager.shared().sendNotify(Constants.EVENT_SHOW_SETTINGS)
			}
			break
		case 2:
			self.Hide {
				EventsManager.shared().sendNotify(Constants.EVENT_SHOW_SUBSRIBTIONS)
			}
			break
		case 3:
			break
		default:
			break
		}
	}

	func Show()
	{
		UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations:
		{
			self.bg.backgroundColor = UIColor.black.withAlphaComponent(0.85)

			self.menu.setX(0)
		}, completion: { (result) -> () in

		})
	}

	func Hide(finish: @escaping () -> ())
	{
		UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations:
		{
			self.bg.backgroundColor = UIColor.black.withAlphaComponent(0)

			self.menu.setX(self.menu.getWidth() * -1)
		}, completion: { (result) -> () in
			finish()
			self.removeFromSuperview()
		})
	}

	required init(coder aDecoder: NSCoder)
	{
		fatalError("init(coder:) has not been implemented")
	}

	@objc func leftSwipe(_ sender: AnyObject)
	{
		self.Hide(finish: {})
	}

	@objc func closeMenu(_ sender: AnyObject)
	{
		self.Hide(finish: {})
	}
}
