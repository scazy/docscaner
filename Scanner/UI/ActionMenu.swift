

import Foundation
import UIKit

protocol ActionsMenuDelegate
{
	func ActionsMenuAction(menuId: Int, itemId: Int)
	func ActionsMenuCancel()
}

enum ActionMenuItemType: Int
{
	case Default = 1
	case Alert
	case Counter
}

class ActionMenuItem
{
	var Title = ""

	var ItemId = 0

	var `Type`: ActionMenuItemType = .Default

	var CounterValue = 0

	init(title: String, itemId: Int, type: ActionMenuItemType = .Default, counterValue: Int = 0)
	{
		self.Title = title

		self.ItemId = itemId

		self.Type = type

		CounterValue = counterValue
	}
}

class ActionsMenu: UIView
{
	var delegate: ActionsMenuDelegate!

	private var _items = [ActionMenuItem]()

	private var _title = ""

	private var _commonHeight: CGFloat = 0

	var bg: UIView!

	var menu: UIView!

	private var _menuId = 0

	init(menuId: Int, items: [ActionMenuItem])
	{
		super.init(frame: CGRect(0, 0, ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT))

		self._items = items

		self._menuId = menuId

		//        self._title = title

		self._init()
	}

	@objc func tap(_ sender: AnyObject)
	{
		self.delegate.ActionsMenuCancel()

		self.Hide()
	}

	func _init()
	{
		bg = UIView()
		bg.backgroundColor = UIColor.black.withAlphaComponent(0.5)
		bg.setX(0)
		bg.setY(0)
		bg.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
		self.addSubview(bg)

		let tap = UITapGestureRecognizer(target:self, action: #selector(self.tap(_:)))
		bg.addGestureRecognizer(tap)

		//----

		menu = UIView()
		self.addSubview(menu)

		let buttons = UIView()
		buttons.layer.cornerRadius = 8
		buttons.clipsToBounds = true
		menu.addSubview(buttons)

		var buttonY: CGFloat = 0

		let titleView = UIView()
		titleView.backgroundColor = UIColor.lightGray
		titleView.setX(0)
		titleView.setY(buttonY)
		titleView.setSize(ScreenSize.SCREEN_WIDTH - 20, 40)
		buttons.addSubview(titleView)

		for i in self._items
		{
			let itemView = UIView()
			itemView.backgroundColor = UIColor.white
			itemView.setX(0)
			itemView.setY(buttonY)
			itemView.setSize(ScreenSize.SCREEN_WIDTH - 20, 55)
			buttons.addSubview(itemView)

			let title = i.Title
			let titleSize = title.getSize(size: 17)

			let itemLabel = UILabel()
			itemLabel.textColor = .blue
			itemLabel.textAlignment = .center
			itemLabel.font = UIFont.systemFont(ofSize: 17)
			itemLabel.setSize(titleSize.width, titleSize.height)
			itemLabel.toCenterX(itemView)
			itemLabel.toCenterY(itemView)
			itemLabel.text = title
			itemView.addSubview(itemLabel)

			if(i.Type == .Alert)
			{
				let alert = UILabel()
				alert.backgroundColor = UIColor.red
				alert.text = "!"
				alert.font = UIFont.boldSystemFont(ofSize: 12)
				alert.textColor = UIColor.white
				alert.textAlignment = .center
				alert.setSize(16, 16)
				alert.layer.cornerRadius = alert.getWidth() / 2
				alert.clipsToBounds = true
				alert.setX(itemLabel.getX() + itemLabel.getWidth() + 1)
				alert.setY(itemLabel.getY() - (alert.getHeight() / 2) + 3)
				itemView.addSubview(alert)
			}

			let itemButton = UIButton()
			itemButton.tag = i.ItemId
			itemButton.addTarget(self, action: #selector(self.actionItem(_:)), for: .touchUpInside)
			itemButton.setX(0)
			itemButton.setY(0)
			itemButton.setSize(itemView.frame.size.width, itemView.frame.size.height)
			itemView.addSubview(itemButton)

			buttonY += itemView.getHeight() + 1
		}

		buttons.setSize(ScreenSize.SCREEN_WIDTH - 20, buttonY)
		buttons.setX(10)
		buttons.setY(0)

		let buttonCancel = UIView()
		buttonCancel.layer.cornerRadius = 8
		buttonCancel.backgroundColor = UIColor.white
		buttonCancel.setX(10)
		buttonCancel.setY(5, relative: buttons)
		buttonCancel.setSize(ScreenSize.SCREEN_WIDTH - 20, 50)
		menu.addSubview(buttonCancel)

		let cancelLabel = UILabel()
		cancelLabel.textColor = UIColor.darkGray
		cancelLabel.textAlignment = .center
		cancelLabel.font = UIFont.boldSystemFont(ofSize: 17)
		cancelLabel.setX(0)
		cancelLabel.setY(0)
		cancelLabel.text = s("cancel_title")
		cancelLabel.setSize(buttonCancel.frame.size.width, buttonCancel.frame.size.height)
		buttonCancel.addSubview(cancelLabel)

		let cancelButton = UIButton()
		cancelButton.addTarget(self, action: #selector(self.actionCancel(_:)), for: .touchUpInside)
		cancelButton.setX(0)
		cancelButton.setY(0)
		cancelButton.setSize(buttonCancel.frame.size.width, buttonCancel.frame.size.height)
		buttonCancel.addSubview(cancelButton)

		_commonHeight =  buttons.frame.size.height + 10 + buttonCancel.frame.size.height + 5

		menu.setX(0)
		menu.setY(ScreenSize.SCREEN_HEIGHT)
		menu.setSize(ScreenSize.SCREEN_WIDTH, _commonHeight)
	}

	required init(coder aDecoder: NSCoder)
	{
		fatalError("init(coder:) has not been implemented")
	}

	@objc func actionItem(_ sender: AnyObject)
	{
		self.delegate.ActionsMenuAction(menuId: self._menuId, itemId: sender.tag)

		self.Hide()
	}

	@objc func actionCancel(_ sender: AnyObject)
	{
		self.delegate.ActionsMenuCancel()

		self.Hide()
	}

	func Show()
	{
		UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations:
		{
			self.bg.backgroundColor = UIColor.black.withAlphaComponent(0.85)
		}, completion: { (result) -> () in

		})

		UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations:
		{
			self.menu.setY(ScreenSize.SCREEN_HEIGHT - self._commonHeight - (UIApplication.shared.statusBarFrame.size.height / 3))
		}, completion: { (result) -> () in

		})
	}

	func Hide()
	{
		UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations:
			{
				self.bg.backgroundColor = UIColor.black.withAlphaComponent(0)
		}, completion: { (result) -> () in

		})

		UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations:
			{
				self.menu.setY(ScreenSize.SCREEN_HEIGHT)
		}, completion: { (result) -> () in
			self.removeFromSuperview()
		})
	}
}

