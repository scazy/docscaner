//
//  Premium.swift
//  Scanner
//
//  Created by Artem  on 27/11/2018.
//  Copyright © 2018 VladimirKuzmin. All rights reserved.
//

import Foundation
import UIKit

class Premium: UIView
{
	init()
	{
		super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))

		var texts = [
			s("premium_future_1"),
			s("premium_future_2"),
			s("premium_future_3"),
			s("premium_future_4"),
			s("premium_future_5")
		]

		self.backgroundColor = UIColor.black.withAlphaComponent(0.85)

		var part = self.getHeight() / 3

		var bannerOffset: CGFloat = 80

		if(DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_5)
		{
			bannerOffset = 20
		}

		var banner = UIView()
		banner.backgroundColor = UIColor.white
		banner.layer.cornerRadius = 10
		banner.setX(15)
		banner.setY(bannerOffset)
		banner.setSize(self.getWidth() - 30, part * 2)
		self.addSubview(banner)

		//---

		var close = UIImageView()
		close.setSize(20, 20)
		close.setX(10)
		close.setY(10)
		close.image = UIImage(named: "close_prem")
		banner.addSubview(close)

		var closeButton = UIButton()
		closeButton.setSize(20, 20)
		closeButton.setX(10)
		closeButton.setY(10)
		closeButton.addTarget(self, action: "closeAction:", for: .touchUpInside)
		banner.addSubview(closeButton)

		//---
		var restoreText = s("restore_purchase")
		var resoreSize = restoreText.getSize(size: 14)

		var restoreLabel = UILabel()
		restoreLabel.text = restoreText
		restoreLabel.font = UIFont.systemFont(ofSize: 14)
		restoreLabel.setSize(resoreSize.width, 18)
		restoreLabel.setY(11)
		restoreLabel.setX(banner.getWidth() - resoreSize.width - 10)
		restoreLabel.textColor = UIColor(red: 0.21, green: 0.42, blue: 0.69, alpha: 1)
		banner.addSubview(restoreLabel)

		var resoreAction = UIButton()
		resoreAction.setY(restoreLabel.getY())
		resoreAction.setX(restoreLabel.getX())
		resoreAction.setSize(resoreAction.getWidth(), resoreAction.getHeight())
		resoreAction.addTarget(self, action: "restoreAction:", for: .touchUpInside)
		banner.addSubview(resoreAction)


		var logoSize: CGFloat = banner.getWidth() / 3

		if(DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_5)
		{
			logoSize = banner.getWidth() / 4

		}

		var logo = UIImageView()
		logo.image = UIImage(named: "logo")
		logo.setSize(logoSize, logoSize)
		logo.setY(50)
		logo.toCenterX(banner)
		logo.layer.cornerRadius = 25
		logo.clipsToBounds = true
		banner.addSubview(logo)

		var title = UILabel()
		title.setX(15)
		title.setY(15, relative: logo)
		title.setSize(banner.getWidth() - 30, 25)
		title.text = s("premium_title")
		title.textAlignment = .center
		title.textColor = .black
		title.font = UIFont.systemFont(ofSize: 21)
		banner.addSubview(title)

		var offset = title.getY() + title.getHeight() + 15

		var featureOffset: CGFloat = 40
		if(DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_5)
		{
			featureOffset = 35
		}

		for text in texts
		{
			var tick = UIImageView()
			tick.setSize(24, 18)
			tick.setX(40)
			tick.setY(offset)
			tick.image = UIImage(named: "tick")
			banner.addSubview(tick)

			var textSize = text.getSize(size: 16, isBold: false, setWidth: 0, setHeight: 18)

			var title = UILabel()
			title.textColor = UIColor.black
			title.font = UIFont.systemFont(ofSize: 16)
			title.setX(10, relative: tick)
			title.text = text
			title.setY(offset)
			title.setSize(textSize.width, 18)
			banner.addSubview(title)

			offset += featureOffset
		}

//		offset += 50

		var buttonOffset: CGFloat = 15
		if(DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_5)
		{
			buttonOffset = 10
		}

		var button1 = UIView()
		button1.setX(15)
		button1.setSize(banner.getWidth() - 30, 40)
		button1.setY(offset)
		button1.layer.borderColor = UIColor(red: 0.21, green: 0.42, blue: 0.69, alpha: 1).cgColor
		button1.layer.borderWidth = 1
		button1.backgroundColor = .white
		button1.layer.cornerRadius = button1.getHeight() / 2
		banner.addSubview(button1)

		var button1Text = UILabel()
		button1Text.setX(0)
		button1Text.setY(0)
		button1Text.setSize(button1.getWidth(), button1.getHeight())
		button1Text.font = UIFont.systemFont(ofSize: 17)
		button1Text.textAlignment = .center
		button1Text.textColor = UIColor(red: 0.21, green: 0.42, blue: 0.69, alpha: 1)
		button1.addSubview(button1Text)

		var button1Action = UIButton()
		button1Action.tag = 1
		button1Action.setX(0)
		button1Action.setY(0)
		button1Action.setSize(button1.getWidth(), button1.getHeight())
		button1Action.addTarget(self, action: "buySubscribe:", for: .touchUpInside)
		button1.addSubview(button1Action)
		//---

		var button2 = UIView()
		button2.setX(15)
		button2.setSize(banner.getWidth() - 30, 40)
		button2.setY(buttonOffset, relative: button1)
		button2.layer.borderColor = UIColor(red: 0.21, green: 0.42, blue: 0.69, alpha: 1).cgColor
		button2.layer.borderWidth = 1
		button2.backgroundColor = .white
		button2.layer.cornerRadius = button2.getHeight() / 2
		banner.addSubview(button2)

		var button2Text = UILabel()
		button2Text.setX(0)
		button2Text.setY(0)
		button2Text.setSize(button2.getWidth(), button2.getHeight())
		button2Text.font = UIFont.systemFont(ofSize: 17)
		button2Text.textAlignment = .center
		button2Text.textColor = UIColor(red: 0.21, green: 0.42, blue: 0.69, alpha: 1)
		button2.addSubview(button2Text)

		var button2Action = UIButton()
		button2Action.tag = 2
		button2Action.setX(0)
		button2Action.setY(0)
		button2Action.setSize(button2.getWidth(), button2.getHeight())
		button2Action.addTarget(self, action: "buySubscribe:", for: .touchUpInside)
		button2.addSubview(button2Action)

		var button3 = UIView()
		button3.setX(15)
		button3.setSize(banner.getWidth() - 30, 40)
		button3.setY(buttonOffset, relative: button2)
//		button3.layer.borderColor = UIColor(red: 0.21, green: 0.42, blue: 0.69, alpha: 1).cgColor
//		button3.layer.borderWidth = 1
		button3.backgroundColor = UIColor(red: 0.21, green: 0.42, blue: 0.69, alpha: 1)
		button3.layer.cornerRadius = button3.getHeight() / 2
		banner.addSubview(button3)

		var button3Text = UILabel()
		button3Text.setX(0)
		button3Text.setY(0)
		button3Text.setSize(button3.getWidth(), button3.getHeight())
		button3Text.font = UIFont.systemFont(ofSize: 17)
		button3Text.textAlignment = .center
		button3Text.textColor = UIColor.white
		button3Text.text = s("premium_trial")
		button3.addSubview(button3Text)

		var button3Action = UIButton()
		button3Action.tag = 3
		button3Action.setX(0)
		button3Action.setY(0)
		button3Action.setSize(button3.getWidth(), button3.getHeight())
		button3Action.addTarget(self, action: "buySubscribe:", for: .touchUpInside)
		button3.addSubview(button3Action)

		var textAfter = UILabel()
		textAfter.text = ""
		textAfter.setX(0)
		textAfter.setY(5, relative: button3)
		textAfter.setSize(banner.getWidth(), 16)
		textAfter.font = UIFont.systemFont(ofSize: 12)
		textAfter.textColor = UIColor(red: 0.13, green: 0.18, blue: 0.23, alpha: 1)
		textAfter.textAlignment = .center
		banner.addSubview(textAfter)

		var finalOffset = textAfter.getHeight() + textAfter.getY() + 20

		banner.setSize(banner.getWidth(), finalOffset)

		for item in Core.shared().getRealm.objects(SubscriptionModel.self).sorted(byKeyPath: "PriceLocale", ascending: true)
		{
			data.append(item)
		}

		if(data.count > 3)
		{

			var priceFormatted1 = NSString(format: "%.2f", data[1].PriceLocale).replacingOccurrences(of: ".00", with: "")

			var priceFormatted2 = NSString(format: "%.2f", data[2].PriceLocale).replacingOccurrences(of: ".00", with: "")

			var priceFormatted3 = NSString(format: "%.2f", data[0].PriceLocale).replacingOccurrences(of: ".00", with: "")

			button2Text.text = "\(data[1].Title) \(priceFormatted1) \(data[1].Currency)"

			button1Text.text = "\(data[2].Title) \(priceFormatted2) \(data[2].Currency)"

			textAfter.text = String(format: s("premium_after"), priceFormatted3, data[0].Currency)
		}
		else
		{
			button2.isHidden = true
			button1.isHidden = true
			textAfter.text = ""
		}

		//----

		var privacyText = s("privacy_title")
		var termText = s("tems_title")

		var privacyTextSize = privacyText.getSize(size: 10)
		var termTextSize = termText.getSize(size: 10)

		var privacyTextLabel = UILabel()
		privacyTextLabel.font = UIFont.systemFont(ofSize: 10)
		privacyTextLabel.text = privacyText
		privacyTextLabel.textColor = UIColor.white.withAlphaComponent(0.5)
		privacyTextLabel.setX(20)
		privacyTextLabel.setSize(privacyTextSize.width, privacyTextSize.height + 8)

		if(DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XR || DeviceType.IS_IPHONE_XMAX)
		{
			privacyTextLabel.setY(self.getHeight() - privacyTextLabel.getHeight() - 30)
		}
		else
		{
			privacyTextLabel.setY(self.getHeight() - privacyTextLabel.getHeight() - 10)
		}

		self.addSubview(privacyTextLabel)

		var privacyTextButton = UIButton()
		privacyTextButton.frame = privacyTextLabel.frame
		privacyTextButton.addTarget(self, action: "privacyAction:", for: .touchUpInside)
		self.addSubview(privacyTextButton)

		//----

		var termTextLabel = UILabel()
		termTextLabel.font = UIFont.systemFont(ofSize: 10)
		termTextLabel.text = termText
		termTextLabel.textColor = UIColor.white.withAlphaComponent(0.5)

		termTextLabel.setSize(termTextSize.width, termTextSize.height + 8)
		termTextLabel.setX(self.getWidth() - 20 - termTextLabel.getWidth())

		if(DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XR || DeviceType.IS_IPHONE_XMAX)
		{
			termTextLabel.setY(self.getHeight() - termTextLabel.getHeight() - 30)
		}
		else
		{
			termTextLabel.setY(self.getHeight() - termTextLabel.getHeight() - 10)
		}

		self.addSubview(termTextLabel)

		var termTextButton = UIButton()
		termTextButton.frame = termTextLabel.frame
		termTextButton.addTarget(self, action: "termAction:", for: .touchUpInside)
		self.addSubview(termTextButton)

		//---

		var textView = UITextView()
		textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		textView.isScrollEnabled = true
		textView.backgroundColor = UIColor.clear
		textView.text = s("premium_privacy")
		textView.isEditable = false
//		textView.isUserInteractionEnabled = false
		textView.font = UIFont.systemFont(ofSize: 10)
		textView.textColor = UIColor.white.withAlphaComponent(0.5)
		textView.setSize(self.getWidth() - 40, ScreenSize.SCREEN_HEIGHT - banner.getY() - banner.getHeight() - 10 - termTextButton.getHeight() - 10 - 10)
		textView.setX(20)
		textView.setY(5, relative: banner)
		self.addSubview(textView)
	}

	@objc func privacyAction(_ sender: AnyObject)
	{
		openURL(url: "http://scaner-app.com/policy")
	}

	@objc func termAction(_ sender: AnyObject)
	{
		openURL(url: "http://scaner-app.com/terms")
	}

	func openURL(url: String)
	{
		var newURL = URL(string: url)

		UIApplication.shared.open(newURL!, options: [:], completionHandler: nil)
	}

	var data = [SubscriptionModel]()

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	@objc func closeAction(_ sender: AnyObject)
	{
		self.removeFromSuperview()
	}

	@objc func buySubscribe(_ sender: AnyObject)
	{
		var id = sender.tag

		if(data.count < 3)
		{
			return
		}

		var m: SubscriptionModel!

		switch(id)
		{
		case 1:
			m = data[2]
			break
		case 2:
			m = data[1]
			break
		case 3:
			m = data[0]
			break
		default:
			m = data[0]
			break
		}

		//EVENT_PREMIUM_HIDE

		EventsManager.shared().sendNotify(Constants.EVENT_PREMIUM_HIDE)

		EventsManager.shared().sendNotify(Constants.EVENT_DO_PURCHASE, data: ["product_id": m.PurchaseId])
	}

	@objc func restoreAction(_ sender: AnyObject)
	{
		EventsManager.shared().sendNotify(Constants.EVENT_PREMIUM_HIDE)

		EventsManager.shared().sendNotify(Constants.EVENT_DO_RESTORE_PURCHASE)
	}
}
