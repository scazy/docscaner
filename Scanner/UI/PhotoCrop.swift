//
//  PhotoCrop.swift
//  Scanner
//
//  Created by Artem  on 20/11/2018.
//  Copyright © 2018 VladimirKuzmin. All rights reserved.
//

import Foundation
import UIKit

class PhotoCrop: UIView, CropPolygonDelegate
{
	var files = [DocModelFile]()

	var startOffset: CGFloat = 0

	var top: UIView!

	var crop: CropPolygon!

	var imageView: UIImageView!

	var image: UIImage!

	var factor: CGFloat!

	var factor2: CGFloat!

	var file: DocModelFile!

	var layout: UIView!

	var _id = ""

	init(id: String)
	{
		super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))

		self.backgroundColor = UIColor.init(red: 0.13, green: 0.18, blue: 0.23, alpha: 1)

		startOffset = UIApplication.shared.statusBarFrame.size.height

		self._id = id

		if let doc = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(id)'").first
		{
			self.initTopControls()

			for i in Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(doc.Id)'")
			{
				files.append(i)
			}

			file = files[0]

			reinitImage()
		}
	}

	func reinitImage()
	{
		if(layout != nil)
		{
			layout.removeFromSuperview()
		}

		layout = UIView()
		layout.setX(20)
		layout.setY(20, relative: top)
		layout.setSize(ScreenSize.SCREEN_WIDTH - 40, ScreenSize.SCREEN_HEIGHT - self.top.getHeight() - 40)
		self.addSubview(layout)

		let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)

		let path = docDirURL.appendingPathComponent(file.FileName)

		let data = try? Data(contentsOf: path)

		//----

		image = UIImage(data: data!)!

		let imageWidthOrigin = image.size.width
		let imageHeightOrigin = image.size.height

		print("** IMAGE \(imageWidthOrigin) \(imageHeightOrigin)")

		let maxImageWidth: CGFloat = layout.getWidth()

		let maxImageHeight: CGFloat = layout.getHeight()

		var imageWidth: CGFloat = 0
		var imageHeight: CGFloat = 0

		factor = 0

		imageHeight = maxImageHeight

		factor = CGFloat(imageHeight) / CGFloat(imageHeightOrigin)

		print("** FACTOR \(CGFloat(imageHeightOrigin) / CGFloat(imageHeight) )")

		imageWidth = CGFloat(imageWidthOrigin) * factor

		if(imageWidth > maxImageWidth)
		{
			imageWidth = maxImageWidth

			factor = CGFloat(imageWidth) / CGFloat(imageWidthOrigin)

			imageHeight = CGFloat(imageHeightOrigin) * factor
		}

		print("** ORIGINAL SIZE \(imageWidthOrigin) \(imageHeightOrigin)")
		print("** NEW SIZE \(imageWidth) \(imageHeight)")

		factor2 = imageWidthOrigin / imageWidth

		imageView = UIImageView()
		imageView.isUserInteractionEnabled = true
		imageView.setSize(imageWidth, imageHeight)
		imageView.toCenterX(layout)
		imageView.toCenterY(layout)
		imageView.image = image

		layout.addSubview(imageView)

		crop = CropPolygon(width: imageView.getWidth(), height: imageView.getHeight(), points: [
			CGPoint(x: 10, y: 10),
			CGPoint(x: imageView.getWidth() - 10, y: 10),
			CGPoint(x: imageView.getWidth() - 10, y: imageView.getHeight() - 10),
			CGPoint(x: 10, y: imageView.getHeight() - 10)
			])
		crop.delegate = self
		imageView.addSubview(crop)
	}

	func CropPolygonChangeSize(points: [CGPoint]) {

	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func initTopControls()
	{
		top = UIView()
		top.backgroundColor = UIColor.init(red: 0.21, green: 0.42, blue: 0.69, alpha: 1)
		top.setX(0)
		top.setY(0)
		top.setSize(ScreenSize.SCREEN_WIDTH, 60 + startOffset)
		top.clipsToBounds = true
		self.addSubview(top)

		///------

		let iconSize: CGFloat = 50 / 2.5

		var shareLayout = UIView()
		shareLayout.setX(ScreenSize.SCREEN_WIDTH - 50)
		shareLayout.setY(startOffset + 20)
		shareLayout.setSize(50, 30)
		top.addSubview(shareLayout)

		var shareIcon = UIImageView()
		shareIcon.setSize(iconSize, iconSize)
		shareIcon.toCenterX(shareLayout)
		shareIcon.toCenterY(shareLayout)
		shareIcon.image = UIImage(named: "save")
		shareLayout.addSubview(shareIcon)

		let shareAction = UIButton()
		shareAction.addTarget(self, action: #selector(self.actionSave(_:)), for: .touchUpInside)
		shareAction.setX(0)
		shareAction.setY(0)
		shareAction.setSize(shareLayout.getWidth(), shareLayout.getHeight())
		shareLayout.addSubview(shareAction)

		//----

		//----

		let libraryLayout = UIView()
		libraryLayout.setX(0)
		libraryLayout.setY(startOffset + 20)
		libraryLayout.setSize(50, 30)
		top.addSubview(libraryLayout)

		let libraryIcon = UIImageView()
		libraryIcon.setSize(12, iconSize)
		libraryIcon.toCenterX(libraryLayout)
		libraryIcon.toCenterY(libraryLayout)
		libraryIcon.image = UIImage(named: "left_menu_back")
		libraryLayout.addSubview(libraryIcon)

		let libraryAction = UIButton()
		libraryAction.addTarget(self, action: #selector(self.backToLibrary(_:)), for: .touchUpInside)
		libraryAction.setX(0)
		libraryAction.setY(0)
		libraryAction.setSize(libraryLayout.getWidth(), libraryLayout.getHeight())
		libraryLayout.addSubview(libraryAction)

	}

	@objc func backToLibrary(_ sender: AnyObject)
	{
		self.removeFromSuperview()
	}

	var isSave = false

	@objc func actionSave(_ sender: AnyObject)
	{
		if(isSave)
		{
			return
		}

		isSave = true

		EventsManager.shared().sendNotify(Constants.EVENT_SHOW_PROCESSING)

		//----

		var points = crop.getPoints()

		var originalImage = imageView.image!

		var nImage2 = originalImage.cropByPoints(points: points, imageCropFactor: factor2)

		imageView.image = nImage2

		print("** NEW \(imageView.image?.size)")

		//----

		let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)

		let fileURL = docDirURL.appendingPathComponent(file.FileName)

		do
		{
			try nImage2.jpegData(compressionQuality: 1)!.write(to: fileURL, options: [.atomic])
		}
		catch(let error)
		{
			print(error)
		}
//
//		waitingView.removeFromSuperview()

//		reinitImage()

		EventsManager.shared().sendNotify(Constants.EVENT_HIDE_PROCESSING)

		EventsManager.shared().sendNotify(Constants.EVENT_OPEN_DOC, data: ["doc_id": self._id])

		self.removeFromSuperview()
	}
}

extension UIImage {
	func fixed() -> UIImage {

		print("** FIXED OR \(self.imageOrientation.rawValue)")
		let docImage = CIImage(image: self)!

		let ciContext = CIContext(options: nil)

		print("*** ** FIXED extent \(docImage.extent)")

		let cgImg = ciContext.createCGImage(docImage, from: docImage.extent)
		let image = UIImage(cgImage: cgImg!, scale: scale, orientation: .left)

		return image
	}
}
