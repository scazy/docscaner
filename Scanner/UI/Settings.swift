//
//  Settings.swift
//  Scanner
//
//  Created by Artem  on 12/11/2018.
//  Copyright © 2018 VladimirKuzmin. All rights reserved.
//

import Foundation

import Foundation
import UIKit

class Settings: UIView
{
	var top: UIView!

	var tableView: UITableView!

	var startOffset: CGFloat = 0

	var _data = [SubscriptionModel]()

	init()
	{
		startOffset = UIApplication.shared.statusBarFrame.size.height

		super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))

		self.backgroundColor = UIColor.init(red: 0.13, green: 0.18, blue: 0.23, alpha: 1)

		initTopControls()

		//----

		var isBW = Core.shared().defaults.bool(forKey: Constants.SETTINGS_IS_BW)

		var bwSwitcher = UISwitch()
		bwSwitcher.isOn = isBW
		bwSwitcher.addTarget(self, action: "bwAction:", for: .valueChanged)
		bwSwitcher.setX(30)
		bwSwitcher.setY(20, relative: top)
		self.addSubview(bwSwitcher)

		var bwTitle = UILabel()
		bwTitle.text = s("settings_bw")
		bwTitle.textColor = UIColor.white
		bwTitle.font = UIFont.systemFont(ofSize: 18)
		bwTitle.setX(20, relative: bwSwitcher)
		bwTitle.setY(20, relative: top)
		bwTitle.setSize(ScreenSize.SCREEN_WIDTH / 2, 30)
		self.addSubview(bwTitle)
	}

	@objc func bwAction(_ sender: UISwitch)
	{
		print("\(sender.isOn)")

		Core.shared().defaults.set(sender.isOn, forKey: Constants.SETTINGS_IS_BW)
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func initTopControls()
	{
		top = UIView()
		top.backgroundColor = UIColor.init(red: 0.21, green: 0.42, blue: 0.69, alpha: 1)
		top.setX(0)
		top.setY(0)
		top.setSize(ScreenSize.SCREEN_WIDTH, 60 + startOffset)
		top.clipsToBounds = true
		self.addSubview(top)

		///------

		let iconSize: CGFloat = 50 / 2.5


		let libraryLayout = UIView()
		libraryLayout.setX(0)
		libraryLayout.setY(startOffset + 20)
		libraryLayout.setSize(50, 30)
		top.addSubview(libraryLayout)

		let libraryIcon = UIImageView()
		libraryIcon.setSize(12, iconSize)
		libraryIcon.toCenterX(libraryLayout)
		libraryIcon.toCenterY(libraryLayout)
		libraryIcon.image = UIImage(named: "left_menu_back")
		libraryLayout.addSubview(libraryIcon)

		let libraryAction = UIButton()
		libraryAction.addTarget(self, action: #selector(self.close(_:)), for: .touchUpInside)
		libraryAction.setX(0)
		libraryAction.setY(0)
		libraryAction.setSize(libraryLayout.getWidth(), libraryLayout.getHeight())
		libraryLayout.addSubview(libraryAction)

		//------

		var libTitle = UILabel()
		//		libTitle.backgroundColor = .yellow
		libTitle.setX(0, relative: libraryLayout)
		libTitle.setY(startOffset + 20)
		libTitle.setSize(ScreenSize.SCREEN_WIDTH - 100, 30)
		libTitle.textColor = UIColor.white
		libTitle.textAlignment = .center
		libTitle.text = "Settings"
		libTitle.font = UIFont.systemFont(ofSize: 16)
		top.addSubview(libTitle)
	}

	@objc func close(_ sender: AnyObject)
	{
		self.removeFromSuperview()
	}
}
