//
//  Subscription.swift
//  Scanner
//
//  Created by Artem  on 22/10/2018.
//  Copyright © 2018 VladimirKuzmin. All rights reserved.
//

import Foundation
import UIKit

class Subscription: UIView
{
	var top: UIView!

	var tableView: UITableView!

	var startOffset: CGFloat = 0

	var _data = [SubscriptionModel]()

	init()
	{
		startOffset = UIApplication.shared.statusBarFrame.size.height

		super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))

		self.backgroundColor = UIColor.init(red: 0.13, green: 0.18, blue: 0.23, alpha: 1)

		initTopControls()

		//----

		tableView = UITableView()
		tableView.backgroundColor = UIColor.clear
		tableView.setX(0)
		tableView.setY(0, relative: top)
		tableView.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT - self.top.getHeight())
		self.addSubview(tableView)

		self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
		self.tableView.separatorColor = UIColor.clear
		self.tableView.delegate = self
		self.tableView.dataSource = self

		self.tableView.register(CellPackage.self, forCellReuseIdentifier: "cellPackage")

		for item in Core.shared().getRealm.objects(SubscriptionModel.self).sorted(byKeyPath: "PriceLocale", ascending: true)
		{
			print("** ADD PRICE")
			self._data.append(item)
		}

		self.tableView.reloadData()
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func initTopControls()
	{
		top = UIView()
		top.backgroundColor = UIColor.init(red: 0.21, green: 0.42, blue: 0.69, alpha: 1)
		top.setX(0)
		top.setY(0)
		top.setSize(ScreenSize.SCREEN_WIDTH, 60 + startOffset)
		top.clipsToBounds = true
		self.addSubview(top)

		///------

		let iconSize: CGFloat = 50 / 2.5


		let libraryLayout = UIView()
		libraryLayout.setX(0)
		libraryLayout.setY(startOffset + 20)
		libraryLayout.setSize(50, 30)
		top.addSubview(libraryLayout)

		let libraryIcon = UIImageView()
		libraryIcon.setSize(12, iconSize)
		libraryIcon.toCenterX(libraryLayout)
		libraryIcon.toCenterY(libraryLayout)
		libraryIcon.image = UIImage(named: "left_menu_back")
		libraryLayout.addSubview(libraryIcon)

		let libraryAction = UIButton()
		libraryAction.addTarget(self, action: #selector(self.close(_:)), for: .touchUpInside)
		libraryAction.setX(0)
		libraryAction.setY(0)
		libraryAction.setSize(libraryLayout.getWidth(), libraryLayout.getHeight())
		libraryLayout.addSubview(libraryAction)

		//------

		var libTitle = UILabel()
		//		libTitle.backgroundColor = .yellow
		libTitle.setX(0, relative: libraryLayout)
		libTitle.setY(startOffset + 20)
		libTitle.setSize(ScreenSize.SCREEN_WIDTH - 100, 30)
		libTitle.textColor = UIColor.white
		libTitle.textAlignment = .center
		libTitle.text = s("menu_subscribe")
		libTitle.font = UIFont.systemFont(ofSize: 16)
		top.addSubview(libTitle)
	}

	@objc func close(_ sender: AnyObject)
	{
		self.removeFromSuperview()
	}
}

extension Subscription: UITableViewDataSource, UITableViewDelegate
{
	func numberOfSections(in tableView: UITableView) -> Int
	{
		return 1
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		return self._data.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		var package = self._data[indexPath.row]

		let cell = tableView.dequeueReusableCell(withIdentifier: "cellPackage", for: indexPath) as! CellPackage
		cell.delegate = self
		(cell as CellBasePackageDelegate).Update(package: package)
		return cell
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
	{
		return 100
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	{
		print("*** didSelectRowAt")

		var package = self._data[indexPath.row]

		EventsManager.shared().sendNotify(Constants.EVENT_PREMIUM_HIDE)

		EventsManager.shared().sendNotify(Constants.EVENT_DO_PURCHASE, data: ["product_id": package.PurchaseId])
	}
}

extension Subscription: CellActionDelegate
{

}

protocol CellActionDelegate
{

}


protocol CellBasePackageDelegate
{
	func Update(package: SubscriptionModel)
}

class CellBasePackage: UITableViewCell, CellBasePackageDelegate
{
	var delegate: CellActionDelegate!

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?)
	{
		super.init(style: style, reuseIdentifier: reuseIdentifier)

		self.backgroundColor = UIColor.clear

		self.selectionStyle = UITableViewCell.SelectionStyle.none
	}

	required init?(coder aDecoder: NSCoder)
	{
		fatalError("init(coder:) has not been implemented")
	}

	override func layoutSubviews()
	{
		super.layoutSubviews()
	}

	override func prepareForReuse()
	{
		super.prepareForReuse()
	}

	func Update(package: SubscriptionModel)
	{

	}
}

class CellPackage : CellBasePackage
{
	var title = UILabel()

	var descriptionLabel = UITextView()

	var price = UILabel()

	var layout = UIView()

	var button = UIView()

	var buttonLabel = UILabel()

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?)
	{
		super.init(style: style, reuseIdentifier: reuseIdentifier)
	}

	required init(coder aDecoder: NSCoder)
	{
		fatalError("init(coder:) has not been implemented")
	}

	override func Update(package: SubscriptionModel)
	{
		layout.setX(10)
		layout.setY(15)
		layout.setSize(self.contentView.getWidth() - 20, self.contentView.getHeight() - 30)

		title.text = package.Title
//		title.textColor = .white
		title.setX(10)
		title.setY(10)
		title.setSize(layout.getWidth(), 20)

		descriptionLabel.text = package.Description
//		descriptionLabel.textColor = UIColor.white.withAlphaComponent(0.5)
		descriptionLabel.setX(10)
		descriptionLabel.setY(5, relative: title)

		var textSize = getLabelSize(str: package.Description, size: 13, isBold:  false, setWidth: layout.getWidth() * 0.65)

		descriptionLabel.setSize(textSize.width, textSize.height)

		var priceFormatted = NSString(format: "%.2f", package.PriceLocale).replacingOccurrences(of: ".00", with: "")

		price.text = "\(priceFormatted) \(package.Currency)"
//		price.textColor = .white
		price.setSize(100, 20)
		price.setY(10)
		price.setX(layout.getWidth() - 10 - 100)

//		button.image = UIImage(named: "button_subscribe")
//		button.text = "Subscribe"
//		button.textAlignment = .center

		var buttonText = s("subs_title")
		var buttonSize = buttonText.getSize(size: 17)

		button.setSize(buttonSize.width + 35, 35)
		button.setY(10, relative: price)
		button.setX(layout.getWidth()  - button.getWidth())
		button.backgroundColor = UIColor(red: 0.21, green: 0.42, blue: 0.69, alpha: 1)
		button.layer.cornerRadius = 35 / 2

		buttonLabel.setX(0)
		buttonLabel.setY(0)
		buttonLabel.setSize(button.getWidth(), button.getHeight())
		buttonLabel.textAlignment = .center
		buttonLabel.text = buttonText
		buttonLabel.textColor = UIColor.white
		buttonLabel.font = UIFont.systemFont(ofSize: 17)
	}

	override func layoutSubviews()
	{
		super.layoutSubviews()

		for subview in contentView.subviews
		{
			subview.removeFromSuperview()
		}

		self.contentView.addSubview(layout)

		self.contentView.backgroundColor = UIColor.clear

		layout.backgroundColor = UIColor.clear


		layout.addSubview(price)
		layout.addSubview(title)
		layout.addSubview(descriptionLabel)
		layout.addSubview(button)
		button.addSubview(buttonLabel)

		title.font = UIFont(name: "IBMPlexSans-Medium", size: 17)

//		descriptionLabel.textColor = UIColor.lightGray

		title.textColor = .white

		descriptionLabel.backgroundColor = UIColor.clear
		descriptionLabel.textContainerInset = UIEdgeInsets(top: 0, left: 0,bottom: 0, right: 0);
		descriptionLabel.isEditable = false
		descriptionLabel.isUserInteractionEnabled = false
		descriptionLabel.isScrollEnabled = false
		descriptionLabel.textContainer.lineFragmentPadding = 0
		descriptionLabel.font = UIFont(name: "IBMPlexSans-Medium", size: 13)
		descriptionLabel.textColor = UIColor.white.withAlphaComponent(0.5)
		descriptionLabel.sizeToFit()

		price.font = UIFont.systemFont(ofSize: 14)
		price.textColor = .white
		price.textAlignment = .right
//		price.textColor = UIColor(red: 0.31, green: 0.31, blue: 0.31, alpha: 1)
		price.font = UIFont(name: "IBMPlexSans-Medium", size: 17)
	}

	override func prepareForReuse()
	{
		super.prepareForReuse()

		price.text = nil

		title.text = nil

		descriptionLabel.text = nil

//		button.image = nil
	}
}

