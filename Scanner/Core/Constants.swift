//
//  Constants.swift
//  Scanner
//
//  Created by Artem  on 24.09.2018.
//  Copyright © 2018 VladimirKuzmin. All rights reserved.
//

import Foundation

struct Constants
{
//	static let ITUNES_API = "https://sandbox.itunes.apple.com/verifyReceipt"

	static let IS_DEV = true

	static let ITUNES_API = "https://buy.itunes.apple.com/verifyReceipt"

	static let ITUNES_SECRET = "062643deee104209b67d53cfdba97e01"

	static let SERVICE_API = "https://scannerimage.space/api/v1/"

	static let EVENT_OPEN_VIEW = "EVENT_OPEN_VIEW"

	static let EVENT_CHANGE_CONTROLLER = "EVENT_CHANGE_CONTROLLER"

	static let EVENT_REMOVE_CONTROLLER = "EVENT_REMOVE_CONTROLLER"

	static let EVENT_VIEW_LOADED = "EVENT_VIEW_LOADED"

	static let EVENT_APP_FOREGROUND = "EVENT_APP_FOREGROUND"

	static let EVENT_APP_BACKGROUND = "EVENT_APP_BACKGROUND"

	static let EVENT_DO_PURCHASE = "EVENT_DO_PURCHASE"

	static let EVENT_OPEN_DOC = "EVENT_OPEN_DOC"

	static let EVENT_RELOAD_LIB = "EVENT_RELOAD_LIB"

	static let NOTIFY_MESSAGE_PREFIX = "event."

	static let EVENT_DO_SHARE = "EVENT_DO_SHARE"

	static let EVENT_SHOW_LEFT_MENU = "EVENT_SHOW_LEFT_MENU"

	static let EVENT_SHOW_SUBSRIBTIONS = "EVENT_SHOW_SUBSRIBTIONS"

	static let EVENT_SHOW_SETTINGS = "EVENT_SHOW_SETTINGS"

	static let SETTINGS_IS_BW = "SETTINGS_IS_BW"

	static let EVENT_HIDE_PROCESSING = "EVENT_HIDE_PROCESSING"

	static let EVENT_SHOW_PROCESSING = "EVENT_SHOW_PROCESSING"

	static let EVENT_ADD_IMAGE_TO_DOC = "EVENT_ADD_IMAGE_TO_DOC"

	static let EVENT_ADDED_IMAGE  = "EVENT_ADDED_IMAGE"

	static let EVENT_PREMIUM = "EVENT_PREMIUM"

	static let EVENT_PREMIUM_HIDE = "EVENT_PREMIUM_HIDE"

	static let EVENT_DO_RESTORE_PURCHASE = "EVENT_DO_RESTORE_PURCHASE"
}
