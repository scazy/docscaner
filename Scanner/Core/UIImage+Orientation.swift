//
//  UIImage+Orientation.swift
//  Scanner
//
//  Created by Artem  on 21/11/2018.
//  Copyright © 2018 VladimirKuzmin. All rights reserved.
//

import Foundation
import UIKit

extension UIImage
{
	func toNoir(_ isRotate: Bool = false, degrees: CGFloat = -90, flip: Bool = true) -> UIImage
	{
		var isBW = Core.shared().defaults.bool(forKey: Constants.SETTINGS_IS_BW)

		if !isBW
		{
			let context = CIContext(options: nil)
			let currentFilter = CIFilter(name: "CIPhotoEffectNoir")
			currentFilter!.setValue(CIImage(image: self), forKey: kCIInputImageKey)
			let output = currentFilter!.outputImage
			let cgimg = context.createCGImage(output!, from: output!.extent)
			let processedImage = UIImage(cgImage: cgimg!)

			let inputImage = CIImage(image: processedImage)!

			let parameters = [
				"inputContrast": NSNumber(value: 1)
			]

			let outputImage = inputImage.applyingFilter("CIColorControls", parameters: parameters)

			let img = context.createCGImage(outputImage, from: outputImage.extent)!

			var image = UIImage(cgImage: img)

			if(isRotate)
			{
				return image.imageRotatedByDegrees(degrees: degrees, flip: flip)
			}

			return image
		}
		else
		{
			return self
		}
	}
}

extension UIImage
{
	func cropByPoints(points: [CGPoint], imageCropFactor: CGFloat) -> UIImage
	{
		let openGLContext = EAGLContext(api: .openGLES2)
		let ciContext =  CIContext(eaglContext: openGLContext!)

		var originalImage = self

		let docImage = CIImage(image: originalImage)!

		var factor = docImage.extent.width / (originalImage.size.width)

		print("** FACTOR \(factor)")

		var outputImage = docImage.applyingFilter("CIPerspectiveCorrection", parameters: [
			"inputTopLeft": CIVector(cgPoint: CGPoint(x: points[0].x * imageCropFactor * factor, y: (originalImage.size.height - (points[0].y * imageCropFactor)) * factor)),
			"inputTopRight": CIVector(cgPoint: CGPoint(x: points[1].x * imageCropFactor * factor, y: (originalImage.size.height - (points[1].y * imageCropFactor)) * factor)),
			"inputBottomRight": CIVector(cgPoint: CGPoint(x: points[2].x * imageCropFactor * factor, y: (originalImage.size.height - (points[2].y * imageCropFactor)) * factor)),
			"inputBottomLeft": CIVector(cgPoint: CGPoint(x: points[3].x * imageCropFactor * factor, y: (originalImage.size.height - (points[3].y * imageCropFactor)) * factor))
			])

		let cgImage = ciContext.createCGImage(outputImage, from: outputImage.extent)

		return UIImage(cgImage: cgImage!, scale: 1, orientation: .up)
	}
}

extension UIImage {
	func fixedOrientation() -> UIImage {

		if imageOrientation == UIImage.Orientation.up {
			return self
		}

		var transform: CGAffineTransform = CGAffineTransform.identity

		switch imageOrientation {
		case UIImage.Orientation.down, UIImage.Orientation.downMirrored:
//			transform = transform.translatedBy(x: size.width, y: size.height)
			transform = transform.rotated(by: CGFloat(Double.pi))
			break
		case UIImage.Orientation.left, UIImage.Orientation.leftMirrored:
//			transform = transform.translatedBy(x: size.width, y: 0)
			transform = transform.rotated(by: CGFloat(Double.pi / 2))
			break
		case UIImage.Orientation.right, UIImage.Orientation.rightMirrored:
//			transform = transform.translatedBy(x: 0, y: size.height)
			transform = transform.rotated(by: CGFloat(-(Double.pi / 2)))
			break
		case UIImage.Orientation.up, UIImage.Orientation.upMirrored:
			break
		}

//		switch imageOrientation {
//		case UIImage.Orientation.upMirrored, UIImage.Orientation.downMirrored:
//			transform.translatedBy(x: size.width, y: 0)
//			transform.scaledBy(x: -1, y: 1)
//			break
//		case UIImage.Orientation.leftMirrored, UIImage.Orientation.rightMirrored:
//			transform.translatedBy(x: size.height, y: 0)
//			transform.scaledBy(x: -1, y: 1)
//		case UIImage.Orientation.up, UIImage.Orientation.down, UIImage.Orientation.left, UIImage.Orientation.right:
//			break
//		}

		let ctx: CGContext = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: self.cgImage!.bitsPerComponent, bytesPerRow: 0, space: self.cgImage!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!

		ctx.concatenate(transform)

		switch imageOrientation {
		case UIImage.Orientation.left, UIImage.Orientation.leftMirrored, UIImage.Orientation.right, UIImage.Orientation.rightMirrored:
			ctx.draw(self.cgImage!, in: CGRect(origin: CGPoint.zero, size: size))
		default:
			ctx.draw(self.cgImage!, in: CGRect(origin: CGPoint.zero, size: size))
			break
		}

		let cgImage: CGImage = ctx.makeImage()!

		return UIImage(cgImage: cgImage)
	}
}
