//
//  Utils.swift
//  Scanner
//
//  Created by Artem  on 24.09.2018.
//  Copyright © 2018 VladimirKuzmin. All rights reserved.
//


import Foundation
import UIKit
import RealmSwift

extension String
{
	func md5() -> String!
	{
		let str = self.cString(using: String.Encoding.utf8)
		let strLen = CUnsignedInt(self.lengthOfBytes(using: String.Encoding.utf8))
		let digestLen = Int(CC_MD5_DIGEST_LENGTH)
		let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)

		CC_MD5(str!, strLen, result)

		let hash = NSMutableString()
		for i in 0..<digestLen {
			hash.appendFormat("%02x", result[i])
		}

		result.deinitialize()

		return String(format: hash as String)
	}
}

extension UIImage {
	public func imageRotatedByDegrees(degrees: CGFloat, flip: Bool) -> UIImage {
		let radiansToDegrees: (CGFloat) -> CGFloat = {
			return $0 * (180.0 / CGFloat(M_PI))
		}
		let degreesToRadians: (CGFloat) -> CGFloat = {
			return $0 / 180.0 * CGFloat(M_PI)
		}

		// calculate the size of the rotated view's containing box for our drawing space
		let rotatedViewBox = UIView(frame: CGRect(origin: CGPoint.zero, size: size))
		let t = CGAffineTransform(rotationAngle: degreesToRadians(degrees));
		rotatedViewBox.transform = t
		let rotatedSize = rotatedViewBox.frame.size

		// Create the bitmap context
		UIGraphicsBeginImageContext(rotatedSize)
		let context = UIGraphicsGetCurrentContext()

		// Move the origin to the middle of the image so we will rotate and scale around the center.
		context!.translateBy(x: rotatedSize.width / 2.0, y: rotatedSize.height / 2.0);

		//   // Rotate the image context
		//		var context = UIGraphicsGetCurrentContext()

		context!.rotate(by: degreesToRadians(degrees));

		// Now, draw the rotated/scaled image into the context
		var yFlip: CGFloat

		if(flip){
			yFlip = CGFloat(-1.0)
		} else {
			yFlip = CGFloat(1.0)
		}

		context!.scaleBy(x: yFlip, y: -1.0)
		//		draw(in: CGRect(x: -origin.x, y: -origin.y, width: size.width, height: size.height))
		draw(in: CGRect(x: -size.width / 2, y: -size.height / 2, width: size.width, height: size.height))

		let newImage = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()

		return newImage!
	}
}

extension UIImage {
	struct RotationOptions: OptionSet {
		let rawValue: Int

		static let flipOnVerticalAxis = RotationOptions(rawValue: 1)
		static let flipOnHorizontalAxis = RotationOptions(rawValue: 2)
	}

	func rotated(by rotationAngle: Measurement<UnitAngle>, options: RotationOptions = []) -> UIImage? {
		guard let cgImage = self.cgImage else { return nil }

		let rotationInRadians = CGFloat(rotationAngle.converted(to: .radians).value)
		let transform = CGAffineTransform(rotationAngle: rotationInRadians)
		var rect = CGRect(origin: .zero, size: self.size).applying(transform)
		rect.origin = .zero

		let renderer = UIGraphicsImageRenderer(size: rect.size)
		return renderer.image { renderContext in
			renderContext.cgContext.translateBy(x: rect.midX, y: rect.midY)
			renderContext.cgContext.rotate(by: rotationInRadians)

			let x = options.contains(.flipOnVerticalAxis) ? -1.0 : 1.0
			let y = options.contains(.flipOnHorizontalAxis) ? 1.0 : -1.0
			renderContext.cgContext.scaleBy(x: CGFloat(x), y: CGFloat(y))

			let drawRect = CGRect(origin: CGPoint(x: -self.size.width/2, y: -self.size.height/2), size: self.size)
			renderContext.cgContext.draw(cgImage, in: drawRect)
		}
	}
}

func s(_ name: String) -> String
{
	return NSLocalizedString(name, comment: "")
}

extension Results
{
	func Limit <T:Object> (offset: Int, limit: Int ) -> Array<T>
	{
		//create variables
		var lim = 0 // how much to take
		var off = 0 // start from
		var l: Array<T> = Array<T>() // results list

		//check indexes
		if off <= offset && offset < self.count - 1
		{
			off = offset
		}

		if limit > self.count
		{
			lim = self.count
		}
		else
		{
			lim = limit
		}

		//do slicing
		for i in off..<lim
		{
			let dog = self[i] as! T
			l.append(dog)
		}

		//results
		return l
	}
}

func randomInt(min: Int, max:Int) -> Int {
	return min + Int(arc4random_uniform(UInt32(max - min + 1)))
}

func ls(_ name: String) -> String
{
	return NSLocalizedString(name, comment: "")
}

extension String {
	func index(from: Int) -> Index {
		return self.index(startIndex, offsetBy: from)
	}

	func substring(from: Int) -> String {
		let fromIndex = index(from: from)
		return substring(from: fromIndex)
	}

	func substring(to: Int) -> String {
		let toIndex = index(from: to)
		return substring(to: toIndex)
	}

	func substring(with r: Range<Int>) -> String {
		let startIndex = index(from: r.lowerBound)
		let endIndex = index(from: r.upperBound)
		return substring(with: startIndex..<endIndex)
	}

	subscript(_ range: CountableRange<Int>) -> String {
		let idx1 = index(startIndex, offsetBy: range.lowerBound)
		let idx2 = index(startIndex, offsetBy: range.upperBound)
		return String(self[idx1..<idx2])
	}
}

extension UIView
{
	func addDashedBorder(width: CGFloat, color: UIColor)
	{
		//        let color = UIColor.red.cgColor

		let shapeLayer:CAShapeLayer = CAShapeLayer()
		let frameSize = self.frame.size
		let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)

		shapeLayer.bounds = shapeRect
		shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
		shapeLayer.fillColor = UIColor.clear.cgColor
		shapeLayer.strokeColor = color.cgColor
		shapeLayer.lineWidth = width
		shapeLayer.lineJoin = CAShapeLayerLineJoin.round
		shapeLayer.lineDashPattern = [6,3]
		shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath

		self.layer.addSublayer(shapeLayer)
	}

	func flipX() {
		transform = CGAffineTransform(scaleX: -transform.a, y: transform.d)
	}

	func flipY() {
		transform = CGAffineTransform(scaleX: transform.a, y: -transform.d)
	}
}

func setAttributedTextBold(withString string: String, boldString: String, font: UIFont) -> NSAttributedString
{
	let attributedString = NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.font: font])
	let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: font.pointSize)]
	let range = (string as NSString).range(of: boldString)
	attributedString.addAttributes(boldFontAttribute, range: range)
	return attributedString
}

func getLabelSize(str: String, size: CGFloat, isBold: Bool = false, setWidth: CGFloat = 5000, setHeight: CGFloat = 100000) -> CGSize
{
	let bubble1 = UILabel(frame: CGRect(x: 0, y: 0, width: setWidth, height: setHeight))

	bubble1.numberOfLines = 0

	bubble1.lineBreakMode = NSLineBreakMode.byWordWrapping

	if(isBold)
	{
		bubble1.font = UIFont.boldSystemFont(ofSize: size)
	}
	else
	{
		bubble1.font = UIFont.systemFont(ofSize: size)
	}

	bubble1.text = str
	bubble1.sizeToFit()

	return bubble1.frame.size
}

func getImageSize(image: UIImage, width: CGFloat, height: CGFloat) -> [CGFloat]
{
	var nWidth: CGFloat = 0
	var factor: CGFloat = 0
	var nHeight: CGFloat = 0

	if(height == 0)
	{
		nWidth = width
		factor = nWidth / image.size.width
		nHeight = factor * image.size.height
	}
	else if(width == 0)
	{
		nHeight = height
		factor = nHeight / image.size.height
		nWidth = factor * image.size.width
	}

	return [nWidth, nHeight]
}

enum UIViewArrange: Int
{
	case Left = 1
	case Right
	case Top
	case Bottom
}

extension String
{
	func getSize(size: CGFloat, isBold: Bool = false, setWidth: CGFloat = 5000, setHeight: CGFloat = 100000) -> CGSize
	{
		return getLabelSize(str: self, size: size, isBold: isBold, setWidth: setWidth, setHeight: setHeight)
	}
}

extension UIView
{
	/* for example

	let button = UIButton()
	button.setX(10)
	button.setY(100)
	button.setSize(150, 30)
	button.backgroundColor = UIColor.red
	self.view.addSubview(button)

	let button2 = UIButton()
	button2.setX(10, relative: button)
	button2.setY(100)
	button2.setSize(150, 30)
	button2.backgroundColor = UIColor.blue
	self.view.addSubview(button2)

	let button3 = UIButton()
	button3.setSize(150, 30)
	button3.setX(10)
	button3.setY(10, relative: button2)
	button3.backgroundColor = UIColor.green
	self.view.addSubview(button3)

	let button4 = UIButton()
	button4.setSize(150, 30)
	button4.toCenterX(self.view)
	button4.setY(10, relative: button3)
	button4.backgroundColor = UIColor.green
	self.view.addSubview(button4)

	*/

	func setSize(_ width: CGFloat, _ height: CGFloat)
	{
		let x = self.frame.origin.x
		let y = self.frame.origin.y

		self.frame = CGRect(x: x, y: y, width: width, height: height)
	}

	func toCenterX(_ relative: UIView)
	{
		self.frame.origin.x = relative.frame.size.width / 2 - self.frame.size.width / 2
	}

	func toCenterY(_ relative: UIView)
	{
		self.frame.origin.y = relative.frame.size.height / 2 - self.frame.size.height / 2
	}

	func setX(_ x: CGFloat)
	{
		self.frame.origin.x = x
	}

	func setY(_ y: CGFloat)
	{
		self.frame.origin.y = y
	}

	func setX(_ x: CGFloat, relative: UIView, position: UIViewArrange = .Right)
	{
		if(position == .Right)
		{
			self.frame.origin.x = relative.getX() + relative.getWidth() + x
		}

		if(position == .Left)
		{
			self.frame.origin.x = relative.getX() - x - self.getWidth()
		}
	}

	func setY(_ y: CGFloat, relative: UIView, position: UIViewArrange = .Bottom)
	{
		if(position == .Bottom)
		{
			self.frame.origin.y = relative.getY() + relative.getHeight() + y
		}

		if(position == .Top)
		{
			self.frame.origin.y = relative.getY() - y - self.getHeight()
		}

	}

	func setX(_ x: CGFloat, disposeOf: UIViewArrange, parent: UIView)
	{
		switch(disposeOf)
		{
		case .Right:
			self.setX(parent.getWidth() - self.getWidth() - x)
			break;
		case .Bottom:
			self.setX(x)
			break;
		case .Left:
			self.setX(x)
			break;
		case .Top:
			self.setX(x)
			break;
		}
	}

	func setY(_ y: CGFloat, disposeOf: UIViewArrange, parent: UIView)
	{
		switch(disposeOf)
		{
		case .Right:
			self.setY(y)
			break;
		case .Bottom:
			self.setY(parent.getHeight() - self.getHeight() - y)
			break;
		case .Left:
			self.setY(y)
			break;
		case .Top:
			self.setY(y)
			break;
		}
	}

	func getWidth() -> CGFloat
	{
		return self.frame.size.width
	}

	func getHeight() -> CGFloat
	{
		return self.frame.size.height
	}

	func getX() -> CGFloat
	{
		return self.frame.origin.x
	}

	func getY() -> CGFloat
	{
		return self.frame.origin.y
	}

	func shake()
	{
		let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
		animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
		animation.duration = 0.6
		animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
		layer.add(animation, forKey: "shake")
	}
}

struct ScreenSize
{
	static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
	static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
	static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
	static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)

	static let SCREEN_HALF_WIDTH = ScreenSize.SCREEN_WIDTH / 2
	static let SCREEN_HALF_HEIGHT = ScreenSize.SCREEN_HEIGHT / 2
	static let SCREEN_THIRD_WIDTH = ScreenSize.SCREEN_WIDTH / 3
	static let SCREEN_THIRD_HEIGHT = ScreenSize.SCREEN_HEIGHT / 3
	static let SCREEN_QUARTER_WIDTH = ScreenSize.SCREEN_WIDTH / 4
	static let SCREEN_QUARTER_HEIGHT = ScreenSize.SCREEN_HEIGHT / 4

	var SCREEN_IS_PORTRAIT = (ScreenSize.SCREEN_HEIGHT / ScreenSize.SCREEN_WIDTH > 1) ? false : true
}

struct DeviceType
{
	static let IS_IPHONE_4_OR_LESS =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
	static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
	static let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
	static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
	static let IS_IPHONE_X = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
	static let IS_IPHONE_XMAX = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0
	static let IS_IPHONE_XR = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0
	static let IS_IPHONE =  UIDevice.current.userInterfaceIdiom == .phone
	static let IS_IPAD =  UIDevice.current.userInterfaceIdiom == .pad
}

extension Timer {
	/**
	Creates and schedules a one-time `NSTimer` instance.

	- Parameters:
	- delay: The delay before execution.
	- handler: A closure to execute after `delay`.

	- Returns: The newly-created `NSTimer` instance.
	*/
	class func schedule(delay: TimeInterval, handler: @escaping (CFRunLoopTimer?) -> Void) -> Timer
	{
		let fireDate = delay + CFAbsoluteTimeGetCurrent()
		let timer = CFRunLoopTimerCreateWithHandler(kCFAllocatorDefault, fireDate, 0, 0, 0, handler)
		CFRunLoopAddTimer(CFRunLoopGetCurrent(), timer, CFRunLoopMode.commonModes)
		return timer!
	}

	/**
	Creates and schedules a repeating `NSTimer` instance.

	- Parameters:
	- repeatInterval: The interval (in seconds) between each execution of
	`handler`. Note that individual calls may be delayed; subsequent calls
	to `handler` will be based on the time the timer was created.
	- handler: A closure to execute at each `repeatInterval`.

	- Returns: The newly-created `NSTimer` instance.
	*/
	class func schedule(repeatInterval interval: TimeInterval, handler: @escaping (CFRunLoopTimer?) -> Void) -> Timer
	{
		let fireDate = interval + CFAbsoluteTimeGetCurrent()
		let timer = CFRunLoopTimerCreateWithHandler(kCFAllocatorDefault, fireDate, interval, 0, 0, handler)
		CFRunLoopAddTimer(CFRunLoopGetCurrent(), timer, CFRunLoopMode.commonModes)
		return timer!
	}
}

public extension UIDevice
{

	var modelName: String {
		var systemInfo = utsname()
		uname(&systemInfo)
		let machineMirror = Mirror(reflecting: systemInfo.machine)
		let identifier = machineMirror.children.reduce("") { identifier, element in
			guard let value = element.value as? Int8, value != 0 else { return identifier }
			return identifier + String(UnicodeScalar(UInt8(value)))
		}

		switch identifier {
		case "iPod5,1":                                 return "iPod Touch 5"
		case "iPod7,1":                                 return "iPod Touch 6"
		case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
		case "iPhone4,1":                               return "iPhone 4s"
		case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
		case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
		case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
		case "iPhone7,2":                               return "iPhone 6"
		case "iPhone7,1":                               return "iPhone 6 Plus"
		case "iPhone8,1":                               return "iPhone 6s"
		case "iPhone8,2":                               return "iPhone 6s Plus"
		case "iPhone8,4":                               return "iPhone SE"
		case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
		case "iPhone9,4", "iPhone9,2":                  return "iPhone 7 Plus"
		case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
		case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
		case "iPhone10,3", "iPhone10,6":                return "iPhone X"
		case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
		case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
		case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
		case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
		case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
		case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
		case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
		case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
		case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
		case "iPad6,7", "iPad6,8":                      return "iPad Pro"
		case "AppleTV5,3":                              return "Apple TV"
		case "i386", "x86_64":                          return "IOS Simulator"
		default:                                        return identifier
		}
	}
}

extension CGRect
{
	init(_ x: CGFloat, _ y: CGFloat, _ w: CGFloat, _ h: CGFloat)
	{
		self.init(x: x, y: y, width: w, height: h)
	}
}

extension String
{
//	func md5() -> String!
//	{
//		let str = self.cString(using: String.Encoding.utf8)
//		let strLen = CUnsignedInt(self.lengthOfBytes(using: String.Encoding.utf8))
//		let digestLen = Int(CC_MD5_DIGEST_LENGTH)
//		let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
//
//		CC_MD5(str!, strLen, result)
//
//		let hash = NSMutableString()
//		for i in 0..<digestLen {
//			hash.appendFormat("%02x", result[i])
//		}
//
//		result.deinitialize()
//
//		return String(format: hash as String)
//	}

	func condenseWhitespace() -> String
	{
		// "".stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString: " "))
		let components = self.components(separatedBy: CharacterSet.whitespacesAndNewlines)
		return components.filter { !$0.isEmpty }.joined(separator: " ")
	}

	func trim() -> String
	{
		return self.trimmingCharacters(in: CharacterSet(charactersIn: " "))
	}
}
