//
//  EventManager.swift
//  Scanner
//
//  Created by Artem  on 24.09.2018.
//  Copyright © 2018 VladimirKuzmin. All rights reserved.
//

import Foundation

class EventsManager
{
	private static let sharedi =  EventsManager()

	public static func shared() -> EventsManager {
		return sharedi
	}

	func sendNotify(_ notify: String, data: [String: Any])
	{
		let nameNotif = String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, notify)

		NotificationCenter.default.post(name: Notification.Name(rawValue: nameNotif), object: nil, userInfo: data)
	}

	func sendNotify(_ notify: String, object: [String: Any])
	{
		let nameNotif = String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, notify)

		NotificationCenter.default.post(name: Notification.Name(rawValue: nameNotif), object: nil, userInfo: object)
	}

	func sendNotify(_ notify: String)
	{
		let nameNotif = String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, notify)

		NotificationCenter.default.post(name: Notification.Name(rawValue: nameNotif), object: nil, userInfo: nil)
	}
}
