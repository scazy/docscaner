//
//  Core.swift
//  Scanner
//
//  Created by Artem  on 24.09.2018.
//  Copyright © 2018 VladimirKuzmin. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import RealmSwift
import Alamofire
import Reachability

enum PremiumStatus: Int
{
	case Free = 1
	case Trial
	case Premium
}

class Core: NSObject
{
	let productIds = Set([ "com.Videomaks.Scanner.ScannerWeekly", // 4.99 / Premium (1 Week) / Use Weekly PDF Scanner App + OCR unlimited!
						   "com.Videomaks.Scanner.ScannerMonthly", //9.99
						   "com.Videomaks.Scanner.Scanner6Month", //29.99
						   "com.Videomaks.Scanner.ScannerYearly"]) //49.99

	var IsDebug = true

	var reachability: Reachability!

	var SCHEMA_VERSION: UInt64 = 10

	var CurrentController = ""

	let defaults = UserDefaults.standard

	private static let sharedi = Core()

	private var _window: UIWindow!

	var _realm: Realm!

	var DeviceId = ""

	var RootWindow: UIWindow
	{
		get
		{
			return _window
		}
	}

	public static func shared() -> Core
	{
		return sharedi
	}

	var Config: [String: AnyObject]
	{
		get
		{
			if let path = Bundle.main.path(forResource: "Info", ofType: "plist"), let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject]
			{
				return dict
			}
			else
			{
				return [String: AnyObject]()
			}
		}
	}

	func Init(window: UIWindow)
	{
		DispatchQueue.main.async(execute: {

			self.addObservers()

			self._window = window

			self._window.backgroundColor = UIColor.white

			self.reachability = Reachability()!

			UINavigationBar.appearance().tintColor = UIColor.white

			let mainController = MainController()

			self._window.rootViewController = mainController

			self._window.makeKeyAndVisible()


		})
	}

	var NetworkStatus: Reachability
	{
		get
		{
			return reachability
		}
	}

	func initDB()
	{
		let config = Realm.Configuration(
			schemaVersion: SCHEMA_VERSION
		)

		Realm.Configuration.defaultConfiguration = config

		self._realm = try! Realm()

		let folderPath = self._realm.configuration.fileURL!.deletingLastPathComponent().path

		print("** REALM PATH \(folderPath)/default.realm")

		try! FileManager.default.setAttributes([FileAttributeKey.protectionKey: FileProtectionType.none], ofItemAtPath: folderPath)

	}

	var getRealm: Realm
	{
		get
		{
			if(self._realm == nil)
			{
				self.initDB()
			}

			return self._realm
		}
	}

	func addObservers()
	{

	}

	func changeViewController(name: String, params: [String: Any] = [String: Any]())
	{
		print("*** changeViewController NAME \(name) PARAMS \(params)")
		DispatchQueue.main.async(execute: {
			EventsManager.shared().sendNotify(Constants.EVENT_CHANGE_CONTROLLER, object: ["controller": name, "params": params])
		})
	}

	func openView(name: String, params: [String: Any] = [String: Any]())
	{
		DispatchQueue.main.async(execute: {
			EventsManager.shared().sendNotify(Constants.EVENT_OPEN_VIEW, object: ["view": name, "params": params])
		})
	}

	func removeViewController(name: String)
	{
		DispatchQueue.main.async(execute: {
			EventsManager.shared().sendNotify(Constants.EVENT_REMOVE_CONTROLLER, object: ["controller": name])
		})
	}

	func checkTrial(finish: @escaping (_ status: PremiumStatus) -> ())
	{
//		EventsManager.shared().sendNotify(Constants.EVENT_PREMIUM)

		self.ValidateRecept { (success) in
			if let receipt = Core.shared().getRealm.objects(ReceiptStatus.self).first
			{
				if(receipt.Status == 1)
				{
					finish(PremiumStatus.Premium)
				}
				else
				{
					//				EventsManager.shared().sendNotify(Constants.EVENT_PREMIUM)
					finish(PremiumStatus.Trial)
				}
			}
			else
			{
				//			EventsManager.shared().sendNotify(Constants.EVENT_PREMIUM)
				finish(PremiumStatus.Free)
			}
		}

		finish(PremiumStatus.Trial)
	}

	func showPremium()
	{
		EventsManager.shared().sendNotify(Constants.EVENT_PREMIUM)
	}

	func ValidateRecept(completion: @escaping (_ success: Bool) -> ())
	{
		var status = 0

		if(Core.shared().NetworkStatus.connection == .none)
		{
			var rs = ReceiptStatus()
			rs.Status = status
			rs.Id = 1

			try! Core.shared().getRealm.write {
				Core.shared().getRealm.add(rs, update: true)
			}

			completion(false)
			return
		}

		print("*** ValidateRecept 1")
		let receiptUrl = Bundle.main.appStoreReceiptURL

		if(receiptUrl == nil)
		{
			print("*** ValidateRecept 2")

			var rs = ReceiptStatus()
			rs.Status = status
			rs.Id = 1

			try! Core.shared().getRealm.write {
				Core.shared().getRealm.add(rs, update: true)
			}

			completion(false)
			return
		}

		do {
			print("*** ValidateRecept 3")

			let receipt = try NSData(contentsOf: receiptUrl!)

			if(receipt == nil)
			{
				print("*** ValidateRecept 4")

				var rs = ReceiptStatus()
				rs.Status = status
				rs.Id = 1

				try! Core.shared().getRealm.write {
					Core.shared().getRealm.add(rs, update: true)
				}

				completion(false)
				return
			}

			print("*** ValidateRecept 5")

//			print("** RECEIPT *\(receipt)*")

			let receiptdata: NSString = receipt!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0)) as NSString

			let dict: [String : Any] = ["receipt-data" : receiptdata, "password": Constants.ITUNES_SECRET]

			let jsonData = try! JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions(rawValue: 0))

			let request = NSMutableURLRequest(url: NSURL(string: Constants.ITUNES_API)! as URL)

			let session = URLSession.shared
			request.httpMethod = "POST"

			request.httpBody = jsonData

			let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error  in

				print("*** ValidateRecept 6")
				DispatchQueue.main.async(execute: {

					print("*** ValidateRecept 7")

					if let responseDatas = data
					{
						print("*** ValidateRecept 8")
						var jsonData = JSON(responseDatas)
						print(jsonData)

						var statusData = jsonData["status"].intValue

						if(statusData == 0)
						{
							let receiptData = jsonData["latest_receipt_info"].arrayValue

							var dates = [Int64]()

							for i in receiptData {
								var recData = i.dictionaryValue
								let expires_date_ms = (recData["expires_date_ms"]?.int64Value)! / 1000
								dates.append(expires_date_ms)
							}

							if(dates.count > 0)
							{
								let current = Int64(NSDate().timeIntervalSince1970)
								let max = dates.max()

								if(current > max!)
								{
									status = 1
								}
							}
						}

						var rs = ReceiptStatus()
						rs.Status = status
						rs.Id = 1

						try! Core.shared().getRealm.write {
							Core.shared().getRealm.add(rs, update: true)
						}

						if(status > 0)
						{
							print("*** ValidateRecept 9")
							completion(true)
						}
						else
						{
							print("*** ValidateRecept 10")
							completion(false)
						}
					}
					else
					{
						print("*** ValidateRecept 11")
						var rs = ReceiptStatus()
						rs.Status = status
						rs.Id = 1

						try! Core.shared().getRealm.write {
							Core.shared().getRealm.add(rs, update: true)
						}

						completion(false)
					}
				})
			})

			task.resume()
		} catch  {
			print("*** ValidateRecept 12")

			var rs = ReceiptStatus()
			rs.Status = status
			rs.Id = 1

			try! Core.shared().getRealm.write {
				Core.shared().getRealm.add(rs, update: true)
			}

			completion(false)
		}
	}
}

protocol ViewControllerDelegate
{
	func setParams(params: [String: Any])
	func willBeAppear()
	func willBeDisappear()
}

public class ControllerModel
{
	var _name: String = ""
	var _controller: UIViewController!

	init(name: String, controller: UIViewController)
	{
		_name = name
		_controller = controller
	}

	public var Name: String
	{
		get
		{
			return self._name
		}
	}

	public var Controller: UIViewController
	{
		get
		{
			return self._controller
		}
	}
}


