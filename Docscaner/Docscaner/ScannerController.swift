//
//  ScannerController.swift
//  Docscaner
//
//  Created by Artem  on 14/03/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import GLKit
import CoreMedia
import CoreImage
import OpenGLES
import QuartzCore

class ScannerController: UIViewController, ViewControllerDelegate, ScannerSwitchMenuDelegate, CameraNoAccessDelegate
{

    
    var isBatch = false
    
    var batchPhoto = 10
    
    var images = [UIImage]()
    
    var prevResult = CGRect.zero
    
    var autoScanImage: UIImage!
    
    var bottom: ScannerBottomMenu!
    
    var ticks = 0
    
    var timer = Timer()
    
    var timerCounter = 3
    
    var timerView: UILabel!
    
    var currentType: ScannerSwitchMenuType = .Manual
    
    var top: ScannerTopMenu!
    
    var previewLayer : AVCaptureVideoPreviewLayer?
    
    var captureDevice : AVCaptureDevice?
    
    var captureSession = AVCaptureSession()
    
    var captureSessionVideo = AVCaptureSession()
    
    var sessionQueue: DispatchQueue!
    
    var videoPreviewLayer: LiveScanner!
    
    var videoOutput: AVCaptureVideoDataOutput!
    
    var videoInput: AVCaptureDeviceInput!
    
    var imageInput: AVCaptureDeviceInput!
    
    var previewLayerLayout: UIView!
    
    let photoOutput = AVCapturePhotoOutput()
    
    var videoDisplayView: GLKView!
    
    var videoDisplayViewBounds: CGRect!
    
    var renderContext: CIContext!
    
    var detector: CIDetector?
    
    var previewLiveLayer: UIView!
    
    func showDetectLoader()
    {

        var d = ScanerDetectLoader(width: 50, height: 50)
        d.setX(100)
        d.setY(100)
        self.view.addSubview(d)
    }
    
	override func viewDidLoad()
	{
		super.viewDidLoad()

		self.view.backgroundColor = .black
        
        top = ScannerTopMenu()
        top.delegate = self
        top.setX(0)
        top.setY(0)
        self.view.addSubview(top)
        
        print("*** viewDidLoad 1")
        self.checkAuthorization { (success) in
            print("*** viewDidLoad 2")
            if(success)
            {
                DispatchQueue.main.async(execute: {
                    self.initCamera()
                })
            }
            else
            {
                DispatchQueue.main.async(execute: {
                    self.createCameraNotAccess()
                    
//                    self.showDetectLoader()
                })
            }
        }
	}
    
    func initTimer()
    {
        if(timerView == nil)
        {
            timerView = UILabel()
            timerView.setSize(ScreenSize.SCREEN_WIDTH, 45)
//            timerView.backgroundColor = UIColor.white.withAlphaComponent(0.75)
//            timerView.layer.cornerRadius = 10
            timerView.textColor = UIColor.black.withAlphaComponent(0.75)
            timerView.textAlignment = .center
            timerView.font = UIFont.systemFont(ofSize: 18)
            timerView.toCenterX(self.view)
            timerView.text = String(format: s("scan_start"), "3")
            timerView.setY(20, relative: top)
            self.view.addSubview(timerView)
        }
        
        timerView.isHidden = false
    }
    
    func createCameraNotAccess()
    {
        var noaccess = CameraNoAccess()
        noaccess.delegate = self
        noaccess.setX(0)
        noaccess.setY(0, relative: top)
        noaccess.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT - top.getHeight())
        self.view.addSubview(noaccess)
        
        noaccess.initUI()
    }

    func initManualCamera()
    {
        captureSession.sessionPreset = AVCaptureSession.Preset.high
        
        let deviceDiscoverySession = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .back)
        
        guard let captureDevice = deviceDiscoverySession else {
            print("Failed to get the camera device")
//            self.IsPosibleTakeImage = false
//            createCameraNotAccess(title: s("access_denied"))
            return
        }
//
        try! captureDevice.lockForConfiguration()
        captureDevice.focusMode = .continuousAutoFocus
        captureDevice.unlockForConfiguration()
        
        do
        {
            imageInput = try AVCaptureDeviceInput(device: captureDevice)
            captureSession.addInput(imageInput)
        }
        catch
        {
            print(error)
            return
        }
        
        previewLayerLayout = UIView()
        previewLayerLayout.setX(0)
        previewLayerLayout.setY(0)
        previewLayerLayout.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        self.view.addSubview(previewLayerLayout)
        self.view.sendSubviewToBack(self.previewLayerLayout)
        
//        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.leftSwipe(_:)))
//        leftSwipe.direction = UISwipeGestureRecognizer.Direction.left
//        previewLayerLayout.addGestureRecognizer(leftSwipe)
//
//        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.rightSwipe(_:)))
//        rightSwipe.direction = UISwipeGestureRecognizer.Direction.right
//        previewLayerLayout.addGestureRecognizer(rightSwipe)
        
        self.previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        
        self.previewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        self.previewLayer?.frame = CGRect(0, 0, ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        
        previewLayerLayout.layer.addSublayer(self.previewLayer!)
        
        //        self.view.sendSubviewToBack(self.previewLayer)
        
        if captureSession.canAddOutput(photoOutput)
        {
            captureSession.addOutput(photoOutput)
            
            photoOutput.isHighResolutionCaptureEnabled = true
            photoOutput.isLivePhotoCaptureEnabled = photoOutput.isLivePhotoCaptureSupported
        }
        else
        {
            captureSession.commitConfiguration()
            return
        }
        
        captureSession.commitConfiguration()
        
        captureSession.startRunning()
    }
    
    func initCamera()
    {
        bottom = ScannerBottomMenu()
        bottom.delegate = self
        bottom.setX(0)
        bottom.setY(ScreenSize.SCREEN_HEIGHT - bottom.getHeight())
        self.view.addSubview(bottom)
        
        var switcher = ScannerSwitchMenu()
        switcher.delegate = self
        switcher.setX(0)
        switcher.setY(ScreenSize.SCREEN_HEIGHT - bottom.getHeight() - switcher.getHeight())
        self.view.addSubview(switcher)
        
//        self.createCameraNotAccess()
        
        initManualCamera()
    }
    
    func checkAuthorization(finish: @escaping (Bool) -> ())
    {
        #if targetEnvironment(simulator)
            finish(false)
        #else
        let granted = AVCaptureDevice.authorizationStatus(for: AVMediaType.video);
        
        switch granted {
        case AVAuthorizationStatus.authorized:
            print("*** checkAuthorization 1")
            finish(true)
            break;
        case AVAuthorizationStatus.denied:
            print("*** checkAuthorization 2")
            finish(false)
            break;
        case AVAuthorizationStatus.restricted:
            print("*** checkAuthorization 3")
            finish(false)
            break;
        case AVAuthorizationStatus.notDetermined:
            print("*** checkAuthorization 4")
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) in
                
                print("*** checkAuthorization 5")
                finish(granted)
            })
        @unknown default: break
            
        }
        #endif
    }
    
	func setParams(params: [String : Any])
	{

	}

	func willBeAppear()
	{

	}

	func willBeDisappear()
	{

	}

	override var preferredStatusBarStyle: UIStatusBarStyle
	{
		return .lightContent
	}

//	override func viewDidAppear(_ animated: Bool)
//	{
//		self.navigationController?.navigationBar.
//	}
    
    func ScannerSwitchMenuManualMode()
    {
        if(self.currentType == .Manual)
        {
            return
        }
        
        self.currentType = .Manual
        
        if(self.previewLayerLayout != nil )
        {
            self.previewLayerLayout.isHidden = false
        }
        
        self.bottom.hideButtonTake(false)
        
        self.LiveScannerClosed()
    }
    
    func ScannerSwitchMenuAutoMode()
    {
        if(self.currentType == .Auto)
        {
            return
        }
        
        self.currentType = .Auto
        
        self.captureSession.stopRunning()
        
        if(self.previewLayerLayout != nil )
        {
            self.previewLayerLayout.isHidden = true
        }
        
        if(self.videoPreviewLayer != nil )
        {
            self.captureSessionVideo.startRunning()
            self.videoPreviewLayer.isHidden = false
        }
        else
        {
            self.initVideoCapture()
        }
        
        self.initTimer()
        
        self.bottom.hideButtonTake(true)
    }
    
    func LiveScannerClosed()
    {
        print("*** LiveScannerClosed")
        
        if(videoPreviewLayer != nil )
        {
            videoPreviewLayer.isHidden = true
        }
        
        self.previewLayerLayout?.isHidden = false
        
        captureSessionVideo.stopRunning()
        captureSession.startRunning()
        
//        cameraAutoScan = false
//        liveButton.text = s("mode_manual")
        
        if(timerView != nil)
        {
            timerView.isHidden = true
        }

    }
    
    @objc func timerAction()
    {
        timerCounter -= 1
        
        if(timerCounter == 0)
        {
            timer.invalidate()
        }
        
        if(timerCounter == 0)
        {
            timerView.isHidden = true
        }
        else
        {
            timerView.isHidden = false
            
            timerView.text = String(format: s("scan_start"), "\(timerCounter)")
        }
    }
    
    func initVideoCapture()
    {
        captureSession.stopRunning()

        videoPreviewLayer = LiveScanner()
        videoPreviewLayer.delegate = self
        self.view.addSubview(videoPreviewLayer)
        self.view.sendSubviewToBack(videoPreviewLayer)
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: "timerAction", userInfo: nil, repeats: true)
        
        //---
        
//        _ = CGRect(0, self.top.getHeight(), ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT - self.top.getHeight() - self.bottomBar.getHeight())
        
        videoDisplayView = GLKView(frame: CGRect.zero, context: EAGLContext(api: .openGLES2)!)
        
        videoDisplayView.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI_2))
        
        videoDisplayView.frame = videoPreviewLayer.VideoLayer.bounds
        
        videoPreviewLayer.VideoLayer.addSubview(videoDisplayView)
        
        //        self.view.sendSubview(toBack: videoDisplayView)
        
        renderContext = CIContext(eaglContext: videoDisplayView.context)
        
        sessionQueue = DispatchQueue(label: "AVSessionQueue", attributes: [])
        
        videoDisplayView.bindDrawable()
        
        videoDisplayViewBounds = CGRect(x: 0, y: 0, width: videoDisplayView.drawableWidth, height: videoDisplayView.drawableHeight)
        
        //-----
        
        
        do
        {
            let device = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .back)
            videoInput = try AVCaptureDeviceInput(device: device!)
            
            
            try! device?.lockForConfiguration()
            device?.focusMode = .continuousAutoFocus
            device?.unlockForConfiguration()
            // Start out with low quality
            
            sessionQueue = DispatchQueue(label: "AVSessionQueue", attributes: [])
            
            //        captureSessionVideo = AVCaptureSession()
            captureSessionVideo.sessionPreset = AVCaptureSession.Preset.high
            
            // Output
            videoOutput = AVCaptureVideoDataOutput()
            videoOutput.videoSettings = [ kCVPixelBufferPixelFormatTypeKey as AnyHashable: kCVPixelFormatType_32BGRA] as? [String : Any]
            videoOutput.alwaysDiscardsLateVideoFrames = true
            videoOutput.setSampleBufferDelegate(self, queue: sessionQueue)
            
            // Join it all together
            captureSessionVideo.addInput(videoInput)
            captureSessionVideo.addOutput(videoOutput)
            
            detector = prepareRectangleDetector()
            
            captureSessionVideo.startRunning()
        }
        catch
        {
            
        }
    }
    
    func CameraNoAccessOpenSettings() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
                
                Core.shared().removeViewController(name: "scannerController")
                
                Core.shared().removeViewController(name: "dashboardController")
            })
        }
    }
}

extension ScannerController: LiveScannerDelegate
{
    func RetakeScanner()
    {
        captureSessionVideo.startRunning()
        
        timerCounter = 3
        
        initTimer()
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: "timerAction", userInfo: nil, repeats: true)
    }
}

extension ScannerController: ScannerBottomMenuDelegate
{
    func ScannerBottomMenuSettings()
    {
        Core.shared().removeViewController(name: "scannerController")

        Core.shared().changeViewController(name: "settingsController")
    }
    
    func ScannerBottomMenuTakeShot()
    {
        print("*** ScannerBottomMenuTakeShot")
        
        let photoSettings = AVCapturePhotoSettings()
        
        photoSettings.isHighResolutionPhotoEnabled = true
        
        var flashMode = 0
        
        if let settings = Core.shared().getRealm.objects(SettingsModel.self).first
        {
            flashMode = settings.FlashMode
        }
        
        switch(flashMode)
        {
        case 0:
            photoSettings.flashMode = .auto
            break
        case 2:
            photoSettings.flashMode = .on
            break;
        case 1:
            photoSettings.flashMode = .off
            break
        default:
            break
        }
        
        if let firstAvailablePreviewPhotoPixelFormatTypes = photoSettings.availablePreviewPhotoPixelFormatTypes.first
        {
            photoSettings.previewPhotoFormat = [kCVPixelBufferPixelFormatTypeKey as String: firstAvailablePreviewPhotoPixelFormatTypes]
        }
        
        self.photoOutput.capturePhoto(with: photoSettings, delegate: self)
    }
}

extension ScannerController: ScannerTopMenuDelegate
{
    func ScannerTopMenuBackAction()
    {
        Core.shared().removeViewController(name: "scannerController")
        
        Core.shared().changeViewController(name: "dashboardController")
    }
    
    func ScannerTopMenuSwitchMode()
    {
        print("*** ScannerTopMenuSwitchMode")
        
        let items = [
            ActionMenuItem(title: s("camera_mode1"), itemId: 1),
            ActionMenuItem(title: s("camera_mode2"), itemId: 2),
        ]
        
        let menu = ActionsMenu(menuId: 1, items: items)
        menu.delegate = self
        self.view.addSubview(menu)
        
        menu.Show()
    }
    
    func ScannerTopMenuSwitchFlashMode(flash: Int)
    {
        if let settings = Core.shared().getRealm.objects(SettingsModel.self).first
        {
            try! Core.shared().getRealm.write {
                settings.FlashMode = flash
            }
        }
    }
}

extension ScannerController: ActionsMenuDelegate
{
    func ActionsMenuAction(menuId: Int, itemId: Int)
    {
        if let settings = Core.shared().getRealm.objects(SettingsModel.self).first
        {
            try! Core.shared().getRealm.write {
                settings.CameraMode = itemId
            }
        }
        
        switch(itemId)
        {
        case 1:
            self.top.setCameraMode(.BW)
            break
        case 2:
            self.top.setCameraMode(.Color)
            break;
        default:
            break
        }
    }
    
    func ActionsMenuCancel()
    {
        
    }
}
