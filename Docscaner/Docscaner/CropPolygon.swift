//
//  CropPolygon.swift
//  Scanner
//
//  Created by Artem  on 20/11/2018.
//  Copyright © 2018 VladimirKuzmin. All rights reserved.
//

import Foundation
import UIKit

protocol CropPolygonDelegate
{
    func CropPolygonChangeSize(points: [CGPoint])
}

class CropPolygon: UIView
{
    var delegate: CropPolygonDelegate!
    
    var point1: UIView!
    
    var point2: UIView!
    
    var point3: UIView!
    
    var point4: UIView!
    
    var point1a: UIView!
    
    var point2a: UIView!
    
    var point3a: UIView!
    
    var point4a: UIView!
    
    var shape: CAShapeLayer!
    
    var pointSize: CGFloat = 15
    
    var pointSize2: CGFloat = 15
    
    var maxWidth: CGFloat = 0
    
    var maxHeight: CGFloat = 0
    
    init(width: CGFloat, height: CGFloat, points: [CGPoint])
    {
        super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))
        
        self.backgroundColor = .clear
        
        var mainColor = UIColor.init(red: 0.21, green: 0.42, blue: 0.69, alpha: 1)
        
        maxHeight = height
        
        maxWidth = width
        
        point1 = UIView()
        point1.tag = 1001
        point1.frame = CGRect(x: points[0].x - (pointSize2 / 2), y: points[0].y - (pointSize2 / 2), width: pointSize2, height: pointSize2)
        point1.backgroundColor = UIColor.clear
        point1.layer.cornerRadius = pointSize2 / 2
        self.addSubview(point1)
        
        point1a = UIView()
        point1a.tag = 2001
        point1a.frame = CGRect(x: points[0].x - (pointSize / 2), y: points[0].y - (pointSize / 2), width: pointSize, height: pointSize)
        point1a.backgroundColor = mainColor
        point1a.layer.cornerRadius = pointSize / 2
        self.addSubview(point1a)
        
        let panGesture1 = UIPanGestureRecognizer(target: self, action: #selector(self.handlePan(_:)))
        point1.isUserInteractionEnabled = true
        point1.addGestureRecognizer(panGesture1)
        
        //----
        
        point2 = UIView()
        point2.tag = 1002
        point2.frame = CGRect(x: points[1].x - (pointSize2 / 2), y: points[1].y - (pointSize2 / 2), width: pointSize2, height: pointSize2)
        point2.backgroundColor = UIColor.clear
        point2.layer.cornerRadius = pointSize2 / 2
        self.addSubview(point2)
        
        point2a = UIView()
        point2a.tag = 2002
        point2a.frame = CGRect(x: points[1].x - (pointSize / 2), y: points[1].y - (pointSize / 2), width: pointSize, height: pointSize)
        point2a.backgroundColor = mainColor
        point2a.layer.cornerRadius = pointSize / 2
        self.addSubview(point2a)
        
        let panGesture2 = UIPanGestureRecognizer(target: self, action: #selector(self.handlePan(_:)))
        point2.isUserInteractionEnabled = true
        point2.addGestureRecognizer(panGesture2)
        
        //----
        
        point3 = UIView()
        point3.tag = 1003
        point3.frame = CGRect(x: points[2].x - (pointSize2 / 2), y: points[2].y - (pointSize2 / 2), width: pointSize2, height: pointSize2)
        point3.backgroundColor = UIColor.clear
        point3.layer.cornerRadius = pointSize2 / 2
        self.addSubview(point3)
        
        point3a = UIView()
        point3a.tag = 2003
        point3a.frame = CGRect(x: points[2].x - (pointSize / 2), y: points[2].y - (pointSize / 2), width: pointSize, height: pointSize)
        point3a.backgroundColor = mainColor
        point3a.layer.cornerRadius = pointSize / 2
        self.addSubview(point3a)
        
        let panGesture3 = UIPanGestureRecognizer(target: self, action: #selector(self.handlePan(_:)))
        point3.isUserInteractionEnabled = true
        point3.addGestureRecognizer(panGesture3)
        
        //-----
        
        point4 = UIView()
        point4.tag = 1004
        point4.frame = CGRect(x: points[3].x - (pointSize2 / 2), y: points[3].y - (pointSize2 / 2), width: pointSize2, height: pointSize2)
        point4.backgroundColor = UIColor.clear
        point4.layer.cornerRadius = pointSize2 / 2
        self.addSubview(point4)
        
        point4a = UIView()
        point4a.tag = 2004
        point4a.frame = CGRect(x: points[3].x - (pointSize / 2), y: points[3].y - (pointSize / 2), width: pointSize, height: pointSize)
        point4a.backgroundColor = mainColor
        point4a.layer.cornerRadius = pointSize / 2
        self.addSubview(point4a)
        
        let panGesture4 = UIPanGestureRecognizer(target: self, action: #selector(self.handlePan(_:)))
        point4.isUserInteractionEnabled = true
        point4.addGestureRecognizer(panGesture4)
        
        //----
        
        shape = CAShapeLayer()
        self.layer.addSublayer(shape)
        
        shape.opacity = 0.5
        shape.lineWidth = 2
        shape.lineJoin = CAShapeLayerLineJoin.miter
        //        shape.strokeColor = UIColor(hue: 0.786, saturation: 0.79, brightness: 0.53, alpha: 1.0).cgColor
        shape.strokeColor = mainColor.cgColor
        //        shape.fillColor = UIColor(hue: 0.786, saturation: 0.15, brightness: 0.89, alpha: 1.0).cgColor
        shape.fillColor = UIColor(red: 199/255.0, green: 236/255.0, blue: 243/255.0, alpha:1.0).cgColor
        
        redrawPolygon()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    func redrawPolygon()
    {
        let path = UIBezierPath()
        path.move(to: point1.center)
        path.addLine(to: point2.center)
        path.addLine(to: point3.center)
        path.addLine(to: point4.center)
        path.close()
        shape.path = path.cgPath
        
        self.bringSubviewToFront(point1a)
        self.bringSubviewToFront(point2a)
        self.bringSubviewToFront(point3a)
        self.bringSubviewToFront(point4a)
        
        self.bringSubviewToFront(point1)
        self.bringSubviewToFront(point2)
        self.bringSubviewToFront(point3)
        self.bringSubviewToFront(point4)
    }
    
    func getPoints() -> [CGPoint]
    {
        return [
            point1.center,
            point2.center,
            point3.center,
            point4.center
        ]
    }
    
    @objc func handlePan(_ gestureRecognizer: UIPanGestureRecognizer)
    {
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed
        {
            let translation = gestureRecognizer.translation(in: self)
            
            print("*** handlePan \(gestureRecognizer.view!.center.x) \(translation.x) \(gestureRecognizer.view!.center.y) \(translation.y)")
            
            var x = gestureRecognizer.view!.center.x + translation.x
            
            var y = gestureRecognizer.view!.center.y + translation.y
            
            if(x < 0)
            {
                return
            }
            
            if(y < 0)
            {
                return
            }
            
            if(x > maxWidth)
            {
                return
            }
            
            if(y > maxHeight)
            {
                return
            }
            
            gestureRecognizer.view!.center = CGPoint(x: x, y: y)
            
            gestureRecognizer.setTranslation(CGPoint.zero, in: self)
            
            if let point = self.viewWithTag(gestureRecognizer.view!.tag + 1000)
            {
                point.center = gestureRecognizer.view!.center
            }
            
            redrawPolygon()
            
            self.delegate.CropPolygonChangeSize(points: self.getPoints())
        }
    }
}
