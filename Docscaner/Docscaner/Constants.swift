//
//  Constants.swift
//  Docscaner
//
//  Created by Artem  on 28/02/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation

struct Constants
{
    static let EVENT_NEW_DOC = "EVENT_NEW_DOC"
    
    static let EVENT_OPEN_FOLDER = "EVENT_OPEN_FOLDER"
    
    static let EVENT_IMPORT_IMAGE = "EVENT_IMPORT_IMAGE"
    
	static let EVENT_OPEN_VIEW = "EVENT_OPEN_VIEW"

	static let EVENT_CHANGE_CONTROLLER = "EVENT_CHANGE_CONTROLLER"

	static let EVENT_REMOVE_CONTROLLER = "EVENT_REMOVE_CONTROLLER"

	static let EVENT_VIEW_LOADED = "EVENT_VIEW_LOADED"

	static let EVENT_APP_FOREGROUND = "EVENT_APP_FOREGROUND"

	static let EVENT_APP_BACKGROUND = "EVENT_APP_BACKGROUND"

	static let EVENT_DO_PURCHASE = "EVENT_DO_PURCHASE"

	static let NOTIFY_MESSAGE_PREFIX = "event."

	static let EVENT_DO_RESTORE_PURCHASE = "EVENT_DO_RESTORE_PURCHASE"

	static let START_REGISTER_PRODUCTS = "START_REGISTER_PRODUCTS"

	static let SERVICE_API = "http://docscanner.pro:8002/api/v1/"
    
    static let OCR_API = "http://docscanner.pro/api/v1/tesseract/recognize"

	static let ITUNES_SECRET = "304631173bb04acdbaa2485720292b5b"
    
    static let SETTINGS_IS_BW = "SETTINGS_IS_BW"
    
    static let EVENT_OPEN_DOC = "EVENT_OPEN_DOC"
    
    static let EVENT_DO_SHARE = "EVENT_DO_SHARE"
    
    static let SIGN_IMAGE = "signature.png"
    
    static let EVENT_RELOAD_LIB = "EVENT_RELOAD_LIB"
    
    static let EVENT_SHOW_PREMIUM = "EVENT_SHOW_PREMIUM"
    
    static let UPDATE_SUBS = "UPDATE_SUBS"
    
    static let UPDATE_SUBS_SUCCESS = "UPDATE_SUBS_SUCCESS"
    
    static let START_AUTH = "START_AUTH";
    
    static let PROMO_CLOSE_CHANGED = "PROMO_CLOSE_CHANGED"
    
    static let METRICS_URL = "http://appmetrics.live/"
}
