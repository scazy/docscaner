//
//  ReceiptStatus.swift
//  Docscaner
//
//  Created by Artem  on 28/02/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import RealmSwift

class ReceiptStatus: Object
{
	override static func primaryKey() -> String?
	{
		return "Id"
	}

	@objc dynamic var Status = 0 // 0 - trial, 2 - full

	@objc dynamic var Id = 0
}
