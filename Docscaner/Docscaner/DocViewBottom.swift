//
//  DocViewBottom.swift
//  Docscaner
//
//  Created by artem on 04/04/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit

enum DocViewBottomType: Int
{
    case Edit = 1
    case Sign = 2
    case Add = 3
    case Share = 4
    case OCR = 5
}

class DocViewBottomItem
{
    var Id: DocViewBottomType!
    var Name: String = ""
    var Icon: String = ""
    
    init(id: DocViewBottomType, name: String, icon: String)
    {
        Id = id
        Name = name
        Icon = icon
    }
}

protocol DocViewBottomDelegate
{
    func DocViewBottomAdd()
    
    func DocViewBottomSign()
    
    func DocViewBottomShare()
    
    func DocViewBottomEdit()
    
    func DocViewBottomOCR()
}

class DocViewBottom: UIView
{
    var delegate: DocViewBottomDelegate!
    
    var items = [DocViewBottomItem(id: DocViewBottomType.Edit, name: s("menu_edit_edit"), icon: "doc_view_edit"),
                    DocViewBottomItem(id: DocViewBottomType.Sign, name: s("menu_edit_sign"), icon: "doc_view_sign"),
                    DocViewBottomItem(id: DocViewBottomType.Add, name: s("menu_edit_add"), icon: "doc_view_new"),
                    DocViewBottomItem(id: DocViewBottomType.OCR, name: s("menu_edit_ocr"), icon: "doc_view_ocr"),
                    DocViewBottomItem(id: DocViewBottomType.Share, name: s("menu_edit_share"), icon: "doc_view_share")
                 ]
    init()
    {
        var startOffset = UIApplication.shared.statusBarFrame.size.height
        
        super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 117 + startOffset))
        
        self.clipsToBounds = true
        
        var bg_top = UIImageView()
        bg_top.setX(0)
        bg_top.setY(0)
        bg_top.setSize(self.getWidth(), self.getHeight())
        bg_top.image = UIImage(named: "doc_view_bottom")
        self.addSubview(bg_top)
        
        var buttons = UIView()
//        buttons.backgroundColor = .red
        buttons.setX(0)
        buttons.setY(0)
        buttons.setSize(self.getWidth(), self.getHeight() / 2 + 20)
        self.addSubview(buttons)
        
        var partSize: CGFloat = self.getWidth() / CGFloat(self.items.count)
        
        var offset: CGFloat = 0
        
        for item in items
        {
            var block = UIView()
//            block.backgroundColor = .red
            block.setX(offset)
            block.setY(0)
            block.setSize(partSize, buttons.getHeight())
            buttons.addSubview(block)
            
            var image = UIImageView()
            image.setSize(block.getWidth() * 0.33, block.getWidth() * 0.33)
            image.toCenterX(block)
            image.toCenterY(block)
            image.image = UIImage(named: item.Icon)
            block.addSubview(image)
            
            var title = UILabel()
            title.font = UIFont(name: "Circe-Bold", size: 13)
            title.setX(0)
            title.setY(20, relative: image)
            title.setSize(block.getWidth(), 16)
            title.textColor = UIColor.white
            title.textAlignment = .center
            title.text = item.Name.uppercased()
            block.addSubview(title)
            
            var button = UIButton()
            button.setX(0)
            button.setY(0)
            button.setSize(block.getWidth(), block.getHeight())
            button.tag = item.Id.rawValue
            button.addTarget(self, action: "buttonAction:", for: .touchUpInside)
            block.addSubview(button)
            
            offset += partSize
        }
    }
    
    @objc func buttonAction(_ sender: AnyObject)
    {
        print("*** BOTTOM buttonAction")
        var type = DocViewBottomType(rawValue: sender.tag)!
        
        print(type)
        switch(type)
        {
        case .Add:
            print("*** BOTTOM buttonAction 1")
            self.delegate.DocViewBottomAdd()
            break
        case .Edit:
            print("*** BOTTOM buttonAction 2")
            self.delegate.DocViewBottomEdit()
            break
        case .Share:
            print("*** BOTTOM buttonAction 3")
            self.delegate.DocViewBottomShare()
            break
        case .Sign:
            print("*** BOTTOM buttonAction 4")
            self.delegate.DocViewBottomSign()
            break
        case .OCR:
            self.delegate.DocViewBottomOCR()
            break
        }
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
}

