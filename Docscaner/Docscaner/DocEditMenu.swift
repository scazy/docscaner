//
//  DocEditMenu.swift
//  Scanner
//
//  Created by Artem  on 25/01/2019.
//  Copyright © 2019 VladimirKuzmin. All rights reserved.
//

import Foundation
import UIKit

enum DocEditCommonItem: Int
{
    case Share = 1
    case OCR = 2
    case Edit = 3
    case Signature = 4
    case More = 5
    
    case Rotate = 6
    case Pen = 7
    case Bright = 8
    
    case RotateLeft = 9
    case RotateCircle = 10
    case RotateRight = 11
    
    case PenPen = 12
    case PenBrush = 13
    case PenColor = 14
    case PenErase = 15
    
//    case BrightColor = 16
//    case BrightBW = 17
    case BrightLight = 19
    case BrightContrast = 18
}



enum DocEditMode: Int
{
    case Common = 0
    case Edit
    case Rotate
    case Pen
    case Bright
    case Contrast
    case Light
    
}

class DocEditMenu: UIView
{
    private var _common: UIView!
    
    private var _edit: UIView!
    
    private var _rotate: UIView!
    
    private var _pen: UIView!
    
    private var _bright: UIView!
    
    private var _contrast: UIView!
    
    private var _light: UIView!
    
    var height: CGFloat = 42
    
    var delegate: DocEditMenuDelegate!
    
    private var _mode = DocEditMode.Common
    
    
    var getMode: DocEditMode
    {
        get
        {
            return self._mode
        }
    }
    
    init()
    {
        var startOffset = UIApplication.shared.statusBarFrame.size.height
        
        super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: height + startOffset))
        
        self.backgroundColor = UIColor.white
        
//        initCommon()
        
//        initContrast()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initBright()
    {
        self._mode = DocEditMode.Bright
        
        _rotate = UIView()
        _rotate.backgroundColor = UIColor.white
        _rotate.setX(0)
        _rotate.setY(0)
        _rotate.setSize(self.getWidth(), height)
        self.addSubview(_rotate)
        
        
        
        var icons = ["edit_bright_contrast", "edit_bright_light"]
        
        var part: CGFloat = self.getWidth() / CGFloat(icons.count)
        
        var offset: CGFloat = 0
        
        var i = 18
        
        for icon in icons
        {
            var backLayout = UIView()
            backLayout.setX(offset)
            backLayout.setY(0)
            backLayout.setSize(part, 42)
            _rotate.addSubview(backLayout)
            
            var icon = UIImage(named: icon)
            var iconSize = getImageSize(image: icon!, width: 0, height: backLayout.getHeight() - 20)
            
            var backIcon = UIImageView()
            backIcon.image = icon
            backIcon.setSize(42 - 14, 42 - 14)
            backIcon.toCenterX(backLayout)
            backIcon.setY(7)
            backLayout.addSubview(backIcon)
            
            var backButton = UIButton()
            backButton.tag = i
            backButton.setX(0)
            backButton.setY(0)
            backButton.setSize(backLayout.getWidth(), backLayout.getHeight())
            backButton.addTarget(self, action: "actionCommon:", for: .touchUpInside)
            backLayout.addSubview(backButton)
            
            offset += part
            
            i += 1
        }
    }
    
    func initPen()
    {
        self._mode = DocEditMode.Pen
        
        _rotate = UIView()
        _rotate.backgroundColor = UIColor.white
        _rotate.setX(0)
        _rotate.setY(0)
        _rotate.setSize(self.getWidth(), height)
        self.addSubview(_rotate)
        
        var part: CGFloat = self.getWidth() / 3
        
        var icons = ["edit_bright_color", "edit_bright_bw", "edit_bright_light", "edit_bright_contrast"]
        
        var offset: CGFloat = 0
        
        var i = 12
        
        for icon in icons
        {
            var backLayout = UIView()
            backLayout.setX(offset)
            backLayout.setY(0)
            backLayout.setSize(part, 42)
            _rotate.addSubview(backLayout)
            
            var icon = UIImage(named: icon)
            var iconSize = getImageSize(image: icon!, width: 0, height: backLayout.getHeight() - 20)
            
            var backIcon = UIImageView()
            backIcon.image = icon
            backIcon.setSize(42 - 14, 42 - 14)
            backIcon.toCenterX(backLayout)
            backIcon.setY(7)
            backLayout.addSubview(backIcon)
            
            var backButton = UIButton()
            backButton.tag = i
            backButton.setX(0)
            backButton.setY(0)
            backButton.setSize(backLayout.getWidth(), backLayout.getHeight())
            backButton.addTarget(self, action: "actionCommon:", for: .touchUpInside)
            backLayout.addSubview(backButton)
            
            offset += part
            
            i += 1
        }
    }
    
    func initLight()
    {
        print("*** initLight")
        
        self._mode = DocEditMode.Light
        
        _light = UIView()
        _light.backgroundColor = UIColor.white
        _light.setX(0)
        _light.setY(0)
        _light.setSize(self.getWidth(), height)
        self.addSubview(_light)
        
        var slider = UISlider()
        slider.isContinuous = false
        slider.setX(20)
        slider.minimumValue = -1
        slider.maximumValue = 1
        slider.setValue(0, animated: false)
        slider.setY(0)
        slider.setSize(_light.getWidth() - 40, _light.getHeight())
        slider.addTarget(self, action: "lightChanged:", for: .valueChanged)
        _light.addSubview(slider)
    }
    
    func initContrast()
    {
        print("*** initContrast")
        self._mode = DocEditMode.Contrast
        
        _contrast = UIView()
        _contrast.backgroundColor = UIColor.white
        _contrast.setX(0)
        _contrast.setY(0)
        _contrast.setSize(self.getWidth(), height)
        self.addSubview(_contrast)
        
        var slider = UISlider()
        slider.isContinuous = false
        slider.setX(20)
        slider.minimumValue = -1
        slider.maximumValue = 1
        slider.setValue(0, animated: false)
        slider.setY(0)
        slider.setSize(_contrast.getWidth() - 40, _contrast.getHeight())
        slider.addTarget(self, action: "contrastChanged:", for: .valueChanged)
        _contrast.addSubview(slider)
    }
    
    @objc func lightChanged(_ sender: UISlider)
    {
        print("*** lightChanged")
        DispatchQueue.main.async(execute: {
            
            let newValue = sender.value
            
            self.delegate.DocEditActionBrightChanged(value: newValue)
        })
    }
    
    @objc func contrastChanged(_ sender: UISlider)
    {
        print("*** contrastChanged")
        DispatchQueue.main.async(execute: {
            
            let newValue = sender.value
            
            self.delegate.DocEditActionContrastChanged(value: newValue)
        })
    }

    func initRotate()
    {
        self._mode = DocEditMode.Rotate
        
        _rotate = UIView()
        _rotate.backgroundColor = UIColor.white
        _rotate.setX(0)
        _rotate.setY(0)
        _rotate.setSize(self.getWidth(), height)
        self.addSubview(_rotate)
        
        var part: CGFloat = self.getWidth() / 3
        
        var icons = ["edit_rotate_left", "edit_rotate_circle", "edit_rotate_right"]
        
        var offset: CGFloat = 0
        
        var i = 9
        
        for icon in icons
        {
            var backLayout = UIView()
            backLayout.setX(offset)
            backLayout.setY(0)
            backLayout.setSize(part, 42)
            _rotate.addSubview(backLayout)
            
            var icon = UIImage(named: icon)
            var iconSize = getImageSize(image: icon!, width: 0, height: backLayout.getHeight() - 20)
            
            var backIcon = UIImageView()
            backIcon.image = icon
            backIcon.setSize(42 - 14, 42 - 14)
            backIcon.toCenterX(backLayout)
            backIcon.setY(7)
            backLayout.addSubview(backIcon)
            
            var backButton = UIButton()
            backButton.tag = i
            backButton.setX(0)
            backButton.setY(0)
            backButton.setSize(backLayout.getWidth(), backLayout.getHeight())
            backButton.addTarget(self, action: "actionCommon:", for: .touchUpInside)
            backLayout.addSubview(backButton)
            
            offset += part
            
            i += 1
        }
    }
    
    func initEdit()
    {
        self._mode = DocEditMode.Edit
        
        _edit = UIView()
        _edit.backgroundColor = UIColor.white
        _edit.setX(0)
        _edit.setY(0)
        _edit.setSize(self.getWidth(), height)
        self.addSubview(_edit)
        
       
        
//        var icons = ["edit_rotate", "edit_pen", "edit_bright"]
        var icons = ["edit_rotate", "edit_bright"]
        
        var offset: CGFloat = 0
        
        var part: CGFloat = self.getWidth() / CGFloat(icons.count)
        
        var i = 6
        
        for icon in icons
        {
            var backLayout = UIView()
            backLayout.setX(offset)
            backLayout.setY(0)
            backLayout.setSize(part, 42)
            _edit.addSubview(backLayout)
            
            var icon = UIImage(named: icon)
            var iconSize = getImageSize(image: icon!, width: 0, height: backLayout.getHeight() - 20)
            
            var backIcon = UIImageView()
            backIcon.image = icon
            backIcon.setSize(42 - 14, 42 - 14)
            backIcon.toCenterX(backLayout)
            backIcon.setY(7)
            backLayout.addSubview(backIcon)
            
            var backButton = UIButton()
            backButton.tag = i
            backButton.setX(0)
            backButton.setY(0)
            backButton.setSize(backLayout.getWidth(), backLayout.getHeight())
            backButton.addTarget(self, action: "actionCommon:", for: .touchUpInside)
            backLayout.addSubview(backButton)
            
            offset += part
            
            i += 2
        }
    }
    
    func initCommon()
    {
        var part: CGFloat = self.getWidth() / 5
        
        _common = UIView()
        _common.backgroundColor = UIColor.white
        _common.setX(0)
        _common.setY(0)
        _common.setSize(self.getWidth(), height)
        self.addSubview(_common)
        
        var icons = ["edit_share", "edit_ocr", "edit_edit", "edit_signature", "edit_more"]
        
        var offset: CGFloat = 0
        var i = 1
        
        for icon in icons
        {
            var backLayout = UIView()
            backLayout.setX(offset)
            backLayout.setY(0)
            backLayout.setSize(part, 42)
            _common.addSubview(backLayout)
            
            var icon = UIImage(named: icon)
            var iconSize = getImageSize(image: icon!, width: 0, height: backLayout.getHeight() - 20)
            
            var backIcon = UIImageView()
            backIcon.image = icon
            backIcon.setSize(42 - 14, 42 - 14)
            backIcon.toCenterX(backLayout)
            backIcon.setY(7)
            backLayout.addSubview(backIcon)
            
            var backButton = UIButton()
            backButton.tag = i
            backButton.setX(0)
            backButton.setY(0)
            backButton.setSize(backLayout.getWidth(), backLayout.getHeight())
            backButton.addTarget(self, action: "actionCommon:", for: .touchUpInside)
            backLayout.addSubview(backButton)
            
            offset += part
            
            i += 1
        }
    }
    
    @objc func actionCommon(_ sender: AnyObject)
    {
        var t: DocEditCommonItem = DocEditCommonItem(rawValue: sender.tag)!
        
        switch(t)
        {
        case .Edit:
            self.delegate.DocEditActionEdit()
            print("*** actionCommon Edit")
            break;
        case .More:
            self.delegate.DocEditActionMore()
            print("*** actionCommon More")
            break;
        case .OCR:
            self.delegate.DocEditActionOCR()
            print("*** actionCommon OCR")
            break;
        case .Share:
            self.delegate.DocEditActionShare()
            print("*** actionCommon Share")
            break;
        case .Signature:
            self.delegate.DocEditActionSignature()
            print("*** actionCommon Signature")
            break;
        case .Rotate:
            self.delegate.DocEditRotate()
            print("*** actionCommon Rotate")
            break;
        case .Bright:
            self.delegate.DocEditBright()
            print("*** actionCommon Bright")
            break;
        case .Pen:
            self.delegate.DocEditPen()
            print("*** actionCommon Pen")
            break;
        case .RotateLeft:
            print("*** actionCommon RotateLeft")
            self.delegate.DocEditRotateLeft()
            break
        case .RotateRight:
            print("*** actionCommon RotateRight")
            self.delegate.DocEditRotateRight()
            break;
        case .RotateCircle:
            self.delegate.DocEditRotateCircle()
            print("*** actionCommon RotateCircle")
            break;
        case .PenPen:
            print("*** actionCommon PenPen")
            break
        case .PenBrush:
            print("*** actionCommon PenBrush")
            break
        case .PenColor:
            print("*** actionCommon PenColor")
            break
        case .PenErase:
            print("*** actionCommon PenErase")
            break
//        case .BrightColor:
//            print("*** actionCommon BrightColor")
//            break
//        case .BrightBW:
//            print("*** actionCommon BrightBW")
//            break
        case .BrightLight:
            print("*** actionCommon BrightLight")
            self.delegate.DocEditLight()
            break
        case .BrightContrast:
            self.delegate.DiocEditContrast()
            print("*** actionCommon BrightContrast")
            break
        default:
            break;
        }
    }
}

protocol DocEditMenuDelegate
{
    func DocEditActionShare()
    
    func DocEditActionOCR()
    
    func DocEditActionEdit()
    
    func DocEditActionSignature()
    
    func DocEditActionMore()
    
    
    //---
    
    
    func DocEditRotate()
    
    func DocEditPen()
    
    func DocEditBright()
    
    func DocEditLight()
    
    func DiocEditContrast()
    
    //---
    
    func DocEditRotateLeft()
    
    func DocEditRotateRight()
    
    func DocEditRotateCircle()
    
    //---
    
    func DocEditActionContrastChanged(value: Float)
    
    func DocEditActionBrightChanged(value: Float)
}
