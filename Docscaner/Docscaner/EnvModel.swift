//
//  EnvModel.swift
//  Docscaner
//
//  Created by Artem  on 28/02/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import RealmSwift

class EnvModel: Object
{
	@objc dynamic var Id = 0

	override static func primaryKey() -> String? {
		return "Id"
	}

	@objc dynamic var country_name = ""

	@objc dynamic var ip = ""

	@objc dynamic var country_code = ""

	@objc dynamic var city = ""

	@objc dynamic var deeplinks = ""
}

class UserModel: Object
{
    override static func primaryKey() -> String?
    {
        return "Id"
    }
    
    @objc dynamic var Token = ""
    
    @objc dynamic var Id: Int = 0
    
    @objc dynamic var IsPremium = false
}
