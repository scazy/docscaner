////
////  PhotoPreviewTool.swift
////  Scanner
////
////  Created by Artem  on 18/10/2018.
////  Copyright © 2018 VladimirKuzmin. All rights reserved.
////
//
//import Foundation
//import UIKit
//import CoreImage
//
//protocol PhotoPreviewToolDelegate
//{
//    func getOriginImage() -> UIImage
//    
//    func passNewImage(image: UIImage)
//    
//    func removeCurrentSlide()
//    
//    func rotateImageLeft()
//    
//    func rotateImageRight()
//    
//    func showCropTool()
//    
//    func changeTool(newMode: PhotoPreviewToolMode)
//}
//
//enum PhotoPreviewToolMode: Int
//{
//    case Crop = 1
//    case Rotate = 2
//    case Bright = 3
//}
//
//class PhotoPreviewTool: UIView
//{
//    var delegate: PhotoPreviewToolDelegate!
//    
//    var mode = PhotoPreviewToolMode.Bright
//    
//    var firstLayout: UIView!
//    
//    func addRotateTools()
//    {
//        for i in firstLayout.subviews {
//            i.removeFromSuperview()
//        }
//        
//        //icon_rotate_right
//        var iconRightView = UIView()
//        iconRightView.setSize(30, 30)
//        iconRightView.setY(10)
//        iconRightView.setX(firstLayout.getWidth() - iconRightView.getWidth() - 30)
//        firstLayout.addSubview(iconRightView)
//        
//        var iconRight = UIImageView()
//        iconRight.setX(0)
//        iconRight.setY(0)
//        iconRight.setSize(iconRightView.getWidth(), iconRightView.getHeight())
//        iconRight.image = UIImage(named: "icon_rotate_left")
//        iconRightView.addSubview(iconRight)
//        
//        var rightAction = UIButton()
//        rightAction.setX(0)
//        rightAction.setY(0)
//        rightAction.setSize(iconRightView.getWidth(), iconRightView.getHeight())
//        rightAction.addTarget(self, action: "rotateLeft:", for: .touchUpInside)
//        iconRightView.addSubview(rightAction)
//        
//        //---
//        
//        var iconLeftView = UIView()
//        iconLeftView.setSize(30, 30)
//        iconLeftView.setY(10)
//        iconLeftView.setX(iconRightView.getX() - 20 - iconLeftView.getWidth())
//        firstLayout.addSubview(iconLeftView)
//        
//        var iconLeft = UIImageView()
//        iconLeft.setX(0)
//        iconLeft.setY(0)
//        iconLeft.setSize(iconLeftView.getWidth(), iconLeftView.getHeight())
//        iconLeft.image = UIImage(named: "icon_rotate_right")
//        iconLeftView.addSubview(iconLeft)
//        
//        var leftAction = UIButton()
//        leftAction.setX(0)
//        leftAction.setY(0)
//        leftAction.setSize(iconLeftView.getWidth(), iconLeftView.getHeight())
//        leftAction.addTarget(self, action: "rotateRight:", for: .touchUpInside)
//        iconLeftView.addSubview(leftAction)
//    }
//    
//    @objc func rotateRight(_ sender: AnyObject)
//    {
//        print("*** rotateRight")
//        self.delegate.rotateImageRight()
//    }
//    
//    @objc func rotateLeft(_ sender: AnyObject)
//    {
//        print("*** rotateLeft")
//        self.delegate.rotateImageLeft()
//    }
//    
//    func addBrightTolls()
//    {
//        for i in firstLayout.subviews {
//            i.removeFromSuperview()
//        }
//        
//        var slider = UISlider()
//        
//        slider.isContinuous = false
//        slider.setX(20)
//        slider.minimumValue = -1
//        slider.maximumValue = 1
//        slider.setValue(0, animated: false)
//        slider.setY(0)
//        slider.setSize(firstLayout.getWidth() - 40, firstLayout.getHeight())
//        slider.addTarget(self, action: "brightChanged:", for: .valueChanged)
//        
//        //            slider.addTarget(self, action: "brightEndEditing:", for: .en)
//        firstLayout.addSubview(slider)
//    }
//    
//    init()
//    {
//        var startOffset = UIApplication.shared.statusBarFrame.size.height
//        super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 120 + startOffset))
//        
//        self.backgroundColor = UIColor.black
//        
//        self.setY(ScreenSize.SCREEN_HEIGHT)
//        
//        //---
//        
//        var heightToolsBlock = self.getHeight() / 3
//        
//        firstLayout = UIView()
//        firstLayout.setX(0)
//        firstLayout.setY(0)
//        firstLayout.setSize(self.getWidth(), heightToolsBlock)
//        self.addSubview(firstLayout)
//        
//        if(self.mode == .Bright)
//        {
//            addBrightTolls()
//        }
//        
//        var secondLayout = UIView()
//        secondLayout.setX(40)
//        secondLayout.setY(0, relative: firstLayout)
//        secondLayout.setSize(self.getWidth() - 80, heightToolsBlock * 2)
//        self.addSubview(secondLayout)
//        
//        var iconBlockWidth = secondLayout.getWidth() / 4
//        
//        //---
//        
//        var cropLayout = UIView()
//        cropLayout.setX(0)
//        cropLayout.setY(0)
//        cropLayout.setSize(iconBlockWidth, secondLayout.getHeight())
//        secondLayout.addSubview(cropLayout)
//        
//        cropLayoutIcon = UIImageView()
//        cropLayoutIcon.image = UIImage(named: "crop_off")
//        cropLayoutIcon.setSize(cropLayout.getWidth() / 3, cropLayout.getWidth() / 3)
//        cropLayoutIcon.toCenterX(cropLayout)
//        cropLayoutIcon.toCenterY(cropLayout)
//        cropLayout.addSubview(cropLayoutIcon)
//        
//        var cropLayoutAction = UIButton()
//        cropLayoutAction.addTarget(self, action: "cropLayoutAction:", for: .touchUpInside)
//        cropLayoutAction.setX(0)
//        cropLayoutAction.setY(0)
//        cropLayoutAction.setSize(cropLayout.getWidth(), cropLayout.getHeight())
//        cropLayout.addSubview(cropLayoutAction)
//        
//        //---
//        
//        var rotateLayout = UIView()
//        rotateLayout.setX(0, relative: cropLayout)
//        rotateLayout.setY(0)
//        rotateLayout.setSize(iconBlockWidth, secondLayout.getHeight())
//        secondLayout.addSubview(rotateLayout)
//        
//        rotateLayoutIcon = UIImageView()
//        rotateLayoutIcon.image = UIImage(named: "rotate_off")
//        rotateLayoutIcon.setSize(rotateLayout.getWidth() / 3, rotateLayout.getWidth() / 3)
//        rotateLayoutIcon.toCenterX(rotateLayout)
//        rotateLayoutIcon.toCenterY(rotateLayout)
//        rotateLayout.addSubview(rotateLayoutIcon)
//        
//        var rotateLayoutAction = UIButton()
//        rotateLayoutAction.addTarget(self, action: "rotateLayoutAction:", for: .touchUpInside)
//        rotateLayoutAction.setX(0)
//        rotateLayoutAction.setY(0)
//        rotateLayoutAction.setSize(rotateLayout.getWidth(), rotateLayout.getHeight())
//        rotateLayout.addSubview(rotateLayoutAction)
//        
//        //---
//        
//        var brightLayout = UIView()
//        brightLayout.setX(0, relative: rotateLayout)
//        brightLayout.setY(0)
//        brightLayout.setSize(iconBlockWidth, secondLayout.getHeight())
//        secondLayout.addSubview(brightLayout)
//        
//        brightLayoutIcon = UIImageView()
//        brightLayoutIcon.image = UIImage(named: "bright_on")
//        brightLayoutIcon.setSize(brightLayout.getWidth() / 3, brightLayout.getWidth() / 3)
//        brightLayoutIcon.toCenterX(brightLayout)
//        brightLayoutIcon.toCenterY(brightLayout)
//        brightLayout.addSubview(brightLayoutIcon)
//        
//        var brightLayoutAction = UIButton()
//        //        brightLayoutAction.backgroundColor = .red
//        brightLayoutAction.addTarget(self, action: #selector(self.brightLayoutAction(_:)), for: .touchUpInside)
//        brightLayoutAction.setX(0)
//        brightLayoutAction.setY(0)
//        brightLayoutAction.setSize(brightLayout.getWidth(), brightLayout.getHeight())
//        brightLayout.addSubview(brightLayoutAction)
//        
//        //---
//        
//        var deleteLayout = UIView()
//        deleteLayout.setX(0, relative: brightLayout)
//        deleteLayout.setY(0)
//        deleteLayout.setSize(iconBlockWidth, secondLayout.getHeight())
//        secondLayout.addSubview(deleteLayout)
//        
//        var deleteLayoutIcon = UIImageView()
//        deleteLayoutIcon.image = UIImage(named: "tools_delete")
//        deleteLayoutIcon.setSize(deleteLayout.getWidth() / 3, deleteLayout.getWidth() / 3)
//        deleteLayoutIcon.toCenterX(deleteLayout)
//        deleteLayoutIcon.toCenterY(deleteLayout)
//        deleteLayout.addSubview(deleteLayoutIcon)
//        
//        var deleteLayoutAction = UIButton()
//        deleteLayoutAction.addTarget(self, action: "deleteLayoutAction:", for: .touchUpInside)
//        deleteLayoutAction.setX(0)
//        deleteLayoutAction.setY(0)
//        deleteLayoutAction.setSize(deleteLayout.getWidth(), deleteLayout.getHeight())
//        deleteLayout.addSubview(deleteLayoutAction)
//        
//        
//    }
//    
//    var cropLayoutIcon: UIImageView!
//    
//    var rotateLayoutIcon: UIImageView!
//    
//    var brightLayoutIcon: UIImageView!
//    
//    func disableActiveIcon()
//    {
//        switch(mode)
//        {
//        case .Bright:
//            brightLayoutIcon.image = UIImage(named: "bright_off")
//            break
//        case .Crop:
//            cropLayoutIcon.image = UIImage(named: "crop_off")
//            break
//        case .Rotate:
//            rotateLayoutIcon.image = UIImage(named: "rotate_off")
//            break
//        default:
//            break
//        }
//        
//        for i in firstLayout.subviews
//        {
//            i.removeFromSuperview()
//        }
//    }
//    
//    @objc func cropLayoutAction(_ sender: AnyObject)
//    {
//        print("*** cropLayoutAction")
//        
//        if(mode == .Crop)
//        {
//            return
//        }
//        
//        self.delegate.changeTool(newMode: PhotoPreviewToolMode.Crop)
//        
//        self.delegate.showCropTool()
//        
//        disableActiveIcon()
//        
//        mode = .Crop
//        
//        cropLayoutIcon.image = UIImage(named: "crop_on")
//    }
//    
//    @objc func rotateLayoutAction(_ sender: AnyObject)
//    {
//        print("*** rotateLayoutAction")
//        
//        if(mode == .Rotate)
//        {
//            return
//        }
//        
//        self.delegate.changeTool(newMode: PhotoPreviewToolMode.Rotate)
//        
//        disableActiveIcon()
//        
//        mode = .Rotate
//        
//        rotateLayoutIcon.image = UIImage(named: "rotate_on")
//        
//        addRotateTools()
//    }
//    
//    @objc func brightLayoutAction(_ sender: AnyObject)
//    {
//        print("*** brightLayoutAction")
//        
//        if(mode == .Bright)
//        {
//            return
//        }
//        
//        self.delegate.changeTool(newMode: PhotoPreviewToolMode.Bright)
//        
//        disableActiveIcon()
//        
//        mode = .Bright
//        
//        brightLayoutIcon.image = UIImage(named: "bright_on")
//        
//        addBrightTolls()
//    }
//    
//    @objc func deleteLayoutAction(_ sender: AnyObject)
//    {
//        self.delegate.changeTool(newMode: PhotoPreviewToolMode.Bright)
//        
//        self.delegate.removeCurrentSlide()
//    }
//    
//    func prepareEditing()
//    {
//        let origin = self.delegate.getOriginImage()
//        
//        let acImage = CIImage(cgImage: origin.cgImage!)
//        
//        context = CIContext(options: nil)
//        
//        brightnessFilter = CIFilter(name: "CIColorControls");
//        brightnessFilter!.setValue(acImage, forKey: "inputImage")
//        brightnessFilter!.setValue(NSNumber(value: 0), forKey: "inputBrightness")
//    }
//    
//    var context: CIContext!
//    
//    var brightnessFilter: CIFilter!
//    
//    @objc func brightChanged(_ sender: UISlider)
//    {
//        print("*** brightChanged")
//        
//        DispatchQueue.main.async(execute: {
//            
//            let newValue = sender.value
//            
//            self.brightnessFilter!.setValue(NSNumber(value: newValue), forKey: "inputBrightness")
//            
//            let outputImage = self.brightnessFilter!.outputImage
//            
//            let imageRef = self.context.createCGImage(outputImage!, from: outputImage!.extent)
//            
//            let newUIImage = UIImage(cgImage: imageRef!)
//            
//            self.delegate.passNewImage(image: newUIImage)
//        })
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    
//    func Show()
//    {
//        self.delegate.changeTool(newMode: PhotoPreviewToolMode.Bright)
//        
//        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations:
//            {
//                self.setY(ScreenSize.SCREEN_HEIGHT - self.getHeight())
//        }, completion: { (result) -> () in
//            self.prepareEditing()
//        })
//    }
//    
//    func Hide(finish: @escaping () -> ())
//    {
//        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations:
//            {
//                self.setY(ScreenSize.SCREEN_HEIGHT)
//        }, completion: { (result) -> () in
//            self.removeFromSuperview()
//            
//            finish()
//        })
//    }
//}
