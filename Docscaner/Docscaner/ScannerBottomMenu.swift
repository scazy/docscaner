//
//  ScannerBottomMenu.swift
//  Docscaner
//
//  Created by Artem  on 14/03/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit

protocol ScannerBottomMenuDelegate
{
    func ScannerBottomMenuTakeShot()
    
    func ScannerBottomMenuSettings()
}

class ScannerBottomMenu: UIView
{
	var Height: CGFloat = (DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_Xr) ? 120 : 90
    
    var delegate: ScannerBottomMenuDelegate!
    
    var previewLayout: UIView!
    
    var buttonTake: UIView!
    
    var lastDocId = ""

	init()
	{
		super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: Height))

		let startOffset = UIApplication.shared.statusBarFrame.size.height

		var bg = UIImageView()
		bg.image = UIImage(named: "scanner_top_notfound")
		bg.setX(0)
		bg.setY(0)
		bg.setSize(self.getWidth(), self.getHeight())
		self.addSubview(bg)

		var buttonSize = self.getHeight() - 40

		buttonTake = UIView()
//		buttonTake.backgroundColor = .red
		buttonTake.setSize(buttonSize, buttonSize)
		buttonTake.toCenterX(self)
		buttonTake.setY(20)
		self.addSubview(buttonTake)

		var shotButton = UIView()
		shotButton.backgroundColor = .white
		shotButton.setSize(buttonSize - 14, buttonSize - 14)
		shotButton.toCenterX(buttonTake)
		shotButton.toCenterY(buttonTake)
		shotButton.layer.cornerRadius = shotButton.getWidth() / 2
		buttonTake.addSubview(shotButton)

		let circlePath = UIBezierPath(arcCenter: CGPoint(x: (buttonSize / 2), y: (buttonSize / 2)), radius: (buttonSize / 2) - 2, startAngle: 0, endAngle: .pi * 2, clockwise: true)

		let circleLayer = CAShapeLayer()
		circleLayer.lineWidth = 4
		circleLayer.path = circlePath.cgPath
		circleLayer.strokeColor = UIColor.white.cgColor
		circleLayer.fillColor = UIColor.clear.cgColor
		buttonTake.layer.addSublayer(circleLayer)

        var takeButtonAction = UIButton()
        takeButtonAction.setX(0)
        takeButtonAction.setY(0)
        takeButtonAction.setSize(buttonTake.getWidth(), buttonTake.getHeight())
        takeButtonAction.addTarget(self, action: "takeShot:", for: .touchUpInside)
        buttonTake.addSubview(takeButtonAction)
		//----

		var buttonSettings = UIButton()
		buttonSettings.setImage(UIImage(named: "scanner_settings"), for: .normal)
		buttonSettings.setSize(self.getHeight() * 0.27, self.getHeight() * 0.27)
		buttonSettings.setY(0)
		buttonSettings.center = buttonTake.center
		buttonSettings.setX(self.getWidth() - buttonSettings.getWidth() - 30)
        buttonSettings.addTarget(self, action: "buttonSettingsAction:", for: .touchUpInside)
		self.addSubview(buttonSettings)

		previewLayout = UIView()
		previewLayout.setSize(self.getHeight() * 0.5, self.getHeight() * 0.5)
//        previewLayout.backgroundColor = .red
		previewLayout.setX(0)
		previewLayout.setY(0)
		previewLayout.layer.cornerRadius = previewLayout.getWidth() / 2
		previewLayout.center = buttonTake.center
		previewLayout.setX(30)
		self.addSubview(previewLayout)

		var buttonPreview = UIButton()
		buttonPreview.setImage(UIImage(named: "scanner_preview"), for: .normal)
		buttonPreview.setSize(self.getHeight() * 0.27, self.getHeight() * 0.27)
		buttonPreview.toCenterX(previewLayout)
		buttonPreview.toCenterY(previewLayout)
        buttonPreview.addTarget(self, action: "openLastDoc:", for: .touchUpInside)
		previewLayout.addSubview(buttonPreview)
        
//        var previewButton = UIButton()
//
//        previewButton.setX(0)
//        previewButton.setY(0)
//        previewButton.setSize(previewLayout.getWidth(), previewLayout.getHeight())
//        previewLayout.addSubview(previewButton)
	}
    
    @objc func buttonSettingsAction(_ sender: AnyObject)
    {

        
        self.delegate.ScannerBottomMenuSettings()
    }
    
    @objc func openLastDoc(_ sender: AnyObject)
    {
        if(self.lastDocId.isEmpty)
        {
            return
        }
        
        EventsManager.shared().sendNotify(Constants.EVENT_OPEN_DOC, data: ["doc_id": self.lastDocId])
    }
    
    func hideButtonTake(_ hide: Bool)
    {
        buttonTake.isHidden = hide
    }
    
    func setPreview(_ image: UIImage, id: String)
    {
        self.removePreview()
        
        self.lastDocId = id
        
        var imagePreview = UIImageView()
        imagePreview.tag = -1001
        imagePreview.layer.cornerRadius = previewLayout.getWidth() / 2
        imagePreview.image = image
        imagePreview.setX(0)
        imagePreview.setY(0)
        imagePreview.setSize(previewLayout.getWidth(), previewLayout.getHeight())
        imagePreview.layer.contentsGravity = CALayerContentsGravity.resizeAspectFill
        imagePreview.layer.masksToBounds = true
        previewLayout.addSubview(imagePreview)
        
        previewLayout.sendSubviewToBack(imagePreview)
    }
    
    func removePreview()
    {
        if let iv = self.viewWithTag(-1001) as? UIImageView
        {
            iv.removeFromSuperview()
        }
    }

	required init?(coder aDecoder: NSCoder)
    {
		fatalError("init(coder:) has not been implemented")
	}
    
    @objc func takeShot(_ sender: AnyObject)
    {
        print("** TAKE SHOT")
        self.delegate.ScannerBottomMenuTakeShot()
    }
}
