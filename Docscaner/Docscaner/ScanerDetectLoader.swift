//
//  ScanerDetectLoader.swift
//  Docscaner
//
//  Created by artem on 03/04/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit

class ScanerDetectLoader: UIView
{
    private var _duration: CGFloat = 0
    
    init(width: CGFloat, height: CGFloat, duration: CGFloat = 3)
    {
        super.init(frame: CGRect(x: 0, y: 0, width: width, height: height))
        
        _duration = duration
        
        configureCircleLayer()
        startCircleAnimation()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    var circleLayer = CAShapeLayer()
    var displayLink: CADisplayLink!
    var startTime: CFTimeInterval!
    
    func configureCircleLayer()
    {
        circleLayer.fillColor = UIColor(hexString: "#ffffff")?.cgColor
        self.layer.addSublayer(circleLayer)
        updatePath(percent: 0)
    }
    
    func startCircleAnimation()
    {
        startTime = CACurrentMediaTime()
        
        displayLink = CADisplayLink(target: self, selector: #selector(handleDisplayLink(_:)))
        
        displayLink.add(to: .current, forMode: .common)
    }
    
    @objc func handleDisplayLink(_ displayLink: CADisplayLink)
    {
        let percent = CGFloat(CACurrentMediaTime() - startTime) / (_duration - 1)
        
        updatePath(percent: min(percent, 1.0))
        
        if percent > 1.0
        {
            displayLink.invalidate()
        }
    }
    
    private func updatePath(percent: CGFloat)
    {
        let w = self.bounds.width
        let center = CGPoint(x: w/2, y: w/2)
        let startAngle: CGFloat = -0.25 * 2 * .pi
        let endAngle: CGFloat = startAngle + percent * 2 * .pi
        let path = UIBezierPath()
        path.move(to: center)
        path.addArc(withCenter: center, radius: w/2, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        path.close()
        
        circleLayer.path = path.cgPath
    }
    
    func Stop()
    {
        displayLink?.invalidate()
    }
}
