//
//  Core.swift
//  Docscaner
//
//  Created by Artem  on 28/02/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import RealmSwift
import Alamofire
import Reachability
import SwiftyStoreKit
import LocalAuthentication

enum PremiumStatus: Int
{
	case Free = 1
	case Trial
	case Premium
}

class PackageSubs
{
	var PurchaseId = ""
	var PriceLocale: Double = 0
	var Currency = ""
}

enum PremiumType: Int
{
	case Regular = 1
	case Gift
}

class Core: NSObject
{
    var isPromoCloseActivated = false
    
    var IsPromoNext = false
    
	var isReceptValidated = false

	var hadSubscription = false

	let productIds = Set([ "com.asim.docscanner.monthly",
						   "com.asim.docscanner.weekly1",
						   "com.asim.docscanner.year",
						   "com.asim.docscanner.6Months"])

	var IsDebug = false

	var Subscriptions = [PackageSubs]()

	var SCHEMA_VERSION: UInt64 = 11

	var CurrentController = ""

	let defaults = UserDefaults.standard

	private static let sharedi = Core()

	private var _window: UIWindow!

	var _realm: Realm!

	var reachability: Reachability!

	var RootWindow: UIWindow
	{
		get
		{
			return _window
		}
	}

	var DeviceId = ""

	public static func shared() -> Core
	{
		return sharedi
	}

	var Config: [String: AnyObject]
	{
		get
		{
			if let path = Bundle.main.path(forResource: "Info", ofType: "plist"), let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject]
			{
				return dict
			}
			else
			{
				return [String: AnyObject]()
			}
		}
	}

	func Init(window: UIWindow)
	{
		DispatchQueue.main.async(execute: {

			print("** Core Init 1")

			self.addObservers()

			self._window = window

			self._window.backgroundColor = UIColor.white

			self.reachability = Reachability()!

			UINavigationBar.appearance().tintColor = UIColor.white

			print("** Core Init 2")

			let mainController = MainController()

			print("** Core Init 3")

			self._window.rootViewController = mainController

			self._window.makeKeyAndVisible()
		})
	}

	func initDB()
	{
		let config = Realm.Configuration(
			schemaVersion: SCHEMA_VERSION
		)

		Realm.Configuration.defaultConfiguration = config

		self._realm = try! Realm()

		let folderPath = self._realm.configuration.fileURL!.deletingLastPathComponent().path

		print("** REALM PATH \(folderPath)/default.realm")

		try! FileManager.default.setAttributes([FileAttributeKey.protectionKey: FileProtectionType.none], ofItemAtPath: folderPath)
	}

	var getRealm: Realm
	{
		get
		{
			if(self._realm == nil)
			{
				self.initDB()
			}

			return self._realm
		}
	}

	func addObservers()
	{

	}

	func changeViewController(name: String, params: [String: Any] = [String: Any]())
	{
		print("\n*** changeViewController NAME \(name) PARAMS \(params)")
		DispatchQueue.main.async(execute: {
			EventsManager.shared().sendNotify(Constants.EVENT_CHANGE_CONTROLLER, object: ["controller": name, "params": params])
		})

	}

	func openView(name: String, params: [String: Any] = [String: Any]())
	{
		DispatchQueue.main.async(execute: {
			EventsManager.shared().sendNotify(Constants.EVENT_OPEN_VIEW, object: ["view": name, "params": params])
		})
	}

	func removeViewController(name: String)
	{
		DispatchQueue.main.async(execute: {
			EventsManager.shared().sendNotify(Constants.EVENT_REMOVE_CONTROLLER, object: ["controller": name])
		})
	}

	var IsConnecting: Bool
	{
		get
		{
			if(self.NetworkStatus.connection == .none)
			{
				return false
			}

			return true
		}
	}

	var NetworkStatus: Reachability
	{
		get
		{
			return reachability
		}
	}

	func startAutorization(finish: @escaping (_ success: Bool) -> ())
	{
		print("** START AUTH 1")

		for sub in Core.shared().productIds
		{
			let m = SubscriptionModel()
			m.PurchaseId = sub

			try! Core.shared().getRealm.write {
				Core.shared().getRealm.add(m, update: true)
			}
		}

		//		self.registerProducts()
        
        EventsManager.shared().sendNotify(Constants.START_REGISTER_PRODUCTS)

        if(Core.shared().IsDebug)
        {
            finish(true)
            return
        }
        
		//---
		

//        finish(true)
//
//        return

		print("** START AUTH 2")

		let headers: HTTPHeaders = [
			"userId": Core.shared().DeviceId,
			"token": (Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String).md5()
		]

		let parameters: Parameters = ["receipt": ""]

//        Alamofire.request(Constants.SERVICE_API + "auth", method: .post, parameters: parameters, headers: headers).responseString { response in
//            print(response)
//        }

		Alamofire.request(Constants.SERVICE_API + "auth", method: .post, parameters: parameters, headers: headers).responseJSON { response in
			print("AUTH")

			if let json = response.result.value
			{
				var jsonData = JSON(json)
				print(jsonData)

				let success = jsonData["success"].boolValue

				if(success == true)
				{
                    var user = jsonData["user"].dictionaryValue
                    let token = user["token"]?.stringValue ?? ""
                    
                    print("** TOKEN \(token)")
//                    //
//                    if(token.isEmpty)
//                    {
//                        finish(true)
//                    }
//                    else
//                    {
//                        var info = jsonData["ip_info"].dictionaryValue
//
//
//                        if let env = Core.shared().getRealm.objects(EnvModel.self).filter("Id = 1").first
//                        {
//
//                        }
//                        else
//                        {
//                            let env = EnvModel()
//                            env.Id = 1
//                            env.city = info["city"]?.stringValue ?? ""
//                            env.country_code = info["country_code"]?.stringValue ?? ""
//                            env.ip = info["ip"]?.stringValue ?? ""
//                            env.country_name = info["country_name"]?.stringValue ?? ""
//                            env.deeplinks = Core.shared().defaults.value(forKey: "deep_link") as? String ?? ""
//
//                            try! Core.shared().getRealm.write {
//                                Core.shared().getRealm.add(env, update: true)
//                            }
//
////                            Core.shared().sendInstall()
//                        }
//
//
////                        Core.shared().getReceiptStatus { (success) in
                    
                    Core.shared().getReceiptStatus { (success) in
                        if let model = Core.shared().getRealm.objects(UserModel.self).first
                        {
                            try! Core.shared().getRealm.write {
                                model.Token = token
                            }
                        }
                        else
                        {
                            var a = UserModel()
                            a.Id = 1
                            a.Token = token
                            
                            try! Core.shared().getRealm.write {
                                Core.shared().getRealm.add(a, update: true)
                            }
                            
                        }
                        
                        Core.shared().checkPremium {
                            finish(true)
                        }
                        
                        
                    }
				}
				else
				{
					finish(false)
				}
			}
			else
			{
				finish(false)
			}
		}
	}
    
    func checkPremium(finish: @escaping ()->())
    {
        Core.shared().getReceiptStatus { (success) in
            if let model = Core.shared().getRealm.objects(UserModel.self).first
            {
                try! Core.shared().getRealm.write {
                    model.IsPremium = success
                }
            }
            else
            {
                var a = UserModel()
                a.Id = 1
                a.IsPremium = success
                
                try! Core.shared().getRealm.write {
                    Core.shared().getRealm.add(a, update: true)
                }
            }
            
            finish()
        }
    }

    var isPremium: Bool
    {
        get
        {
            if let model = Core.shared().getRealm.objects(UserModel.self).first
            {
                return model.IsPremium
            }
            
            return false
        }
    }
    
    func clearReceiptStatus()
    {
        Core.shared().isReceptValidated = false
        
        Core.shared().hadSubscription = false
    }
    
    func ValidateRecept(completion: @escaping (_ success: Bool) -> ())
    {
        print("*** ValidateRecept 1")
        
        
        if(Core.shared().IsDebug)
        {
            completion(true)
            
            print("*** ValidateRecept 2")
            
            return
        }
        
        print("*** ValidateRecept 1")
        if(Core.shared().isReceptValidated)
        {
            print("*** ValidateRecept 3")
            //            print("*** ValidateRecept 2")
            completion(Core.shared().hadSubscription)
        }
        else
        {
            print("*** ValidateRecept 4")
            Core.shared().getReceiptStatus { (success) in
                //                print("*** ValidateRecept 3")
                
                print("*** ValidateRecept 5")
                
                Core.shared().isReceptValidated = true
                Core.shared().hadSubscription = success
                completion(success)
            }
        }

    }
    
    private func getReceiptStatus(completion: @escaping (_ success: Bool) -> ())
    {
        
        //        Core.shared().clearReceiptStatus()
        
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: Constants.ITUNES_SECRET)
        
        SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
            switch result {
            case .success(let receipt):
                print("Receipt verification ok")
                
                let purchaseResult = SwiftyStoreKit.verifySubscriptions(productIds: self.productIds, inReceipt: receipt)
                
                var isOk = false
                
                switch purchaseResult {
                case .purchased(let expiryDate, let items):
                    print("is valid until \(expiryDate) * \(expiryDate.timeIntervalSince1970) * \(NSDate().timeIntervalSince1970)")
                    
                    if(expiryDate.timeIntervalSince1970 < NSDate().timeIntervalSince1970)
                    {
                        isOk = false
                    }
                    else
                    {
                        isOk = true
                    }
                    
                    
                case .expired(let expiryDate, let items):
                    print("is expired since \(expiryDate)\n")
                case .notPurchased:
                    print("The user has never purchased ")
                }
                
                var status = ReceiptStatus()
                status.Id = 1
                status.Status = 1
                
                try! Core.shared().getRealm.write {
                    Core.shared().getRealm.add(status, update: true)
                }
                
                completion(isOk)
                
            case .error(let error):
                print("Receipt verification failed: \(error)")
                
                var status = ReceiptStatus()
                status.Id = 1
                status.Status = 0
                
                try! Core.shared().getRealm.write {
                    Core.shared().getRealm.add(status, update: true)
                }
                
                completion(false)
            }
        }
    }
    
    func getBiometricType() -> BiometricType
    {
        let context = LAContext()
//        if #available(iOS 11, *)
//        {
//            let _ = authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
//            switch(authContext.biometryType)
//            {
//            case .none:
//                return .none
//            case .touchID:
//                return .touch
//            case .faceID:
//                return .face
//            @unknown default:
//                return .none
//            }
//        }
//        else
//        {
//            return authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) ? .touch : .none
//        }
        
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error)
        {
            return .touch
        }
        else
        {
            return .none
        }
    }
    
    func sendAppReceipt(_ receipt: String)
    {
        print("*** sendAppReceipt")
        //        print(receipt)
        
        let headers: HTTPHeaders = [
            "device_id": Core.shared().DeviceId,
            "token": (Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String).md5()
        ]
        
        let parameters: Parameters = ["receipt": receipt]
        
        print(parameters)
        
        Alamofire.request(Constants.METRICS_URL + "metrica/subscription", method: .post, parameters: parameters, headers: headers).responseJSON  { response in
            print("RECEIPT RESPONSE")
            print(response)
            
        }
    }
    
    let queue = DispatchQueue(label: "ru.rustruck.Palmistry.response-queue", qos: .utility, attributes: [.concurrent])
    
    func sendAppMetrics(_ name: AppMetricsEvent, type: AppMetricsType, params: [String: Any] = [String: Any]())
    {
        let headers: HTTPHeaders = [
            "device_id": Core.shared().DeviceId,
            "token": (Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String).md5()
        ]
        
        let parameters: Parameters = ["event": name.rawValue, "type": type.rawValue, "params": params]
        
        print("METRICS REQUEST params \(parameters)")
        
        Alamofire.request(Constants.METRICS_URL + "metrica/target", method: .post, parameters: parameters, headers: headers).responseJSON { (response) in
            
        }
    }
    
    func sendInstall()
    {
        if let env = Core.shared().getRealm.objects(EnvModel.self).filter("Id = 1").first
        {
            var locale = "en" //NSLocale.preferredLanguages[0]
            
            
            let headers: HTTPHeaders = [
                "device_id": Core.shared().DeviceId,
                "token": (Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String).md5(),
                "country": env.country_name,
                "country_code": env.country_code,
                "ip": env.ip,
                "city": env.city,
                "device_model": UIDevice.current.modelName,
                "locale": locale,
                "ios_version": UIDevice.current.systemVersion,
                "build_id": Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
            ]
            
            let parameters: Parameters = ["deeplinks": env.deeplinks]
            
            print("METRICS REQUEST params \(parameters)")
            
            Alamofire.request(Constants.METRICS_URL + "metrica/install", method: .post, parameters: parameters, headers: headers).responseJSON { (response) in
                
            }
        }
    }
}

protocol ViewControllerDelegate
{
	func setParams(params: [String: Any])
	func willBeAppear()
	func willBeDisappear()
}

public class ControllerModel
{
	var _name: String = ""
	var _controller: UIViewController!

	init(name: String, controller: UIViewController)
	{
		_name = name
		_controller = controller
	}

	public var Name: String
	{
		get
		{
			return self._name
		}
	}

	public var Controller: UIViewController
	{
		get
		{
			return self._controller
		}
	}
}


enum BiometricType {
    case none
    case touch
    case face
}

enum AppMetricsType: String
{
    case System = "system"
    case User = "user"
    case Purchase = "purchase"
}

enum AppMetricsEvent: String
{
    case UserPushDisabled = "user_push_disabled"
    case UserPushEnabled = "user_push_enabled"
    case PurchaseSubscribtionInit = "purchase_subscribtion_init"
    case PurchaseSubscriptionPaid = "purchase_subscription_paid"
    case PurchaseSubscriptionFailed = "purchase_subscription_failed"
    case PurchaseSubscriptionError = "purchase_subscription_error"
    case PurchaseSubscriptionRestore = "purchase_subscription_restore"
    case UserChangeController = "user_change_controller"
    case UserChangePromo = "user_change_promo"
    case UserSettingsEdit = "user_settings_edit"
    case UserInitView = "user_init_view"
    case UserShowPremium = "user_show_premium"
    case UserShowPremiumGift = "user_show_premium_gift"
}
