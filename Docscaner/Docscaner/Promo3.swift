//
//  Promo1.swift
//  Docscaner
//
//  Created by artem on 25/04/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit


class Promo3: UIView, PromoActions
{
    var delegate: PromoDelegate!

    
    func Show()
    {
        self.isHidden = false
        
        UIView.animate(withDuration: timeout, animations: {
            self.promo_1_img.toCenterX(self)
            self.promo_1_icon.toCenterX(self)
            self.mainTitle.toCenterX(self)
            self.mainSubTitle.toCenterX(self)
            self.promoBG.setX(0)
        }) { (success) in
            Core.shared().sendAppMetrics(.UserChangePromo, type: .User, params: ["name": "promo3"])
        }
    }
    
    func Hide(finish: @escaping () -> ())
    {
        UIView.animate(withDuration: 0.5, animations: {
            self.promo_1_img.setX(self.promo_1_img.getWidth() * -1)
            self.promo_1_icon.setX(self.promo_1_icon.getWidth() * -1)
            self.mainTitle.setX(self.mainTitle.getWidth() * -1)
            self.mainSubTitle.setX(self.mainSubTitle.getWidth() * -1)
            self.promoBG.setX(self.promoBG.getWidth() * -1)
        }) { (success) in
            //            self.promoBG.setY(0)
            self.isHidden = true
            
            finish()
        }
    }
    
    
    
    var promo_1_img: UIImageView!
    
    var promoBG: UIImageView!
    
    var promo_1_icon: UIImageView!
    
    var mainTitle: UILabel!
    
    var mainSubTitle: UILabel!
    
    var nextLayout: UIView!
    
    var dotLayout: UIView!
    
    var dotLayout2: UIView!
    
    var dotLayout3: UIView!
    
    var timeout: TimeInterval = 0.7
    
    init(_ timeout: TimeInterval = 0.7, _ delegate: PromoDelegate)
    {
        super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))
        
        self.delegate = delegate
        
        self.timeout = timeout
        
        let startOffset = UIApplication.shared.statusBarFrame.size.height
        
//        self.alpha = 0
        
        self.isHidden = true
        
        var fontSize: CGFloat = 22
        var fontSize2: CGFloat = 20
        var tOff: CGFloat = 40
        var tOff2: CGFloat = 20
        
        if(DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P)
        {
            fontSize = 19
            fontSize2 = 17
            tOff = 30
            tOff2 = 10
        }
        
        var promo1 = UIImage(named: "promo_3")
        var imageSize = getImageSize(image: promo1!, width: 0, height: ScreenSize.SCREEN_HEIGHT / 2)
        
        promo_1_img = UIImageView()
        promo_1_img.image = promo1
        promo_1_img.setY(0)
        promo_1_img.setSize(imageSize[0], imageSize[1])
        promo_1_img.setX(ScreenSize.SCREEN_WIDTH)
        self.addSubview(promo_1_img)
        
        promoBG = UIImageView()
        promoBG.setX(ScreenSize.SCREEN_WIDTH)
        promoBG.setY(0)
        promoBG.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT * 0.4)
        promoBG.image = UIImage(named: "promo_bg")
        self.addSubview(promoBG)
        
        var iconsImage = UIImage(named: "promo_3_icons")
        var iconsSize = getImageSize(image: iconsImage!, width: 0, height: promo_1_img.getHeight() * 0.75)
        
        promo_1_icon = UIImageView()
        promo_1_icon.image = iconsImage
        promo_1_icon.setSize(iconsSize[0], iconsSize[1])
        promo_1_icon.setX(ScreenSize.SCREEN_WIDTH)
        promo_1_icon.setY(promo_1_img.getHeight() / 7)
        self.addSubview(promo_1_icon)
        
        //----
        
        var mainText = s("promo_3_title")
        var mainTitleSize = getLabelSizeForFont(str: mainText, size: fontSize, fontName: "Circe-Bold", setWidth: ScreenSize.SCREEN_WIDTH - 80)
        
        mainTitle = UILabel()
        mainTitle.setY(tOff, relative: promo_1_img)
        mainTitle.setSize(mainTitleSize.width, mainTitleSize.height)
        mainTitle.setX(ScreenSize.SCREEN_WIDTH)
        mainTitle.text = mainText
        mainTitle.font = UIFont(name: "Circe-Bold", size: fontSize)
        mainTitle.textAlignment = .center
        mainTitle.textColor = UIColor.white
        self.addSubview(mainTitle)
        
        
        var mainSubText = s("promo_3_subtitle")
        var mainSubitleSize = getLabelSizeForFont(str: mainSubText, size: fontSize2, fontName: "Circe-Regular", setWidth: ScreenSize.SCREEN_WIDTH - 80)
        
        mainSubTitle = UILabel()
        mainSubTitle.numberOfLines = 0
        mainSubTitle.setY(tOff2, relative: mainTitle)
        mainSubTitle.setSize(mainSubitleSize.width, mainSubitleSize.height)
        mainSubTitle.setX(ScreenSize.SCREEN_WIDTH)
        mainSubTitle.text = mainSubText
        mainSubTitle.font = UIFont(name: "Circe-Regular", size: fontSize2)
        mainSubTitle.textAlignment = .center
        mainSubTitle.textColor = UIColor.white
        self.addSubview(mainSubTitle)
        
        

    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
}
