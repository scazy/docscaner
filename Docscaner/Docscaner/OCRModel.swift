//
//  OCRModel.swift
//  Docscaner
//
//  Created by artem on 24/04/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import RealmSwift

class OCRModel: Object
{
    override static func primaryKey() -> String?
    {
        return "Id"
    }
    
    @objc dynamic var Id = ""
    
    @objc dynamic var Name = ""
    
    @objc dynamic var IsChecked = false
}
