//
//  PhotoScanner.swift
//  Scanner
//
//  Created by Artem  on 27/09/2018.
//  Copyright © 2018 VladimirKuzmin. All rights reserved.
//ise

import Foundation
//import TesseractOCR
import UIKit
import SwiftyJSON

class OCRScanner: UIView
{
    var files = [DocModelFile]()
    
    //    var bottom: UIView!
    
    var top: UIView!
    
    var startOffset: CGFloat = 0
    
    var textEdit: UITextView!
    
    var processingLayout: UIView!
    
    //    var recognizedText = ""
    
    var keyboardHeight: CGFloat = 0
    
    var isKeyboard = false
    
    var _doc: DocModel!
    
    var currentId = 0
    
    @objc func keyboardWillShow(_ sender: NSNotification)
    {
        print("*** keyboardWillShow isKeyboard \(isKeyboard)")
        
        if(isKeyboard)
        {
            return
        }
        
        DispatchQueue.main.async(execute: {
            
            if let userInfo = (sender as NSNotification).userInfo
            {
                self.isKeyboard = true
                
                self.keyboardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect).size.height
                
                //                self.textEdit.setSize(self.getWidth() - 20, self.getHeight() - self.top.getHeight() -  self.keyboardHeight - 40)
                
                self.layout.setSize(ScreenSize.SCREEN_WIDTH - 40, ScreenSize.SCREEN_HEIGHT - self.top.getHeight() - 40 - self.keyboardHeight)
                
                self.textEdit.setSize(self.layout.getWidth() - 20, self.layout.getHeight() - 20)
                
                self.textEdit.setY(10)
            }
        })
    }
    
    @objc func keyboardWillHide(_ sender: NSNotification)
    {
        print("*** keyboardWillHide")
        
        if(!self.isKeyboard)
        {
            return
        }
        
        self.endEdit()
    }
    
    func endEdit()
    {
        self.keyboardHeight = 0
        //
        self.isKeyboard = false
        //
        layout.setX(20)
        layout.setY(20, relative: top)
        layout.setSize(ScreenSize.SCREEN_WIDTH - 40, ScreenSize.SCREEN_HEIGHT - top.getHeight() - 40)
        
        textEdit.setX(10)
        textEdit.setY(10)
        textEdit.setSize(layout.getWidth() - 20, layout.getHeight() - 20)
        //
        self.endEditing(true)
    }
    
    @objc func leftSwipe(_ sender: AnyObject)
    {
        print("*** DOC leftSwipe")
        self.endEdit()
        
        if(currentId == files.count - 1)
        {
            return
        }
        
        saveCurrentPage()
        
        currentId += 1
        
        self.textEdit.text = self.files[currentId].ORCText
        
        libTitle.text = s("page_title") + " \(currentId + 1)/\(files.count)"
    }
    
    @objc func rightSwipe(_ sender: AnyObject)
    {
        print("*** DOC rightSwipe")
        
        self.endEdit()
        
        if(currentId == 0)
        {
            return
        }
        
        saveCurrentPage()
        
        currentId -= 1
        
        self.textEdit.text = self.files[currentId].ORCText
        
        libTitle.text = s("page_title") + " \(currentId + 1)/\(files.count)"
    }
    
    var layout: UIView!
    
    var _id = ""
    
    init(id: String)
    {
        super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))
        
        _id = id
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.backgroundColor = UIColor.black
        
        startOffset = UIApplication.shared.statusBarFrame.size.height
        
        initTop()
        
        layout = UIView()
        layout.backgroundColor = UIColor.white
        layout.setX(20)
        layout.setY(20, relative: top)
        layout.setSize(ScreenSize.SCREEN_WIDTH - 40, ScreenSize.SCREEN_HEIGHT - top.getHeight() - 40)
        self.addSubview(layout)
        
        //-----
        
        //        initBottom()
        
        textEdit = UITextView()
        //        textEdit.isEditable = false
        //        textEdit.isSelectable = false
        textEdit.backgroundColor = UIColor.white
        textEdit.font = UIFont.systemFont(ofSize: 12)
        textEdit.setX(10)
        textEdit.setY(10)
        textEdit.setSize(layout.getWidth() - 20, layout.getHeight() - 20)
        layout.addSubview(textEdit)
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.leftSwipe(_:)))
        leftSwipe.direction = UISwipeGestureRecognizer.Direction.left
        textEdit.addGestureRecognizer(leftSwipe)
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.rightSwipe(_:)))
        rightSwipe.direction = UISwipeGestureRecognizer.Direction.right
        textEdit.addGestureRecognizer(rightSwipe)
        
        if let doc = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(id)'").first
        {
            self._doc = doc
            
            print(self._doc)
            
            for i in Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(id)'")
            {
                files.append(i)
            }
            
            libTitle.text = s("page_title") + " 1/\(files.count)"
            
            var tick = 0
            
            if(self.files[0].ORCText.count == 0)
            {
                initProcessing2()
                
                self.processingTitle2.text = s("recognizing_title")
                
                for fileName in self.files
                {
                    
                    print("** START OCR")
                    
                    let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)
                    
                    let path = docDirURL.appendingPathComponent(fileName.FileName)
                    
                    let data = try? Data(contentsOf: path)
                    
                    var imageData = UIImage(data: data!)!
                    
                    let imgData = imageData.jpegData(compressionQuality: 1)
                    
                    let imagefile = HTTPFile.data("imagefromios.jpg", imgData!, "image/jpeg")
                    
                    var total = 0
                    
                    var token = ""
                    
                    if let userData = Core.shared().getRealm.objects(UserModel.self).filter("Id = 1").first
                    {
                        token = userData.Token
                    }
                    
                    print("** USER TOKEN \(token)")
                    
                    if(token.isEmpty)
                    {
                        return
                    }
                    
                    var ocr = [String]()
                    
                    for i in Core.shared().getRealm.objects(OCRModel.self).filter("IsChecked = true")
                    {
                        ocr.append(i.Id)
                    }
                    
                    var ocr_joined = ocr.joined(separator: "+")
                    
                    Just.post(Constants.SERVICE_API + "tesseract/recognize", data: ["lang": ocr_joined], headers: [
                        "userId": Core.shared().DeviceId,
                        "token": (Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String).md5(),
                        "userToken": token
                        ], files: ["image": imagefile],
                           asyncProgressHandler: { (progress) -> Void in
                            
                            DispatchQueue.main.async(execute: {
                                print(progress)
                                
                                if(total == 0)
                                {
                                    total = Int(progress.bytesExpectedToProcess)
                                }
                                
                                let current = Int(progress.bytesProcessed) * 100 / total
                                
                                print("\(current)% BYTES \(progress.bytesProcessed)")
                            })
                            
                    }) { (result) -> Void in
                        DispatchQueue.main.async(execute: {
                            
                            if result.ok
                            {
                                print(result)
                                print(result.json)
                                
                                let jsonData = JSON(result.json!)
                                var success = jsonData["success"].boolValue
                                
                                if(success)
                                {
                                    var text = jsonData["response"].dictionaryValue["text"]?.stringValue ?? ""
                                    
                                    //                                    self.recognizedText = text
                                    //
                                    //                                    self.textEdit.text = self.recognizedText
                                    //
                                    try! Core.shared().getRealm.write {
                                        fileName.ORCText = text
                                        //                                        self._doc.ORCText = self.recognizedText
                                    }
                                    
                                    tick += 1
                                    
                                    if(tick == 1)
                                    {
                                        self.textEdit.text = text
                                    }
                                    
                                    if(tick == self.files.count)
                                    {
                                        //                                        var text = ""
                                        //
                                        //                                        for i in self.files
                                        //                                        {
                                        //                                            text += i.ORCText + "\n\n"
                                        //                                        }
                                        //
                                        //
                                        //                                        try! Core.shared().getRealm.write {
                                        //                                            self._doc.ORCText = text
                                        //                                        }
                                        //
                                        //                                        self.textEdit.text = text
                                        
                                        self.processingLayout2.removeFromSuperview()
                                    }
                                }
                                else
                                {
                                    var responseError = jsonData["response"].dictionaryValue["text"]?.stringValue ?? ""
                                    
                                    self.processingLayout2.removeFromSuperview()
                                    
                                    let alertController = UIAlertController(title: s("ocr_error"), message: s("ocr_error_desc"), preferredStyle: .alert)
                                    
                                    let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                                        (action : UIAlertAction!) -> Void in
                                    })
                                    
                                    alertController.addAction(okAction)
                                    
                                    self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                                }
                            }
                        })
                    }
                }
            }
            else
            {
                //                self.recognizedText = self._doc.ORCText
                self.textEdit.text = files[currentId].ORCText
            }
        }
    }
    
    var processingLayout2: UIView!
    var processingTitle2: UILabel!
    
    func initProcessing2()
    {
        processingLayout2 = UIView()
        processingLayout2.backgroundColor = UIColor.init(red: 0.21, green: 0.42, blue: 0.69, alpha: 1)
        processingLayout2.setX(0)
        processingLayout2.setY(0)
        processingLayout2.setSize(self.getWidth(), self.getHeight())
        self.addSubview(processingLayout2)
        
        //----
        
        let circlePath = UIBezierPath(arcCenter: processingLayout2.center, radius: 40, startAngle: 0, endAngle: .pi * 2, clockwise: true)
        
        let animation = CAKeyframeAnimation(keyPath: #keyPath(CALayer.position))
        animation.duration = 1.5
        animation.repeatCount = MAXFLOAT
        animation.path = circlePath.cgPath
        
        let ball = UIView()
        ball.layer.cornerRadius = 6
        ball.frame = CGRect(x: 0, y: 0, width: 12, height: 12)
        ball.backgroundColor = .white
        processingLayout2.addSubview(ball)
        ball.layer.add(animation, forKey: nil)
        
        let circleLayer = CAShapeLayer()
        circleLayer.lineWidth = 2
        circleLayer.path = circlePath.cgPath
        circleLayer.strokeColor = UIColor.white.cgColor
        circleLayer.fillColor = UIColor.clear.cgColor
        
        processingLayout2.layer.addSublayer(circleLayer)
        
        processingTitle2 = UILabel()
        processingTitle2.text = ""
        processingTitle2.setX(0)
        processingTitle2.setSize(processingLayout2.getWidth(), 20)
        processingTitle2.textColor = UIColor.white
        processingTitle2.textAlignment = .center
        processingTitle2.font = UIFont.systemFont(ofSize: 18)
        processingTitle2.setY((processingLayout2.getHeight() / 2) + 20 + 40)
        processingLayout2.addSubview(processingTitle2)
        
        //----
        
        let back = UIView()
        back.setX(5)
        back.setY(startOffset + 20)
        back.setSize(30, 30)
        processingLayout2.addSubview(back)
        
        let icon = UIImageView()
        icon.setSize(12, 50 / 2.5)
        icon.toCenterX(back)
        icon.toCenterY(back)
        icon.image = UIImage(named: "left_menu_back")
        back.addSubview(icon)
        
        var titleCancel = UILabel()
        titleCancel.setX(5, relative: back)
        titleCancel.setY(startOffset + 20)
        titleCancel.setSize(ScreenSize.SCREEN_WIDTH / 2, 30)
        titleCancel.textColor = .white
        titleCancel.text = s("botton_cancel")
        titleCancel.font = UIFont.systemFont(ofSize: 18)
        processingLayout2.addSubview(titleCancel)
        
        let buttonCancel = UIButton()
        buttonCancel.addTarget(self, action: "cancelProcessing:", for: .touchUpInside)
        buttonCancel.setX(0)
        buttonCancel.setY(startOffset + 20)
        buttonCancel.setSize(ScreenSize.SCREEN_WIDTH / 2, 30)
        processingLayout2.addSubview(buttonCancel)
    }
    
    @objc func cancelProcessing(_ sender: AnyObject)
    {
        if(processingLayout2 != nil)
        {
            processingLayout2.removeFromSuperview()
        }
    }
    
    func initProcessing()
    {
        processingLayout = UIView()
        processingLayout.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        processingLayout.setX(0)
        processingLayout.setY(0)
        processingLayout.setSize(self.getWidth(), self.getHeight())
        self.addSubview(processingLayout)
        
        var waitingLoader = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        waitingLoader.setSize(50, 50)
        waitingLoader.toCenterX(processingLayout)
        waitingLoader.toCenterY(processingLayout)
        waitingLoader.startAnimating()
        processingLayout.addSubview(waitingLoader)
        
        processingTitle = UILabel()
        processingTitle.textAlignment = .center
        processingTitle.textColor = .white
        processingTitle.font = UIFont.systemFont(ofSize: 16)
        processingTitle.setX(10)
        processingTitle.setY(10, relative: waitingLoader)
        processingTitle.setSize(processingLayout.getWidth() - 20, 20)
        processingTitle.text = s("recognize_processing")
        processingLayout.addSubview(processingTitle)
    }
    
    func initTop()
    {
        top = UIView()
        top.backgroundColor = UIColor.init(red: 0.21, green: 0.42, blue: 0.69, alpha: 1)
        top.setX(0)
        top.setY(0)
        top.setSize(ScreenSize.SCREEN_WIDTH, 60 + startOffset)
        top.clipsToBounds = true
        self.addSubview(top)
        
        //        let layer1 = CAGradientLayer()
        //        layer1.colors = [
        //            UIColor(red: 0.13, green: 0.64, blue: 0.85, alpha: 1).cgColor,
        //            UIColor(red: 0.13, green: 0.85, blue: 0.76, alpha: 1).cgColor
        //        ]
        //
        //        layer1.locations = [0, 1]
        //        layer1.startPoint = CGPoint(x: 0.25, y: 0.5)
        //        layer1.endPoint = CGPoint(x: 0.75, y: 0.5)
        //        layer1.transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 1, b: 1, c: -1, d: 1, tx: 0, ty: 0))
        //        layer1.bounds = top.bounds.insetBy(dx: -0.5 * (top.bounds.size.width * 2), dy: -0.5 * (top.bounds.size.height * 2))
        //        layer1.position = top.center
        //        top.layer.addSublayer(layer1)
        ///------
        
        let iconSize: CGFloat = 50 / 2.5
        
        //        let editLayout = UIView()
        //        //        editLayout.backgroundColor = .red
        //        editLayout.setX(top.getWidth() - 50 - 50 - 10)
        //        editLayout.setY(startOffset + 20)
        //        editLayout.setSize(50, 30)
        //        top.addSubview(editLayout)
        ////
        //        var editIcon = UIImageView()
        //        editIcon.setSize(iconSize, iconSize)
        //        editIcon.toCenterX(editLayout)
        //        editIcon.toCenterY(editLayout)
        //        editIcon.image = UIImage(named: "lib_icon_edit")
        //        editLayout.addSubview(editIcon)
        //
        //        let editAction = UIButton()
        //        editAction.setX(0)
        //        editAction.setY(0)
        //        editAction.setSize(editLayout.getWidth(), editLayout.getHeight())
        //        editLayout.addSubview(editAction)
        
        let saveLayout = UIView()
        saveLayout.setX(top.getWidth() - 50 - 10)
        saveLayout.setY(startOffset + 20)
        saveLayout.setSize(50, 30)
        top.addSubview(saveLayout)
        
        var saveIcon = UIImageView()
        saveIcon.setSize(iconSize, iconSize)
        saveIcon.toCenterX(saveLayout)
        saveIcon.toCenterY(saveLayout)
        saveIcon.image = UIImage(named: "image_save")
        saveLayout.addSubview(saveIcon)
        
        let saveAction = UIButton()
        saveAction.addTarget(self, action: #selector(self.actionSave(_:)), for: .touchUpInside)
        saveAction.setX(0)
        saveAction.setY(0)
        saveAction.setSize(saveLayout.getWidth(), saveLayout.getHeight())
        saveLayout.addSubview(saveAction)
        
        //----
        
        //share_icon
        
        let saveLayout2 = UIView()
        saveLayout2.setX(top.getWidth() - 50 - 10 - 50)
        saveLayout2.setY(startOffset + 20)
        saveLayout2.setSize(50, 30)
        top.addSubview(saveLayout2)
        
        var saveIcon2 = UIImageView()
        saveIcon2.setSize(iconSize, iconSize)
        saveIcon2.toCenterX(saveLayout2)
        saveIcon2.toCenterY(saveLayout2)
        saveIcon2.image = UIImage(named: "share_icon")
        saveLayout2.addSubview(saveIcon2)
        
        let saveAction2 = UIButton()
        saveAction2.addTarget(self, action: #selector(self.actionSave2(_:)), for: .touchUpInside)
        saveAction2.setX(0)
        saveAction2.setY(0)
        saveAction2.setSize(saveLayout2.getWidth(), saveLayout2.getHeight())
        saveLayout2.addSubview(saveAction2)
        
        //----
        
        let libraryLayout = UIView()
        libraryLayout.setX(10)
        libraryLayout.setY(startOffset + 20)
        libraryLayout.setSize(50, 30)
        top.addSubview(libraryLayout)
        
        let libraryIcon = UIImageView()
        libraryIcon.setSize(12, iconSize)
        libraryIcon.toCenterX(libraryLayout)
        libraryIcon.toCenterY(libraryLayout)
        libraryIcon.image = UIImage(named: "left_menu_back")
        libraryLayout.addSubview(libraryIcon)
        
        let libraryAction = UIButton()
        libraryAction.addTarget(self, action: "closeAction:", for: .touchUpInside)
        libraryAction.setX(0)
        libraryAction.setY(0)
        libraryAction.setSize(libraryLayout.getWidth(), libraryLayout.getHeight())
        libraryLayout.addSubview(libraryAction)
        
        //------
        
        libTitle = UILabel()
        //        libTitle.backgroundColor = .yellow
        libTitle.setX(0, relative: libraryLayout)
        libTitle.setY(startOffset + 20)
        libTitle.setSize(ScreenSize.SCREEN_WIDTH - 140, 30)
        libTitle.textColor = UIColor.white
        libTitle.textAlignment = .center
        //        libTitle.text = self._doc.Name
        libTitle.font = UIFont.systemFont(ofSize: 16)
        top.addSubview(libTitle)
    }
    
    var libTitle: UILabel!
    
    func saveCurrentPage()
    {
        try! Core.shared().getRealm.write {
            self.files[currentId].ORCText = self.textEdit.text
        }
    }
    
    @objc func closeAction(_ sender: AnyObject)
    {
        self.removeFromSuperview()
    }
    
    @objc func actionSave(_ sender: AnyObject)
    {
        self.endEdit()
        saveCurrentPage()
    }
    
    @objc func actionSave2(_ sender: AnyObject)
    {
        self.endEdit()
        
        if(!Core.shared().isPremium && !Core.shared().IsDebug)
        {
            EventsManager.shared().sendNotify(Constants.EVENT_SHOW_PREMIUM)
            return
        }
        
        if let doc = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(self._id)'").first
        {
            var items = [
                ActionMenuItem(title: s("share_image"), itemId: 1),
                ActionMenuItem(title: s("share_pdf"), itemId: 2),
                ActionMenuItem(title: s("save_to_library"), itemId: 3)
            ]
            
            if let docfile = Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(self._id)'").first
            {
                if(docfile.ORCText.count > 0)
                {
                    items.append(ActionMenuItem(title: s("share_recognized_pdf"), itemId: 4))
                    items.append(ActionMenuItem(title: s("share_recognized_plain_text"), itemId: 5))
                }
            }
            
            let menu = ActionsMenu(menuId: 3, items: items)
            
            menu.delegate = self
            
            self.addSubview(menu)
            
            menu.Show()
        }
//        saveCurrentPage()
    }
    
    var processingTitle: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension OCRScanner: ActionsMenuDelegate
{
    func ActionsMenuAction(menuId: Int, itemId: Int) {
        switch(itemId)
        {
        case 1: //share_image
            EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._id, "type": "image"])
            break;
        case 2: //share_pdf
            EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._id, "type": "pdf"])
            break;
        case 3: //save_to_library
            EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._id, "type": "image_save"])
            break;
        case 5:
            EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._id, "type": "text"])
            break
        case 4:
            EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._id, "type": "ocrpdf"])
            break;
        default:
            break
        }
    }
    
    func ActionsMenuCancel() {
        
    }
    
    
}
