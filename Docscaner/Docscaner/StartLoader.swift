//
//  StartLoader.swift
//  Docscaner
//
//  Created by Artem  on 28/02/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit

protocol StartLoaderDelegate
{
	func StartLoaderEnd()
}

class StartLoader: UIView
{
	var delegate: StartLoaderDelegate!

	func StartLoading()
	{
		UIView.animate(withDuration: 3, delay: 2, options: .curveEaseIn, animations: {
			self.alpha = 0
		}) { (success) in
			self.delegate.StartLoaderEnd()
			self.removeFromSuperview()
		}
	}

	init(delegate: StartLoaderDelegate)
	{
		super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))

		let startOffset = UIApplication.shared.statusBarFrame.size.height

		self.delegate = delegate

		backgroundColor = .white

		let logo = UIImage(named: "start_logo")
		var logoSize = getImageSize(image: logo!, width: ScreenSize.SCREEN_WIDTH * 0.65, height: 0)

		let image = UIImageView()
		image.setSize(logoSize[0], logoSize[1])
		image.toCenterX(self)
		image.toCenterY(self)
		image.image = logo
		self.addSubview(image)


		let title = UILabel()
		title.textColor = UIColor(red: 0.37, green: 0.37, blue: 0.37, alpha: 1)
		title.font = UIFont(name: "SFUIText-Bold", size: 13)
		title.setSize(ScreenSize.SCREEN_WIDTH, 50)
		title.numberOfLines = 0
		title.lineBreakMode = .byWordWrapping


		let paragraphStyle = NSMutableParagraphStyle()
		paragraphStyle.lineSpacing = 8

		title.attributedText = NSMutableAttributedString(string: s("start_title"), attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])

		title.toCenterX(self)
		title.setY(ScreenSize.SCREEN_HEIGHT - title.getHeight() - 50 - startOffset)
				title.textAlignment = .center
		self.addSubview(title)

		self.StartLoading()
	}

	required init?(coder aDecoder: NSCoder)
	{
		fatalError("init(coder:) has not been implemented")
	}

}
