//
//  DashboardController.swift
//  Docscaner
//
//  Created by Artem  on 28/02/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit

enum DashboardSort: Int
{
    case Name = 1
    case Date
    case `Type`
    case Size
}

class DashboardController: UIViewController, ViewControllerDelegate, DashboardBottomMenuDelegate, DashboardTopMenuDelegate
{
    var _sort = DashboardSort.Date
    
    func DashboardTopMenuSearch(text: String)
    {
        
    }
    
	var messagesScroll: UICollectionView!

	var layout: UICollectionViewFlowLayout!

	var top: DashboardTopMenu!

	var bottom: DashboardBottomMenu!

	var _data = [DocModel]()

	var parentId = "";
    
    var _cells = [AnyObject]()

	func DashboardBottomMenuOpenSettings()
	{
        Core.shared().changeViewController(name: "settingsController")
	}

	func DashboardBottomMenuOpenGalery()
	{
        EventsManager.shared().sendNotify(Constants.EVENT_IMPORT_IMAGE, data: ["folder_id": self.parentId])
	}

//	override var preferredStatusBarStyle: UIStatusBarStyle
//	{
//		return .default
//	}

	override func viewDidAppear(_ animated: Bool)
	{
		self.navigationController?.navigationBar.barStyle = UIBarStyle.black
	}

	override func viewDidLoad()
	{
		super.viewDidLoad()

		self.view.backgroundColor = UIColor(red: 0.96, green: 0.96, blue: 0.96, alpha: 1)

		top = DashboardTopMenu()
		top.delegate = self
		top.setX(0)
		top.setY(0)
		self.view.addSubview(top)

		bottom = DashboardBottomMenu()
		bottom.delegate = self
		bottom.setX(0)
		bottom.setY(ScreenSize.SCREEN_HEIGHT - bottom.getHeight())
		self.view.addSubview(bottom)

		initCollectionView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.observerNewDoc(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_NEW_DOC)), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.observerOpenFolder(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_OPEN_FOLDER)), object: nil)
        
        //Constants.EVENT_RELOAD_LIB
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.observerReload(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_RELOAD_LIB)), object: nil)
	}
    
    @objc func observerReload(_ notification: NSNotification)
    {
        DispatchQueue.main.async(execute: {
            self.parentId = ""
            self.reinitData(parentId: "")
        })
    }
    
    @objc func observerOpenFolder(_ notification: NSNotification)
    {
        DispatchQueue.main.async(execute: {
            let params = notification.userInfo as! Dictionary<String, AnyObject>
            
            var folder_id = params["folder_id"] as? String ?? ""
            
            self.reinitData(parentId: folder_id)
        })
    }
    
    @objc func observerNewDoc(_ notification: NSNotification)
    {
        DispatchQueue.main.async(execute: {
            self.reinitData(parentId: "")
        })
    }

    var empty: DashboardEmpty!
    
	func initEmpty()
	{
		if(empty != nil)
        {
            empty.removeFromSuperview()
            empty = nil
        }
        
        empty = DashboardEmpty(width: ScreenSize.SCREEN_WIDTH - 40, height: ScreenSize.SCREEN_HEIGHT - top.getHeight() - bottom.getHeight() - 40)
		empty.setX(20)
		empty.setY(20, relative: top)
		self.view.addSubview(empty)
	}

	func setParams(params: [String : Any])
	{

	}

	func willBeAppear()
	{

	}

	func willBeDisappear()
	{

	}

	func DashboardBottomMenuOpenCamera()
	{
		Core.shared().changeViewController(name: "scannerController")
	}
    
    func DashboardTopMenuNewFolder()
    {
        print("*** LibraryTopNewAction")
        
        var folderName = ""
        
        let alert = UIAlertController(title: s("new_folder_title"), message: nil, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: s("botton_cancel"), style: .cancel, handler: { (action) -> Void in}))
        
        let saveAction = UIAlertAction(title: s("button_add"), style: .default, handler: { (action) -> Void in
            NotificationCenter.default.removeObserver(self, name: UITextField.textDidChangeNotification, object: self)
            
            self.addNewFolder(folderName)
        })
        
        alert.addAction(saveAction)
        
        alert.addTextField(configurationHandler: { (textField) in
            textField.text = ""
            textField.placeholder = s("new_folder_placeholder")
            saveAction.isEnabled = false
            NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main) { (notification) in
                saveAction.isEnabled = textField.text!.count > 0
                
                folderName = textField.text!
            }
        })
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func addNewFolder(_ name: String)
    {
        let doc2 = DocModel()
        doc2.Id = UUID().uuidString
        doc2.CreatedAt = Int64(NSDate().timeIntervalSince1970)
        doc2.Name = name
        doc2.IsFolder = true
        
        try! Core.shared().getRealm.write
        {
            Core.shared().getRealm.add(doc2, update: true)
        }
        
        goToFolder(id: doc2.Id)
    }
    
    func goToFolder(id: String)
    {
        if(id.isEmpty)
        {
            goToLibrary()
        }
        
        if let folder = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(id)'").first
        {
            self.top.setTopTitle(newTitle: folder.Name)
            
            self.top.addBack()
            
            self.top.setNewEnable(isActive: false)
            
            self.reinitData(parentId: folder.Id)
        }
    }
    
    func goToLibrary()
    {
        self.top.setTopTitle(newTitle: s("library_title_default"))
        self.top.setNewEnable(isActive: true)
        
        self.top.hideBack()
        
        print("*** renderScroll 4")
        self.reinitData(parentId: "")
    }
    
    func DashboardTopMenuFilter()
    {
        var items = [
            ActionMenuItem(title: s("sort_name"), itemId: 1),
            ActionMenuItem(title: s("sort_date"), itemId: 2),
            ActionMenuItem(title: s("sort_type"), itemId: 3),
//            ActionMenuItem(title: s("sort_size"), itemId: 4)
        ]
        
        let menu = ActionsMenu(menuId: 10, items: items)
        
        menu.delegate = self
        
        self.view.addSubview(menu)
        
        menu.Show()
    }

    func DashboardTopMenuActions()
    {
        IsEditMode = (IsEditMode) ? false : true
        
        for i in self._cells
        {
            (i as! DocCellDelegate).EnableEdit(isEdit: IsEditMode)
        }
        
        if(!IsEditMode)
        {
            DisableEditMode(true)
        }
    }
    
    func DisableEditMode(_ disableAll: Bool = false)
    {
        if(disableAll)
        {
            self.top.EnableEditMode(isEnable: false)
            
            self.bottom.EnableEditMode(false)
            
            for i in _cells
            {
                if((i as! DocCellDelegate).getIsChecked())
                {
                    (i as! DocCellDelegate).setChecked()
                }
                
                (i as! DocCellDelegate).EnableEdit(isEdit: false)                
            }
        }
        
        self._selected = [Int]()
    }
    
    func DashboardTopMenuBack()
    {
        DispatchQueue.main.async(execute: {
            self.goToLibrary()
        })
    }
    
    var IsEditMode = false
    
    var _selected = [Int]()
    
    func DashboardBottomEditModeAdd()
    {
        
    }
    
    func DashboardBottomEditModeDelete()
    {
        var text = String(format: s("remove_multi_desc"), String(self._selected.count))
        
        let alertController = UIAlertController(title: s("remove_multi_items"), message: text, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: s("remove_title"), style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
            DispatchQueue.main.async(execute: {
                for i in self._selected
                {
                    let docId = self._data[i].Id
                    
                    self.removeWholeDoc(docId)
                }
                
                self.reinitData(parentId: self.parentId)
            })
        })
        
        alertController.addAction(okAction)
        
        let cancelAction = UIAlertAction(title: s("botton_cancel"), style: .cancel, handler: {
            (action : UIAlertAction!) -> Void in
        })
        
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func removeWholeDoc(_ docId: String)
    {
        if let doc = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(docId)'").first
        {
            try! Core.shared().getRealm.write {
                Core.shared().getRealm.delete(doc)
            }
        }
    }
    
    func DashboardBottomEditModeEdit()
    {
        
    }
    
    func DashboardBottomEditModeMore()
    {

    }
    
    func DashboardBottomEditModeRename()
    {
        if(self._selected.count == 1)
        {
            var docId = self._data[self._selected[0]].Id
            
            if let doc = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(docId)'").first
            {
                var folderName = ""
                
                let alert = UIAlertController(title: (doc.IsFolder) ? s("folder_name_title") : s("file_name_title"), message: nil, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: s("botton_cancel"), style: .cancel, handler: { (action) -> Void in}))
                
                let saveAction = UIAlertAction(title: s("button_rename"), style: .default, handler: { (action) -> Void in
                    NotificationCenter.default.removeObserver(self, name: UITextField.textDidChangeNotification, object: self)
                    
                    self.renameDoc(folderName, docId: docId)
                })
                
                alert.addAction(saveAction)
                
                alert.addTextField(configurationHandler: { (textField) in
                    textField.text = doc.Name
                    textField.placeholder = (doc.IsFolder) ? s("folder_name_title") : s("file_name_title")
                    saveAction.isEnabled = false
                    NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main) { (notification) in
                        saveAction.isEnabled = textField.text!.count > 0
                        folderName = textField.text!
                    }
                })
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func renameDoc(_ name: String, docId: String)
    {
        if let doc = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(docId)'").first
        {
            try! Core.shared().getRealm.write {
                doc.Name = name
            }
            
            self.reinitData(parentId: self.parentId)
            
            self.DisableEditMode(true)
        }
    }
    
    func DashboardBottomEditModeShare()
    {
        if(!Core.shared().isPremium && !Core.shared().IsDebug)
        {
            EventsManager.shared().sendNotify(Constants.EVENT_SHOW_PREMIUM)
            return
        }
        
        let docId = self._data[self._selected[0]].Id

        if let doc = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(docId)'").first
        {
            var items = [
                ActionMenuItem(title: s("share_image"), itemId: 1),
                ActionMenuItem(title: s("share_pdf"), itemId: 2),
                ActionMenuItem(title: s("save_to_library"), itemId: 3)
            ]

            let menu = ActionsMenu(menuId: 3, items: items)
            
            menu.delegate = self
            
            self.view.addSubview(menu)
            
            menu.Show()
        }
    }
    
    func DashboardTopMentEditCancel()
    {
        self.DisableEditMode(true)
    }
}

extension DashboardController: ActionsMenuDelegate
{
    func ActionsMenuAction(menuId: Int, itemId: Int) {
        
        if(menuId == 3)
        {
            let docId = self._data[self._selected[0]].Id
            
            switch(itemId)
            {
            case 1: //share_image
                EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": docId, "type": "image"])
                break;
            case 2: //share_pdf
                EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": docId, "type": "pdf"])
                break;
            case 3: //save_to_library
                EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": docId, "type": "image_save"])
                break;
            default:
                break
            }
        }
        
        if(menuId == 10)
        {
            switch(itemId)
            {
            case 1:
                self._sort = .Name
                break
            case 2:
                self._sort = .Date
                break
            case 3:
                self._sort = .Type
                break
            default:
                self._sort = .Date
                break
            }
            
            self.reinitData(parentId: self.parentId)
        }
    }
    
    func ActionsMenuCancel()
    {
        
    }
}
