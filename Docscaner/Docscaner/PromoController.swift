//
//  PromoController.swift
//  Docscaner
//
//  Created by artem on 25/04/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit

class PromoController: UIViewController, ViewControllerDelegate, PromoDelegate
{
    func promoScroll(offset: CGFloat)
    {
        var s: CGFloat = 0
        
        if(DeviceType.IS_IPHONE_Xr || DeviceType.IS_IPHONE_X)
        {
            s = ScreenSize.SCREEN_HEIGHT - nextLayout.getHeight() - 65 - UIApplication.shared.statusBarFrame.size.height
        }
        else
        {
            s = ScreenSize.SCREEN_HEIGHT - nextLayout.getHeight() - 85 - UIApplication.shared.statusBarFrame.size.height
        }
        
        if(offset * -1 == UIApplication.shared.statusBarFrame.size.height)
        {
            nextLayout.setY(s)
            
            crumbs.setY(nextLayout.getY() - crumbs.getHeight())
            
            return
        }
        
        crumbs.setY(nextLayout.getY() - crumbs.getHeight())
        nextLayout.setY(s - offset - UIApplication.shared.statusBarFrame.size.height)
    }
    
    var subs_id = "com.asim.docscanner.year"
    
    func closePromo()
    {
        if let settings = Core.shared().getRealm.objects(SettingsModel.self).first
        {
            try! Core.shared().getRealm.write {
                settings.IsPromo = true
            }
            Core.shared().changeViewController(name: settings.LaunchController)
            Core.shared().removeViewController(name: "promoController")
        }
    }
    
    func getNextOffset() -> CGFloat
    {
        return nextLayout.getY() + nextLayout.getHeight()
    }
    
    var nextLayout: UIView!
    
    func Next()
    {
        if let dot = self.view.viewWithTag(1000 + self.current + 1)
        {
            dot.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        }
                        
        slides[self.current].Hide {
            
        }
        
        if(self.current + 1 < self.slides.count)
        {
            self.current += 1
            
            var locale = NSLocale.preferredLanguages[0]
            
            
            if(self.current + 1 == self.slides.count)
            {
                if(!Core.shared().IsPromoNext /*|| locale != "en"*/)
                {
                    part1.isHidden = true
                    part2.isHidden = false
                    part3.isHidden = false
                }
                else
                {
                    part1.isHidden = true
                    part2.isHidden = false
                    part3.isHidden = false
                    
                    part2.text = s("promo_next")
                }
            }

                        
            slides[self.current].Show()
            
            if let dot = self.view.viewWithTag(1000 + self.current + 1)
            {
                dot.backgroundColor = UIColor.white
            }
        }
        else
        {
            // DO SUBSCRIBE
            EventsManager.shared().sendNotify(Constants.EVENT_DO_PURCHASE, data: ["product_id": subs_id])
        }
    }
    
    func setParams(params: [String : Any])
    {
        
    }
    
    func willBeAppear() {
        
    }
    
    func willBeDisappear() {
        
    }
    
    var slides = [PromoActions]()
    
    var current = 0
    
    var nextStartOffset: CGFloat = 0
    
    var crumbs: UIView!
    
    @objc func nextAction(_ sender: AnyObject)
    {
        Next()
    }
    
    @objc func observerUpdateSubs(_ notification: NSNotification)
    {
        DispatchQueue.main.async(execute: {
            if let subsModel = Core.shared().getRealm.objects(SubscriptionModel.self).filter("PurchaseId = '\(self.subs_id)'").first
            {
                self.part3.text = String(format: s("promo_trial_desc"), subsModel.PriceLocale, subsModel.Currency).replacingOccurrences(of: ".00", with: "")
            }
        })
    }
    
    @objc func observerPurchaseSuccess(_ notification: NSNotification)
    {
        DispatchQueue.main.async(execute: {
            if let settings = Core.shared().getRealm.objects(SettingsModel.self).first
            {
                try! Core.shared().getRealm.write {
                    settings.IsPromo = true
                }
                Core.shared().changeViewController(name: settings.LaunchController)
                Core.shared().removeViewController(name: "promoController")
            }
        })
    }
    
    var part1: UILabel!
    
    var part2: UILabel!
    
    var part3: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.observerUpdateSubs(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" ,Constants.NOTIFY_MESSAGE_PREFIX, Constants.UPDATE_SUBS)), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.observerPurchaseSuccess(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" ,Constants.NOTIFY_MESSAGE_PREFIX, Constants.UPDATE_SUBS_SUCCESS)), object: nil)
        
        self.view.backgroundColor = UIColor(red: 0, green: 0.21, blue: 0.39, alpha: 1)
        
        initBG()

        var fontSize: CGFloat = 22
        var fontSize2: CGFloat = 20
        var fontSize3: CGFloat = 16
        var tOff: CGFloat = 12
        
        if(DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P)
        {
            fontSize = 19
            fontSize2 = 17
            fontSize3 = 12
            tOff = 8
        }
        
        let startOffset = UIApplication.shared.statusBarFrame.size.height
        
        nextLayout = UIView()
        nextLayout.setX(30)
        nextLayout.setSize(ScreenSize.SCREEN_WIDTH - 60, 50)
        nextLayout.backgroundColor = .white
        nextLayout.layer.cornerRadius = nextLayout.getHeight() / 2
        
        nextStartOffset = nextLayout.getY()
        
        if(DeviceType.IS_IPHONE_Xr || DeviceType.IS_IPHONE_X)
        {
            nextLayout.setY(ScreenSize.SCREEN_HEIGHT - nextLayout.getHeight() - 65 - startOffset)
        }
        else
        {
            nextLayout.setY(ScreenSize.SCREEN_HEIGHT - nextLayout.getHeight() - 85 - startOffset)
        }
        
        self.view.addSubview(nextLayout)
        
        var r = nextLayout.getHeight() / 2
        
        part1 = UILabel()
        part1.setX(r)
        part1.setY(0)
        part1.setSize(nextLayout.getWidth() - (r * 2), nextLayout.getHeight())
        part1.textColor = UIColor(red: 0.11, green: 0.31, blue: 0.69, alpha: 1)
        part1.font = UIFont(name: "Circe-Bold", size: fontSize)
        part1.textAlignment = .center
        part1.text = s("promo_next")
        nextLayout.addSubview(part1)
        
        part2 = UILabel()
        part2.isHidden = true
        part2.setX(r)
        part2.setY(0)
        part2.setSize(nextLayout.getWidth() - (r * 2), nextLayout.getHeight() * 0.66)
        part2.textColor = UIColor(red: 0.11, green: 0.31, blue: 0.69, alpha: 1)
        part2.font = UIFont(name: "Circe-Regular", size: fontSize)
        part2.textAlignment = .center
        part2.text = s("promo_subscribe")
        nextLayout.addSubview(part2)
        
        part3 = UILabel()
        part3.isHidden = true
        part3.setX(r)
        part3.setY(0, relative: part2)
        part3.setSize(nextLayout.getWidth() - (r * 2), nextLayout.getHeight()  * 0.25)
        part3.textColor = UIColor(red: 0.11, green: 0.31, blue: 0.69, alpha: 1)
        part3.font = UIFont(name: "Circe-Regular", size: fontSize3)
        part3.textAlignment = .center
        part3.text = s("promo_trial_desc")
        nextLayout.addSubview(part3)
        
        if let subsModel = Core.shared().getRealm.objects(SubscriptionModel.self).filter("PurchaseId = '\(subs_id)'").first
        {
            part3.text = String(format: s("promo_trial_desc"), subsModel.PriceLocale, subsModel.Currency).replacingOccurrences(of: ".00", with: "")
        }
        
        var subsButton = UIButton()
        subsButton.addTarget(self, action: "nextAction:", for: .touchUpInside)
        subsButton.setX(0)
        subsButton.setY(0)
        subsButton.setSize(nextLayout.getWidth(), nextLayout.getHeight())
        nextLayout.addSubview(subsButton)
        
        //------
        
        var crumbCount = 4
        
        var currentCrumb = 1
        
        if(crumbCount > 0)
        {
            var partSize: CGFloat = 30
            
            crumbs = UIView()
            crumbs.setSize(CGFloat(partSize * CGFloat(crumbCount)), 35)
            crumbs.toCenterX(self.view)
            crumbs.setY(nextLayout.getY() - crumbs.getHeight())
            self.view.addSubview(crumbs)
            
            var offset: CGFloat = 0
            
            for i in 1...crumbCount
            {
                var crumb = UIView()
                crumb.setX(offset)
                crumb.setY(0)
                crumb.setSize(CGFloat(partSize), partSize)
                crumbs.addSubview(crumb)
                
                var vw = UIView()
                vw.setSize(7, 7)
                vw.toCenterX(crumb)
                vw.toCenterY(crumb)
                vw.backgroundColor = .clear
                crumb.addSubview(vw)
                
                var dot = UIView()
                dot.setSize(7, 7)
                dot.layer.cornerRadius = dot.getWidth() / 2
                dot.tag = 1000 + i
                if(i == currentCrumb)
                {
                    dot.backgroundColor = UIColor.white
                }
                else
                {
                    dot.backgroundColor = UIColor.white.withAlphaComponent(0.5)
                }
                dot.toCenterX(crumb)
                dot.toCenterY(crumb)
                crumb.addSubview(dot)
                
                offset += partSize
            }
        }
        
        slides = [Promo1(0, self), Promo2(0.5, self), Promo3(0.5, self), PromoFinish(0.5, self)]
        
//        slides = []
        
        for i in slides
        {
            self.view.addSubview(i as! UIView)
        }
        
        slides[self.current].Show()
        
        self.view.bringSubviewToFront(nextLayout)
    }
    

    
    func initBG()
    {
        let bgImage = UIImage(named: "subs_bg_main")
        
        var originalSize = getImageSize(image: bgImage!, width: ScreenSize.SCREEN_WIDTH, height: 0)
        
        let bg = UIImageView()
        //        bg.layer.contentsGravity = CALayerContentsGravity.resizeAspectFill
        //        bg.layer.masksToBounds = true
        bg.image = bgImage
        bg.setX(0)
        bg.setY(0)
        bg.setSize(originalSize[0], originalSize[1])
        self.view.addSubview(bg)
        
    }
}
