//
//  MainController.swift
//  Docscaner
//
//  Created by Artem  on 28/02/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import StoreKit
import AdSupport
import Reachability
import LocalAuthentication
import Firebase

@available(iOS 10.0, *)

class MainController: UIViewController, SKProductsRequestDelegate, SKPaymentTransactionObserver, StartLoaderDelegate
{
	private var _controllers = [ControllerModel]()

	var currentControllerName = ""

	private var _notRemoveControllers = ["test"]

	var products = [SKProduct]()

	var loader: UIView!


	func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction])
	{
		print("*** updatedTransactions")

		Core.shared().clearReceiptStatus()

		for transaction: AnyObject in transactions
		{
			let trans = transaction as! SKPaymentTransaction
			print(trans)

			switch trans.transactionState {

			case .purchased, .restored:
				let trans_id = transaction.transaction?.transactionIdentifier

				//				transaction.transaction?.transactionIdentifier

				let id = transaction.payment.productIdentifier

				var receiptData: Data!

				do
				{
					receiptData = try Data(contentsOf: Bundle.main.appStoreReceiptURL!)
				}
				catch
				{

				}

				if(receiptData != nil)
				{
					let receiptString = receiptData.base64EncodedString(options: NSData.Base64EncodingOptions())

					print("buy, ok ID \(id) TRANS \(trans_id) RECEIPT \(receiptString)")

                    Core.shared().sendAppReceipt(receiptString)
				}

				queue.finishTransaction(trans)

                Core.shared().sendAppMetrics(.PurchaseSubscriptionPaid, type: .Purchase, params: ["amount": Double(truncating: self.processProduct.price), "currency": self.processProduct.priceLocale.currencyCode, "product_id": self.processProduct.productIdentifier])




//                self.authorize()
                
                Core.shared().checkPremium {
                    if(self.loader != nil)
                    {
                        self.loader.removeFromSuperview()
                    }
                    
                    EventsManager.shared().sendNotify(Constants.UPDATE_SUBS_SUCCESS)
                }

				break;
			case .failed:
				print("buy error")
				print(trans.error)
				queue.finishTransaction(trans)

				if(loader != nil)
				{
					loader.removeFromSuperview()
				}

                EventsManager.shared().sendNotify(Constants.UPDATE_SUBS_SUCCESS)
                
                Core.shared().sendAppMetrics(.PurchaseSubscriptionFailed, type: .Purchase, params: ["amount": Double(truncating: self.processProduct.price), "currency": self.processProduct.priceLocale.currencyCode, "product_id": self.processProduct.productIdentifier])
				break;
			case .deferred:
				print("buy, deffered ")
				break
			case .purchasing:
				print("buy, purchasing ")
				break
			default:
				print("default")
				break;
			}
		}
	}

	func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse)
	{
		let products = response.products

		print("*** productsRequest")
		//		print(products)

		var productsData = [SubscriptionModel]()

		for i in products
		{
			if #available(iOS 11.2, *)
			{
				print("** PRODUCT PRICE \(i.price) LOCALE \(i.priceLocale.currencyCode) \(i.priceLocale) ID \(i.productIdentifier) \(i.localizedTitle)  \(i.localizedDescription) UNITS \(i.subscriptionPeriod?.unit.rawValue)")
			}
			else
			{
				// Fallback on earlier versions
			}

			self.products.append(i)

			var product = SubscriptionModel()
			product.PurchaseId = i.productIdentifier
			product.PriceLocale = Double(truncating: i.price)
			product.Currency = i.priceLocale.currencyCode!
			product.Title = i.localizedTitle
			product.Description = i.localizedDescription

			productsData.append(product)
		}

		try! Core.shared().getRealm.write {
			Core.shared().getRealm.add(productsData, update: true)
		}

		Core.shared().Subscriptions = [PackageSubs]()
		for i in Core.shared().getRealm.objects(SubscriptionModel.self)
		{
			var s = PackageSubs()
			s.Currency = i.Currency
			s.PriceLocale = i.PriceLocale
			s.PurchaseId = i.PurchaseId
			Core.shared().Subscriptions.append(s)
		}

        EventsManager.shared().sendNotify(Constants.UPDATE_SUBS)

		print("*** productsRequest invalid")
		print(response.invalidProductIdentifiers)
	}

	@objc func observerDoPurchaseResore(_ notification: NSNotification)
	{
		DispatchQueue.main.async(execute: {
			if (SKPaymentQueue.canMakePayments())
			{
				SKPaymentQueue.default().restoreCompletedTransactions()
			}
		})
	}

	var processProduct: SKProduct!

	@objc func observerDoPurchase(_ notification: NSNotification)
	{
		print("*** observerDoPurchase")

		DispatchQueue.main.async(execute: {
			let params = notification.userInfo as! Dictionary<String, AnyObject>

			let product_id = params["product_id"] as? String ?? ""

			if(self.loader != nil)
			{
				self.loader.removeFromSuperview()
			}

			self.loader = UIView()
			self.loader.setX(0)
			self.loader.setY(0)
			self.loader.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
			self.loader.backgroundColor = UIColor.black.withAlphaComponent(0.75)
			self.view.addSubview(self.loader)
			self.view.bringSubviewToFront(self.loader)

			var waitingLoader = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
			waitingLoader.setSize(50, 50)
			waitingLoader.toCenterX(self.loader)
			waitingLoader.toCenterY(self.loader)
			waitingLoader.startAnimating()
			self.loader.addSubview(waitingLoader)

			print("*** observerDoPurchase ID \(product_id)")

			if(SKPaymentQueue.canMakePayments())
			{
				for product in self.products
				{
					if product.productIdentifier == product_id
					{

						self.processProduct = product


                        Core.shared().sendAppMetrics(.PurchaseSubscribtionInit, type: .Purchase, params: ["amount": Double(truncating: product.price), "currency": product.priceLocale.currencyCode!, "product_id": product_id])

						//----

						let pay = SKPayment(product: product)
						SKPaymentQueue.default().add(self)
						SKPaymentQueue.default().add(pay)
					}
				}
			}
			else
			{
				print("*** CANNOT PAYMENTS")
			}
		})
	}

	@objc func observerDoStartAuth(_ notification: NSNotification)
	{
		print("*** observerDoStartAuth")

		DispatchQueue.main.async(execute: {
			self.authorize()
		})
	}

	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)
	}

    func authenticateUser(finish: @escaping (_ success: Bool) -> ())
    {
        
        let context = LAContext()
        var error: NSError?
        
        print("*** authenticateUser 1")
        
        if let settings = Core.shared().getRealm.objects(SettingsModel.self).first
        {
            if(!settings.UsePinCode)
            {
                print("*** authenticateUser 2")
                finish(true)
            }
            else
            {
                print("*** authenticateUser 3")
                var type = Core.shared().getBiometricType()
                
                DispatchQueue.main.async(execute: {

                
                    
                    switch(type)
                    {
                    case .none:
                        print("*** authenticateUser 4")
                        
                        var folderName = ""
                        
                        let alert = UIAlertController(title: s("pin_set"), message: nil, preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: s("botton_cancel"), style: .cancel, handler: { (action) -> Void in}))
                        
                        let saveAction = UIAlertAction(title: s("button_enter"), style: .default, handler: { (action) -> Void in
                            NotificationCenter.default.removeObserver(self, name: UITextField.textDidChangeNotification, object: self)
                            
                            if(settings.PinCode == folderName)
                            {
                                finish(true)
                            }
                            else
                            {
                                finish(false)
                            }
                        })
                        
                        alert.addAction(saveAction)
                        
                        alert.addTextField(configurationHandler: { (textField) in
                            textField.text = ""
                            textField.placeholder = s("pin_set_placeholder")
                            saveAction.isEnabled = false
                            NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main) { (notification) in
                                saveAction.isEnabled = textField.text!.count > 0
                                
                                folderName = textField.text!
                            }
                        })
                        
                        self.present(alert, animated: true, completion: nil)
                        
                        break
                    case .touch, .face:
                        print("*** authenticateUser 5")
                        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error)
                        {
                            print("*** authenticateUser 6")
                            print(error)
                            let reason = "Identify yourself!"
                            
                            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                                [unowned self] success, authenticationError in
                                
                                DispatchQueue.main.async {
                                    if success
                                    {
                                        finish(true)
                                    }
                                    else
                                    {
                                        //                                    let ac = UIAlertController(title: "Authentication failed", message: "Sorry!", preferredStyle: .alert)
                                        //                                    ac.addAction(UIAlertAction(title: "OK", style: .default))
                                        //                                    Core.shared().RootWindow.rootViewController?.present(ac, animated: true)
                                        finish(false)
                                    }
                                }
                            }
                        }
                        else
                        {
                            print("*** authenticateUser 7")
                            //                        let ac = UIAlertController(title: "Touch ID not available", message: "Your device is not configured for Touch ID.", preferredStyle: .alert)
                            //                        ac.addAction(UIAlertAction(title: "OK", style: .default))
                            //                        Core.shared().RootWindow.rootViewController?.present(ac, animated: true)
                            finish(false)
                        }
                        break
                    @unknown default:
                        print("*** authenticateUser 8")
                        break
                    }
                    
                })
            }
        }
        
        
    }

	override func viewDidLoad()
	{
		super.viewDidLoad()
        
        if !Core.shared().IsDebug
        {
            var remoteConfig = RemoteConfig.remoteConfig()
            //        remoteConfig.configSettings = RemoteConfigSettings(developerModeEnabled: true)
            remoteConfig.setDefaults(fromPlist: "RemoteConfigDefaults")
            
            remoteConfig.fetch(withExpirationDuration: TimeInterval(0)) { (status, error) -> Void in
                if status == .success {
                    print("Config fetched!")
                    remoteConfig.activateFetched()
                } else {
                    print("Config not fetched")
                    print("Error: \(error?.localizedDescription ?? "No error available.")")
                }
                
                Core.shared().isPromoCloseActivated = remoteConfig["promo_close_1"].boolValue
                
                EventsManager.shared().sendNotify(Constants.PROMO_CLOSE_CHANGED)
                
                //            print("** REMOTE CONFIG \(promoClose)")
            }
        }
        




		self.view.frame.origin.y = 0

		if ASIdentifierManager.shared().isAdvertisingTrackingEnabled
		{
			Core.shared().DeviceId = ASIdentifierManager.shared().advertisingIdentifier.uuidString
		}
		else
		{
			Core.shared().DeviceId = (UIDevice.current.identifierForVendor?.uuidString)!
		}

		Core.shared().Subscriptions = [PackageSubs]()
		for i in Core.shared().getRealm.objects(SubscriptionModel.self)
		{
			var s = PackageSubs()
			s.Currency = i.Currency
			s.PriceLocale = i.PriceLocale
			s.PurchaseId = i.PurchaseId
			Core.shared().Subscriptions.append(s)
		}

//		EventsManager.shared().sendNotify(Constants.UPDATE_SUBS)

		registerObservers()
        
        initSettings()

//
        EventsManager.shared().sendNotify(Constants.START_AUTH)
        
        
        
        
//
//
//        var subs = MainSubs()
//        self.view.addSubview(subs)
        
        initSettings()

//        testDocs()
        
//        var e = SettingsOCR()
//        self.view.addSubview(e)

//        Core.shared().changeViewController(name: "promoController")
	}
    
    @objc func observerStartAuth(_ notification: NSNotification)
    {
        DispatchQueue.main.async(execute: {
            self.authenticateUser { (success) in
                if(success)
                {
                     self.authorize()
                }
                else
                {
                    EventsManager.shared().sendNotify(Constants.START_AUTH)
                }
               
            }
            
        })
    }

    func initSettings()
    {
        if let settings = Core.shared().getRealm.objects(SettingsModel.self).first
        {
            
        }
        else
        {
            var settings = SettingsModel()
            settings.Id = 1
            settings.CameraMode = 1
            settings.FlashMode = 1
            settings.LaunchController = "dashboardController"
            settings.PinCode = ""
            settings.SaveOriginal = false
            settings.ScanType = "JPG"
            settings.SaveOriginal = false
            settings.UsePinCode = false
            
            try! Core.shared().getRealm.write {
                Core.shared().getRealm.add(settings, update: true)
            }
            
            Core.shared().sendInstall()
            
        }
    }
    
	func registerProducts()
	{
		if(SKPaymentQueue.canMakePayments())
		{
			print("*** CAN PAYMENTS")

			var purchasedProductIdentifiers1: Set<String> = Set<String>()

			for i in Core.shared().getRealm.objects(SubscriptionModel.self)
			{
				purchasedProductIdentifiers1.insert(i.PurchaseId)
			}

			let request: SKProductsRequest = SKProductsRequest(productIdentifiers: purchasedProductIdentifiers1)

			request.delegate = self
			request.start()
		}
		else
		{
			print("*** CANNOT PAYMENTS")
		}
	}


//	override var preferredStatusBarStyle : UIStatusBarStyle
//	{
//		return UIStatusBarStyle.lightContent
//	}

	//MARK: Register observers

	func registerObservers()
	{
		NotificationCenter.default.addObserver(self, selector: #selector(self.observerChangeController(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" ,Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_CHANGE_CONTROLLER)), object: nil)

		NotificationCenter.default.addObserver(self, selector: #selector(self.observerRemoveController(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_REMOVE_CONTROLLER)), object: nil)


		NotificationCenter.default.addObserver(self, selector: #selector(self.observerDoPurchase(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_DO_PURCHASE)), object: nil)

		NotificationCenter.default.addObserver(self, selector: #selector(self.observerDoPurchaseResore(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_DO_RESTORE_PURCHASE)), object: nil)

		NotificationCenter.default.addObserver(self, selector: #selector(self.observerAppForeground(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" ,Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_APP_FOREGROUND)), object: nil)

//		NotificationCenter.default.addObserver(self, selector: #selector(self.observerDoRegister(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" ,Constants.NOTIFY_MESSAGE_PREFIX, Constants.DO_REGISTER_DEVICE)), object: nil)


//		NotificationCenter.default.addObserver(self, selector: #selector(self.observerShowProcessing(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_SHOW_PROCESSING)), object: nil)
//
        NotificationCenter.default.addObserver(self, selector: #selector(self.observerImportImage(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_IMPORT_IMAGE)), object: nil)

		//START_REGISTER_PRODUCTS

		NotificationCenter.default.addObserver(self, selector: #selector(self.observerStartRegisterProduct(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.START_REGISTER_PRODUCTS)), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.observerOpenDoc(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_OPEN_DOC)), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.observerShare(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_DO_SHARE)), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.observerShowPremium(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_SHOW_PREMIUM)), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.observerStartAuth(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.START_AUTH)), object: nil)
        //Constants.EVENT_SHOW_PREMIUM
	}
    
    @objc func observerOpenDoc(_ notification: NSNotification)
    {
        DispatchQueue.main.async(execute: {
            let params = notification.userInfo as! Dictionary<String, AnyObject>
            
            let doc_id = params["doc_id"] as? String ?? ""
            
            var preview = DocView(id: doc_id)
            preview.delegate = self
            self.view.addSubview(preview)
            
            UIView.animate(withDuration: 0.15, animations: {
                preview.setX(0)
            })
            
        })
    }
    
    var premium: MainSubs!
    
    @objc func observerShowPremium(_ notification: NSNotification)
    {
        DispatchQueue.main.async(execute: {
            if(self.premium != nil)
            {
                self.premium.removeFromSuperview()
                
                self.premium = nil
            }
            
            self.premium = MainSubs()
            self.view.addSubview(self.premium)
        })
    }

	@objc func observerStartRegisterProduct(_ notification: NSNotification)
	{
		DispatchQueue.main.async(execute: {
			self.registerProducts()
		})
	}

	@objc func observerShowProcessing(_ notification: NSNotification)
	{
		DispatchQueue.main.async(execute: {
			self.showProcessing()
		})
	}

	@objc func observerHideProcessing(_ notification: NSNotification)
	{
		DispatchQueue.main.async(execute: {
			self.hideProcessing()
		})
	}

	var waitingView: UIView!

	func showProcessing()
	{
		if(waitingView != nil)
		{
			waitingView.removeFromSuperview()
		}

		waitingView = UIView()
		waitingView.backgroundColor = UIColor.black.withAlphaComponent(0.75)
		waitingView.setX(0)
		waitingView.setY(0)
		waitingView.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
		self.view.addSubview(waitingView)

		var waitingLoader = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
		waitingLoader.setSize(50, 50)
		waitingLoader.toCenterX(waitingView)
		waitingLoader.toCenterY(waitingView)
		waitingLoader.startAnimating()
		waitingView.addSubview(waitingLoader)

		var waitingTitle = UILabel()
		waitingTitle.textAlignment = .center
		waitingTitle.setSize(ScreenSize.SCREEN_WIDTH, 20)
		waitingTitle.text = s("loading_title")
		waitingTitle.setX(0)
		waitingTitle.setY(10, relative: waitingLoader)
		waitingTitle.textColor = UIColor.white
		waitingView.addSubview(waitingTitle)

		self.view.bringSubviewToFront(waitingView)
	}

	func hideProcessing()
	{
		if(waitingView != nil)
		{
			waitingView.removeFromSuperview()
		}
	}

	@objc func observerDoRegister(_ notification: NSNotification)
	{
		print("*** observerDoRegister")
		DispatchQueue.main.async(execute: {

			var info = notification.userInfo as! Dictionary<String, AnyObject>

			let token = info["token"] as! String

			let parameters: Parameters = ["token": token, "device_id": Core.shared().DeviceId, "package": (Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String)]

			Alamofire.request("http://pushmaster.ru/api/v1/auth", method: .post, parameters: parameters).responseJSON { response in

				if let json = response.result.value
				{
					var jsonData = JSON(json)
				}
				else
				{

				}
			}
		})
	}

	@objc func observerAppForeground(_ notification: NSNotification)
	{
		DispatchQueue.main.async(execute: {

			Core.shared().clearReceiptStatus()
			self.authorize()
		})
	}

	func removeAllControllers()
	{
		self.currentControllerName = ""

		for i in self._controllers
		{
			i.Controller.view.removeFromSuperview()
		}

		self._controllers = [ControllerModel]()
	}

	@objc func observerRemoveController(_ notification: NSNotification)
	{
		DispatchQueue.main.async(execute: {

			var info = notification.userInfo as! Dictionary<String, AnyObject>

			let controllerName = info["controller"] as! String

			if let controllerModel = self._controllers.filter({ $0.Name == controllerName }).first
			{
				controllerModel.Controller.view.removeFromSuperview()
			}

			if let ii = self._controllers.firstIndex(where: { $0.Name == controllerName })
			{
				self._controllers.remove(at: ii)
			}
		})
	}

	@objc func observerChangeController(_ notification: NSNotification)
	{
        var timeInter: TimeInterval = 0.3
        
		DispatchQueue.main.async(execute: {

			Core.shared().RootWindow.rootViewController!.view.bringSubviewToFront(self.view)

			var info = notification.userInfo as! Dictionary<String, AnyObject>

			let controllerName = info["controller"] as! String
			let controllerParams = info["params"] as? [String: Any] ?? [String: Any]()

			print("*** observerChangeController 1 * \(controllerName)")

			if(controllerName.isEmpty)
			{
				print("*** observerChangeController 1.1")
				return
			}

			if(self.currentControllerName == controllerName)
			{
				print("*** observerChangeController 1.2")

				let controllerModel = self._controllers.filter({ $0.Name == controllerName }).first
				(controllerModel?.Controller as? ViewControllerDelegate)?.setParams(params: controllerParams)
				(controllerModel?.Controller as? ViewControllerDelegate)?.willBeAppear()
				return
			}

			print("*** observerChangeController 2")

			self.currentControllerName = controllerName

			for i in Core.shared().RootWindow.rootViewController!.view.subviews
			{
//				UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {

//                    i.frame.origin.x = ScreenSize.SCREEN_WIDTH
//				}, completion: nil)
                
                UIView.animate(withDuration: timeInter, animations: {
                    i.frame.origin.x = ScreenSize.SCREEN_WIDTH * -1
                })
			}

			for i in self._controllers
			{
				if(i.Name != self.currentControllerName)
				{
					(i.Controller as? ViewControllerDelegate)?.willBeDisappear()
				}
			}

			if(self._controllers.filter({ $0.Name == controllerName }).count == 0)
			{

				print("*** observerChangeController 3")

				let controller = self.getControllerByName(name: controllerName, params: controllerParams)
				let controllerModel = ControllerModel(name: controllerName, controller: controller)

				(controllerModel.Controller as? ViewControllerDelegate)?.setParams(params: controllerParams)

				self._controllers.append(controllerModel)
				//				self.view.addSubview(controllerModel.Controller.view)
				Core.shared().RootWindow.rootViewController!.view.addSubview(controllerModel.Controller.view)

				(controllerModel.Controller as? ViewControllerDelegate)?.willBeAppear()

				//				self.view.bringSubview(toFront: controllerModel.Controller.view)

				Core.shared().RootWindow.rootViewController!.view.bringSubviewToFront(controllerModel.Controller.view)

                controllerModel.Controller.view.frame.origin.x = ScreenSize.SCREEN_WIDTH
                UIView.animate(withDuration: timeInter, animations: {
                    controllerModel.Controller.view.frame.origin.x = 0
                })
                
//                controllerModel.Controller.view.frame.origin.x = 0

			}
			else
			{

				print("*** observerChangeController 4")
				let controllerModel = self._controllers.filter({ $0.Name == controllerName }).first

				(controllerModel?.Controller as? ViewControllerDelegate)?.setParams(params: controllerParams)

				//				self.view.bringSubview(toFront: (controllerModel?.Controller.view)!)

				Core.shared().RootWindow.rootViewController!.view.bringSubviewToFront((controllerModel?.Controller.view)!)

				(controllerModel?.Controller as? ViewControllerDelegate)?.willBeAppear()

//                controllerModel?.Controller.view.frame.origin.x = 0
                
                controllerModel!.Controller.view.frame.origin.x = ScreenSize.SCREEN_WIDTH
                UIView.animate(withDuration: timeInter, animations: {
                    controllerModel!.Controller.view.frame.origin.x = 0
                })

			}
		})
	}

	func getControllerByName(name: String, params: Dictionary<String, Any>) -> UIViewController
	{
		switch(name)
		{
        case "signatureController":
            return SignatureController()
		case "dashboardController":
			return DashboardController()
		case "scannerController":
			return ScannerController()
        case "settingsController":
            return SettingsController()
        case "promoController":
            return PromoController()
		default:
			return DashboardController()
		}
	}

	func StartMain()
	{

        if let settings = Core.shared().getRealm.objects(SettingsModel.self).first
        {
            if settings.IsPromo
            {
                Core.shared().changeViewController(name: settings.LaunchController)
            }
            else
            {
                Core.shared().changeViewController(name: "promoController")
            }
            
        }
		
	}

	var startLoader: StartLoader!

	func authorize()
	{
		startLoader = StartLoader(delegate: self)
		self.view.addSubview(startLoader)

		print("*** authorize 1")
		Core.shared().startAutorization { (success) in

            print("*** authorize 2")
//			self.removeAllControllers()
//
//			self.StartMain()

			self.didAuth = true

			if(self.didLogo)
			{
				self.StartMain()
			}
		}
	}

	func StartLoaderEnd()
	{
		didLogo = true

		if(didAuth)
		{
			StartMain()
		}
	}

	var didLogo = false

	var didAuth = false

	func testDocs()
	{
        if Core.shared().getRealm.objects(OCRModel.self).count == 0
        {
            var ocr1 = OCRModel()
            ocr1.Id = "eng"
            ocr1.Name = ""
            ocr1.IsChecked = true
            
            var ocr2 = OCRModel()
            ocr2.Id = "deu"
            ocr2.Name = ""
            ocr2.IsChecked = false
            
            var ocr3 = OCRModel()
            ocr3.Id = "fra"
            ocr3.Name = ""
            ocr3.IsChecked = false
            
            var ocr4 = OCRModel()
            ocr4.Id = "ita"
            ocr4.Name = ""
            ocr4.IsChecked = false
            
            var ocr5 = OCRModel()
            ocr5.Id = "spa"
            ocr5.Name = ""
            ocr5.IsChecked = false
            
            var ocr6 = OCRModel()
            ocr6.Id = "tur"
            ocr6.Name = ""
            ocr6.IsChecked = false
            
            var ocr7 = OCRModel()
            ocr7.Id = "por"
            ocr7.Name = ""
            ocr7.IsChecked = false
            
            try! Core.shared().getRealm.write
            {
                Core.shared().getRealm.add([ocr1, ocr2, ocr3, ocr4, ocr5, ocr6, ocr7], update: true)
            }
        }
        
        
//        if(Core.shared().IsDebug)
//        {
			let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)

			var testFiles = ["test_8.jpg", "test2.jpg", "test3.jpg", "test4.jpg", "test_7.jpg", "signature.png"]

			if Core.shared().getRealm.objects(DocModel.self).count == 0
			{
				var ii = 0

				for i in testFiles
				{
					let documentsURL1 = Bundle.main.resourceURL?.appendingPathComponent(i)

					do
					{
						try FileManager.default.copyItem(atPath: (documentsURL1?.path)!, toPath: docDirURL.appendingPathComponent(i).path)
					}
					catch let error as NSError
					{
						print("Couldn't copy file to final location! Error:\(error.description)")
					}
				}

				let doc = DocModel()
				doc.Id = UUID().uuidString
				doc.CreatedAt = Int64(NSDate().timeIntervalSince1970)
				doc.Name = "Sample group"

				var docFile = DocModelFile()
				docFile.Id = testFiles[0]
				docFile.DocId = doc.Id
				docFile.Width = 0
				docFile.Height = 0
				docFile.FileName = testFiles[0]

				var docFile2 = DocModelFile()
				docFile2.Id = testFiles[1]
				docFile2.DocId = doc.Id
				docFile2.Width = 0
				docFile2.Height = 0
				docFile2.FileName = testFiles[1]

				var docFile3 = DocModelFile()
				docFile3.Id = testFiles[2]
				docFile3.DocId = doc.Id
				docFile3.Width = 0
				docFile3.Height = 0
				docFile3.FileName = testFiles[2]

				try! Core.shared().getRealm.write
				{
					Core.shared().getRealm.add(doc, update: true)
					Core.shared().getRealm.add(docFile, update: true)
					Core.shared().getRealm.add(docFile2, update: true)
					Core.shared().getRealm.add(docFile3, update: true)
				}

				//-----

				let doc2 = DocModel()
				doc2.Id = UUID().uuidString
				doc2.CreatedAt = Int64(NSDate().timeIntervalSince1970)
				doc2.Name = "Sample doc"

				var docFile2_1 = DocModelFile()
				docFile2_1.Id = UUID().uuidString
				docFile2_1.DocId = doc2.Id
				docFile2_1.Width = 0
				docFile2_1.Height = 0
				docFile2_1.FileName = testFiles[3]
                docFile2_1.IsSignature = true

				let doc3 = DocModel()
                doc3.IsFolder = true
				doc3.Id = UUID().uuidString
				doc3.CreatedAt = Int64(NSDate().timeIntervalSince1970)
				doc3.Name = "Sample folder"
                
                let doc4 = DocModel()
                doc4.IsFolder = false
                doc4.Id = UUID().uuidString
                doc4.CreatedAt = Int64(NSDate().timeIntervalSince1970)
                doc4.Name = "Sample"
                doc4.ParentId = doc3.Id

				var docFile3_1 = DocModelFile()
				docFile3_1.Id = UUID().uuidString
				docFile3_1.DocId = doc4.Id
				docFile3_1.Width = 0
				docFile3_1.Height = 0
				docFile3_1.FileName = testFiles[4]
                docFile3_1.ParentId = doc3.Id

				try! Core.shared().getRealm.write
				{
					Core.shared().getRealm.add(doc2, update: true)
					Core.shared().getRealm.add(doc3, update: true)
                    Core.shared().getRealm.add(doc4, update: true)
					Core.shared().getRealm.add(docFile2_1, update: true)
					Core.shared().getRealm.add(docFile3_1, update: true)
				}
			}
//        }
	}
    
//    var imagePicker = UIImagePickerController(navigationBarStyle: UIBarStyle.black)
    
    var imagePicker = UIImagePickerController(navigationBarStyle: UIBarStyle.black)
    
    var _toDocIdImport = ""
    
    var _toFolderIdImport = ""
    
    @objc func observerImportImage(_ notification: NSNotification)
    {
        DispatchQueue.main.async(execute: {
            
            let params = notification.userInfo as! Dictionary<String, AnyObject>
            
            self._toFolderIdImport = params["folder_id"] as? String ?? ""
            
            self._toDocIdImport = ""
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary)
            {
                print("Button capture")
                
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum;
                self.imagePicker.allowsEditing = false
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
    }
    
    func getFilename() -> String
    {
        let date = NSDate()
        
        let df1 = DateFormatter()
        let locale = Locale(identifier: s("locale_id"))
        df1.locale = locale
        
        df1.dateFormat = s("date_format")
        let stringDate =  df1.string(from: date as Date)
        
        return String(format: s("file_name_format"), stringDate)
        
    }
}

extension MainController: DocEditDelegate
{
    func DocEditBack(id: String)
    {
        EventsManager.shared().sendNotify(Constants.EVENT_OPEN_DOC, data: ["doc_id": id])
    }
}

extension MainController: DocViewDelegate
{
    func DocViewOpenEdit(id: String)
    {
        var edit = DocEdit(id: id)
        edit.delegate = self
        self.view.addSubview(edit)
        
        UIView.animate(withDuration: 0.15, animations: {
            edit.setX(0)
        })
    }
    
    func DocViewImportFromLibrary(id: String)
    {
        _toDocIdImport = id
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary)
        {
            print("Button capture")

            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .savedPhotosAlbum;
            self.imagePicker.allowsEditing = false

            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
}

extension MainController: UINavigationControllerDelegate, UIImagePickerControllerDelegate
{
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!)
    {

    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        self.dismiss(animated: true, completion: { () -> Void in
            if(!self._toDocIdImport.isEmpty)
            {
                EventsManager.shared().sendNotify(Constants.EVENT_OPEN_DOC, data: ["doc_id": self._toDocIdImport])
            }
        })
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool)
    {
        UIApplication.shared.statusBarStyle = .default
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        print("didFinishPickingImage")
        
        DispatchQueue.main.async(execute: {
            
            var waitingView = UIView()
            waitingView.backgroundColor = UIColor.black.withAlphaComponent(0.75)
            waitingView.setX(0)
            waitingView.setY(0)
            waitingView.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
            self.view.addSubview(waitingView)
            
            var waitingLoader = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
            waitingLoader.setSize(50, 50)
            waitingLoader.toCenterX(waitingView)
            waitingLoader.toCenterY(waitingView)
            waitingLoader.startAnimating()
            waitingView.addSubview(waitingLoader)
            
            var waitingTitle = UILabel()
            waitingTitle.textAlignment = .center
            waitingTitle.setSize(ScreenSize.SCREEN_WIDTH, 20)
            waitingTitle.text = s("importing")
            waitingTitle.setX(0)
            waitingTitle.setY(10, relative: waitingLoader)
            waitingTitle.textColor = UIColor.white
            waitingView.addSubview(waitingTitle)
            
            self.view.bringSubviewToFront(waitingView)
            
            self.dismiss(animated: true, completion: { () -> Void in
                
                var image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
                
                var isRotate = false
                
                var degrees: CGFloat = 0
                
                var isFlip = true
                
                switch(image.imageOrientation)
                {
                case .down:
                    isRotate = true
                    degrees = 90
                    isFlip = false
                    break
                case .left:
                    isRotate = true
                    degrees = 90
                    break
                case .right:
                    isRotate = true
                    degrees = -90
                    break
                case .up:
                    isRotate = false
                    degrees = 0
                    break
                default: break
                }
                
                var newImage = image.toNoir(isRotate, degrees: degrees, flip: isFlip)
                
                if(image.imageOrientation == .down)
                {
                    newImage = newImage.imageRotatedByDegrees(degrees: 90, flip: true)
                }

                var fileName = UUID().uuidString + ".jpg"
                
                let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)
                
                let fileURL = docDirURL.appendingPathComponent(fileName)
                
                print("** IMPORT IMAGE OR \(image.imageOrientation.rawValue)  \(newImage.imageOrientation.rawValue)")
                
                do
                {
                    try newImage.jpegData(compressionQuality: 1)!.write(to: fileURL, options: [.atomic])
                }
                catch(let error)
                {
                    print(error)
                }
                
                if(self._toDocIdImport.isEmpty)
                {
                    let doc = DocModel()
                    doc.ParentId = self._toFolderIdImport
                    doc.Id = UUID().uuidString
                    doc.CreatedAt = Int64(NSDate().timeIntervalSince1970)
                    doc.Name = self.getFilename()
                    
                    try! Core.shared().getRealm.write {
                        Core.shared().getRealm.add(doc)
                    }
                    
                    self._toDocIdImport = doc.Id
                }

                if let doc = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(self._toDocIdImport)'").first
                {
                    let file = DocModelFile()
                    file.ParentId = self._toFolderIdImport
                    file.Width = Float(image.size.width)
                    file.Height = Float(image.size.height)
                    file.FileName = fileName
                    file.Id = UUID().uuidString
                    file.DocId = doc.Id
                    file.TypeFile = "image"
    
                    try! Core.shared().getRealm.write {
                        Core.shared().getRealm.add(file)
                    }
                    
                    waitingView.removeFromSuperview()
                    
                    if(self._toFolderIdImport.isEmpty)
                    {
                        EventsManager.shared().sendNotify(Constants.EVENT_OPEN_DOC, data: ["doc_id": doc.Id])
                    }
                    else
                    {
                        EventsManager.shared().sendNotify(Constants.EVENT_OPEN_FOLDER, data: ["folder_id": self._toFolderIdImport])
                    }
                    
                    
                }
                else
                {
                    waitingView.removeFromSuperview()
                }
                
                self._toFolderIdImport = ""
            })
            
        })
    }
}

extension UIImagePickerController
{
    convenience init(navigationBarStyle: UIBarStyle)
    {
        self.init()
        self.navigationBar.barStyle = navigationBarStyle
    }
}

extension MainController
{
    @objc func observerShare(_ notification: NSNotification)
    {
        DispatchQueue.main.async(execute: {
            let params = notification.userInfo as! Dictionary<String, AnyObject>

            let doc_id = params["id"] as? String ?? ""
            
            let type = params["type"] as? String ?? ""
            
            print("** observerShare \(doc_id) \(type)")
            
            if let doc = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(doc_id)'").first
            {
                switch(type)
                {
                case "image_save":
                    self._shareImageSave(doc_id: doc_id)
                    break;
                case "pdf":
                    self._sharePDF(doc_id: doc_id)
                    break
                case "image":
                    self._shareImage(doc_id: doc_id)
                    break
                case "text":
                    self._shareText(doc_id: doc_id)
                    break
                case "ocrpdf":
                    self._shareOcrPDF(doc_id: doc_id)
                    break;
                default:
                    break
                }
            }
        })
    }
    
    func _shareText(doc_id: String)
    {
        var data = [String]()
        
        for i in Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(doc_id)'")
        {
            data.append(i.ORCText)
        }
        
        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: data, applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceView=self.view
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func _shareOcrPDF(doc_id: String)
    {
        let a4PaperSize = CGSize(width: 595, height: 842)
        let pdf = SimplePDF(pageSize: a4PaperSize)
        
        pdf.addLineSpace(30)
        
        pdf.setContentAlignment(.left)
        
        var tick = 0
        
        for i in Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(doc_id)'")
        {
            if(tick > 0)
            {
                pdf.beginNewPage()
            }
            
            pdf.addText(i.ORCText)
            
            tick += 1
        }
        
        //                    if let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
        
        let fileName = "example.pdf"
        
        let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)
        
        let fileURL = docDirURL.appendingPathComponent(fileName)
        
        
        //                        let documentsFileName = documentDirectories + "/" + fileName
        
        let pdfData = pdf.generatePDFdata()
        
        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [pdfData], applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func _shareImage(doc_id: String)
    {
        var dataA = [UIImage]()
        
        let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)
        
        for i in Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(doc_id)'")
        {
            let path = docDirURL.appendingPathComponent(i.FileName)
            
            let data = try? Data(contentsOf: path)
            
            var image = UIImage(data: data!)!
            
            dataA.append(image)
        }
        
        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: dataA, applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func _sharePDF(doc_id: String)
    {
        var files = [DocModelFile]()
        
        let pdfData = NSMutableData()
        
        let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)
        
        for i in Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(doc_id)'")
        {
            files.append(i)
        }
        
        let path = docDirURL.appendingPathComponent(files[0].FileName)
        
        let data = try? Data(contentsOf: path)
        
        var image = UIImage(data: data!)!
        
//        let imgView = UIImageView.init(image: image)
        
        
        let pageWidth: Double = Double(image.size.width) * Double(image.scale) * Double(72) / 96
        let pageHeight: Double = Double(image.size.height) * Double(image.scale) * Double(72) / 96
        
//        let imageRect = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        
        var mediaBox: CGRect = CGRect(x: 0, y: 0, width: CGFloat(pageWidth), height: CGFloat(pageHeight))
        
//        UIGraphicsBeginPDFContextToData(pdfData, &mediaBox, nil)
        
        
        //---
        
        var signBox: CGRect!
        var sign: UIImage!
        
        if files[0].IsSignature
        {
//            let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)
            
            let pathSign = docDirURL.appendingPathComponent(Constants.SIGN_IMAGE)
            
            let dataSign = try? Data(contentsOf: pathSign)
            
            sign = UIImage(data: dataSign!)
            
            var signSize = getImageSize(image: sign!, width: CGFloat(pageWidth) * 0.33, height: 0)
            
            if(files[0].SignatureDocHeight > 0 &&
                files[0].SignatureDocWidth > 0 &&
                files[0].SignatureHeight > 0 &&
                files[0].SignatureWidth > 0 &&
                files[0].SignatureX > 0 &&
                files[0].SignatureY > 0
                )
            {
                var factorSign: CGFloat = CGFloat(files[0].SignatureDocWidth) / CGFloat(pageWidth)
                
                print("*** factorSign \(factorSign)")
                
                signBox = CGRect(x: CGFloat(files[0].SignatureX) * factorSign,
                                 y: CGFloat(files[0].SignatureY) * factorSign,
                                width: CGFloat(files[0].SignatureWidth) * factorSign,
                                height: CGFloat(files[0].SignatureHeight) * factorSign)
                
//                signImageBlock.setSize(CGFloat(slides[slideId].SignatureWidth) * factorSign, CGFloat(slides[slideId].SignatureHeight) * factorSign)
//                signImageBlock.setX(CGFloat(slides[slideId].SignatureX) * factorSign)
//                signImageBlock.setY(CGFloat(slides[slideId].SignatureY) * factorSign)
            }
//            else
//            {
//                    signImageBlock.setSize(signSize[0], signSize[1])
//                    signImageBlock.setX(imageView.getX() + imageView.getWidth() - signImageBlock.getWidth() - 15)
//                    signImageBlock.setY(imageView.getY() + imageView.getHeight() - signImageBlock.getHeight() - 15)
//
//            }
        }
        
        //---
        
        let pdfConsumer: CGDataConsumer = CGDataConsumer(data: pdfData as CFMutableData)!
        let pdfContext: CGContext = CGContext(consumer: pdfConsumer, mediaBox: &mediaBox, nil)!
        
        pdfContext.beginPage(mediaBox: &mediaBox)
        pdfContext.draw(image.cgImage!, in: mediaBox)
        
        if files[0].IsSignature
        {
            pdfContext.draw(sign.cgImage!, in: signBox)
        }
        pdfContext.endPage()
        
//        UIGraphicsBeginPDFPage()
        
//        let context = UIGraphicsGetCurrentContext()
        
//        imgView.layer.render(in: context!)

        
//        UIGraphicsEndPDFContext()
        
        
        //try saving in doc dir to confirm:
        let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
        
        let path1 = dir?.appendingPathComponent("imagepdf.pdf")
        
        do {
            try pdfData.write(to: path1!, options: NSData.WritingOptions.atomic)
        } catch {
            print("error catched")
        }
        
        let myPDF = NSData(contentsOf: path1!)
        
        //                let documento = NSData(contentsOfFile: myPDF.pat)
        
        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [myPDF!], applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceView=self.view
        
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.airDrop,
            UIActivity.ActivityType.postToFacebook,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.copyToPasteboard,
            UIActivity.ActivityType.airDrop,
            UIActivity.ActivityType.postToTwitter,
            UIActivity.ActivityType.message
        ]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func _shareImageSave(doc_id: String)
    {
        let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)
        
        for i in Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(doc_id)'")
        {
            let path = docDirURL.appendingPathComponent(i.FileName)
            
            let data = try? Data(contentsOf: path)
            
            var image = UIImage(data: data!)!
            
            UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.ImageSaved(_:didFinishSavingWithError:contextInfo:)), nil)
        }
        

    }
    
    @objc func ImageSaved(_ image: UIImage!, didFinishSavingWithError error: NSError!, contextInfo:UnsafeRawPointer)
    {
        print("**** ImageSaved")
        print(error)
        
        var title = s("saved_title")
        
        var icon = "✓"
        
        if(error != nil)
        {
            title = s("saved_title_failed")
            icon = "✗"
        }
        
        var successSave = UIView()
        successSave.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        successSave.layer.cornerRadius = 15
        successSave.setSize(ScreenSize.SCREEN_WIDTH / 3, ScreenSize.SCREEN_WIDTH / 3)
        successSave.toCenterX(self.view)
        successSave.toCenterY(self.view)
        self.view.addSubview(successSave)
        
        var successSaveIcon = UILabel()
        successSaveIcon.setX(0)
        successSaveIcon.setY(0)
        successSaveIcon.setSize(successSave.getWidth(), successSave.getHeight())
        successSaveIcon.textAlignment = .center
        successSaveIcon.textColor = .white
        successSaveIcon.font = UIFont.systemFont(ofSize: 56)
        successSaveIcon.text = icon
        successSave.addSubview(successSaveIcon)
        
        var successSaveTitle = UILabel()
        successSaveTitle.setX(0)
        successSaveTitle.setY((successSave.getHeight() / 3) * 2)
        successSaveTitle.setSize(successSave.getWidth(), 20)
        successSaveTitle.textAlignment = .center
        successSaveTitle.textColor = .white
        successSaveTitle.font = UIFont.systemFont(ofSize: 14)
        successSaveTitle.text = title
        successSave.addSubview(successSaveTitle)
        
        UIView.animate(withDuration: 0.5, delay: 1.5, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveEaseInOut, animations:
        {
            successSave.alpha = 0
        }, completion: { (finished: Bool) -> Void in
            successSave.removeFromSuperview()
        })
    }

}
