//
//  ScannerControllerCaptureVideo.swift
//  Docscaner
//
//  Created by lynxmac on 25/03/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import GLKit
import CoreMedia
import CoreImage
import OpenGLES
import QuartzCore

extension ScannerController: AVCaptureVideoDataOutputSampleBufferDelegate
{
    func prepareRectangleDetector() -> CIDetector
    {
        let options: [String: Any] = [CIDetectorAccuracy: CIDetectorAccuracyHigh, CIDetectorAspectRatio: 1.0]
        
        return CIDetector(ofType: CIDetectorTypeRectangle, context: nil, options: options)!
    }
    
    func drawHighlightOverlayForPoints(_ image: CIImage, topLeft: CGPoint, topRight: CGPoint, bottomLeft: CGPoint, bottomRight: CGPoint) -> CIImage
    {
        var overlay = CIImage(color: CIColor(red: 0, green: 0, blue: 255, alpha: 0.5))
        
        overlay = overlay.cropped(to: image.extent)
        
        overlay = overlay.applyingFilter("CIPerspectiveTransformWithExtent",
                                         parameters: [
                                            "inputExtent": CIVector(cgRect: image.extent),
                                            "inputTopLeft": CIVector(cgPoint: topLeft),
                                            "inputTopRight": CIVector(cgPoint: topRight),
                                            "inputBottomLeft": CIVector(cgPoint: bottomLeft),
                                            "inputBottomRight": CIVector(cgPoint: bottomRight)
            ])
        return overlay.composited(over: image)
    }
    
    func performRectangleDetection(_ image: CIImage) -> ScannerDetectionResult
    {
        var resultImage: CIImage?
        
        var bounds: CGRect = CGRect.zero
        
        var points = [CGPoint]()
        
        if let detector = detector
        {
            // Get the detections
            let features = detector.features(in: image)
            
            for feature in features as! [CIRectangleFeature]
            {
                bounds = feature.bounds
                
                resultImage = drawHighlightOverlayForPoints(image, topLeft: feature.topLeft, topRight: feature.topRight, bottomLeft: feature.bottomLeft, bottomRight: feature.bottomRight)
                
                points = [feature.topLeft, feature.topRight, feature.bottomLeft, feature.bottomRight]
            }
        }
        
        var result = ScannerDetectionResult(image: resultImage, bounds: bounds, points: points)
        
        return result
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection)
    {
        print("** didOutputSampleBuffer 2")
        
        let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
        
        // Force the type change - pass through opaque buffer
        
        let opaqueBuffer = Unmanaged<CVImageBuffer>.passUnretained(imageBuffer!).toOpaque()
        
        let pixelBuffer = Unmanaged<CVPixelBuffer>.fromOpaque(opaqueBuffer).takeUnretainedValue()
        
        let sourceImage = CIImage(cvPixelBuffer: pixelBuffer, options: nil)
        
        // Do some detection on the image
        
        let detectionResult = performRectangleDetection(sourceImage)
        
        var outputImage = sourceImage
        
        var isDetect = false
        
        var nImage: UIImage!
        
        if detectionResult.ResultImage != nil
        {
            print("*** detectionResult 1")
            
            var nRect = CGRect(ceil(detectionResult.Bounds!.origin.x), ceil(detectionResult.Bounds!.origin.y), ceil(detectionResult.Bounds!.size.width), ceil(detectionResult.Bounds!.size.height))
            
//            print("RECTANGLE \(detectionResult.Bounds) \(nRect)")
            
            print("*** detectionResult 1.2")
            
            outputImage = detectionResult.ResultImage!
            
            print("*** detectionResult 1.3")
            
            //---
            
            let openGLContext = EAGLContext(api: .openGLES2)
            let ciContext =  CIContext(eaglContext: openGLContext!)
            
            print("*** detectionResult 1.4")
            
            //points = [feature.topLeft, feature.topRight, feature.bottomLeft, feature.bottomRight]
            
            var outputImage = sourceImage.applyingFilter("CIPerspectiveCorrection", parameters: [
                "inputTopLeft": CIVector(cgPoint: detectionResult.Points[0]),
                "inputTopRight": CIVector(cgPoint: detectionResult.Points[1]),
                "inputBottomRight": CIVector(cgPoint: detectionResult.Points[3]),
                "inputBottomLeft": CIVector(cgPoint: detectionResult.Points[2])
                ])
            
            print("*** detectionResult 1.5")
            
            let cgImage = ciContext.createCGImage(outputImage, from: outputImage.extent)
            
            print("*** detectionResult 1.5.1")
            
            nImage = UIImage(cgImage: cgImage!, scale: 1, orientation: .up)
            
            print("*** detectionResult 1.6")

            ticks += 1
            
            print("*** TICKS \(ticks)")
            
            if(ticks >= 10 /*&& nRect == prevResult*/)
            {
                print("*** isDetect")
                isDetect = true
            }
            
            prevResult = nRect
            
             print("*** detectionResult 2")
        }
        
        // Do some clipping
        var drawFrame = outputImage.extent
        
        let imageAR = drawFrame.width / drawFrame.height
        
        let viewAR = videoDisplayViewBounds.width / videoDisplayViewBounds.height
        
        if imageAR > viewAR
        {
            drawFrame.origin.x += (drawFrame.width - drawFrame.height * viewAR) / 2.0
            drawFrame.size.width = drawFrame.height / viewAR
        }
        else
        {
            drawFrame.origin.y += (drawFrame.height - drawFrame.width / viewAR) / 2.0
            drawFrame.size.height = drawFrame.width / viewAR
        }
        
        videoDisplayView.bindDrawable()
        
        if videoDisplayView.context != EAGLContext.current()
        {
            EAGLContext.setCurrent(videoDisplayView.context)
        }
        
        // clear eagl view to grey
        glClearColor(0.5, 0.5, 0.5, 1.0);
        glClear(0x00004000)
        
        // set the blend mode to "source over" so that CI will use that
        glEnable(0x0BE2);
        glBlendFunc(1, 0x0303);
        
        renderContext.draw(outputImage, in: videoDisplayViewBounds, from: drawFrame)
        
        videoDisplayView.display()
        
        print("*** DETECT 1 isDetect \(isDetect) timerCounter \(timerCounter)")
        
        if(isDetect && timerCounter == 0)
        {
            print("*** DETECT 2")
            captureSessionVideo.stopRunning()
            isDetect = false
            ticks = 0
            
            print("*** DETECT 3")
            if(videoPreviewLayer != nil )
            {
                print("*** DETECT 4")
                DispatchQueue.main.async(execute: {
                    print("*** DETECT 5")
                    self.addPreviewLive(image: nImage)
                })
            }
        }
    }
    
    func addPreviewLive(image: UIImage)
    {
        var newImage = image.toNoir(true)
        
        autoScanImage = newImage
        
        print("*** DETECT 6")
        
        if(previewLiveLayer != nil)
        {
            previewLiveLayer.removeFromSuperview()
        }
        
        print("*** DETECT 7")
        
        previewLiveLayer = UIView()
        previewLiveLayer.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        previewLiveLayer.setX(0)
        previewLiveLayer.setY(0)
        previewLiveLayer.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        self.view.addSubview(previewLiveLayer)
        
        let maxImageWidth: CGFloat = ScreenSize.SCREEN_WIDTH - 80
        
        let maxImageHeight: CGFloat = ScreenSize.SCREEN_HEIGHT / 2

        let imageWidthOrigin = newImage.size.width
        let imageHeightOrigin = newImage.size.height
        
        var imageWidth: CGFloat = 0
        var imageHeight: CGFloat = 0
        
        var factor: CGFloat = 0
        
        ///---
        
        if(imageWidthOrigin < imageHeightOrigin)
        {
            imageHeight = maxImageHeight
            
            factor = CGFloat(imageHeight) / CGFloat(imageHeightOrigin)
            
            imageWidth = CGFloat(imageWidthOrigin) * factor
        }
        else
        {
            imageWidth = maxImageWidth
            
            factor = CGFloat(imageWidth) / CGFloat(imageWidthOrigin)
            
            imageHeight = CGFloat(imageHeightOrigin) * factor
        }

        
        var previewView = UIView()
        previewView.setSize(imageWidth + 20, imageHeight + 20)
        previewView.backgroundColor = UIColor.white
        previewView.toCenterX(previewLiveLayer)
        previewView.toCenterY(previewLiveLayer)
        previewLiveLayer.addSubview(previewView)
        
        var previewImage = UIImageView()
        previewImage.setX(10)
        previewImage.setY(10)
        previewImage.setSize(imageWidth, imageHeight)
        previewImage.image = newImage
        previewView.addSubview(previewImage)
        
        var buttonView = UIView()
        buttonView.setSize(ScreenSize.SCREEN_WIDTH - 80, 50)
        buttonView.setY(20, relative: previewView)
        buttonView.toCenterX(previewLiveLayer)
        buttonView.backgroundColor = UIColor.white
        buttonView.layer.cornerRadius = 15
        previewLiveLayer.addSubview(buttonView)
        
        var buttonLabel = UILabel()
        buttonLabel.setX(0)
        buttonLabel.setY(0)
        buttonLabel.setSize(buttonView.getWidth(), buttonView.getHeight())
        buttonLabel.text = s("retake_title")
        buttonLabel.textAlignment = .center
        buttonLabel.font = UIFont.systemFont(ofSize: 16)
        buttonLabel.textColor = .black
        buttonView.addSubview(buttonLabel)
        
        var buttonAction = UIButton()
        buttonAction.setX(0)
        buttonAction.setY(0)
        buttonAction.setSize(buttonView.getWidth(), buttonView.getHeight())
        buttonAction.addTarget(self, action: #selector(self.retakeScan(_:)), for: .touchUpInside)
        buttonView.addSubview(buttonAction)
        
        //---
        
        var cancelView = UIView()
        cancelView.setSize(ScreenSize.SCREEN_WIDTH - 80, 50)
        cancelView.setY(20, relative: buttonView)
        cancelView.toCenterX(previewLiveLayer)
        cancelView.backgroundColor = UIColor.white
        cancelView.layer.cornerRadius = 15
        previewLiveLayer.addSubview(cancelView)
        
        var buttonLabel1 = UILabel()
        buttonLabel1.setX(0)
        buttonLabel1.setY(0)
        buttonLabel1.setSize(buttonView.getWidth(), buttonView.getHeight())
        buttonLabel1.text = s("scan_close")
        buttonLabel1.textAlignment = .center
        buttonLabel1.font = UIFont.systemFont(ofSize: 16)
        buttonLabel1.textColor = .black
        cancelView.addSubview(buttonLabel1)
        
        var buttonAction1 = UIButton()
        buttonAction1.setX(0)
        buttonAction1.setY(0)
        buttonAction1.setSize(buttonView.getWidth(), buttonView.getHeight())
        buttonAction1.addTarget(self, action: #selector(self.closeScan(_:)), for: .touchUpInside)
        cancelView.addSubview(buttonAction1)
        
        print("*** DETECT 8")
    }
    
    func saveAutoScanImage()
    {
        var image = autoScanImage
        
        var newImage = image!.rotated(by: Measurement(value: 90, unit: .degrees))
        
        var fileName = UUID().uuidString + ".jpg"
        
        let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)
        
        let fileURL = docDirURL.appendingPathComponent(fileName)
        
        do
        {
            try image!.jpegData(compressionQuality: 1)!.write(to: fileURL, options: [.atomic])
        }
        catch(let error)
        {
            print(error)
        }
        
        //---
        
        let doc = DocModel()
        doc.Id = UUID().uuidString
        doc.CreatedAt = Int64(NSDate().timeIntervalSince1970)
        doc.Name = getFilename()
        
        let file = DocModelFile()
        file.Width = Float(newImage!.size.width)
        file.Height = Float(newImage!.size.height)
        file.FileName = fileName
        file.Id = UUID().uuidString
        file.DocId = doc.Id
        file.TypeFile = "image"
        
        try! Core.shared().getRealm.write {
            Core.shared().getRealm.add(doc)
            Core.shared().getRealm.add(file)
        }
        
        EventsManager.shared().sendNotify(Constants.EVENT_OPEN_DOC, data: ["doc_id": doc.Id])
        
        EventsManager.shared().sendNotify(Constants.EVENT_RELOAD_LIB)
    }
    
    @objc func closeScan(_ sender: AnyObject)
    {
        if(previewLayer != nil)
        {
            previewLiveLayer.removeFromSuperview()
        }
        
        saveAutoScanImage()
        
        self.LiveScannerClosed()
    }
    
    @objc func retakeScan(_ sender: AnyObject)
    {
        if(previewLayer != nil)
        {
            previewLiveLayer.removeFromSuperview()
        }
        
        self.RetakeScanner()
    }
}
