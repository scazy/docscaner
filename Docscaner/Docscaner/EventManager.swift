//
//  EventManager.swift
//  Docscaner
//
//  Created by Artem  on 28/02/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation

class EventsManager
{
	private static let sharedi =  EventsManager()

	public static func shared() -> EventsManager {
		return sharedi
	}

	func sendNotify(_ notify: String, data: [String: Any])
	{
		let nameNotif = String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, notify)

		NotificationCenter.default.post(name: Notification.Name(rawValue: nameNotif), object: nil, userInfo: data)
	}

	func sendNotify(_ notify: String, object: [String: Any])
	{
		let nameNotif = String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, notify)

		NotificationCenter.default.post(name: Notification.Name(rawValue: nameNotif), object: nil, userInfo: object)
	}

	func sendNotify(_ notify: String)
	{
		let nameNotif = String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, notify)

		NotificationCenter.default.post(name: Notification.Name(rawValue: nameNotif), object: nil, userInfo: nil)
	}
}
