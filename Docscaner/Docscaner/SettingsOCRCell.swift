//
//  SettingsOCRCell.swift
//  Docscaner
//
//  Created by artem on 24/04/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit

protocol CellOCRBaseDelegate
{
    func Update(post: OCRModel)
    
    func SetChecked()
}

protocol CellOCRActionDelegate
{
    
}

class CellOCRBase: UITableViewCell, CellOCRBaseDelegate
{
    func SetChecked()
    {
        print("*** CellOCRBase SetChecked")
    }
    
    var delegate: CellOCRActionDelegate!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clear
        
        self.selectionStyle = UITableViewCell.SelectionStyle.none
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
    }
    
    override func prepareForReuse()
    {
        super.prepareForReuse()
    }
    
    func Update(post: OCRModel)
    {
        
    }
    

}

//----

class CellOCR : CellOCRBase
{

    var name = UILabel()
    
    var checkbox = UIView()
    
    var checkboxImage = UIImageView()
    
    var IsChecked = false

    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse()
    {
        super.prepareForReuse()
//
        self.name.text = nil

        checkboxImage.image = nil
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        for subview in contentView.subviews
        {
            subview.removeFromSuperview()
        }
        
        self.contentView.backgroundColor = UIColor.clear
//
        self.contentView.addSubview(name)
        
        self.contentView.addSubview(checkbox)
        
        checkbox.addSubview(checkboxImage)

        name.font = UIFont.systemFont(ofSize: 17)
        
//        self.contentView.backgroundColor = .red
        
//        self.name.backgroundColor = .green
    }

    override func Update(post: OCRModel)
    {
        
        self.IsChecked = post.IsChecked
        
        
        self.name.setY(0)
        self.name.setX(20)
        self.name.setSize(self.contentView.getWidth(), self.contentView.getHeight())
        self.name.text = s("ocr_\(post.Id)")

        checkbox.setSize(20, 20)
        checkbox.backgroundColor = UIColor(red: 0.77, green: 0.79, blue: 0.79, alpha: 1)
        checkbox.layer.cornerRadius = 2
        checkbox.setX(self.contentView.getWidth() - 15 - checkbox.getWidth())
        checkbox.toCenterY(self.contentView)
//        checkbox.isHidden = true
        
        checkboxImage.image = UIImage(named: "folder_checkbox")
        checkboxImage.setX(0)
        checkboxImage.setY(0)
        checkboxImage.setSize(checkbox.getWidth(), checkbox.getHeight())
        checkboxImage.isHidden = true
        
        if(IsChecked)
        {
            checkbox.backgroundColor = UIColor(red: 0, green: 0.6, blue: 0.77, alpha: 1)
            checkboxImage.isHidden = false
        }
    }
    
    override func SetChecked()
    {
        print("*** CellOCR SetChecked \(IsChecked)")
        if(!IsChecked)
        {
            checkbox.backgroundColor = UIColor(red: 0, green: 0.6, blue: 0.77, alpha: 1)
            checkboxImage.isHidden = false
            self.IsChecked = true
        }
        else
        {
            checkbox.backgroundColor = UIColor(red: 0.77, green: 0.79, blue: 0.79, alpha: 1)
            checkboxImage.isHidden = true
            self.IsChecked = false
        }
    }
}

