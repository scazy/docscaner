//
//  SettingsOCR.swift
//  Docscaner
//
//  Created by artem on 24/04/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit

protocol SettingsOCRDelegate
{
    func SettingsOCRSave()
}

class SettingsOCR: UIView
{
    private var height: CGFloat = 42
    
    var tableView: UITableView!
    
    var _data = [OCRModel]()
    
    var _cells = [CellOCR]()
    
    var delegate: SettingsOCRDelegate!
    
    init()
    {
        var startOffset = UIApplication.shared.statusBarFrame.size.height
        
        super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))
        
        self.backgroundColor = .white
        
        var topPart = self.getWidth() / 7
        
        //----
        
        var top = UIView()
        top.backgroundColor = .white
        top.setX(0)
        top.setY(0)
        top.setSize(self.getWidth(), 42 + startOffset)
        self.addSubview(top)
        
        var settingsLayout = UIView()
        //        settingsLayout.backgroundColor = .red
        settingsLayout.setX(5)
        settingsLayout.setY(startOffset)
        settingsLayout.setSize(height, height)
        top.addSubview(settingsLayout)
        
        var lib_i_5 = UIImage(named: "folder_back")
        var lib_i_5Size = getImageSize(image: lib_i_5!, width: 0, height: settingsLayout.getHeight() - 20)
        
        var settingsIcon = UIImageView()
        settingsIcon.image = lib_i_5
        settingsIcon.setSize(settingsLayout.getWidth() - 14, settingsLayout.getWidth() - 14)
        settingsIcon.setX(7)
        settingsIcon.setY(7)
        settingsLayout.addSubview(settingsIcon)
        
        var settingsButton = UIButton()
        settingsButton.setX(0)
        settingsButton.setY(0)
        settingsButton.setSize(settingsLayout.getWidth(), settingsLayout.getHeight())
        settingsButton.addTarget(self, action: "backAction:", for: .touchUpInside)
        settingsLayout.addSubview(settingsButton)
        
        var libTitle = UILabel()
        libTitle.setX(10, relative: settingsIcon)
        libTitle.setY(startOffset)
        libTitle.setSize(ScreenSize.SCREEN_WIDTH, settingsLayout.getHeight())
        libTitle.textColor = UIColor(red: 0.45, green: 0.45, blue: 0.45, alpha: 1)
        libTitle.text = "OCR"
        libTitle.font = UIFont(name: "Circe-Bold", size: 27)
        top.addSubview(libTitle)
        
        //----
        
        for i in Core.shared().getRealm.objects(OCRModel.self)
        {
            self._data.append(i)
        }
        
        _cells = [CellOCR]()
        
        self.tableView = UITableView()
//        self.tableView.backgroundColor = .green
        self.tableView.setX(0)
        self.tableView.setY(0, relative: top)
        self.tableView.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT - top.getHeight() - 10)
        self.tableView.separatorColor = UIColor.lightGray
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.addSubview(self.tableView)

        
        self.tableView.register(CellOCR.self, forCellReuseIdentifier: "ocrCell")
        
        self.tableView.reloadData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func backAction(_ sender: AnyObject)
    {
//        self.delegate.SettingsTopBack()
        self.delegate.SettingsOCRSave()
        
        self.removeFromSuperview()
    }
}

extension SettingsOCR: UITableViewDataSource, UITableViewDelegate, CellOCRActionDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        print("*** numberOfSections")
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        print("*** numberOfRowsInSection \(self._data.count)")
        return self._data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        print("*** cellForRowAt \(indexPath.row)")
        
        let post = self._data[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ocrCell", for: indexPath) as! CellOCR
        cell.delegate = self
        (cell as CellOCRBaseDelegate).Update(post: post)
        
        _cells.append(cell)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        print("*** heightForRowAt")

        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print("*** didSelectRowAt")
        let post = self._data[indexPath.row]
        
        var count = Core.shared().getRealm.objects(OCRModel.self).filter("IsChecked = true").count
        
        var isChecked = self._data[indexPath.row].IsChecked ? false : true
        
        if(count == 1 && !isChecked)
        {
            self.shake()
            return
        }
        
        (_cells[indexPath.row] as CellOCRBaseDelegate).SetChecked()
        
        
        
        try! Core.shared().getRealm.write {
            self._data[indexPath.row].IsChecked = isChecked
        }
//        let cell = tableView.dequeueReusableCell(withIdentifier: "ocrCell", for: indexPath) as! CellOCR
//        cell.delegate = self
//        (cell as CellOCRBaseDelegate).SetChecked()
//
//        var note = ViewNote(id: post.Id)
//        self.view.addSubview(note)
        
        //        EventsManager.shared().sendNotify(Constants.EVENT_OPEN_NOTE, data: ["id": post.Id])
    }
}
