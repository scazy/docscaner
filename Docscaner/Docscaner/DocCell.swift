//
//  DocCell.swift
//  Docscaner
//
//  Created by Artem  on 11/03/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit

@objc
protocol DocCellDelegate
{
	func Update(doc: DocModel)
    
    func EnableEdit(isEdit: Bool)
    
    func IsEditableMode() -> Bool
    
    func setChecked()
    
    func getIsChecked() -> Bool
}

protocol DocCellActionsDelegate
{

}

class DocBaseCell: UICollectionViewCell
{
	var _doc: DocModel!

	var Layout = UIView()

	var delegate: DocCellActionsDelegate!

	var docDirURL: URL!
    
    var IsChecked = false
    
    var IsEditable = false

	override init(frame: CGRect)
	{
		print("** ROOT init")
		super.init(frame: frame)

		docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)
	}

	override func layoutSubviews()
	{
		print("** ROOT layoutSubviews")
		super.layoutSubviews()
	}

	override func prepareForReuse()
	{
		super.prepareForReuse()
	}

	required init?(coder aDecoder: NSCoder)
	{
		fatalError("init(coder:) has not been implemented")
	}
}

class DocCell: DocBaseCell, DocCellDelegate
{
    func IsEditableMode() -> Bool
    {
        return IsEditable
    }
    
    func getIsChecked() -> Bool
    {
        return IsChecked
    }
    
    func EnableEdit(isEdit: Bool)
    {
        print("*** DocCell EnableEdit")
        
        self.setEditable(isEditable: isEdit)
        
        IsEditable = isEdit
        
        self.IsChecked = false
    }
    
    var title = UILabel()
    
    var subtitle = UILabel()
    
    var itemLayout = UIView()
    
    let item = UIView()
    
    var checkbox = UIView()
    
    var checkboxImage = UIImageView()
    
	override init(frame: CGRect)
	{
		super.init(frame: frame)

		print("** PARENT  init")
	}

	override func layoutSubviews()
	{
		super.layoutSubviews()

		print("** PARENT  layoutSubviews")
        
        contentView.addSubview(self.Layout)
        contentView.sendSubviewToBack(self.Layout)
        
        self.Layout.addSubview(item)
        
        item.addSubview(itemLayout)
        
        self.Layout.addSubview(title)
        
        self.Layout.addSubview(subtitle)
        
        self.Layout.addSubview(checkbox)
        
        checkbox.addSubview(checkboxImage)
	}

	override func prepareForReuse()
	{
        super.prepareForReuse()
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func Update(doc: DocModel)
	{
		print("** PARENT  Update")
		self._doc = doc

		self.Layout.setX(1)
		self.Layout.setY(1)
		self.Layout.setSize(self.contentView.getWidth() - 2, self.contentView.getHeight() - 2)
//		self.Layout.backgroundColor = .red

		item.layer.cornerRadius = 10
		item.setSize(self.Layout.getWidth() - 20, self.Layout.getWidth() - 20)
		item.setX(10)
		item.setY(10)
//		item.backgroundColor = .green
		
		itemLayout.setX(0)
		itemLayout.setY(0)
		itemLayout.setSize(item.getWidth(), item.getHeight())

		title.setX(15)
		title.setY(3, relative: item)
//        title.font = UIFont.boldSystemFont(ofSize: 12)
		title.setSize(self.Layout.getWidth() - 30, 14)
		title.textColor = UIColor(red: 0.36, green: 0.36, blue: 0.36, alpha: 1)
		title.font = UIFont(name: "SFProText-Medium", size: 12)

		subtitle.setX(15)
		subtitle.setY(0, relative: title)
//        subtitle.font = UIFont.systemFont(ofSize: 9)
		subtitle.setSize(self.Layout.getWidth() - 30, 14)
		subtitle.textColor = UIColor(red: 0.36, green: 0.36, blue: 0.36, alpha: 1)
		subtitle.font = UIFont(name: "SFProText-Regular", size: 10)
		

		var countOfFiles = Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(doc.Id)'").count

		title.text = doc.Name
        
        for i in itemLayout.subviews
        {
            i.removeFromSuperview()
        }

		if(countOfFiles == 1)
		{
			let iv = UIImageView()
            
            let ivs = UIView()

			if let image = Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(doc.Id)'").first
			{
				let path = docDirURL.appendingPathComponent(image.FileName)

				let data = try? Data(contentsOf: path)

				let image = UIImage(data: data!)!

				print("** LIB IMAGE OR \(image.imageOrientation.rawValue)")

				var imageSize = getImageSize(image: image, width: 0, height: itemLayout.getHeight())

				if(imageSize[0] > imageSize[1])
				{
					var imageSize1 = getImageSize(image: image, width: itemLayout.getWidth(), height: 0)

					iv.setSize(imageSize1[0], imageSize1[1])
				}
				else
				{
					iv.setSize(imageSize[0], imageSize[1])
				}

				iv.toCenterX(itemLayout)
				iv.toCenterY(itemLayout)
				iv.image = image
                
                //---
                
                ivs.setSize(iv.getWidth(), iv.getHeight())
                ivs.setX(iv.getX())
                ivs.setY(iv.getY())
                
                ivs.layer.shadowColor = UIColor.black.cgColor
                ivs.layer.shadowOpacity = 0.2
                ivs.layer.shadowOffset = CGSize(width: 1, height: 1)
                ivs.layer.shadowRadius = 3
                ivs.layer.shadowPath = UIBezierPath(rect: ivs.bounds).cgPath
                ivs.layer.shouldRasterize = true
                ivs.layer.rasterizationScale = UIScreen.main.scale

				//---

				let date = Date(timeIntervalSince1970: TimeInterval(doc.CreatedAt))
				let dateFormatter = DateFormatter()
				dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
				dateFormatter.locale = NSLocale.current
				dateFormatter.dateFormat = "yyyy-MM-dd"

				let countBytes = ByteCountFormatter()
				countBytes.allowedUnits = [.useAll]
				countBytes.countStyle = .file
				let fileSize = countBytes.string(fromByteCount: Int64(data!.count))

				print("File size: \(fileSize)")

				subtitle.text = String(format: "%@, %@", dateFormatter.string(from: date), fileSize)
			}

            itemLayout.addSubview(ivs)
			itemLayout.addSubview(iv)
            

		}
		else
		{
			var ii2 = 0

			subtitle.text = "\(countOfFiles) scans"

			print("*** MULTI")
			for image in Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(doc.Id)'")
			{
				//						print("*** MULTI 1")
				if(ii2 > 3)
				{
					break
				}

                let ivs = UIView()
                
				var iv = UIImageView()
				let path = docDirURL.appendingPathComponent(image.FileName)
				let data = try? Data(contentsOf: path)
				let imageData = UIImage(data: data!)!

				var imageSize = getImageSize(image: imageData, width: 0, height: itemLayout.getHeight() - 25)

				if(imageSize[1] > imageSize[0])
				{
					
					iv.image = imageData
					iv.setSize(imageSize[0], imageSize[1])
					iv.toCenterY(itemLayout)
					iv.toCenterX(itemLayout)
                    
                    ivs.setSize(iv.getWidth(), iv.getHeight())
                    ivs.setX(iv.getX())
                    ivs.setY(iv.getY())
                    
                    ivs.layer.shadowColor = UIColor.black.cgColor
                    ivs.layer.shadowOpacity = 0.2
                    ivs.layer.shadowOffset = CGSize(width: 1, height: 1)
                    ivs.layer.shadowRadius = 3
                    ivs.layer.shadowPath = UIBezierPath(rect: ivs.bounds).cgPath
                    ivs.layer.shouldRasterize = true
                    ivs.layer.rasterizationScale = UIScreen.main.scale
                    
                    itemLayout.addSubview(ivs)
					itemLayout.addSubview(iv)

					if(ii2 == 0 || ii2 == 2)
					{
						var degrees: Float = 0

						var offset: CGFloat = 0

						if(ii2 == 0)
						{
							degrees = -10
							offset = iv.getX() - 25
						}
						else
						{
							degrees = 10
							offset = iv.getX() + 25
						}

                        ivs.setX(offset)
                        
                        ivs.transform = CGAffineTransform(rotationAngle: CGFloat(degrees * Float.pi / 180))
                        
						iv.setX(offset)

						iv.transform = CGAffineTransform(rotationAngle: CGFloat(degrees * Float.pi / 180))
					}

					ii2 += 1
				}
			}
		}
        
        checkbox.setSize(20, 20)
        checkbox.backgroundColor = UIColor(red: 0.77, green: 0.79, blue: 0.79, alpha: 1)
        checkbox.layer.cornerRadius = 2
        checkbox.setX(self.Layout.getWidth() - 15 - checkbox.getWidth())
        checkbox.setY(15)
        checkbox.isHidden = true
        
        checkboxImage.image = UIImage(named: "folder_checkbox")
        checkboxImage.setX(0)
        checkboxImage.setY(0)
        checkboxImage.setSize(checkbox.getWidth(), checkbox.getHeight())
        checkboxImage.isHidden = true
	}
    
    func setEditable(isEditable: Bool)
    {
        if(isEditable)
        {
            checkbox.isHidden = false
            
        }
        else
        {
            checkbox.isHidden = true
        }
        
        checkboxImage.isHidden = true
    }
    
    func setChecked()
    {
        if(!IsChecked)
        {
            checkbox.backgroundColor = UIColor(red: 0, green: 0.6, blue: 0.77, alpha: 1)
            checkboxImage.isHidden = false
            self.IsChecked = true
        }
        else
        {
            checkbox.backgroundColor = UIColor(red: 0.77, green: 0.79, blue: 0.79, alpha: 1)
            checkboxImage.isHidden = true
            self.IsChecked = false
        }
    }
}
