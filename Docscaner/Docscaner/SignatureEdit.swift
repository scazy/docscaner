//
//  SignatureEdit.swift
//  Docscaner
//
//  Created by artem on 17/04/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit

protocol SignatureEditDelegate
{
    func SignatureEditUpdateSign()
}

class SignatureEdit: UIView, UIScrollViewDelegate
{
    var delegate: SignatureEditDelegate!
    
    var scrollStrip: UIScrollView!
 
    var startOffset: CGFloat = 0
    
    var top: UIView!
    
    var docName = ""
    
    var scroll: UIView!
    
    private var _image: UIImage!
    
    private var _id: String = ""
    
    var imageView: UIImageView!
    
    var files = [DocModelFile]()

    var currentFile = 0
    
    var layout: UIView!
    
    init(id: String, slide: Int = 0)
    {
        super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))

        self.backgroundColor = UIColor.white
        
        startOffset = UIApplication.shared.statusBarFrame.size.height
        
        if let doc = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(id)'").first
        {
            print(doc)
            
            self._id = doc.Id
            
            self.docName = doc.Name
            
            self.initTopControls()

            currentFile = slide
            
            self.files = [DocModelFile]()
            
            for i in Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(doc.Id)'")
            {
                files.append(i)
            }
            
//            self.scroll = UIView()
//
//            self.scroll.setX(10)
//            self.scroll.setY(10, relative: top)
//            self.scroll.setSize(ScreenSize.SCREEN_WIDTH - 20, ScreenSize.SCREEN_HEIGHT - top.getHeight() - 20)
//            self.scroll.backgroundColor = UIColor.white
//            self.addSubview(scroll)
            
            if(files.count > 0)
            {
                self.initImage(image: files[self.currentFile])
            }
        }
        else
        {
            
        }
    }
    
    func initTopControls()
    {
        top = UIView()
        top.backgroundColor = UIColor.white
        top.setX(0)
        top.setY(0)
        top.setSize(ScreenSize.SCREEN_WIDTH, 42 + startOffset)
        top.clipsToBounds = true
        self.addSubview(top)
        
        ///------
        
        let iconSize: CGFloat = 50 / 2.5
        
        
        var topPart = self.getWidth() / 7
        
        //----
        
        var backLayout = UIView()
        backLayout.setX(5)
        backLayout.setY(startOffset)
        backLayout.setSize(42, 42)
        top.addSubview(backLayout)
        
        var lib_i_5 = UIImage(named: "edit_back")
        var lib_i_5Size = getImageSize(image: lib_i_5!, width: 0, height: backLayout.getHeight() - 20)
        
        var backIcon = UIImageView()
        backIcon.image = lib_i_5
        backIcon.setSize(backLayout.getWidth() - 14, backLayout.getWidth() - 14)
        backIcon.setX(7)
        backIcon.setY(7)
        backLayout.addSubview(backIcon)
        
        var backButton = UIButton()
        backButton.setX(0)
        backButton.setY(0)
        backButton.setSize(backLayout.getWidth(), backLayout.getHeight())
        backButton.addTarget(self, action: "backAction:", for: .touchUpInside)
        backLayout.addSubview(backButton)
        
        var libTitle = UILabel()
        //        libTitle.backgroundColor = .green
        libTitle.setX(0, relative: backLayout)
        libTitle.setY(startOffset)
        libTitle.setSize(ScreenSize.SCREEN_WIDTH - backLayout.getWidth() - backLayout.getWidth(), 42)
        libTitle.textColor = UIColor.black
        libTitle.textAlignment = .center
        libTitle.text = s("signature_edit_title")
        libTitle.font = UIFont.boldSystemFont(ofSize: 17)
        top.addSubview(libTitle)
        
//        var buttonActionLayout = UIView()
//        //        buttonActionLayout.backgroundColor = .red
//        buttonActionLayout.setX(ScreenSize.SCREEN_WIDTH - (42 * 2) - 15)
//        buttonActionLayout.setY(startOffset)
//        buttonActionLayout.setSize(42 * 2, 42)
//        top.addSubview(buttonActionLayout)
//
//        var buttonActionLabel = UILabel()
//        buttonActionLabel.isHidden = true
//        buttonActionLabel.tag = -23847
//        buttonActionLabel.text = "Save"
//        buttonActionLabel.setX(0)
//        buttonActionLabel.setY(0)
//        buttonActionLabel.setSize(buttonActionLayout.getWidth(), buttonActionLayout.getHeight())
//        buttonActionLabel.textColor =  UIColor(red: 0.22, green: 0.55, blue: 0.84, alpha: 1)
//        buttonActionLabel.textAlignment = .right
//        buttonActionLabel.font = UIFont.systemFont(ofSize: 17)
//        buttonActionLayout.addSubview(buttonActionLabel)
//
//        var buttonAction = UIButton()
//        buttonAction.tag = -23848
//        buttonAction.setX(0)
//        buttonAction.setY(0)
//        buttonAction.setSize(buttonActionLayout.getWidth(), buttonActionLayout.getHeight())
//        buttonAction.addTarget(self, action: "topAction:", for: .touchUpInside)
//        buttonActionLayout.addSubview(buttonAction)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initImage(image: DocModelFile)
    {
        let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)
        
        let path = docDirURL.appendingPathComponent(image.FileName)
        
        let data = try? Data(contentsOf: path)
        
        renderImage(image: UIImage(data: data!)!)
    }
    
    func renderImage(image: UIImage)
    {
        self._image = image
        
        let imageWidthOrigin = self._image.size.width
        let imageHeightOrigin = self._image.size.height
        
        print("** IMAGE \(imageWidthOrigin) \(imageHeightOrigin)")
        
        let maxImageWidth: CGFloat = ScreenSize.SCREEN_WIDTH - 40
        
        let maxImageHeight: CGFloat = ScreenSize.SCREEN_HEIGHT - 40 - top.getHeight()
        
        var imageWidth: CGFloat = 0
        var imageHeight: CGFloat = 0
        
        var factor: CGFloat = 0
        
        imageHeight = maxImageHeight
        
        factor = CGFloat(imageHeight) / CGFloat(imageHeightOrigin)
        
        print("** FACTOR \(CGFloat(imageHeightOrigin) / CGFloat(imageHeight) )")
        
        imageWidth = CGFloat(imageWidthOrigin) * factor
        
        if(imageWidth > maxImageWidth)
        {
            imageWidth = maxImageWidth
            
            factor = CGFloat(imageWidth) / CGFloat(imageWidthOrigin)
            
            imageHeight = CGFloat(imageHeightOrigin) * factor
        }
        
        var factor2 = imageWidthOrigin / imageWidth

        
        layout = UIView()
        layout.setSize(imageWidth, imageHeight)
        layout.toCenterY(self)
        layout.setX(20)
        layout.layer.borderColor = UIColor.black.cgColor
        layout.layer.borderWidth = 1
        self.addSubview(layout)
        
        imageView = UIImageView()

        imageView.isUserInteractionEnabled = true
        imageView.setSize(imageWidth, imageHeight)
        imageView.setX(0)
        imageView.setY(0)
        imageView.image = self._image
        layout.addSubview(imageView)
        
        //-----

        let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)
        
        let path = docDirURL.appendingPathComponent(Constants.SIGN_IMAGE)
        
        let data = try? Data(contentsOf: path)
        
        var sign = UIImage(data: data!)
        
        var signSize = getImageSize(image: sign!, width: layout.getWidth() * 0.33, height: 0)
        
        var signImageBlock = UIView()
        signImageBlock.tag = -987
        signImageBlock.layer.borderWidth = 2
        signImageBlock.layer.borderColor = UIColor(red: 0, green: 0.72, blue: 0.87, alpha: 0.5).cgColor
        
        
        if(self.files[self.currentFile].SignatureDocHeight > 0 &&
            self.files[self.currentFile].SignatureDocWidth > 0 &&
            self.files[self.currentFile].SignatureHeight > 0 &&
            self.files[self.currentFile].SignatureWidth > 0 &&
            self.files[self.currentFile].SignatureX > 0 &&
            self.files[self.currentFile].SignatureY > 0
            )
        {
            signImageBlock.setSize(CGFloat(self.files[self.currentFile].SignatureWidth), CGFloat(self.files[self.currentFile].SignatureHeight))
            signImageBlock.setX(CGFloat(self.files[self.currentFile].SignatureX))
            signImageBlock.setY(CGFloat(self.files[self.currentFile].SignatureY))
        }
        else
        {
            signImageBlock.setSize(signSize[0], signSize[1])
            signImageBlock.toCenterX(layout)
            signImageBlock.setY(layout.getHeight() - signImageBlock.getHeight() - 15)
        }
        
        layout.addSubview(signImageBlock)
        
        var signImage = UIImageView()
        
        signImage.setSize(signImageBlock.getWidth(), signImageBlock.getHeight())
        signImage.setX(0)
        signImage.image = sign
        signImage.setY(0)
        signImageBlock.addSubview(signImage)
        
        var panGesture = UIPanGestureRecognizer(target: self, action: "dragSign:")
        signImageBlock.isUserInteractionEnabled = true
        signImageBlock.addGestureRecognizer(panGesture)
    }
    
    @objc func dragSign(_ sender: UIPanGestureRecognizer)
    {
        if sender.state == .began || sender.state == .changed
        {
            
            let translation = sender.translation(in: self.layout)
            
            var x = sender.view!.getX()
            
            var y = sender.view!.getY()
            
//            if(x < 0)
//            {
//                return
//            }
//
//            if(y < 0)
//            {
//                return
//            }
            
            print("*** dragSign \(x) \(y)")
            
            isMove = true
            
            sender.view!.center = CGPoint(x: sender.view!.center.x + translation.x, y: sender.view!.center.y + translation.y)
            sender.setTranslation(CGPoint.zero, in: sender.view)
        }
    }
    
    var isMove = false
    
    @objc func backAction(_ sender: AnyObject)
    {
        if(!isMove)
        {
            self.removeFromSuperview()
            return
        }
        
        if let signImageBlock = self.viewWithTag(-987)
        {
            try! Core.shared().getRealm.write {
                self.files[currentFile].SignatureX = Float(signImageBlock.getX())
                self.files[currentFile].SignatureY = Float(signImageBlock.getY())
                self.files[currentFile].SignatureWidth = Float(signImageBlock.getWidth())
                self.files[currentFile].SignatureHeight = Float(signImageBlock.getHeight())
                self.files[currentFile].SignatureDocHeight = Float(self.layout.getHeight())
                self.files[currentFile].SignatureDocWidth = Float(self.layout.getWidth())
                
                self.files[currentFile].IsSignature = true
            }
        }
        
        self.delegate.SignatureEditUpdateSign()
        
        self.removeFromSuperview()
    }
}
