//
//  SettingsModel.swift
//  Docscaner
//
//  Created by artem on 17/04/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import RealmSwift

class SettingsModel: Object
{
    override static func primaryKey() -> String?
    {
        return "Id"
    }
    
    @objc dynamic var Id = 0
    
    @objc dynamic var CameraMode = 0
    
    @objc dynamic var FlashMode = 0
    
    @objc dynamic var PinCode = ""
    
    @objc dynamic var UsePinCode = false
    
    @objc dynamic var LaunchController = ""
    
    @objc dynamic var ScanType = ""
    
    @objc dynamic var SaveOriginal = false
    
    @objc dynamic var Signature = false
    
    @objc dynamic var Subs = 0
    
    @objc dynamic var ODR = "ENG"
    
    @objc dynamic var IsPromo = false
}
