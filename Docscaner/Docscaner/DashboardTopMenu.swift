//
//  DashboardTopMenu.swift
//  Docscaner
//
//  Created by Artem  on 04/03/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit

protocol DashboardTopMenuDelegate
{
    func DashboardTopMenuNewFolder()
    
    func DashboardTopMenuFilter()
    
    func DashboardTopMenuActions()
    
    func DashboardTopMenuBack()
    
    func DashboardTopMentEditCancel()
    
    func DashboardTopMenuSearch(text: String)
}

class DashboardTopMenu: UIView, UITextFieldDelegate
{
	var delegate: DashboardTopMenuDelegate!
    
    var title: UILabel!
    
    var folderButton: UIView!

	var Height: CGFloat = (DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_Xr) ? 140 : 120
    
    var startOffset: CGFloat = 0

    var backImage: UIImageView!
    
    var backtitle: UILabel!
    
    var backButton: UIButton!
    
    var textField: UITextField!
    
    func addBack()
    {
        backImage.isHidden = true
        backtitle.isHidden = true
        backButton.isHidden = true
        
        UIView.animate(withDuration: 0.3, animations: {
            self.title.setX(10, relative: self.backtitle)
        }) { (success) in
            self.backImage.isHidden = false
            self.backtitle.isHidden = false
            self.backButton.isHidden = false
        }
    }
    
    func hideBack()
    {
        backImage.isHidden = true
        backtitle.isHidden = true
        backButton.isHidden = true
        
        UIView.animate(withDuration: 0.3, animations: {
            self.title.setX(20)
        }) { (success) in

        }
    }
    
    @objc func buttonBackAction(_ sender: AnyObject)
    {
        DispatchQueue.main.async(execute: {
            self.delegate.DashboardTopMenuBack()
        })
    }
    
	init()
	{
		super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: Height))
//		self.backgroundColor = .green

		startOffset = UIApplication.shared.statusBarFrame.size.height

		print("** START \(startOffset)")
        
        backImage = UIImageView()
        backImage.image = UIImage(named: "folder_back")
        backImage.setX(20)
        backImage.setY(10 + startOffset)
        backImage.setSize(30, 30)
        self.addSubview(backImage)
        
        var size = getLabelSizeForFont(str: s("library_back"), size: 17, fontName: "Circe-Bold")
        
        backtitle = UILabel()
        backtitle.textColor = UIColor(red: 0.45, green: 0.45, blue: 0.45, alpha: 1)
        backtitle.font = UIFont(name: "Circe-Bold", size: 17)
        backtitle.text = s("library_back")
        backtitle.setX(0, relative: backImage)
        backtitle.setY(10 + startOffset)
        backtitle.setSize(size.width, 30)
        self.addSubview(backtitle)
        
        backButton = UIButton()
        backButton.addTarget(self, action: "buttonBackAction:", for: .touchUpInside)
        backButton.setX(0)
        backButton.setY(10 + startOffset)
        backButton.setSize(size.width + 30, 30)
        self.addSubview(backButton)
        
        backImage.isHidden = true
        backButton.isHidden = true
        backtitle.isHidden = true
        
        //-----

		title = UILabel()
//		title.backgroundColor = .red
		title.textColor = UIColor(red: 0.45, green: 0.45, blue: 0.45, alpha: 1)
		title.font = UIFont(name: "Circe-Bold", size: 24)
		title.text = s("library_title_default")
		title.setX(20)
		title.setY(10 + startOffset)
		title.setSize(self.getWidth() - 40, 30)
        self.addSubview(title)

		//---

		folderButton = UIView()
		folderButton.setSize(38, 38)
		folderButton.setY(title.getY())
		folderButton.setX(self.getWidth() - 20 - folderButton.getWidth())
		self.addSubview(folderButton)

		var folderButtonImage = UIImageView()
		folderButtonImage.setSize(folderButton.getWidth() * 0.55, folderButton.getHeight() * 0.55)
		folderButtonImage.toCenterX(folderButton)
		folderButtonImage.toCenterY(folderButton)
		folderButtonImage.image = UIImage(named: "add_folder")
		folderButton.addSubview(folderButtonImage)
        
        var folderButtonAction = UIButton()
        folderButtonAction.setSize(folderButton.getWidth(), folderButton.getHeight())
        folderButtonAction.setX(0)
        folderButtonAction.setY(0)
        folderButtonAction.addTarget(self, action: "newFolderAction:", for: .touchUpInside)
        folderButton.addSubview(folderButtonAction)

		//---

		var moreButton = UIView()
		moreButton.setSize(38, 38)
		moreButton.setY(5, relative: title)
		moreButton.setX(self.getWidth() - 20 - moreButton.getWidth())
		self.addSubview(moreButton)

		var moreButtonImage = UIImageView()
		moreButtonImage.setSize(moreButton.getWidth() * 0.55, moreButton.getHeight() * 0.55)
		moreButtonImage.toCenterX(moreButton)
		moreButtonImage.toCenterY(moreButton)
		moreButtonImage.image = UIImage(named: "more")
		moreButton.addSubview(moreButtonImage)
        
        var moreAction = UIButton()
        moreAction.setSize(moreButton.getWidth(), moreButton.getHeight())
        moreAction.setX(0)
        moreAction.setY(0)
        moreAction.addTarget(self, action: "moreAction:", for: .touchUpInside)
        moreButton.addSubview(moreAction)

		var filterButton = UIView()
		filterButton.setSize(38, 38)
//        filterButton.backgroundColor = .red
		filterButton.setY(5, relative: title)
		filterButton.setX(self.getWidth() - 20 - filterButton.getWidth() - moreButton.getWidth() - 5)
		self.addSubview(filterButton)

		var filterButtonImage = UIImageView()
		filterButtonImage.setSize(filterButton.getWidth() * 0.55, filterButton.getHeight() * 0.55)
		filterButtonImage.toCenterX(filterButton)
		filterButtonImage.toCenterY(filterButton)
		filterButtonImage.image = UIImage(named: "filter")
		filterButton.addSubview(filterButtonImage)
        
        var filterAction = UIButton()
        filterAction.setSize(filterButton.getWidth(), filterButton.getHeight())
        filterAction.setX(0)
        filterAction.setY(0)
        filterAction.addTarget(self, action: "filterAction:", for: .touchUpInside)
        filterButton.addSubview(filterAction)

		//----

		var searchLayout = UIView()
//        searchLayout.backgroundColor = .red
		searchLayout.setX(20)
		searchLayout.setY(5, relative: title)
		searchLayout.backgroundColor = UIColor(red: 0.45, green: 0.45, blue: 0.45, alpha: 0.15)
		searchLayout.layer.cornerRadius = 11
		searchLayout.setSize(self.getWidth() - 20 - filterButton.getWidth() - 20 - moreButton.getWidth() - 10, 38)
		self.addSubview(searchLayout)
        
        textField = UITextField()
        textField.delegate = self
        textField.addTarget(self, action: "textEdit:", for: .editingChanged)
        textField.delegate = self
        textField.returnKeyType = .done
//        textField.keyboardAppearance = .dark
        //        textField.textAlignment = .cen
        textField.setX(15)
        textField.setY(0)
        textField.setSize(searchLayout.getWidth() - 30, searchLayout.getHeight())
        textField.backgroundColor = .clear
        textField.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
        textField.font = UIFont(name: "Circe-Regular", size: 17)
        textField.returnKeyType = .done
        textField.attributedPlaceholder = NSAttributedString(string: s("library_search"), attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)])
        searchLayout.addSubview(textField)
        
        //----
        
//        EnableEditMode(isEnable: true)
	}
    
    @objc func textEdit(_ sender: UITextField)
    {
        self.delegate.DashboardTopMenuSearch(text: sender.text!)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.endEditing(true)
        return true
    }
    
    func EnableEditMode(isEnable: Bool)
    {
        if let layout = self.viewWithTag(-5)
        {
            
        }
        else
        {
            var block = UIView()
            block.tag = -5
            block.setX(0)
            
            block.setSize(self.getWidth(), self.title.getY() + self.title.getHeight())
            block.backgroundColor = UIColor(red: 0, green: 0.71, blue: 0.87, alpha: 1)
            
            block.setY(block.getHeight() * -1)
            self.addSubview(block)
            
            //----
            
            var size = getLabelSizeForFont(str: s("botton_cancel"), size: 17, fontName: "Circe-Bold")
            
            var backtitle = UILabel()
            backtitle.textColor = UIColor.white
            backtitle.font = UIFont(name: "Circe-Bold", size: 17)
            backtitle.text = s("botton_cancel")
            backtitle.setX(20)
            backtitle.setY(startOffset)
            backtitle.setSize(size.width, 40)
            block.addSubview(backtitle)
            
            var backButton = UIButton()
//            backButton.backgroundColor = .red
            backButton.addTarget(self, action: "buttonCancelEditModeAction:", for: .touchUpInside)
            backButton.setX(backtitle.getX())
            backButton.setY(backtitle.getY())
            backButton.setSize(backtitle.getWidth(), backtitle.getHeight())
            block.addSubview(backButton)
            
            var selectedTitle = UILabel()
            selectedTitle.tag = -10
            selectedTitle.textAlignment = .center
            selectedTitle.text = String(format: s("selected_items"), "\(0)")
            selectedTitle.font = UIFont(name: "Circe-Bold", size: 25)
            selectedTitle.setX(size.width + 20)
            selectedTitle.setY(startOffset)
            selectedTitle.setSize(block.getWidth() - (size.width * 2) - 40, 40)
            selectedTitle.textColor = UIColor.white
            block.addSubview(selectedTitle)
        }
        
        if let layout = self.viewWithTag(-5)
        {
            var offset: CGFloat = 0
            if(!isEnable)
            {
                offset = layout.getHeight() * -1
            }
            
            UIView.animate(withDuration: 0.3) {
                layout.setY(offset)
            }
        }
    }
    
    func SetSelected(count: Int)
    {
        if let label = self.viewWithTag(-10) as? UILabel
        {
            label.text = String(format: s("selected_items"), "\(count)")
        }
    }
    
    @objc func buttonCancelEditModeAction(_ sender: AnyObject)
    {
        print("*** buttonCancelEditModeAction")
        //EnableEditMode(isEnable: false)
        
        //DisableEditMode
        
        self.delegate.DashboardTopMentEditCancel()
    }
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
    
    @objc func newFolderAction(_ sender: AnyObject)
    {
        self.delegate.DashboardTopMenuNewFolder()
    }
    
    @objc func moreAction(_ sender: AnyObject)
    {
        self.delegate.DashboardTopMenuActions()
    }
    
    @objc func filterAction(_ sender: AnyObject)
    {
        self.delegate.DashboardTopMenuFilter()
    }
    
    func setTopTitle(newTitle: String)
    {
        self.title.text = newTitle
    }
    
    func setNewEnable(isActive: Bool)
    {
        if(isActive)
        {
            folderButton.isHidden = false
            
            textField.attributedPlaceholder = NSAttributedString(string: s("library_search"), attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)])
        }
        else
        {
            folderButton.isHidden = true
            
            textField.attributedPlaceholder = NSAttributedString(string: s("folder_search"), attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)])
        }
    }
}
