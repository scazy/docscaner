//
//  LiveScanner.swift
//  Scanner
//
//  Created by Artem  on 04/10/2018.
//  Copyright © 2018 VladimirKuzmin. All rights reserved.
//

import Foundation
import UIKit

protocol LiveScannerDelegate
{
    func LiveScannerClosed()
    
    func RetakeScanner()
}

class LiveScanner: UIView
{
    var video: UIView!
    
    //    var previewImage: UIImageView!
    
    var delegate: LiveScannerDelegate!
    
    init()
    {
        super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))
        self.backgroundColor = UIColor.black
        
        self.video = UIView()
        self.video.setX(0)
        self.video.setY(0)
        self.video.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        self.addSubview(video)
    }
    func initBottomControls()
    {
        //        var bottomBar = UIView()
        //        bottomBar.backgroundColor = UIColor(red: 51/255.0, green: 51/255.0, blue: 51/255.0, alpha:1.0)
        //        bottomBar.setX(0)
        //        bottomBar.setY(ScreenSize.SCREEN_HEIGHT - 110)
        //        bottomBar.setSize(ScreenSize.SCREEN_WIDTH, 110)
        //        self.addSubview(bottomBar)
    }
    
    var top = UIView()
    
    func initTopControls()
    {
        //        var startOffset = UIApplication.shared.statusBarFrame.size.height
        //
        //        top = UIView()
        //        top.backgroundColor = UIColor(red: 51/255.0, green: 51/255.0, blue: 51/255.0, alpha:1.0)
        //        top.setX(0)
        //        top.setY(0)
        //        top.setSize(ScreenSize.SCREEN_WIDTH, 50 + startOffset)
        //        self.addSubview(top)
        //
        //        let iconSize: CGFloat = 50 / 2.5
        //
        //        let flashLayout = UIView()
        //        flashLayout.setX(top.getWidth() - 70)
        //        flashLayout.setY(startOffset + 10)
        //        flashLayout.setSize(70, 30)
        //        top.addSubview(flashLayout)
        //
        //        var flashIcon = UIImageView()
        //        flashIcon.setSize(iconSize, iconSize)
        //        flashIcon.toCenterX(flashLayout)
        //        flashIcon.toCenterY(flashLayout)
        //
        //        var imageName = "close-button"
        //
        //        flashIcon.image = UIImage(named: imageName)
        //        flashLayout.addSubview(flashIcon)
        //
        //        let flashAction = UIButton()
        //        flashAction.addTarget(self, action: #selector(self.closeAction(_:)), for: .touchUpInside)
        //        flashAction.setX(0)
        //        flashAction.setY(0)
        //        flashAction.setSize(flashLayout.getWidth(), flashLayout.getHeight())
        //        flashLayout.addSubview(flashAction)
        //
        //        //----
        //
        //        let libraryLayout = UIView()
        //        libraryLayout.setX(0)
        //        libraryLayout.setY(startOffset + 10)
        //        libraryLayout.setSize(70, 30)
        //        top.addSubview(libraryLayout)
        //
        //        let libraryIcon = UIImageView()
        //        libraryIcon.setSize(iconSize, iconSize)
        //        libraryIcon.toCenterX(libraryLayout)
        //        libraryIcon.toCenterY(libraryLayout)
        //        libraryIcon.image = UIImage(named: "library")
        //        libraryLayout.addSubview(libraryIcon)
        //
        //        let libraryAction = UIButton()
        //        libraryAction.addTarget(self, action: #selector(self.openLibraryAction(_:)), for: .touchUpInside)
        //        libraryAction.setX(0)
        //        libraryAction.setY(0)
        //        libraryAction.setSize(libraryLayout.getWidth(), libraryLayout.getHeight())
        //        libraryLayout.addSubview(libraryAction)
        //
        //
        //        var detectorView = UIView()
        //        detectorView.setX(0, relative: libraryLayout)
        //        detectorView.setY(startOffset + 10)
        //        detectorView.setSize(ScreenSize.SCREEN_WIDTH - libraryLayout.getWidth() - flashLayout.getWidth(), 30)
        //        top.addSubview(detectorView)
        //
        //        var detectorTitle = UILabel()
        //        detectorTitle.setX(0)
        //        detectorTitle.setY(0)
        //        detectorTitle.setSize(detectorView.getWidth(), detectorView.getHeight())
        //        detectorTitle.text = "Auto-capture docs: on"
        //        detectorTitle.textColor = UIColor.white.withAlphaComponent(0.75)
        //        detectorTitle.textAlignment = .center
        //        detectorTitle.font = UIFont.systemFont(ofSize: 14)
        //        detectorView.addSubview(detectorTitle)
        //
        //        var detectorButton = UIButton()
        //        detectorButton.setX(0)
        //        detectorButton.setY(0)
        //        detectorButton.setSize(detectorView.getWidth(), detectorView.getHeight())
        //        detectorButton.addTarget(self, action: #selector(self.captureAction(_:)), for: .touchUpInside)
        //        detectorView.addSubview(detectorButton)
    }
    
    @objc func closeAction(_ sender: AnyObject)
    {
        self.delegate.LiveScannerClosed()
    }
    
    @objc func captureAction(_ sender: AnyObject)
    {
        
    }
    
    @objc func openLibraryAction(_ sender: AnyObject)
    {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var VideoLayer: UIView
    {
        get
        {
            return self.video
        }
        
        set
        {
            self.video = newValue
        }
    }
}

