//
//  ScannerControllerCapturePhoto.swift
//  Docscaner
//
//  Created by lynxmac on 25/03/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import GLKit
import CoreMedia
import CoreImage
import OpenGLES
import QuartzCore

extension ScannerController: AVCapturePhotoCaptureDelegate
{
    @available(iOS 11.0, *)
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?)
    {
        print("*** didFinishProcessingPhoto 1")

        let data = photo.fileDataRepresentation()
        let image =  UIImage(data: data!)

        var newImage = image?.toNoir(true)

        saveImageAndOpen(image: newImage!)
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?)
    {
        if let error = error
        {
            print("Error capturing photo: \(error)")
        }
        else
        {
            if let sampleBuffer = photoSampleBuffer, let previewBuffer = previewPhotoSampleBuffer, let dataImage = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: sampleBuffer, previewPhotoSampleBuffer: previewBuffer)
            {
                if let image = UIImage(data: dataImage)
                {
                    print("** TAKE PHOTO SUCCESS")
                    self.stopCature()

//                    self.IsPosibleTakeImage = false

                    var newImage = image.toNoir(true)

                    saveImageAndOpen(image: newImage)
                }
            }
        }
    }
    
    func stopCature()
    {
        self.captureSession.stopRunning()
    }
    
    func getFilename() -> String
    {
        let date = NSDate()
        
        let df1 = DateFormatter()
        let locale = Locale(identifier: s("locale_id"))
        df1.locale = locale
        
        df1.dateFormat = s("date_format")
        let stringDate =  df1.string(from: date as Date)
        
        return String(format: s("file_name_format"), stringDate)
        
    }
    
    func saveImageAndOpen(image: UIImage)
    {
        print("*** saveImageAndOpen 1")
        
        var shutterView = UIView()
        shutterView.alpha = 0
        shutterView.backgroundColor = UIColor.white
        shutterView.setX(0)
        shutterView.setY(0)
        shutterView.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        self.view.addSubview(shutterView)
        
        UIView.animate(withDuration: 0.075, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveEaseInOut, animations:
            {
                shutterView.alpha = 1
        }, completion: { (finished: Bool) -> Void in
            UIView.animate(withDuration: 0.075, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveEaseInOut, animations:
                {
                    shutterView.alpha = 0
            }, completion: { (finished: Bool) -> Void in
                
            })
        })
        
        if(self.isBatch)
        {
//            print("*** saveImageAndOpen 2")
//
//            if(self.images.count < self.batchPhoto)
//            {
//                self.images.append(image)
//
////                batchCount.text = "\(self.images.count)"
//            }
//            else
//            {
//                self.stopCature()
//
////                self.IsPosibleTakeImage = false
//            }
//
//            for i in photoPreview.subviews
//            {
//                i.removeFromSuperview()
//            }
//
//            var imagePreview = UIImageView()
//            imagePreview.layer.cornerRadius = photoPreview.getWidth() / 2
//            imagePreview.image = image
//            imagePreview.setX(0)
//            imagePreview.setY(0)
//            imagePreview.setSize(photoPreview.getWidth(), photoPreview.getHeight())
//            imagePreview.layer.contentsGravity = CALayerContentsGravity.resizeAspectFill
//            imagePreview.layer.masksToBounds = true
//            photoPreview.addSubview(imagePreview)
//
//            photoPreview.isHidden = false
//            photoPreviewButton.isHidden = false
        }
        else
        {
//            self.stopCature()
            
            print("*** saveImageAndOpen 3")

//            batchCount.text = ""

            self.images = [UIImage]()

            self.images.append(image)

            //----

            var fileName = UUID().uuidString + ".jpg"
            //
            //---

            let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)

            let fileURL = docDirURL.appendingPathComponent(fileName)

            do
            {
                try image.jpegData(compressionQuality: 1)!.write(to: fileURL, options: [.atomic])
            }
            catch(let error)
            {
                print(error)
            }

            //---

            let doc = DocModel()
            doc.Id = UUID().uuidString
            doc.CreatedAt = Int64(NSDate().timeIntervalSince1970)
            doc.Name = getFilename()

            let file = DocModelFile()
            file.Width = Float(image.size.width)
            file.Height = Float(image.size.height)
            file.FileName = fileName
            file.Id = UUID().uuidString
            file.DocId = doc.Id
            file.TypeFile = "image"

            try! Core.shared().getRealm.write {
                Core.shared().getRealm.add(doc)
                Core.shared().getRealm.add(file)
            }
            
            self.bottom.setPreview(image, id: doc.Id)

//            for i in photoPreview.subviews
//            {
//                i.removeFromSuperview()
//            }
//
//            var imagePreview = UIImageView()
//            imagePreview.layer.cornerRadius = photoPreview.getWidth() / 2
//            imagePreview.image = image
//            imagePreview.setX(0)
//            imagePreview.setY(0)
//            imagePreview.setSize(photoPreview.getWidth(), photoPreview.getHeight())
//            imagePreview.layer.contentsGravity = CALayerContentsGravity.resizeAspectFill
//            imagePreview.layer.masksToBounds = true
//            photoPreview.addSubview(imagePreview)
//
//            photoPreview.isHidden = false
//            photoPreviewButton.isHidden = false
//
//            lastDocId = doc.Id
//            //
//
//            var crop = PhotoCrop(id: lastDocId)
//            self.view.addSubview(crop)
//
//            EventsManager.shared().sendNotify(Constants.EVENT_OPEN_DOC, data: ["doc_id": doc.Id])
        }
    }
}
