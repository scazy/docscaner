//
//  DashboardCollection.swift
//  Docscaner
//
//  Created by Artem  on 08/03/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit

extension DashboardController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, DocCellActionsDelegate
{
	func initCollectionView()
	{
		if(messagesScroll != nil)
		{
			messagesScroll.removeFromSuperview()
			messagesScroll = nil
		}

		let layoutWidth: CGFloat = ScreenSize.SCREEN_WIDTH
		let layoutHeight: CGFloat = ScreenSize.SCREEN_HEIGHT - self.top.getHeight() - self.bottom.getHeight()

		layout = UICollectionViewFlowLayout()

		layout.scrollDirection = .vertical
		layout.itemSize = CGSize(width: layoutWidth, height: layoutHeight)
		layout.sectionInset = UIEdgeInsets.zero

		messagesScroll = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
		messagesScroll.contentInset = .zero
		messagesScroll.preservesSuperviewLayoutMargins = false
		messagesScroll.layoutMargins = .zero
		messagesScroll.setCollectionViewLayout(layout, animated: true)
		messagesScroll.delegate = self
		messagesScroll.dataSource = self
		messagesScroll.backgroundColor = UIColor.clear
		messagesScroll.setY(0, relative: self.top)
		messagesScroll.setX(0)
		messagesScroll.setSize(layoutWidth, layoutHeight)
		self.view.addSubview(messagesScroll)

		messagesScroll.register(DocCell.self, forCellWithReuseIdentifier: "docCell")
		messagesScroll.register(FolderCell.self, forCellWithReuseIdentifier: "folderCell")

		self.reinitData(parentId: "")

		print("** DOCS \(self._data.count)")
	}
    
    func reinitData(parentId: String)
    {
        self._data = [DocModel]()
        
        DisableEditMode(true)
        
        self.parentId = parentId
        
        var sort = "CreatedAt"
        var ascending = false
        
        switch(_sort)
        {
        case .Date:
            sort = "CreatedAt"
            ascending = false
            break;
        case .Name:
            sort = "Name"
            ascending = true
            break
//        case .Size:
//            sort = "Name"
//            break
        case .Type:
            sort = "IsFolder"
            ascending = false
            break;
        default:
            sort = "CreatedAt"
            ascending = false
        }
        
        for i in Core.shared().getRealm.objects(DocModel.self).filter("ParentId = '\(self.parentId)'").sorted(byKeyPath: sort, ascending: ascending)
        {
            self._data.append(i)
        }
        
        if(empty != nil)
        {
            empty.removeFromSuperview()
            empty = nil
        }
        
        if(self._data.count == 0)
        {
            initEmpty()
        }
        
        _cells = [AnyObject]()
        
        messagesScroll.reloadData()
    }

	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
	{
		print("*** numberOfItemsInSection")
		return self._data.count
	}

	func numberOfSections(in collectionView: UICollectionView) -> Int
	{
		print("*** numberOfSections")
		return 1
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
	{
		print("*** sizeForItemAt")
		return CGSize(width: (ScreenSize.SCREEN_WIDTH / 3), height: (ScreenSize.SCREEN_WIDTH / 3) + 20)
	}

	// make a cell for each cell index path
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
	{
		print("*** cellForItemAt")

		let doc = _data[indexPath.item]

		if(!doc.IsFolder)
		{
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "docCell", for: indexPath as IndexPath) as! DocCell
			cell.delegate = self
			(cell as DocCellDelegate).Update(doc: doc)
            
            self._cells.append(cell)
			return cell
		}
		else
		{
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "folderCell", for: indexPath as IndexPath) as! FolderCell
			cell.delegate = self
			(cell as DocCellDelegate).Update(doc: doc)
            
            self._cells.append(cell)
			return cell
		}
	}

	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
	{
		print("*** didSelectItemAt")
        
        var doc = self._data[indexPath.item]
        
        if((self._cells[indexPath.item] as! DocCellDelegate).IsEditableMode())
        {
            (self._cells[indexPath.item] as! DocCellDelegate).setChecked()
            
            
            if let idx = self._selected.index(where: { $0 == indexPath.item })
            {
                self._selected.remove(at: idx)
            }
            
//            if(self._selected.indices.contains(indexPath.item))
//            {
//                self._selected.remove(at: indexPath.item)
//            }
            
            if((self._cells[indexPath.item] as! DocCellDelegate).getIsChecked())
            {
                self._selected.append(indexPath.item)
            }
            
            print("*** didSelectItemAt COUNT \(self._selected.count)")
            
            if(self._selected.count > 0)
            {
                self.top.EnableEditMode(isEnable: true)
                self.bottom.EnableEditMode(true)
                
                if(self._selected.count == 1)
                {
                    let doc = _data[self._selected.first!]
                    
                    if(!doc.IsFolder)
                    {
                        self.bottom.EnableSomeEditMode(.SingleDoc)
                    }
                    else
                    {
                        self.bottom.EnableSomeEditMode(.SingleFolder)
                    }
                }
                else
                {
                    self.bottom.EnableSomeEditMode(.Multiple)
                }
                
                self.top.SetSelected(count: self._selected.count)
            }
            else
            {
                self.DisableEditMode(true)
            }
        }
        else
        {
            self.DisableEditMode()
            
            if(doc.IsFolder)
            {
                self.goToFolder(id: doc.Id)
            }
            else
            {
                EventsManager.shared().sendNotify(Constants.EVENT_OPEN_DOC, data: ["doc_id": doc.Id])
            }
        }
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
	{
		return UIEdgeInsets.zero
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
	{
		return 0
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
	{
		return 0
	}
}


