//
//  DashboardBottomMenu.swift
//  Docscaner
//
//  Created by Artem  on 04/03/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit

protocol DashboardBottomMenuDelegate
{
	func DashboardBottomMenuOpenSettings()

	func DashboardBottomMenuOpenGalery()

	func DashboardBottomMenuOpenCamera()
    
    func DashboardBottomEditModeAdd()
    
    func DashboardBottomEditModeDelete()
        
    func DashboardBottomEditModeEdit()
    
    func DashboardBottomEditModeMore()
    
    func DashboardBottomEditModeRename()
    
    func DashboardBottomEditModeShare()
}

class DashboardBottomMenu: UIView
{
	var delegate: DashboardBottomMenuDelegate!

	var Height: CGFloat = 120
    
    var bottom: UIView!
    
    var takeButton: UIImageView!
    
    var takeButtonAction: UIButton!

	init()
	{
		super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: Height))

		bottom = UIView()
		bottom.setX(0)
		bottom.setSize(self.getWidth(), 100)
		bottom.backgroundColor = UIColor.white
		bottom.setY(self.getHeight() - bottom.getHeight())
		self.addSubview(bottom)

		takeButton = UIImageView()
		takeButton.image = UIImage(named: "take_button")
		takeButton.setY(0)
		takeButton.setSize(ScreenSize.SCREEN_WIDTH / 5, ScreenSize.SCREEN_WIDTH / 5)
		takeButton.toCenterX(self)
		self.addSubview(takeButton)

		takeButtonAction = UIButton()
		takeButtonAction.setX(takeButton.getX())
		takeButtonAction.setY(takeButton.getY())
		takeButtonAction.setSize(takeButton.getWidth(), takeButton.getHeight())
		takeButtonAction.addTarget(self, action: "openCamera:", for: .touchUpInside)
		self.addSubview(takeButtonAction)

		var buttonSize: CGFloat = ScreenSize.SCREEN_WIDTH / 12

		var buttonGalery = UIImageView()
		buttonGalery.setSize(buttonSize, buttonSize)
		buttonGalery.setX(buttonGalery.getWidth())
		buttonGalery.setY(buttonGalery.getWidth())
		buttonGalery.image = UIImage(named: "galery_button")
		self.addSubview(buttonGalery)
        
        var buttonGaleryAction = UIButton()
        buttonGaleryAction.setX(buttonGalery.getX())
        buttonGaleryAction.setY(buttonGalery.getY())
        buttonGaleryAction.setSize(buttonGalery.getWidth(), buttonGalery.getWidth())
        buttonGaleryAction.addTarget(self, action: "openGallery:", for: .touchUpInside)
        self.addSubview(buttonGaleryAction)

		var buttonSettings = UIImageView()
		buttonSettings.setSize(buttonSize, buttonSize)
		buttonSettings.setY(buttonSettings.getWidth())
		buttonSettings.setX(self.getWidth() - (buttonSettings.getWidth() * 2))
		buttonSettings.image = UIImage(named: "settings_button")
		self.addSubview(buttonSettings)
        
//        EnableEditMode(true)
        
        var buttonSettingsAction = UIButton()
        buttonSettingsAction.setX(buttonSettings.getX())
        buttonSettingsAction.setY(buttonSettings.getY())
        buttonSettingsAction.setSize(buttonSettings.getWidth(), buttonSettings.getWidth())
        buttonSettingsAction.addTarget(self, action: "openSettings:", for: .touchUpInside)
        self.addSubview(buttonSettingsAction)
	}
    
    func EnableEditMode(_ isEnable: Bool)
    {
        if let block = self.viewWithTag(-40)
        {
            
        }
        else
        {
            var block = UIView()
            block.tag = -40
            block.backgroundColor = UIColor(red: 0, green: 0.71, blue: 0.87, alpha: 1)
            block.setX(bottom.getX())
            block.setY(self.getHeight())
            block.setSize(bottom.getWidth(), bottom.getHeight())
            self.addSubview(block)
        }
        
        if let block = self.viewWithTag(-40)
        {
            var offset: CGFloat = 0
            if(isEnable)
            {
                takeButton.isHidden = true
                takeButtonAction.isHidden = true
                
                offset = bottom.getY()
            }
            else
            {
                takeButton.isHidden = false
                takeButtonAction.isHidden = false
                offset = self.getHeight()
            }
            
            UIView.animate(withDuration: 0.3) {
                block.setY(offset)
            }
        }
        
//        self.EnableSomeEditMode(.Multiple)
    }
    
    func EnableSomeEditMode(_ mode: BottomMenuEditModeType)
    {
        var items = [BottomMenuEditItem]()
        
        switch(mode)
        {
        case .SingleDoc:
            items = [
//            BottomMenuEditItem(id: BottomMenuEditItemType.Edit, name: s("menu_edit_edit"), icon: "doc_view_edit"),
//             BottomMenuEditItem(id: BottomMenuEditItemType.Add, name: s("menu_edit_add"), icon: "doc_view_new"),
             BottomMenuEditItem(id: BottomMenuEditItemType.Share, name: s("menu_edit_share"), icon: "doc_view_share"),
             BottomMenuEditItem(id: BottomMenuEditItemType.Delete, name: s("menu_edit_remove"), icon: "doc_view_delete"),
             BottomMenuEditItem(id: BottomMenuEditItemType.Rename, name: s("menu_edit_rename"), icon: "doc_view_rename")
            ]
            break
        case .Multiple:
            items = [
             BottomMenuEditItem(id: BottomMenuEditItemType.Share, name: s("menu_edit_share"), icon: "doc_view_share"),
             BottomMenuEditItem(id: BottomMenuEditItemType.Delete, name: s("menu_edit_remove"), icon: "doc_view_delete")
            ]
            break
        case .SingleFolder:            
             items = [BottomMenuEditItem(id: BottomMenuEditItemType.Rename, name: s("menu_edit_rename"), icon: "doc_view_rename"),
//             BottomMenuEditItem(id: BottomMenuEditItemType.Share, name: s("menu_edit_share"), icon: "doc_view_share"),
             BottomMenuEditItem(id: BottomMenuEditItemType.Delete, name: s("menu_edit_remove"), icon: "doc_view_delete")
            ]
            break
        }
        
        var partSize: CGFloat = 0
        
        if let block = self.viewWithTag(-40)
        {
            for i in block.subviews
            {
                i.removeFromSuperview()
            }
            
            var partSize = block.getWidth() / CGFloat(items.count)
            
            var offset: CGFloat = 0
            
            for item in items
            {
                var block2 = UIView()
                block2.setX(offset)
                block2.setY(0)
                block2.setSize(partSize, block.getHeight() / 2)
                block.addSubview(block2)
                
                var image = UIImageView()
                image.setSize(block2.getHeight() * 0.4, block2.getHeight() * 0.4)
                image.toCenterX(block2)
                image.toCenterY(block2)
                image.image = UIImage(named: item.Icon)
                block2.addSubview(image)
                
                var title = UILabel()
                title.font = UIFont(name: "Circe-Bold", size: 10)
                title.setX(0)
                title.setY(20, relative: image)
                title.setSize(block2.getWidth(), 16)
                title.textColor = UIColor.white
                title.textAlignment = .center
                title.text = item.Name.uppercased()
                block2.addSubview(title)
                
                var button = UIButton()
                button.setX(0)
                button.setY(0)
                button.setSize(block2.getWidth(), block2.getHeight())
                button.tag = item.Id.rawValue
                button.addTarget(self, action: "buttonEditAction:", for: .touchUpInside)
                block2.addSubview(button)
                
                offset += partSize
            }
        }
    }
    
    @objc func buttonEditAction(_ sender: AnyObject)
    {
        
        var type = BottomMenuEditItemType(rawValue: sender.tag)!
        
        print("*** buttonEditAction \(type)")
        
        switch type {
        case .Add:
            print("*** buttonEditAction 1")
            self.delegate.DashboardBottomEditModeAdd()
            break
        case .Delete:
            print("*** buttonEditAction 2")
            self.delegate.DashboardBottomEditModeDelete()
            break
        case .Edit:
            print("*** buttonEditAction 3")
            self.delegate.DashboardBottomEditModeEdit()
            break
        case .More:
            print("*** buttonEditAction 4")
            self.delegate.DashboardBottomEditModeMore()
            break;
        case .Rename:
            print("*** buttonEditAction 5")
            self.delegate.DashboardBottomEditModeRename()
            break
        case .Share:
            print("*** buttonEditAction 6")
            self.delegate.DashboardBottomEditModeShare()
            break
        default:
            print("*** buttonEditAction 7")
            break
        }
    }
    
    @objc func openGallery(_ sender: AnyObject)
    {
        self.delegate.DashboardBottomMenuOpenGalery()
    }

	@objc func openCamera(_ sender: AnyObject)
	{
		print("*** openCamera")
		self.delegate.DashboardBottomMenuOpenCamera()
	}

	required init?(coder aDecoder: NSCoder)
    {
		fatalError("init(coder:) has not been implemented")
	}
    
    @objc func openSettings(_ sender: AnyObject)
    {
        self.delegate.DashboardBottomMenuOpenSettings()
    }
}

class BottomMenuEditItem
{
    var Id: BottomMenuEditItemType!
    var Name: String = ""
    var Icon: String = ""
    
    init(id: BottomMenuEditItemType, name: String, icon: String)
    {
        Id = id
        Name = name
        Icon = icon
    }
}

enum BottomMenuEditModeType: Int
{
    case SingleDoc = 1
    case SingleFolder = 2
    case Multiple = 3
}

enum BottomMenuEditItemType: Int
{
    case Edit = 1
    case Add = 3
    case Share = 4
    case Delete = 5
    case More = 6
    case Rename = 7
}
