//
//  SettingsController.swift
//  Scanner
//
//  Created by Artem  on 05/02/2019.
//  Copyright © 2019 VladimirKuzmin. All rights reserved.
//

import Foundation
import UIKit

class SettingsController: UIViewController, ViewControllerDelegate, SettingsTopDelegate, ActionsMenuDelegate, SettingsOCRDelegate
{

    
    var top: SettingsTop!
    
    var docsScroll: UIScrollView!
    
    var _settings: SettingsModel!
    
    func setParams(params: [String : Any])
    {
        
    }
    
    func willBeAppear()
    {
        
    }
    
    func willBeDisappear()
    {
        
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.top = SettingsTop()
        self.top.delegate = self
        self.view.addSubview(self.top)
        
        self.view.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
        
        var startOffset = UIApplication.shared.statusBarFrame.size.height

        docsScroll = UIScrollView()
        docsScroll.backgroundColor = UIColor.clear
        //        docsScroll.delegate = self
        docsScroll.tag = -918789
        docsScroll.setX(0)
        docsScroll.setY(0, relative: self.top)
        docsScroll.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT - top.getHeight())
        docsScroll.contentSize = CGSize(width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT - top.getHeight())
        self.view.addSubview(docsScroll)
        
        if let settings = Core.shared().getRealm.objects(SettingsModel.self).first
        {
            self._settings = settings
            initBlocks()
        }
    }
    
    var blockName: UIView!
    
    func initBlockName()
    {
        blockName = UIView()
        blockName.backgroundColor = UIColor.white
        blockName.setSize(ScreenSize.SCREEN_WIDTH, 84)
        blockName.setX(0)
        blockName.setY(30)
        docsScroll.addSubview(blockName)
        
        var sep_name1 = UIView()
        sep_name1.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.16)
        sep_name1.setSize(ScreenSize.SCREEN_WIDTH , 1)
        sep_name1.setX(0)
        sep_name1.setY(blockName.getHeight() - 1)
        blockName.addSubview(sep_name1)
        
        var sep_name2 = UIView()
        sep_name2.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.16)
        sep_name2.setSize(ScreenSize.SCREEN_WIDTH , 1)
        sep_name2.setX(0)
        sep_name2.setY(0)
        blockName.addSubview(sep_name2)
    }
    
    var subsBlock: UIView!
    
    func initBlockSubs()
    {
        subsBlock = UIView()
        subsBlock.backgroundColor = UIColor.white
        subsBlock.setSize(ScreenSize.SCREEN_WIDTH, blockHeight)
        subsBlock.setX(0)
//        subsBlock.setY(30, relative: blockName)
        subsBlock.setY(30)
        docsScroll.addSubview(subsBlock)
        
        var sep_subs1 = UIView()
        sep_subs1.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.16)
        sep_subs1.setSize(ScreenSize.SCREEN_WIDTH , 1)
        sep_subs1.setX(0)
        sep_subs1.setY(subsBlock.getHeight() - 1)
        subsBlock.addSubview(sep_subs1)
        
        var sep_subs2 = UIView()
        sep_subs2.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.16)
        sep_subs2.setSize(ScreenSize.SCREEN_WIDTH , 1)
        sep_subs2.setX(0)
        sep_subs2.setY(0)
        subsBlock.addSubview(sep_subs2)
        
        var subsLabel = UILabel()
        subsLabel.text = s("settings_subs")
        subsLabel.font = UIFont.systemFont(ofSize: 17)
        subsLabel.setX(20)
        subsLabel.setY(0)
        subsLabel.setSize(ScreenSize.SCREEN_WIDTH - 20, blockHeight)
        subsBlock.addSubview(subsLabel)
        
        var subsNext = UIImageView()
        subsNext.alpha = 0.5
        subsNext.setSize(blockHeight - 15, blockHeight - 15)
        subsNext.setX(ScreenSize.SCREEN_WIDTH - blockHeight + 10)
        subsNext.setY(7.5)
        subsNext.image = UIImage(named: "settings_next")
        subsBlock.addSubview(subsNext)
        
        var valueTitle = UILabel()
        valueTitle.textAlignment = .right
        
        if(Core.shared().isPremium && !Core.shared().IsDebug)
        {
            valueTitle.text = s("subs_title_premium")
        }
        else
        {
            valueTitle.text = s("subs_title_free")
        }
        
        valueTitle.textColor = UIColor(red: 0.55, green: 0.55, blue: 0.55, alpha: 1)
        valueTitle.font = UIFont.systemFont(ofSize: 17)
        valueTitle.setY(0)
        valueTitle.setSize(ScreenSize.SCREEN_WIDTH / 2, blockHeight)
        valueTitle.setX(ScreenSize.SCREEN_WIDTH - subsNext.getWidth() - valueTitle.getWidth() - 10)
        subsBlock.addSubview(valueTitle)
        
        var button = UIButton()
        button.setX(0)
        button.setY(0)
        button.setSize(subsBlock.getWidth(), subsBlock.getHeight())
        button.addTarget(self, action: "subsAction:", for: .touchUpInside)
        subsBlock.addSubview(button)
    }
    
    @objc func subsAction(_ sender: AnyObject)
    {
        EventsManager.shared().sendNotify(Constants.EVENT_SHOW_PREMIUM)
    }
    
    var blockHeight: CGFloat = 50
    
    var signBlock: UIView!
    
    func initBlockSign()
    {
        signBlock = UIView()
        signBlock.backgroundColor = UIColor.white
        signBlock.setSize(ScreenSize.SCREEN_WIDTH, blockHeight)
        signBlock.setX(0)
        signBlock.setY(30, relative: subsBlock)
        docsScroll.addSubview(signBlock)
        
        var sep_sign1 = UIView()
        sep_sign1.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.16)
        sep_sign1.setSize(ScreenSize.SCREEN_WIDTH , 1)
        sep_sign1.setX(0)
        sep_sign1.setY(signBlock.getHeight() - 1)
        signBlock.addSubview(sep_sign1)
        
        var sep_sign2 = UIView()
        sep_sign2.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.16)
        sep_sign2.setSize(ScreenSize.SCREEN_WIDTH , 1)
        sep_sign2.setX(0)
        sep_sign2.setY(0)
        signBlock.addSubview(sep_sign2)
        
        var signLabel = UILabel()
        signLabel.text = s("settings_signature")
        signLabel.font = UIFont.systemFont(ofSize: 17)
        signLabel.setX(20)
        signLabel.setY(0)
        signLabel.setSize(ScreenSize.SCREEN_WIDTH - 20, blockHeight)
        signBlock.addSubview(signLabel)
        
        var signNext = UIImageView()
        signNext.alpha = 0.5
        signNext.setSize(blockHeight - 15, blockHeight - 15)
        signNext.setX(ScreenSize.SCREEN_WIDTH - blockHeight + 10)
        signNext.setY(7.5)
        signNext.image = UIImage(named: "settings_next")
        signBlock.addSubview(signNext)
        
        var valueTitle = UILabel()
        valueTitle.textAlignment = .right
        
        if(_settings.Signature)
        {
            valueTitle.text = s("settings_signature_exists")
        }
        else
        {
            valueTitle.text = s("settings_signature_setup")
        }
        
        valueTitle.textColor = UIColor(red: 0.55, green: 0.55, blue: 0.55, alpha: 1)
        valueTitle.font = UIFont.systemFont(ofSize: 17)
        valueTitle.setY(0)
        valueTitle.setSize(ScreenSize.SCREEN_WIDTH / 2, blockHeight)
        valueTitle.setX(ScreenSize.SCREEN_WIDTH - signNext.getWidth() - valueTitle.getWidth() - 10)
        signBlock.addSubview(valueTitle)
        
        var button = UIButton()
        button.setX(0)
        button.setY(0)
        button.setSize(signBlock.getWidth(), signBlock.getHeight())
        button.addTarget(self, action: "signatureAction:", for: .touchUpInside)
        signBlock.addSubview(button)
    }
    
    var pinBlock: UIView!
    
    func initBlockPin()
    {
        pinBlock = UIView()
        pinBlock.backgroundColor = UIColor.white
        pinBlock.setSize(ScreenSize.SCREEN_WIDTH, blockHeight)
        pinBlock.setX(0)
        pinBlock.setY(30, relative: signBlock)
        docsScroll.addSubview(pinBlock)
        
        var sep_pin1 = UIView()
        sep_pin1.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.16)
        sep_pin1.setSize(ScreenSize.SCREEN_WIDTH , 1)
        sep_pin1.setX(0)
        sep_pin1.setY(pinBlock.getHeight() - 1)
        pinBlock.addSubview(sep_pin1)
        
        var sep_pin2 = UIView()
        sep_pin2.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.16)
        sep_pin2.setSize(ScreenSize.SCREEN_WIDTH , 1)
        sep_pin2.setX(0)
        sep_pin2.setY(0)
        pinBlock.addSubview(sep_pin2)
        
        var pinLabel = UILabel()
        pinLabel.text = s("settings_pincode")
        pinLabel.font = UIFont.systemFont(ofSize: 17)
        pinLabel.setX(20)
        pinLabel.setY(0)
        pinLabel.setSize(ScreenSize.SCREEN_WIDTH - 20, blockHeight)
        pinBlock.addSubview(pinLabel)
        
        var switchBlock = UISwitch()
        switchBlock.addTarget(self, action: "pinAction:", for: .valueChanged)
//        switchBlock.isEnabled = false
        if(_settings.UsePinCode)
        {
            switchBlock.isOn = true
        }
        switchBlock.setX(pinBlock.getWidth() - switchBlock.getWidth() - 20)
        switchBlock.toCenterY(pinBlock)
        pinBlock.addSubview(switchBlock)
    }
    
    @objc func pinAction(_ sender: UISwitch)
    {
        print("*** pinAction")
        print(Core.shared().getBiometricType())
        
        if(Core.shared().getBiometricType() == .none)
        {
            if(!sender.isOn)
            {
                try! Core.shared().getRealm.write {
                    self._settings.UsePinCode = sender.isOn
                    self._settings.PinCode = ""
                }
                return
            }
            
            var folderName = ""
            
            let alert = UIAlertController(title: s("pin_set"), message: nil, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: s("botton_cancel"), style: .cancel, handler: { (action) -> Void in}))
            
            let saveAction = UIAlertAction(title: s("button_add"), style: .default, handler: { (action) -> Void in
                NotificationCenter.default.removeObserver(self, name: UITextField.textDidChangeNotification, object: self)
                
                try! Core.shared().getRealm.write {
                    self._settings.UsePinCode = sender.isOn
                    self._settings.PinCode = folderName
                }
            })
            
            alert.addAction(saveAction)
            
            alert.addTextField(configurationHandler: { (textField) in
                textField.text = ""
                textField.placeholder = s("pin_set_placeholder")
                saveAction.isEnabled = false
                NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main) { (notification) in
                    saveAction.isEnabled = textField.text!.count > 0
                    
                    folderName = textField.text!
                }
            })
            
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            try! Core.shared().getRealm.write {
                self._settings.UsePinCode = sender.isOn
            }
        }
    }
    
    var launchBlock: UIView!
    
    func initBlockLaunch()
    {
        launchBlock = UIView()
        launchBlock.backgroundColor = UIColor.white
        launchBlock.setSize(ScreenSize.SCREEN_WIDTH, blockHeight)
        launchBlock.setX(0)
        launchBlock.setY(30, relative: pinBlock)
        docsScroll.addSubview(launchBlock)
        
        var sep_launch2 = UIView()
        sep_launch2.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.16)
        sep_launch2.setSize(ScreenSize.SCREEN_WIDTH , 1)
        sep_launch2.setX(0)
        sep_launch2.setY(0)
        launchBlock.addSubview(sep_launch2)
        
        var launchSep = UIView()
        launchSep.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.16)
        launchSep.setSize(ScreenSize.SCREEN_WIDTH - 20, 1)
        launchSep.setX(20)
        launchSep.setY(launchBlock.getHeight() - 1)
        launchBlock.addSubview(launchSep)
        
        var launchLabel = UILabel()
        launchLabel.text = s("settings_startup")
        launchLabel.font = UIFont.systemFont(ofSize: 17)
        launchLabel.setX(20)
        launchLabel.setY(0)
        launchLabel.setSize(ScreenSize.SCREEN_WIDTH - 20, blockHeight)
        launchBlock.addSubview(launchLabel)
        
        var launchNext = UIImageView()
        launchNext.alpha = 0.5
        launchNext.setSize(blockHeight - 15, blockHeight - 15)
        launchNext.setX(ScreenSize.SCREEN_WIDTH - blockHeight + 15)
        launchNext.setY(7.5)
        launchNext.image = UIImage(named: "settings_next")
        launchBlock.addSubview(launchNext)
        
        var valueTitle = UILabel()
        valueTitle.tag = -4001
        valueTitle.textAlignment = .right
        
        if(_settings.LaunchController == "dashboardController")
        {
            valueTitle.text = s("settings_startup_lib")
        }
        else
        {
            valueTitle.text = s("settings_startup_scanner")
        }
        
        valueTitle.textColor = UIColor(red: 0.55, green: 0.55, blue: 0.55, alpha: 1)
        valueTitle.font = UIFont.systemFont(ofSize: 17)
        valueTitle.setY(0)
        valueTitle.setSize(ScreenSize.SCREEN_WIDTH / 2, blockHeight)
        valueTitle.setX(ScreenSize.SCREEN_WIDTH - launchNext.getWidth() - valueTitle.getWidth() - 10)
        launchBlock.addSubview(valueTitle)
        
        var button = UIButton()
        button.setX(0)
        button.setY(0)
        button.setSize(launchBlock.getWidth(), launchBlock.getHeight())
        button.addTarget(self, action: "launchAction:", for: .touchUpInside)
        launchBlock.addSubview(button)
    }
    
    @objc func launchAction(_ sender: AnyObject)
    {
        var items = [
            ActionMenuItem(title: s("settings_startup_lib"), itemId: 1),
            ActionMenuItem(title: s("settings_startup_scanner"), itemId: 2)
        ]
        
        let menu = ActionsMenu(menuId: 1, items: items)
        menu.delegate = self
        self.view.addSubview(menu)
        
        menu.Show()
    }
    
    @objc func flashAction(_ sender: AnyObject)
    {
        var items = [
            ActionMenuItem(title: s("settings_flash_auto"), itemId: 1),
            ActionMenuItem(title: s("settings_flash_none"), itemId: 2),
            ActionMenuItem(title: s("settings_flash_always"), itemId: 3)
        ]
        
        let menu = ActionsMenu(menuId: 2, items: items)
        menu.delegate = self
        self.view.addSubview(menu)
        
        menu.Show()
    }
    
    @objc func typeAction(_ sender: AnyObject)
    {
        var items = [
            ActionMenuItem(title: s("settings_scan_type_jpg"), itemId: 1),
            ActionMenuItem(title: s("settings_scan_type_pdf_jpg"), itemId: 2)
        ]
        
        let menu = ActionsMenu(menuId: 4, items: items)
        menu.delegate = self
        self.view.addSubview(menu)
        
        menu.Show()
    }
    
    @objc func filterAction(_ sender: AnyObject)
    {
        var items = [
            ActionMenuItem(title: s("camera_mode1"), itemId: 1),
            ActionMenuItem(title: s("camera_mode2"), itemId: 2)
        ]
        
        let menu = ActionsMenu(menuId: 3, items: items)
        menu.delegate = self
        self.view.addSubview(menu)
        
        menu.Show()
    }
    
    func ActionsMenuAction(menuId: Int, itemId: Int)
    {
        switch(menuId)
        {
        case 1:
            _actionsLaunch(itemId: itemId)
            break;
        case 2:
            _actionsFlash(itemId: itemId)
            break
        case 3:
            _actionsFilter(itemId: itemId)
            break;
        case 4:
            _actionsType(itemId: itemId)
            break;
        default:
            break
        }
    }
    
    func _actionsFilter(itemId: Int)
    {
        var title = s("camera_mode1")
        
        if(itemId == 2)
        {
            title = s("camera_mode2")
        }
                
        if let titleLabel = self.view.viewWithTag(-4003) as? UILabel
        {
            titleLabel.text = title
        }
        
        try! Core.shared().getRealm.write {
            self._settings.CameraMode = itemId
        }
    }
    
    func _actionsType(itemId: Int)
    {
        var title = s("settings_scan_type_jpg")
        var id = 1
        
        if(itemId == 2)
        {
            title = s("settings_scan_type_pdf_jpg")
            id = 2
        }
        
        if let titleLabel = self.view.viewWithTag(-4004) as? UILabel
        {
            titleLabel.text = title
        }
        
        try! Core.shared().getRealm.write {
            self._settings.CameraMode = id
        }
    }
    
    func _actionsFlash(itemId: Int)
    {
        var title = ""
        
        switch(itemId)
        {
        case 0:
            title = s("settings_flash_auto")
            break
        case 1:
            title = s("settings_flash_none")
            break
        case 2:
            title = s("settings_flash_always")
            break
        default:
            title = s("settings_flash_auto")
        }
        
        if let titleLabel = self.view.viewWithTag(-4002) as? UILabel
        {
            titleLabel.text = title
        }
        
        try! Core.shared().getRealm.write {
            self._settings.FlashMode = itemId
        }
    }
    
    func _actionsLaunch(itemId: Int)
    {
        var controller = "dashboardController"
        var title = s("settings_startup_lib")
        
        if(itemId == 2)
        {
            title = s("settings_startup_scanner")
            controller = "scannerController"
        }
        
        if let titleLabel = self.view.viewWithTag(-4001) as? UILabel
        {
            titleLabel.text = title
        }
        
        try! Core.shared().getRealm.write {
            self._settings.LaunchController = controller
        }
    }
    
    func ActionsMenuCancel()
    {
        
    }
    
    var flashBlock: UIView!
    
    func initBlockFlash()
    {
        flashBlock = UIView()
        flashBlock.backgroundColor = UIColor.white
        flashBlock.setSize(ScreenSize.SCREEN_WIDTH, blockHeight)
        flashBlock.setX(0)
        flashBlock.setY(0, relative: launchBlock)
        docsScroll.addSubview(flashBlock)
        
        var flashSep = UIView()
        flashSep.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.16)
        flashSep.setSize(ScreenSize.SCREEN_WIDTH - 20, 1)
        flashSep.setX(20)
        flashSep.setY(flashBlock.getHeight() - 1)
        flashBlock.addSubview(flashSep)
        
        var flashLabel = UILabel()
        flashLabel.text = s("settings_flash")
        flashLabel.font = UIFont.systemFont(ofSize: 17)
        flashLabel.setX(20)
        flashLabel.setY(0)
        flashLabel.setSize(ScreenSize.SCREEN_WIDTH - 20, blockHeight)
        flashBlock.addSubview(flashLabel)
        
        var flashNext = UIImageView()
        flashNext.alpha = 0.5
        flashNext.setSize(blockHeight - 15, blockHeight - 15)
        flashNext.setX(ScreenSize.SCREEN_WIDTH - blockHeight + 15)
        flashNext.setY(7.5)
        flashNext.image = UIImage(named: "settings_next")
        flashBlock.addSubview(flashNext)
        
        var valueTitle = UILabel()
        valueTitle.textAlignment = .right
        
        var title = ""
        switch(_settings.FlashMode)
        {
        case 0:
            title = s("settings_flash_auto")
            break
        case 1:
            title = s("settings_flash_none")
            break
        case 2:
            title = s("settings_flash_always")
            break
        default:
            title = s("settings_flash_auto")
        }
        
        valueTitle.text = title
        valueTitle.tag = -4002
        valueTitle.textColor = UIColor(red: 0.55, green: 0.55, blue: 0.55, alpha: 1)
        valueTitle.font = UIFont.systemFont(ofSize: 17)
        valueTitle.setY(0)
        valueTitle.setSize(ScreenSize.SCREEN_WIDTH / 2, blockHeight)
        valueTitle.setX(ScreenSize.SCREEN_WIDTH - flashNext.getWidth() - valueTitle.getWidth() - 10)
        flashBlock.addSubview(valueTitle)
        
        var button = UIButton()
        button.setX(0)
        button.setY(0)
        button.setSize(flashBlock.getWidth(), flashBlock.getHeight())
        button.addTarget(self, action: "flashAction:", for: .touchUpInside)
        flashBlock.addSubview(button)
    }
    
    func SettingsOCRSave()
    {
        updateOCRTitle()
    }
    
    func updateOCRTitle()
    {
        if let title = self.view.viewWithTag(-4010) as? UILabel
        {
            var count = Core.shared().getRealm.objects(OCRModel.self).filter("IsChecked = true").count
                                    
            switch(count)
            {
            case 1:
                if let ocr = Core.shared().getRealm.objects(OCRModel.self).filter("IsChecked = true").first
                {
                    title.text = s("ocr_\(ocr.Id)")
                }
                break;
            case 2:
                var ocrs = Core.shared().getRealm.objects(OCRModel.self).filter("IsChecked = true")
                
                title.text = s("ocr_\(ocrs.first!.Id)") + "+" + s("ocr_\(ocrs.last!.Id)")
                break;
            default:
                title.text = "\(count) " + s("ocr_languages")
                break
            }
        }
    }
    
    var ocrBlock: UIView!
    
    func initBlockOCR()
    {
        ocrBlock = UIView()
        ocrBlock.backgroundColor = UIColor.white
        ocrBlock.setSize(ScreenSize.SCREEN_WIDTH, blockHeight)
        ocrBlock.setX(0)
        ocrBlock.setY(0, relative: flashBlock)
        docsScroll.addSubview(ocrBlock)
        
        var filterSep = UIView()
        filterSep.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.16)
        filterSep.setSize(ScreenSize.SCREEN_WIDTH - 20, 1)
        filterSep.setX(20)
        filterSep.setY(ocrBlock.getHeight() - 1)
        ocrBlock.addSubview(filterSep)
        
        var filterLabel = UILabel()
        filterLabel.text = s("settings_ocr")
        filterLabel.font = UIFont.systemFont(ofSize: 17)
        filterLabel.setX(20)
        filterLabel.setY(0)
        filterLabel.setSize(ScreenSize.SCREEN_WIDTH - 20, blockHeight)
        ocrBlock.addSubview(filterLabel)
        
        var filterNext = UIImageView()
        filterNext.alpha = 0.5
        filterNext.setSize(blockHeight - 15, blockHeight - 15)
        filterNext.setX(ScreenSize.SCREEN_WIDTH - blockHeight + 15)
        filterNext.setY(7.5)
        filterNext.image = UIImage(named: "settings_next")
        ocrBlock.addSubview(filterNext)
        
        var valueTitle = UILabel()
        valueTitle.tag = -4010
        valueTitle.textAlignment = .right
        valueTitle.text = "ENG"
//        }
        
        valueTitle.textColor = UIColor(red: 0.55, green: 0.55, blue: 0.55, alpha: 1)
        valueTitle.font = UIFont.systemFont(ofSize: 17)
        valueTitle.setY(0)
        valueTitle.setSize(ScreenSize.SCREEN_WIDTH / 2, blockHeight)
        valueTitle.setX(ScreenSize.SCREEN_WIDTH - filterNext.getWidth() - valueTitle.getWidth() - 10)
        ocrBlock.addSubview(valueTitle)
        
        var button = UIButton()
        button.setX(0)
        button.setY(0)
        button.setSize(ocrBlock.getWidth(), ocrBlock.getHeight())
        button.addTarget(self, action: "ocrAction:", for: .touchUpInside)
        ocrBlock.addSubview(button)
        
        updateOCRTitle()
    }
    
    @objc func ocrAction(_ sender: AnyObject)
    {
        var e = SettingsOCR()
        e.delegate = self
        self.view.addSubview(e)
    }
    
    var filterBlock: UIView!
    
    func initFilterBlock()
    {
        filterBlock = UIView()
        filterBlock.backgroundColor = UIColor.white
        filterBlock.setSize(ScreenSize.SCREEN_WIDTH, blockHeight)
        filterBlock.setX(0)
        filterBlock.setY(0, relative: ocrBlock)
        docsScroll.addSubview(filterBlock)
        
        var filterSep = UIView()
        filterSep.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.16)
        filterSep.setSize(ScreenSize.SCREEN_WIDTH - 20, 1)
        filterSep.setX(20)
        filterSep.setY(filterBlock.getHeight() - 1)
        filterBlock.addSubview(filterSep)
        
        var filterLabel = UILabel()
        filterLabel.text = s("settings_filter")
        filterLabel.font = UIFont.systemFont(ofSize: 17)
        filterLabel.setX(20)
        filterLabel.setY(0)
        filterLabel.setSize(ScreenSize.SCREEN_WIDTH - 20, blockHeight)
        filterBlock.addSubview(filterLabel)
        
        var filterNext = UIImageView()
        filterNext.alpha = 0.5
        filterNext.setSize(blockHeight - 15, blockHeight - 15)
        filterNext.setX(ScreenSize.SCREEN_WIDTH - blockHeight + 15)
        filterNext.setY(7.5)
        filterNext.image = UIImage(named: "settings_next")
        filterBlock.addSubview(filterNext)
        
        var valueTitle = UILabel()
        valueTitle.tag = -4003
        valueTitle.textAlignment = .right
        
        if(_settings.CameraMode == 1)
        {
            valueTitle.text = s("camera_mode1")
        }
        else
        {
            valueTitle.text = s("camera_mode2")
        }
        
        valueTitle.textColor = UIColor(red: 0.55, green: 0.55, blue: 0.55, alpha: 1)
        valueTitle.font = UIFont.systemFont(ofSize: 17)
        valueTitle.setY(0)
        valueTitle.setSize(ScreenSize.SCREEN_WIDTH / 2, blockHeight)
        valueTitle.setX(ScreenSize.SCREEN_WIDTH - filterNext.getWidth() - valueTitle.getWidth() - 10)
        filterBlock.addSubview(valueTitle)
        
        var button = UIButton()
        button.setX(0)
        button.setY(0)
        button.setSize(filterBlock.getWidth(), filterBlock.getHeight())
        button.addTarget(self, action: "filterAction:", for: .touchUpInside)
        filterBlock.addSubview(button)
    }
    
    var typeBlock: UIView!
    
    func initTypeBlock()
    {
        typeBlock = UIView()
        typeBlock.backgroundColor = UIColor.white
        typeBlock.setSize(ScreenSize.SCREEN_WIDTH, blockHeight)
        typeBlock.setX(0)
        typeBlock.setY(0, relative: filterBlock)
        docsScroll.addSubview(typeBlock)
        
        var typeSep = UIView()
        typeSep.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.16)
        typeSep.setSize(ScreenSize.SCREEN_WIDTH - 20, 1)
        typeSep.setX(20)
        typeSep.setY(typeBlock.getHeight() - 1)
        typeBlock.addSubview(typeSep)
        
        var typeLabel = UILabel()
        typeLabel.text = s("settings_scan_type")
        typeLabel.font = UIFont.systemFont(ofSize: 17)
        typeLabel.setX(20)
        typeLabel.setY(0)
        typeLabel.setSize(ScreenSize.SCREEN_WIDTH - 20, blockHeight)
        typeBlock.addSubview(typeLabel)
        
        var typeNext = UIImageView()
        typeNext.alpha = 0.5
        typeNext.setSize(blockHeight - 15, blockHeight - 15)
        typeNext.setX(ScreenSize.SCREEN_WIDTH - blockHeight + 15)
        typeNext.setY(7.5)
        typeNext.image = UIImage(named: "settings_next")
        typeBlock.addSubview(typeNext)
        
        var valueTitle = UILabel()
        valueTitle.tag = -4004
        valueTitle.textAlignment = .right
        
        if(_settings.ScanType == "JPG")
        {
            valueTitle.text = s("settings_scan_type_jpg")
        }
        else
        {
            valueTitle.text = s("settings_scan_type_pdf_jpg")
        }
        valueTitle.textColor = UIColor(red: 0.55, green: 0.55, blue: 0.55, alpha: 1)
        valueTitle.font = UIFont.systemFont(ofSize: 17)
        valueTitle.setY(0)
        valueTitle.setSize(ScreenSize.SCREEN_WIDTH / 2, blockHeight)
        valueTitle.setX(ScreenSize.SCREEN_WIDTH - typeNext.getWidth() - valueTitle.getWidth() - 10)
        typeBlock.addSubview(valueTitle)
        
        var button = UIButton()
        button.setX(0)
        button.setY(0)
        button.setSize(typeBlock.getWidth(), typeBlock.getHeight())
        button.addTarget(self, action: "typeAction:", for: .touchUpInside)
        typeBlock.addSubview(button)
    }
    
    var saveBlock: UIView!
    
    func initSaveBlock()
    {
        saveBlock = UIView()
        saveBlock.backgroundColor = UIColor.white
        saveBlock.setSize(ScreenSize.SCREEN_WIDTH, blockHeight)
        saveBlock.setX(0)
        saveBlock.setY(0, relative: typeBlock)
        docsScroll.addSubview(saveBlock)
        
        var saveSep = UIView()
        saveSep.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.16)
        saveSep.setSize(ScreenSize.SCREEN_WIDTH, 1)
        saveSep.setX(0)
        saveSep.setY(saveBlock.getHeight() - 1)
        saveBlock.addSubview(saveSep)
        
        var saveLabel = UILabel()
        saveLabel.text = s("settings_scan_save_library")
        saveLabel.font = UIFont.systemFont(ofSize: 17)
        saveLabel.setX(20)
        saveLabel.setY(0)
        saveLabel.setSize(ScreenSize.SCREEN_WIDTH - 20, blockHeight)
        saveBlock.addSubview(saveLabel)
        
        var switchBlock = UISwitch()
        switchBlock.addTarget(self, action: "saveAction:", for: .valueChanged)
        if(_settings.SaveOriginal)
        {
            switchBlock.isOn = true
        }
        switchBlock.setX(saveBlock.getWidth() - switchBlock.getWidth() - 20)
        switchBlock.toCenterY(saveBlock)
        saveBlock.addSubview(switchBlock)
    }
    
    @objc func saveAction(_ sender: UISwitch)
    {
        if(!Core.shared().isPremium && !Core.shared().IsDebug)
        {
            EventsManager.shared().sendNotify(Constants.EVENT_SHOW_PREMIUM)
            return
        }
        
        try! Core.shared().getRealm.write {
            self._settings.SaveOriginal = sender.isOn
        }
    }
    
    var contactBlock: UIView!
    
    func initContactBlock()
    {
        contactBlock = UIView()
        contactBlock.isHidden = true
        contactBlock.backgroundColor = UIColor.white
        contactBlock.setSize(ScreenSize.SCREEN_WIDTH, blockHeight)
        contactBlock.setX(0)
        contactBlock.setY(30, relative: saveBlock)
        docsScroll.addSubview(contactBlock)
        
        var sep_contact1 = UIView()
        sep_contact1.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.16)
        sep_contact1.setSize(ScreenSize.SCREEN_WIDTH , 1)
        sep_contact1.setX(0)
        sep_contact1.setY(contactBlock.getHeight() - 1)
        contactBlock.addSubview(sep_contact1)
        
        var sep_contact2 = UIView()
        sep_contact2.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.16)
        sep_contact2.setSize(ScreenSize.SCREEN_WIDTH , 1)
        sep_contact2.setX(0)
        sep_contact2.setY(0)
        contactBlock.addSubview(sep_contact2)
        
        var contactLabel = UILabel()
        contactLabel.text = s("settings_support")
        contactLabel.font = UIFont.systemFont(ofSize: 17)
        contactLabel.setX(20)
        contactLabel.setY(0)
        contactLabel.setSize(ScreenSize.SCREEN_WIDTH - 20, blockHeight)
        contactBlock.addSubview(contactLabel)
        
        var contactNext = UIImageView()
        contactNext.alpha = 0.5
        contactNext.setSize(blockHeight - 15, blockHeight - 15)
        contactNext.setX(ScreenSize.SCREEN_WIDTH - blockHeight + 15)
        contactNext.setY(7.5)
        contactNext.image = UIImage(named: "settings_next")
        contactBlock.addSubview(contactNext)
    }
    
    func initBlocks()
    {
//        initBlockName()
        
        initBlockSubs()
        
        initBlockSign()

        initBlockPin()
        
        initBlockLaunch()
        
        initBlockFlash()
        
        initBlockOCR()

        initFilterBlock()
        
        initTypeBlock()

        initSaveBlock()
        
        initContactBlock()
        
        docsScroll.contentSize = CGSize(width: ScreenSize.SCREEN_WIDTH, height: contactBlock.getHeight() + contactBlock.getY() + 30)
    }
    
    func SettingsTopBack()
    {
        Core.shared().changeViewController(name: "dashboardController")
    }
    
    @objc func signatureAction(_ sender: AnyObject)
    {
        if(!Core.shared().isPremium && !Core.shared().IsDebug)
        {
            EventsManager.shared().sendNotify(Constants.EVENT_SHOW_PREMIUM)
            return
        }
        
        Core.shared().changeViewController(name: "signatureController")
        
        Core.shared().removeViewController(name: "settingsController")
    }
    
    func SignatureSettingsBack()
    {
        
    }
}
