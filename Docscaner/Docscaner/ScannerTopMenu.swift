//
//  ScannerTopMenu.swift
//  Docscaner
//
//  Created by Artem  on 14/03/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit

protocol ScannerTopMenuDelegate
{
    func ScannerTopMenuBackAction()
    
    func ScannerTopMenuSwitchMode()
    
    func ScannerTopMenuSwitchFlashMode(flash: Int)
}

class ScannerTopMenu: UIView
{
    var delegate: ScannerTopMenuDelegate!
    
	var Height: CGFloat = (DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_Xr) ? 90 : 70
    
    var startOffset: CGFloat = 0
    
    var _flash = 0
    
    var _filter = 0
    
    var flashLayout: UIView!
    
	init()
	{
		super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: Height))

		startOffset = UIApplication.shared.statusBarFrame.size.height

		var bg = UIImageView()
		bg.image = UIImage(named: "scanner_top_notfound")
		bg.setX(0)
		bg.setY(0)
		bg.setSize(self.getWidth(), self.getHeight())
		self.addSubview(bg)
        
        if let settings = Core.shared().getRealm.objects(SettingsModel.self).first
        {
            _flash = settings.FlashMode
            _filter = settings.CameraMode
        }
        
        addFlash(self.getFlashIcon(_flash))

		//----

		var backLayout = UIView()
//        backLayout.backgroundColor = .red
		backLayout.setSize(self.getHeight() - startOffset, self.getHeight() - startOffset)
		backLayout.setY(startOffset)
		backLayout.setX(0)
		self.addSubview(backLayout)

		var backImage = UIImageView()
		backImage.image = UIImage(named: "scanner_back")
		backImage.setX(10)
		backImage.setY(10)
		backImage.setSize(flashLayout.getWidth() - 20, backLayout.getHeight() - 20)
		backLayout.addSubview(backImage)
        
        var backButton = UIButton()
        backButton.setY(0)
        backButton.setX(0)
        backButton.setSize(backLayout.getWidth(), backLayout.getHeight())
        backButton.addTarget(self, action: "backAction:", for: .touchUpInside)
        backLayout.addSubview(backButton)
        
        //----

        addSwitchMode(s("camera_mode\(_filter)"))
	}
    
    func getFlashIcon(_ flash: Int) -> String
    {
        switch(flash)
        {
        case 0:
            return "flash_auto"
        case 1:
            return "flash_none"
        case 2:
            return "flash_active"
        default:
            return "flash_none"
        }
    }
    
    @objc func flashAction(_ sender: AnyObject)
    {
        switch(_flash)
        {
        case 0:
            _flash = 1
            break
        case 1:
            _flash = 2
            break
        case 2:
            _flash = 0
            break
        default:
            _flash = 1
        }
        addFlash(self.getFlashIcon(_flash))
        self.delegate.ScannerTopMenuSwitchFlashMode(flash: _flash)
    }
    
    func addFlash(_ icon: String)
    {
        if let flashLayout = self.viewWithTag(-400) as? UILabel
        {
            flashLayout.removeFromSuperview()
        }
        
        if let flashImage = self.viewWithTag(-401) as? UIImageView
        {
            flashImage.removeFromSuperview()
        }
        
        if let flashButton = self.viewWithTag(-402) as? UIButton
        {
            flashButton.removeFromSuperview()
        }
        
        flashLayout = UIView()
        flashLayout.tag = -400
        flashLayout.setSize(self.getHeight() - startOffset, self.getHeight() - startOffset)
        flashLayout.setY(startOffset)
        flashLayout.setX(self.getWidth() - flashLayout.getWidth() )
        self.addSubview(flashLayout)
        
        var flashImage = UIImageView()
        flashImage.tag = -401
        flashImage.image = UIImage(named: icon)
        flashImage.setX(10)
        flashImage.setY(10)
        flashImage.setSize(flashLayout.getWidth() - 20, flashLayout.getHeight() - 20)
        flashLayout.addSubview(flashImage)
        
        var flashButton = UIButton()
        flashButton.tag = -402
        flashButton.setX(0)
        flashButton.setY(0)
        flashButton.setSize(flashLayout.getWidth(), flashLayout.getWidth())
        flashButton.addTarget(self, action: "flashAction:", for: .touchUpInside)
        flashLayout.addSubview(flashButton)
    }
    
    private func addSwitchMode(_ title1: String)
    {
        if let title = self.viewWithTag(-300) as? UILabel
        {
            title.removeFromSuperview()
        }
        
        if let selector = self.viewWithTag(-301) as? UIImageView
        {
            selector.removeFromSuperview()
        }
        
        if let switchButton = self.viewWithTag(-302) as? UIButton
        {
            switchButton.removeFromSuperview()
        }
        
        
        var textStr = title1
        var textSize = getLabelSizeForFont(str: textStr, size: 22, fontName: "Circe-Regular")
        
        var title = UILabel()
        title.tag = -300
        title.font = UIFont(name: "Circe-Regular", size: 22)
        title.setY(startOffset)
        title.textColor = .white
        title.textAlignment = .center
        title.text = textStr
        title.setSize(textSize.width, self.getHeight() - startOffset)
        title.toCenterX(self)
        self.addSubview(title)
        
        var selector = UIImageView()
        selector.tag = -301
        selector.setX(10, relative: title)
        selector.setY(startOffset + (title.getHeight() / 2) - 5)
        selector.setSize(10, 10)
        selector.image = UIImage(named: "filter_selector")
        self.addSubview(selector)
        
        var switchButton = UIButton()
        switchButton.tag = -302
        switchButton.setX(title.getX())
        switchButton.setY(title.getY())
        switchButton.setSize(title.getWidth() + selector.getWidth() + 10, title.getHeight())
        switchButton.addTarget(self, action: "switchModeAction:", for: .touchUpInside)
        self.addSubview(switchButton)
    }
    
    func setCameraMode(_ mode: CameraMode)
    {
        switch(mode)
        {
        case .BW:
            addSwitchMode(s("camera_mode1"))
            break
        case .Color:
            addSwitchMode(s("camera_mode2"))
            break;
        }
    }
    
    @objc func switchModeAction(_ sender: AnyObject)
    {
        self.delegate.ScannerTopMenuSwitchMode()
    }
    
    @objc func backAction(_ sender: AnyObject)
    {
        self.delegate.ScannerTopMenuBackAction()
    }

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}

enum CameraMode: Int
{
    case BW = 1
    case Color = 2
}
