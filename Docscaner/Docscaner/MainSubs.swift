//
//  MainSubs.swift
//  Docscaner
//
//  Created by artem on 18/04/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit

class MainSubs: UIView
{
    var subs_id = "com.asim.docscanner.year"
    
    init()
    {
        super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))
        
        self.backgroundColor = UIColor(red: 0, green: 0.21, blue: 0.39, alpha: 1)
        
        initBG()
        
        let startOffset = UIApplication.shared.statusBarFrame.size.height
        
        var scroll = UIScrollView()
        scroll.setX(0)
        scroll.setY(0)
        scroll.setSize(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        scroll.contentSize = CGSize(width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        self.addSubview(scroll)
        
        var mainTitle = UILabel()
        mainTitle.textColor = UIColor.white
        mainTitle.font = UIFont(name: "Circe-Bold", size: 27)
        mainTitle.text = "Premium info"
        mainTitle.textAlignment = .center
        mainTitle.setY(20)
        mainTitle.setX(15)
        mainTitle.setSize(ScreenSize.SCREEN_WIDTH - 30, 40)
        scroll.addSubview(mainTitle)
        
        //
        var mainImgData = UIImage(named: "subs_top")
        
        var mainImgSize = getImageSize(image: mainImgData!, width: ScreenSize.SCREEN_WIDTH  * 0.66, height: 0)
        
        var mainImg = UIImageView()
        mainImg.setY(20, relative: mainTitle)
        mainImg.setSize(mainImgSize[0], mainImgSize[1])
        mainImg.image = mainImgData
        mainImg.toCenterX(scroll)
        scroll.addSubview(mainImg)
        
        //----
        
//        var titles = ["Without any limits", "Text recognition (OCR)", "Sign, edit and share"]
        
        var titles = [s("subs1_title"), s("subs2_title"), s("subs3_title"), s("subs4_title")]
        
        var ss = [CGFloat]()
        var hh = [CGFloat]()
        
        
        for i in titles
        {
            var s = getLabelSizeForFont(str: i, size: 22, fontName: "Circe-Regular")
            ss.append(s.width)
            hh.append(s.height)
        }
        
        var subsWidth = ss.max()
        var subsHeight = hh.max()
        
        var offsetY: CGFloat = mainImg.getHeight() + mainImg.getY() + 40
        
        var fontSize: CGFloat = 22
        var tOff: CGFloat = 15
        
        if(DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P)
        {
            fontSize = 19
            tOff = 8
        }
        
        var ii = 1
        for i in titles
        {
            var t = UILabel()
            t.text = i
            t.textAlignment = .left
            t.textColor = UIColor.white
            t.font = UIFont(name: "Circe-Regular", size: 22)
            t.setSize(subsWidth!, subsHeight!)
            t.toCenterX(self)
            t.setY(offsetY)
            scroll.addSubview(t)
            
            var k = UIImageView()
            k.setSize(subsHeight! - 10, subsHeight! - 10)
            k.image = UIImage(named: "subs\(ii)")
            k.setY(offsetY + 5)
            k.setX(t.getX() - 5 - subsHeight!)
            scroll.addSubview(k)
            
            offsetY += subsHeight! + tOff
            
            ii += 1
        }
        
        //--
        
        var close = UIButton()
        close.addTarget(self, action: "close:", for: .touchUpInside)
        close.setImage(UIImage(named: "subs_close"), for: .normal)
        close.setSize(subsHeight! - 15, subsHeight! - 15)
        close.setY(mainTitle.getY())
        close.setX(ScreenSize.SCREEN_WIDTH - 10 - subsHeight!)
        scroll.addSubview(close)
        
        //--
        
        var attrs = [
            NSAttributedString.Key.font : UIFont(name: "Circe-Regular", size: 18),
            NSAttributedString.Key.foregroundColor : UIColor.white,
            NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
        
        var resore = UIButton()
        resore.setX(30)
        resore.setY(offsetY + 20)
        resore.titleLabel?.textColor = UIColor(red: 0, green: 0.78, blue: 0.88, alpha: 1)
        resore.setSize(ScreenSize.SCREEN_WIDTH - 60, 20)
        resore.setAttributedTitle(NSMutableAttributedString(string: s("button_restore"), attributes: attrs), for: .normal)
        resore.addTarget(self, action: "restore:", for: .touchUpInside)
        scroll.addSubview(resore)
        
        var subscribeLayout = UIView()
        subscribeLayout.setX(30)
        subscribeLayout.setSize(ScreenSize.SCREEN_WIDTH - 60, 65)
        subscribeLayout.backgroundColor = .white
        subscribeLayout.layer.cornerRadius = subscribeLayout.getHeight() / 2
        subscribeLayout.setY(30, relative: resore)
        scroll.addSubview(subscribeLayout)
        
        var r = subscribeLayout.getHeight() / 2
        
        

        
        var part1 = UILabel()
        part1.setX(r)
        part1.setY(0)
        part1.setSize(subscribeLayout.getWidth() - (r * 2), subscribeLayout.getHeight())
        part1.textColor = UIColor(red: 0.11, green: 0.31, blue: 0.69, alpha: 1)
        part1.font = UIFont(name: "Circe-ExtraBold", size: fontSize)
        part1.textAlignment = .left
        part1.text = "Subscribe"
        subscribeLayout.addSubview(part1)
        
        var part2 = UILabel()
//        part2.backgroundColor = .red
        part2.setX(r)
        part2.setY(0)
        part2.setSize(subscribeLayout.getWidth() - (r * 2), subscribeLayout.getHeight())
        part2.textColor = UIColor(red: 0.11, green: 0.31, blue: 0.69, alpha: 1)
        part2.font = UIFont(name: "Circe-Regular", size: fontSize)
        part2.textAlignment = .right
        part2.text = ""
        subscribeLayout.addSubview(part2)
        
        var descText = ""
        if let subsModel = Core.shared().getRealm.objects(SubscriptionModel.self).filter("PurchaseId = 'com.asim.docscanner.year'").first
        {
//            part2.text = "\(subsModel.PriceLocale) \(subsModel.Currency)/YEAR"
            
            part2.text = String(format: "%.2f %@/YEAR", subsModel.PriceLocale, subsModel.Currency).replacingOccurrences(of: ".00", with: "")
            
            descText = String(format: s("subs_policy"), subsModel.PriceLocale, subsModel.Currency).replacingOccurrences(of: ".00", with: "")
        }
        
        var subsButton = UIButton()
        subsButton.addTarget(self, action: "subsAction:", for: .touchUpInside)
        subsButton.setX(0)
        subsButton.setY(0)
        subsButton.setSize(subscribeLayout.getWidth(), subscribeLayout.getHeight())
        subscribeLayout.addSubview(subsButton)
        
        //---

        var descSize = getLabelSize(str: descText, size: 14, isBold: false, setWidth: self.getWidth() - 30)
        
        var textView = UITextView()
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        textView.isScrollEnabled = true
        textView.backgroundColor = UIColor.clear
        textView.text = descText
        textView.isEditable = false
        textView.font = UIFont.systemFont(ofSize: 14)
        textView.textColor = UIColor.white
        textView.setSize(self.getWidth() - 30, descSize.height + 30)
        textView.setX(15)
        textView.setY(20, relative: subscribeLayout)
        textView.textAlignment = .center
        scroll.addSubview(textView)
        
        //-----
        
        var attrsTerms = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12),
            NSAttributedString.Key.foregroundColor : UIColor.yellow,
            NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
        
        var terms = UIButton()
        terms.setX(30)
        terms.setY(10, relative: textView)
        terms.titleLabel?.textColor = UIColor.yellow
        terms.setSize(ScreenSize.SCREEN_WIDTH - 60, 18)
        terms.setAttributedTitle(NSMutableAttributedString(string: s("subs_policy_title"), attributes: attrsTerms), for: .normal)
        terms.addTarget(self, action: "openLink1:", for: .touchUpInside)
        scroll.addSubview(terms)
        
        var policy = UIButton()
        policy.setX(30)
        policy.setY(10, relative: terms)
        policy.titleLabel?.textColor = UIColor.yellow
        policy.setSize(ScreenSize.SCREEN_WIDTH - 60, 18)
        policy.setAttributedTitle(NSMutableAttributedString(string: s("subs_policy_title2"), attributes: attrsTerms), for: .normal)
        policy.addTarget(self, action: "openLink2:", for: .touchUpInside)
        scroll.addSubview(policy)
        
        scroll.contentSize = CGSize(width: ScreenSize.SCREEN_WIDTH, height: policy.getY() + policy.getHeight() + 20)
    }
    
    @objc func close(_ sender: AnyObject)
    {
        self.removeFromSuperview()
    }
    
    @objc func openLink1(_ sender: AnyObject)
    {
        openURL(url: "http://docscanner.pro/Terms.html")
    }
    
    @objc func openLink2(_ sender: AnyObject)
    {
        openURL(url: "http://docscanner.pro/Privacy.html")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func subsAction(_ sender: AnyObject)
    {
        EventsManager.shared().sendNotify(Constants.EVENT_DO_PURCHASE, data: ["product_id": subs_id])
        
         self.removeFromSuperview()
    }
    
    func initBG()
    {
        var bgImage = UIImage(named: "subs_bg_main")

        var originalSize = getImageSize(image: bgImage!, width: ScreenSize.SCREEN_WIDTH, height: 0)

        var bg = UIImageView()
//        bg.layer.contentsGravity = CALayerContentsGravity.resizeAspectFill
//        bg.layer.masksToBounds = true
        bg.image = bgImage
        bg.setX(0)
        bg.setY(0)
        bg.setSize(originalSize[0], originalSize[1])
        self.addSubview(bg)
        
    }
    
    func openURL(url: String)
    {
        var newURL = URL(string: url)
        
        UIApplication.shared.open(newURL!, options: [:], completionHandler: nil)
    }
}
