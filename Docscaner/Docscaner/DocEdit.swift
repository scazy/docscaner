//
//  PhotoPreviewSave.swift
//  Scanner
//
//  Created by Artem  on 26/09/2018.
//  Copyright © 2018 VladimirKuzmin. All rights reserved.
//

import Foundation
import UIKit

protocol DocEditDelegate
{
//    func PhotoPreviewRetakeImage()
//
//    func PhotoPreviewOCR(id: String)
//
//    func PhotoPreviewOpenLibrary()
    
    func DocEditBack(id: String)
}

class DocEdit: UIView, UIScrollViewDelegate, ActionsMenuDelegate
{
    var infoPlaceholder: UIView!
    
    var scrollStrip: UIScrollView!
    
    var delegate: DocEditDelegate!
    
    var bottom: DocEditMenu!
    
    var startOffset: CGFloat = 0
    
    var top: UIView!
    
    var docName = ""
    
    var scroll: UIView!
    
    private var _image: UIImage!
    
    private var _id: String = ""
    
    var imageView: UIImageView!
    
    var files = [DocModelFile]()
    
    var infoText: UILabel!
    
    var currentFile = 0
    
//    var tools: PhotoPreviewTool!
    
    var buttonScan: UIView!
    
    var newPhoto: UIView!
    
    var editLayout: UIView!
    
    var shareIcon: UIImageView!
    
    var isEdit = false
    
    var factor2: CGFloat = 0
    
    var buttonScanTitle: UILabel!
    
    var context: CIContext!
    
    var brightnessFilter: CIFilter!
    
    var contrastFilter: CIFilter!
    
    func passNewImage(image: UIImage)
    {
        imageView.image = image
    }
    
    init(id: String)
    {
        super.init(frame: CGRect(x: ScreenSize.SCREEN_WIDTH, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))
        
//        NotificationCenter.default.addObserver(self, selector: #selector(self.observerEventAddedImage(_:)), name: NSNotification.Name(rawValue: String(format: "%@%@" , Constants.NOTIFY_MESSAGE_PREFIX, Constants.EVENT_ADDED_IMAGE)), object: nil)
        
        self.backgroundColor = UIColor.white
        
        startOffset = UIApplication.shared.statusBarFrame.size.height
        
        if let doc = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(id)'").first
        {
            print(doc)
            
            self._id = doc.Id
            
            self.docName = doc.Name
            
            self.initTopControls()
            
            self.initBottomControls()
            
            //-----
            
            currentFile = 0
            
            self.files = [DocModelFile]()
            
            for i in Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(doc.Id)'")
            {
                files.append(i)
            }
            
            self.scroll = UIView()
            //            scroll.backgroundColor = .red
            //            self.scroll.delegate = self
            self.scroll.setX(10)
            self.scroll.setY(10, relative: top)
            self.scroll.setSize(ScreenSize.SCREEN_WIDTH - 20, ScreenSize.SCREEN_HEIGHT - top.getHeight() - bottom.getHeight() - 20)
            //            self.scroll.frame = CGRect(0, 0, ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
            self.scroll.backgroundColor = UIColor.white
            //            self.scroll.alwaysBounceVertical = false
            //            self.scroll.alwaysBounceHorizontal = false
            //            self.scroll.showsVerticalScrollIndicator = true
            //            self.scroll.flashScrollIndicators()
            
            //            self.scroll.minimumZoomScale = 1.0
            //            self.scroll.maximumZoomScale = 10.0
            
            self.addSubview(scroll)
            
            if(files.count > 0)
            {
                self.initImage(image: files[self.currentFile])
            }
            
            print("** FILES COINT \(files.count)")
            
//            self.reinitFilesStrip()

        }
        else
        {
            
        }
    }
    
    func getOriginImage() -> UIImage
    {
        return imageView.image!
    }

    @objc func backAction(_ sender: AnyObject)
    {
        var mode = self.bottom.getMode
        
        hideCroptool()
        
        switch(mode)
        {
        case .Bright:
            self.bottom.initEdit()
            break
        case .Common:
            
            
            
            UIView.animate(withDuration: 0.15, animations: {
                self.setX(ScreenSize.SCREEN_WIDTH)
            }) { (success) in
                self.delegate.DocEditBack(id: self._id)
                self.removeFromSuperview()
            }
            
//            Core.shared().changeViewController(name: "libraryController")
            break;
        case .Pen:
            self.bottom.initEdit()
            break;
        case .Rotate:
            self.bottom.initEdit()
            break;
        case .Edit:
//            self.bottom.initCommon()
            UIView.animate(withDuration: 0.15, animations: {
                self.setX(ScreenSize.SCREEN_WIDTH)
            }) { (success) in
                self.delegate.DocEditBack(id: self._id)
                self.removeFromSuperview()
            }
            break;
        case .Contrast:
            self.bottom.initBright()
            break
        case .Light:
            self.bottom.initBright()
            break
        }
    }

//    func reinitFilesStrip()
//    {
//
//    }
    
    func renderImage(image: UIImage)
    {
        self._image = image
        
        let imageWidthOrigin = self._image.size.width
        let imageHeightOrigin = self._image.size.height
        
        print("** IMAGE \(imageWidthOrigin) \(imageHeightOrigin)")
        
        let maxImageWidth: CGFloat = ScreenSize.SCREEN_WIDTH - 40
        
        let maxImageHeight: CGFloat = ScreenSize.SCREEN_HEIGHT - 40 - top.getHeight() - bottom.getHeight()
        
        var imageWidth: CGFloat = 0
        var imageHeight: CGFloat = 0
        
        var factor: CGFloat = 0
        
        imageHeight = maxImageHeight
        
        factor = CGFloat(imageHeight) / CGFloat(imageHeightOrigin)
        
        print("** FACTOR \(CGFloat(imageHeightOrigin) / CGFloat(imageHeight) )")
        
        imageWidth = CGFloat(imageWidthOrigin) * factor
        
        if(imageWidth > maxImageWidth)
        {
            imageWidth = maxImageWidth
            
            factor = CGFloat(imageWidth) / CGFloat(imageWidthOrigin)
            
            imageHeight = CGFloat(imageHeightOrigin) * factor
        }
        
        factor2 = imageWidthOrigin / imageWidth
        
        print("** ORIGINAL SIZE \(imageWidthOrigin) \(imageHeightOrigin)")
        print("** NEW SIZE \(imageWidth) \(imageHeight)")
        
        if(self.imageView != nil)
        {
            self.imageView.removeFromSuperview()
        }
        
        imageView = UIImageView()
        imageView.layer.borderColor = UIColor.black.cgColor
        imageView.layer.borderWidth = 1
        imageView.isUserInteractionEnabled = true
        imageView.setSize(imageWidth, imageHeight)
        imageView.toCenterX(self.scroll)
        imageView.toCenterY(self.scroll)
        imageView.image = self._image
        
        self.scroll.addSubview(imageView)
        
        //        let upSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeUp(_:)))
        //        upSwipe.direction = UISwipeGestureRecognizer.Direction.up
        //        imageView.addGestureRecognizer(upSwipe)
        
        //---
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.leftSwipe(_:)))
        leftSwipe.direction = UISwipeGestureRecognizer.Direction.left
        imageView.addGestureRecognizer(leftSwipe)
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.rightSwipe(_:)))
        rightSwipe.direction = UISwipeGestureRecognizer.Direction.right
        imageView.addGestureRecognizer(rightSwipe)
    }
    
    func initImage(image: DocModelFile)
    {
        let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)
        
        let path = docDirURL.appendingPathComponent(image.FileName)
        
        let data = try? Data(contentsOf: path)
        
        renderImage(image: UIImage(data: data!)!)
    }
    
    func actionLeft()
    {
        
    }
    
    func actionRight()
    {
        
    }
    
    @objc func buttonItemStripeAction(_ sender: AnyObject)
    {
        print("*** buttonItemStripeAction CURRENT \(self.currentFile) TAG \(sender.tag)")
        
        var id = sender.tag - 20000
        
        if let item = scrollStrip.viewWithTag(10000 + self.currentFile)
        {
            item.layer.borderWidth = 0
            item.layer.borderColor = UIColor.clear.cgColor
        }
        
        self.currentFile = id
        
        if let item = scrollStrip.viewWithTag(10000 + self.currentFile)
        {
            item.layer.borderWidth = 2
            item.layer.borderColor = UIColor(red: 0.13, green: 0.73, blue: 0.81, alpha: 1).cgColor
        }
        
        //            infoText.text = "Files: \(self.currentFile + 1) / \(files.count)"
        
        initImage(image: self.files[self.currentFile])
    }
    
    @objc func leftSwipe(_ sender: AnyObject)
    {
        print("** current file \(self.currentFile) files \(self.files.count)")
        
        if(self.currentFile + 1 < self.files.count)
        {
            if let item = scrollStrip.viewWithTag(10000 + self.currentFile)
            {
                item.layer.borderWidth = 0
                item.layer.borderColor = UIColor.clear.cgColor
            }
            
            self.currentFile += 1
            
            if let item = scrollStrip.viewWithTag(10000 + self.currentFile)
            {
                item.layer.borderWidth = 2
                item.layer.borderColor = UIColor(red: 0.13, green: 0.73, blue: 0.81, alpha: 1).cgColor
            }
            
            //            infoText.text = "Files: \(self.currentFile + 1) / \(files.count)"
            
            initImage(image: self.files[self.currentFile])
        }
    }
    
    @objc func rightSwipe(_ sender: AnyObject)
    {
        print("** current file \(self.currentFile) files \(self.files.count)")
        
        if(self.currentFile > 0)
        {
            if let item = scrollStrip.viewWithTag(10000 + self.currentFile)
            {
                item.layer.borderWidth = 0
                item.layer.borderColor = UIColor.clear.cgColor
            }
            
            self.currentFile -= 1
            
            if let item = scrollStrip.viewWithTag(10000 + self.currentFile)
            {
                item.layer.borderWidth = 2
                item.layer.borderColor = UIColor(red: 0.13, green: 0.73, blue: 0.81, alpha: 1).cgColor
            }
            
            //            infoText.text = "Files: \(self.currentFile + 1) / \(files.count)"
            
            initImage(image: self.files[self.currentFile])
        }
    }
    
    @objc func swipeUp(_ sender: UISwipeGestureRecognizer)
    {
//        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveEaseInOut, animations:
//            {
//                self.frame.origin.y = 0 - ScreenSize.SCREEN_HEIGHT
//        }, completion: { (finished: Bool) -> Void in
//
//            self.delegate.PhotoPreviewOpenLibrary()
//            self.removeFromSuperview()
//        })
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView?
    {
        return self.imageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView)
    {
        let width = self.imageView.frame.width
        let height = self.imageView.frame.height
        
        self.imageView.toCenterX(self.scroll)
        self.imageView.toCenterY(self.scroll)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func openLeftMenuAction(_ sender: AnyObject)
    {
//        EventsManager.shared().sendNotify(Constants.EVENT_SHOW_LEFT_MENU)
    }
    
    func initTopControls()
    {
        top = UIView()
        top.backgroundColor = UIColor.white
        top.setX(0)
        top.setY(0)
        top.setSize(ScreenSize.SCREEN_WIDTH, 42 + startOffset)
        top.clipsToBounds = true
        self.addSubview(top)
        
        ///------
        
        let iconSize: CGFloat = 50 / 2.5

        
        var topPart = self.getWidth() / 7
        
        //----
        
        var backLayout = UIView()
        backLayout.setX(5)
        backLayout.setY(startOffset)
        backLayout.setSize(42, 42)
        top.addSubview(backLayout)
        
        var lib_i_5 = UIImage(named: "edit_back")
        var lib_i_5Size = getImageSize(image: lib_i_5!, width: 0, height: backLayout.getHeight() - 20)
        
        var backIcon = UIImageView()
        backIcon.image = lib_i_5
        backIcon.setSize(backLayout.getWidth() - 14, backLayout.getWidth() - 14)
        backIcon.setX(7)
        backIcon.setY(7)
        backLayout.addSubview(backIcon)
        
        var backButton = UIButton()
        backButton.setX(0)
        backButton.setY(0)
        backButton.setSize(backLayout.getWidth(), backLayout.getHeight())
        backButton.addTarget(self, action: "backAction:", for: .touchUpInside)
        backLayout.addSubview(backButton)
        
        var libTitle = UILabel()
        //        libTitle.backgroundColor = .green
        libTitle.setX(0, relative: backLayout)
        libTitle.setY(startOffset)
        libTitle.setSize(ScreenSize.SCREEN_WIDTH - backLayout.getWidth() - backLayout.getWidth(), 42)
        libTitle.textColor = UIColor.black
        libTitle.textAlignment = .center
        libTitle.text = self.docName
        libTitle.font = UIFont.boldSystemFont(ofSize: 17)
        top.addSubview(libTitle)
        
        var buttonActionLayout = UIView()
        //        buttonActionLayout.backgroundColor = .red
        buttonActionLayout.setX(ScreenSize.SCREEN_WIDTH - (42 * 2) - 15)
        buttonActionLayout.setY(startOffset)
        buttonActionLayout.setSize(42 * 2, 42)
        top.addSubview(buttonActionLayout)
        
        var buttonActionLabel = UILabel()
        buttonActionLabel.isHidden = true
        buttonActionLabel.tag = -23847
        buttonActionLabel.text = s("save_title")
        buttonActionLabel.setX(0)
        buttonActionLabel.setY(0)
        buttonActionLabel.setSize(buttonActionLayout.getWidth(), buttonActionLayout.getHeight())
        buttonActionLabel.textColor =  UIColor(red: 0.22, green: 0.55, blue: 0.84, alpha: 1)
        buttonActionLabel.textAlignment = .right
        buttonActionLabel.font = UIFont.systemFont(ofSize: 17)
        buttonActionLayout.addSubview(buttonActionLabel)
        
        var buttonAction = UIButton()
        buttonAction.tag = -23848
        buttonAction.setX(0)
        buttonAction.setY(0)
        buttonAction.setSize(buttonActionLayout.getWidth(), buttonActionLayout.getHeight())
        buttonAction.addTarget(self, action: "topAction:", for: .touchUpInside)
        buttonActionLayout.addSubview(buttonAction)
    }
    
    func showSaveAction(_ title: String = s("save_title"))
    {
        if let v = self.viewWithTag(-23847) as? UILabel
        {
            v.text = title
            v.isHidden = false
        }
        
        if let v = self.viewWithTag(-23848) as? UIButton
        {
            v.isHidden = false
        }
    }
    
    func hideSaveAction()
    {
        if let v = self.viewWithTag(-23847) as? UILabel
        {
            v.text = ""
            v.isHidden = true
        }
        
        if let v = self.viewWithTag(-23848) as? UIButton
        {
            v.isHidden = true
        }
    }
    
    @objc func topAction(_ sender: AnyObject)
    {
        var mode = self.bottom.getMode
        
        switch(mode)
        {
        case .Bright:
            break
        case .Common:
            break
        case .Edit:
            break
        case .Pen:
            break;
        case .Rotate:
            saveCrop()
            hideCroptool()
            hideSaveAction()
            bottom.initEdit()
            break;
        case .Contrast:
            break
        case .Light:
            break
        }
    }
    
    func initBottomControls()
    {
        bottom = DocEditMenu()
        bottom.delegate = self
        //        bottom.backgroundColor = UIColor.black
        bottom.setX(0)
        bottom.setY(ScreenSize.SCREEN_HEIGHT - bottom.getHeight())
        //        bottom.setSize(ScreenSize.SCREEN_WIDTH, 42 + startOffset)
        self.addSubview(bottom)
        
        bottom.initEdit()
        

        
    }

    func saveToPath(image: UIImage, fileName: String)
    {
        //        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        //        let filePath = "\(paths[0])/\(fileName).jpg"
        
        //        UIImageJ
        
        let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let path = docDirURL.appendingPathComponent(fileName)
        
        //try FileManager.default.copyItem(atPath: (documentsURL1?.path)!, toPath: docDirURL.appendingPathComponent(i).path)
        try! image.jpegData(compressionQuality: 1)?.write(to: path, options: [.atomic])
        
        //        UIImage.jpegData(compressionQuality: 1).write(to: URL(fileURLWithPath: path.absoluteString), options: [.atomic])
        
        //        try? UIImageJPEGRepresentation(image, 1.0)!.write(to: URL(fileURLWithPath: path.absoluteString), options: [.atomic])
        //        UIImagePNGRepresentation(image)?.writeToFile(filePath, atomically: true)
    }
    
    //    @objc func actionShare(_ sender: AnyObject)
    //    {
    //
    //    }
    
    
    
    func ActionsMenuCancel()
    {
        
    }

    @objc func saveImageAction(_ sender: AnyObject)
    {
        //        var doc = self.saveToLibrary(image: self._image)
        //
        //        self.saveToPath(image: self._image, fileName: doc.FileName)
        //
        //        self.delegate.PhotoPreviewOpenLibrary()
    }
    
    @objc func retakeImageAction(_ sender: AnyObject)
    {
//        self.delegate.PhotoPreviewRetakeImage()
//
//        self.removeFromSuperview()
    }
    
    @objc func renameFileAction(_ sender: AnyObject)
    {
        
    }
    
    @objc func openLibraryAction(_ sender: AnyObject)
    {
//        self.delegate.PhotoPreviewOpenLibrary()
//
//        self.removeFromSuperview()
    }
    
    var crop: CropPolygon!
    
//    var editMode = PhotoPreviewToolMode.Bright
}



extension DocEdit: CropPolygonDelegate
{
    func CropPolygonChangeSize(points: [CGPoint])
    {
        
    }
    
    func saveCrop()
    {
        print("** SAVE CROP ORIENTATION \(self._image.imageOrientation.rawValue)")
        
        DispatchQueue.main.async(execute: {
            var points = self.crop.getPoints()
            
            var originalImage = self.imageView.image
            
            let updatedImage = originalImage?.cropByPoints(points: points, imageCropFactor: self.factor2)
            
            self.saveToPath(image: updatedImage!, fileName: self.files[self.currentFile].FileName)
            
            self.renderImage(image: updatedImage!)
        })
    }
}

extension DocEdit
{
//    func changeTool(newMode: PhotoPreviewToolMode)
//    {
//        if(crop != nil)
//        {
//            crop.removeFromSuperview()
//        }
//
////        editMode = newMode
//    }

    func hideCroptool()
    {
        if(crop != nil)
        {
            crop.removeFromSuperview()
        }
    }

    func showCropTool()
    {
        print("** SHOW CROP TOOL ORIENTATION \(imageView.image?.imageOrientation.rawValue) SCALE \(imageView.image?.scale)")
        //imageView

        hideCroptool()

        crop = CropPolygon(width: imageView.getWidth(), height: imageView.getHeight(), points: [
            CGPoint(x: 10, y: 10),
            CGPoint(x: imageView.getWidth() - 10, y: 10),
            CGPoint(x: imageView.getWidth() - 10, y: imageView.getHeight() - 10),
            CGPoint(x: 10, y: imageView.getHeight() - 10)
            ])
        crop.delegate = self
        imageView.addSubview(crop)
    }
    

}
//
//    func rotateImageLeft()
//    {
//        print("*** rotateImageLeft")
//        var image = imageView.image!.imageRotatedByDegrees(degrees: 90, flip: true)
//
//        renderImage(image: image)
//    }
//
//    func rotateImageRight()
//    {
//        print("*** rotateImageRight")
//        var image = imageView.image!.imageRotatedByDegrees(degrees: -90, flip: true)
//
//        renderImage(image: image)
//    }
//
//    func getOriginImage() -> UIImage
//    {
//        return imageView.image!
//    }
//

//
//    func removeCurrentSlide()
//    {
//        var text = ""
//
//        //        if(files.count > 1)
//        //        {
//        //            text = s("remove_image_slide")
//        //        }
//        //        else
//        //        {
//        text = s("remove_whole_image")
//        //        }
//
//        let alertController = UIAlertController(title: s("remove_image_title"), message: text, preferredStyle: .alert)
//
//        let okAction = UIAlertAction(title: s("remove_title"), style: .default, handler: {
//            (action : UIAlertAction!) -> Void in
//
//            DispatchQueue.main.async(execute: {
//                //                if(self.files.count > 1)
//                //                {
//                //                    self.removeCurrentSlideAction()
//                //                }
//                //                else
//                //                {
//                self.removeWholeDoc()
//                //                }
//            })
//        })
//
//        alertController.addAction(okAction)
//
//        let cancelAction = UIAlertAction(title: s("botton_cancel"), style: .cancel, handler: {
//            (action : UIAlertAction!) -> Void in
//        })
//
//        alertController.addAction(cancelAction)
//
//        self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
//    }
//
//    func removeWholeDoc()
//    {
//        if let doc = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(self._id)'").first
//        {
//            try! Core.shared().getRealm.write {
//                Core.shared().getRealm.delete(doc)
//            }
//        }
//
//        EventsManager.shared().sendNotify(Constants.EVENT_RELOAD_LIB)
//
//        self.removeFromSuperview()
//    }
//
//    func removeCurrentSlideAction()
//    {
//
//    }
//
//    func observerEventAddedImage()
//    {
//
//    }
//
//    //    @objc func openScan(_ sender: AnyObject)
//    //    {
//    //        EventsManager.shared().sendNotify(Constants.EVENT_ADD_IMAGE_TO_DOC, data: ["doc_id": self._id])
//    //    }
//
//    @objc func observerEventAddedImage(_ notification: NSNotification)
//    {
//        DispatchQueue.main.async(execute: {
//            var info = notification.userInfo as! Dictionary<String, AnyObject>
//
//            var doc_id = info["doc_id"] as! String
//
//            var image = info["image"] as! UIImage
//
//            var isRotate = false
//
//            var degrees: CGFloat = 0
//
//            switch(image.imageOrientation)
//            {
//            case .down:
//                isRotate = true
//                degrees = 180
//                break
//            case .left:
//                isRotate = true
//                degrees = 90
//                break
//            case .right:
//                isRotate = true
//                degrees = -90
//                break
//            case .up:
//                isRotate = false
//                degrees = 0
//                break
//            default: break
//            }
//
//            var newImage = image.toNoir(isRotate, degrees: degrees)
//            //
//            //                var newImage = self.imagePostProcessing(input: i2)
//
//            var fileName = UUID().uuidString + ".jpg"
//
//            let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)
//
//            let fileURL = docDirURL.appendingPathComponent(fileName)
//
//            do
//            {
//                try newImage.jpegData(compressionQuality: 1)!.write(to: fileURL, options: [.atomic])
//            }
//            catch(let error)
//            {
//                print(error)
//            }
//
//            let file = DocModelFile()
//            file.Width = Float(image.size.width)
//            file.Height = Float(image.size.height)
//            file.FileName = fileName
//            file.Id = UUID().uuidString
//            file.DocId = self._id
//            file.TypeFile = "image"
//
//            try! Core.shared().getRealm.write {
//                Core.shared().getRealm.add(file)
//            }
//
//            print("*** OLD FILES \(self.files.count)")
//
//            self.files.append(file)
//
//            self.currentFile = self.files.count - 1
//
//            print("*** NEW FILES \(self.files.count)")
//
//            self.reinitFilesStrip()
//
//            self.initImage(image: self.files[self.currentFile])
//
//            //            waitingView.removeFromSuperview()
//
////            EventsManager.shared().sendNotify(Constants.EVENT_HIDE_PROCESSING)
//        })
//    }
//
//
//}

extension DocEdit: DocEditMenuDelegate
{
    func DocEditLight()
    {
        print("*** DocEditLight")
        
        self.prepareBrightEditing()
        bottom.initLight()
    }
    
    func DiocEditContrast()
    {
        print("*** DiocEditContrast")
        
        self.prepareContrastEditing()
        bottom.initContrast()
    }
    
    func prepareContrastEditing()
    {
        print("*** prepareContrastEditing")
        
        let origin = self.getOriginImage()

        let acImage = CIImage(cgImage: origin.cgImage!)

        context = CIContext(options: nil)

        contrastFilter = CIFilter(name: "CIColorControls");
        contrastFilter!.setValue(acImage, forKey: "inputImage")
        contrastFilter!.setValue(NSNumber(value: 0), forKey: "inputContrast")
        
        showSaveAction()
    }
    
    func prepareBrightEditing()
    {
        print("*** prepareBrightEditing")
        
        let origin = self.getOriginImage()
        
        let acImage = CIImage(cgImage: origin.cgImage!)
        
        context = CIContext(options: nil)
        
        brightnessFilter = CIFilter(name: "CIColorControls");
        brightnessFilter!.setValue(acImage, forKey: "inputImage")
        brightnessFilter!.setValue(NSNumber(value: 0), forKey: "inputBrightness")
        
        showSaveAction()
    }
    
    func DocEditActionContrastChanged(value: Float)
    {
         print("*** DocEditActionContrastChanged")
        
        contrastFilter!.setValue(NSNumber(value: value), forKey: "inputContrast")
        
        let outputImage = contrastFilter!.outputImage
        
        let imageRef = self.context.createCGImage(outputImage!, from: outputImage!.extent)
        
        let newUIImage = UIImage(cgImage: imageRef!)
        
        self.passNewImage(image: newUIImage)
    }
    
    func DocEditActionBrightChanged(value: Float)
    {
        print("*** DocEditActionBrightChanged")
        
        brightnessFilter!.setValue(NSNumber(value: value), forKey: "inputBrightness")

        let outputImage = brightnessFilter!.outputImage

        let imageRef = self.context.createCGImage(outputImage!, from: outputImage!.extent)

        let newUIImage = UIImage(cgImage: imageRef!)

        self.passNewImage(image: newUIImage)
    }
    
    func DocEditRotateLeft()
    {
        print("*** rotateImageLeft")
        var image = imageView.image!.imageRotatedByDegrees(degrees: 90, flip: true)

        renderImage(image: image)
        
        showCropTool()
        
        showSaveAction()
        
//        imageView.removeFromSuperview()
    }
    
    func DocEditRotateRight()
    {
        print("*** rotateImageRight")
        var image = imageView.image!.imageRotatedByDegrees(degrees: -90, flip: true)

        renderImage(image: image)
        
        showCropTool()
        
        showSaveAction()
    }
    
    func DocEditRotateCircle()
    {
        var image = imageView.image!.imageRotatedByDegrees(degrees: 180, flip: true)
        
        renderImage(image: image)
        
        showCropTool()
        
        showSaveAction()
    }
    
    func DocEditRotate()
    {
        self.bottom.initRotate()
        
        showCropTool()
        
        showSaveAction()
    }
    
    func DocEditPen()
    {
        self.bottom.initPen()
    }
    
    func DocEditBright()
    {
        self.bottom.initBright()
    }
    
    func DocEditActionShare()
    {
//        print("*** DocEditActionShare")
//
//        if let doc = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(self._id)'").first
//        {
//            var items = [
//                ActionMenuItem(title: s("share_image"), itemId: 1),
//                ActionMenuItem(title: s("share_pdf"), itemId: 2),
//                ActionMenuItem(title: s("save_to_library"), itemId: 3)
//            ]
//
//            if(doc.ORCText.count > 0)
//            {
//                items.append(ActionMenuItem(title: s("share_recognized_pdf"), itemId: 4))
//                items.append(ActionMenuItem(title: s("share_recognized_plain_text"), itemId: 5))
//            }
//
//            let menu = ActionsMenu(menuId: 1, items: items)
//
//            menu.delegate = self
//
//            self.addSubview(menu)
//
//            menu.Show()
//        }
        //        }
    }
    
    func DocEditActionOCR()
    {
//        print("*** DocEditActionOCR")
//
//        if(Core.shared().NetworkStatus.connection == .none)
//        {
//            return
//        }
//
//        self.delegate.PhotoPreviewOCR(id: self._id)
        
    }
    
    func DocEditActionEdit()
    {
        print("*** DocEditActionEdit")
        
        self.bottom.initEdit()
    }
    
    func DocEditActionSignature()
    {
        print("*** DocEditActionSignature")
    }
    
    func DocEditActionMore()
    {
        print("*** DocEditActionMore")
        
        var items = [
            ActionMenuItem(title: "Rename", itemId: 5),
            ActionMenuItem(title: "Add tags", itemId: 1),
            ActionMenuItem(title: "Move to folder", itemId: 2),
            ActionMenuItem(title: "Delete current slide", itemId: 3),
            ActionMenuItem(title: "Delete whole doc", itemId: 4)
        ]
        
        let menu = ActionsMenu(menuId: 2, items: items)
        
        menu.delegate = self
        
        self.addSubview(menu)
        
        menu.Show()
    }
    
    func ActionsMenuAction(menuId: Int, itemId: Int)
    {
//        if(menuId == 1)
//        {
//            switch(itemId)
//            {
//            case 1:
//                EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._id, "type": "image"])
//                break;
//            case 2:
//                EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._id, "type": "pdf"])
//                break;
//            case 3:
//                EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._id, "type": "image_save"])
//                break;
//            case 4:
//                EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._id, "type": "ocrpdf"])
//                break;
//            case 5:
//                EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._id, "type": "text"])
//                break;
//            default:
//                break;
//            }
//        }
//
//        if(menuId == 2)
//        {
//            switch(itemId)
//            {
//            case 1:
//                break
//            case 2:
//                break
//            case 3:
//                break
//            case 4:
//                break
//            case 5:
//                break
//            default:
//                break;
//            }
//        }
    }
}
