//
//  AppDelegate.swift
//  Docscaner
//
//  Created by Artem  on 28/02/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import UIKit
import SwiftyStoreKit
import FBSDKCoreKit
import FacebookCore
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?


	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
//        setNavigationBar()
        
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                    if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                    // Unlock content
                    print("*** didFinishLaunchingWithOptions 5")
                case .failed, .purchasing, .deferred:
                    break // do nothing
                }
            }
        }
        
        if !Core.shared().IsDebug
        {
            FirebaseApp.configure()
        }
        

        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
		Core.shared().Init(window: window!)
        
        if(launchOptions != nil && launchOptions![UIApplication.LaunchOptionsKey.url] == nil)
        {
            FBSDKAppLinkUtility.fetchDeferredAppLink({ (URL, error) -> Void in
                if error != nil
                {
                    //Handle error
                }
                
                if URL != nil
                {
                    application.openURL(URL!)
                }
            })
        }

		return true
	}
    
    
    // docscanner://?promo_next=true&sub1=1&sub2=2
    
    // docscanner://promo_next/true
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool
    {
        if(!url.absoluteString.isEmpty)
        {
            Core.shared().defaults.setValue(url.absoluteString, forKey: "deep_link")
        }
        
        print("*** DEEP LINKS \(url.absoluteString)")
        
        var path = url.path.components(separatedBy: "/")
        
        if(path.count > 0)
        {
            let host = url.host
            
            path[0] = host!
            
            var ii = 0
            
            var params = [String: Any]()
            
            for i in path
            {
                if ii % 2 == 0
                {
                    print(i)
                    params[path[ii]] = path[ii + 1]
                }
                
                ii += 1
            }
            
            if(params["promo_next"] != nil)
            {
                print(params)
                
                if((params["promo_next"] as? String)?.lowercased() == "true")
                {
                    Core.shared().IsPromoNext = true
                }
                
                print("*** DEEP LINKS \(Core.shared().IsPromoNext)")
            }
        }

        
        let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        
        return handled
    }

	func applicationWillResignActive(_ application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
	}

	func applicationDidEnterBackground(_ application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	}

	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        print("*** applicationWillEnterForeground")
	}

	func applicationDidBecomeActive(_ application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        AppEventsLogger.activate(application)
	}

	func applicationWillTerminate(_ application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	}

//    func setNavigationBar()
//    {
//        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
//        UINavigationBar.appearance().shadowImage = UIImage()
//        UINavigationBar.appearance().tintColor = UIColor.white
//        UINavigationBar.appearance().barTintColor = UIColor.white
//        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
//    }

}

