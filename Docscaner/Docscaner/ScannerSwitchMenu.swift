//
//  ScannerSwitchMenu.swift
//  Docscaner
//
//  Created by Artem  on 14/03/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit

enum ScannerSwitchMenuType: Int
{
    case Auto = 1
    case Manual = 2
}

protocol ScannerSwitchMenuDelegate
{
    func ScannerSwitchMenuManualMode()
    
    func ScannerSwitchMenuAutoMode()
}

class ScannerSwitchMenu: UIView
{
	var Height: CGFloat = 40
    
    var itemAutoTitle: UILabel!
    
    var itemManualTitle: UILabel!
    
    var strip: UIView!
    
    var delegate: ScannerSwitchMenuDelegate!
    
    var currentItem = ScannerSwitchMenuType.Manual

	init()
	{
		super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: Height))
		
		self.backgroundColor = UIColor(red: 0, green: 0.39, blue: 0.6, alpha: 0.4)
        
        strip = UIView()
        strip.setX(0)
        strip.setY(0)
        strip.setSize(self.getWidth() * 0.66, self.getHeight())
        self.addSubview(strip)
        
        var itemAuto = UIView()
        itemAuto.setX(0)
        itemAuto.setY(0)
        itemAuto.setSize(strip.getWidth() / 2, strip.getHeight())
        strip.addSubview(itemAuto)
        
        itemAutoTitle = UILabel()
        itemAutoTitle.alpha = 0.5
        itemAutoTitle.setX(0)
        itemAutoTitle.setY(0)
        itemAutoTitle.text = s("switch_mode_auto").uppercased()
        itemAutoTitle.textColor = .white
        itemAutoTitle.textAlignment = .center
        itemAutoTitle.setSize(itemAuto.getWidth(), itemAuto.getHeight())
        itemAutoTitle.font = UIFont(name: "Circe-Bold", size: 14)
        itemAuto.addSubview(itemAutoTitle)
        
        var itemAutoButton = UIButton()
//        itemAutoButton.backgroundColor = .red
        itemAutoButton.tag = 1
        itemAutoButton.addTarget(self, action: "switchMenu:", for: .touchUpInside)
        itemAutoButton.setX(0)
        itemAutoButton.setY(0)
        itemAutoButton.setSize(itemAuto.getWidth(), itemAuto.getHeight())
        itemAuto.addSubview(itemAutoButton)
        
        //-----
        
        var itemManual = UIView()
        itemManual.setX(0, relative: itemAuto)
        itemManual.setY(0)
        itemManual.setSize(strip.getWidth() / 2, strip.getHeight())
        strip.addSubview(itemManual)
        
        itemManualTitle = UILabel()
        itemManualTitle.setX(0)
        itemManualTitle.setY(0)
        itemManualTitle.text = s("switch_mode_manual").uppercased()
        itemManualTitle.textColor = .white
        itemManualTitle.textAlignment = .center
        itemManualTitle.setSize(itemManual.getWidth(), itemManual.getHeight())
        itemManualTitle.font = UIFont(name: "Circe-Bold", size: 14)
        itemManual.addSubview(itemManualTitle)
        
        var itemManualButton = UIButton()
        itemManualButton.tag = 2
        itemManualButton.addTarget(self, action: "switchMenu:", for: .touchUpInside)
        itemManualButton.setX(0)
        itemManualButton.setY(0)
        itemManualButton.setSize(itemManual.getWidth(), itemManual.getHeight())
        itemManual.addSubview(itemManualButton)
        
	}
    
    @objc func switchMenu(_ sender: AnyObject)
    {
        print("*** switchMenu")
        
        var n = ScannerSwitchMenuType(rawValue: sender.tag)!
        
        if(n == currentItem)
        {
            return
        }

        switch n
        {
        case .Auto:

            UIView.animate(withDuration: 0.5) {
                self.strip.setX(self.getWidth() / 3)
                self.itemManualTitle.alpha = 0.5
                self.itemAutoTitle.alpha = 1
            }
            
            self.delegate.ScannerSwitchMenuAutoMode()
            break
        case .Manual:
            
            UIView.animate(withDuration: 0.5) {
                self.strip.setX(0)
                self.itemManualTitle.alpha = 1
                self.itemAutoTitle.alpha = 0.5
            }
            
            self.delegate.ScannerSwitchMenuManualMode()
            break
        default:
            break
        }
        
        currentItem = n
    }

	required init?(coder aDecoder: NSCoder) {
        
		fatalError("init(coder:) has not been implemented")
	}
}
