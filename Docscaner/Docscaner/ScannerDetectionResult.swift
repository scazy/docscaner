//
//  ScannerDetectionResult.swift
//  Docscaner
//
//  Created by lynxmac on 25/03/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit

class ScannerDetectionResult
{
    var ResultImage: CIImage?
    
    var Bounds: CGRect?
    
    var Points = [CGPoint]()
    
    init(image: CIImage?, bounds: CGRect, points:  [CGPoint])
    {
        ResultImage = image
        
        Bounds = bounds
        
        Points = points
    }
}
