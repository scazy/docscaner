//
//  DashboardEmpty.swift
//  Docscaner
//
//  Created by Artem  on 04/03/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit

class DashboardEmpty: UIView
{
	init(width: CGFloat, height: CGFloat)
	{
		super.init(frame: CGRect(x: 0, y: 0, width: width, height: height))

//		self.backgroundColor = .red

		let arrawImage = UIImage(named: "arrow_empty")
		var arrowSize = getImageSize(image: arrawImage!, width: 0, height: 40)

		let arrow = UIImageView()
		arrow.image = arrawImage
		arrow.setSize(arrowSize[0], arrowSize[1])
		arrow.toCenterX(self)
		arrow.setY(height - arrow.getHeight())
		self.addSubview(arrow)

		let placeholderImage = UIImage(named: "placeholder")
		var placeholderImageSize = getImageSize(image: placeholderImage!, width: self.getWidth() / 2, height: 0)

		let placeholder = UIImageView()
		placeholder.image = placeholderImage
		placeholder.setSize(placeholderImageSize[0], placeholderImageSize[1])
		placeholder.setY(40)
		placeholder.toCenterX(self)
		self.addSubview(placeholder)

		let subTitleText = s("dashboard_empty_title")

		let subTitleSize = getLabelSizeForFont(str: subTitleText, size: 13, fontName: "SFUIText-Bold", setWidth: self.getWidth() - 40)

		let subtitle = UILabel()
		subtitle.textAlignment = .center
		subtitle.text = subTitleText
		subtitle.setSize(self.getWidth() - 40, subTitleSize.height)
		subtitle.setX(20)
		subtitle.setY(arrow.getY() - 20 - subtitle.getHeight())
		subtitle.textColor = UIColor(red: 0.63, green: 0.63, blue: 0.63, alpha: 1)
		subtitle.font = UIFont(name: "SFUIText-Bold", size: 13)
		subtitle.numberOfLines = 0
		subtitle.lineBreakMode = .byWordWrapping
		subtitle.sizeToFit()
		self.addSubview(subtitle)

		let titleText = s("dashboard_no_files")
		let titleSize = getLabelSizeForFont(str: titleText, size: 21, fontName: "Circe-Bold", setWidth: self.getWidth() - 40)

		let title = UILabel()
//		title.backgroundColor = .red
		title.textColor = UIColor(red: 0.34, green: 0.34, blue: 0.34, alpha: 1)
		title.numberOfLines = 0
		title.lineBreakMode = .byWordWrapping
		title.sizeToFit()
		title.textAlignment = .center
		title.text = titleText
		title.setSize(self.getWidth() - 40, titleSize.height)
		title.setX(20)
		title.setY(subtitle.getY() - 40 - title.getHeight())
		title.font = UIFont(name: "Circe-Bold", size: 21)		
		self.addSubview(title)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
