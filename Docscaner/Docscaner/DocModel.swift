//
//  DocModel.swift
//  Docscaner
//
//  Created by Artem  on 06/03/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import RealmSwift

class DocModel: Object
{
	@objc dynamic var Id: String = ""

	@objc dynamic var Name: String = ""

	@objc dynamic var CreatedAt: Int64 = 0

	override static func primaryKey() -> String?
	{
		return "Id"
	}

	@objc dynamic var IsTest: Bool = false

	@objc dynamic var OCRFileName: String = ""

	@objc dynamic var PDFFileName: String = ""

	@objc dynamic var IsFolder: Bool = false

	@objc dynamic var ParentId = ""
}

class DocModelFile: Object
{
	@objc dynamic var Id: String = ""

	@objc dynamic var DocId: String = ""

	override static func primaryKey() -> String?
	{
		return "Id"
	}

	@objc dynamic var FileName: String = ""

	@objc dynamic var TypeFile: String = ""

	@objc dynamic var Width: Float = 0

	@objc dynamic var Height: Float = 0

	@objc dynamic var ORCText: String = ""

	@objc dynamic var Size: Int = 0

	@objc dynamic var ParentId = ""
    
    @objc dynamic var SignatureX: Float = 0
    
    @objc dynamic var SignatureY: Float = 0
    
    @objc dynamic var SignatureWidth: Float = 0
    
    @objc dynamic var SignatureHeight: Float = 0
    
    @objc dynamic var SignatureDocWidth: Float = 0
    
    @objc dynamic var SignatureDocHeight: Float = 0
    
    @objc dynamic var IsSignature = false
    
    func getImage() -> UIImage
    {
        let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)
        
        let path = docDirURL.appendingPathComponent(self.FileName)
        
        let data = try? Data(contentsOf: path)
        
        return UIImage(data: data!)!
    }
}
