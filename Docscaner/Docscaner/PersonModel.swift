//
//  PersonModel.swift
//  Docscaner
//
//  Created by Artem  on 28/02/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import RealmSwift

class PersonModel: Object
{
	@objc dynamic var IsPremium = false

	@objc dynamic var Id: Int64 = 0

	@objc dynamic var Token = ""
        
	override static func primaryKey() -> String?
	{
		return "Id"
	}
}
