//
//  FolderCell.swift
//  Docscaner
//
//  Created by Artem  on 11/03/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit

class FolderCell: DocBaseCell, DocCellDelegate
{
    func EnableEdit(isEdit: Bool)
    {
        print("*** FolderCell Edit")
        
        self.setEditable(isEditable: isEdit)
        
        self.IsChecked = false
        
        IsEditable = isEdit
    }
    
    func IsEditableMode() -> Bool
    {
        return IsEditable
    }
    
    func getIsChecked() -> Bool
    {
        return IsChecked
    }
    
    let item = UIView()
    
    var itemLayout = UIView()
    
    let title = UILabel()
    
    let subtitle = UILabel()
    
    let folderLayout = UIView()
    
    let coverFolder = UIImageView()
    
    var checkbox = UIView()
    
    var checkboxImage = UIImageView()
    
	override init(frame: CGRect)
	{
		super.init(frame: frame)
	}

	override func layoutSubviews()
	{
		super.layoutSubviews()

		//		Layout.backgroundColor = .red
        self.Layout.addSubview(item)
        
        item.addSubview(itemLayout)
        
        self.Layout.addSubview(title)
        
        self.Layout.addSubview(subtitle)
        
        itemLayout.addSubview(folderLayout)
        
        
        
        itemLayout.addSubview(coverFolder)
        
        itemLayout.bringSubviewToFront(coverFolder)
        
        self.Layout.addSubview(checkbox)
        
        checkbox.addSubview(checkboxImage)
        
	}

	override func prepareForReuse()
	{

	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func Update(doc: DocModel)
	{
		self._doc = doc

		contentView.addSubview(self.Layout)
		contentView.sendSubviewToBack(self.Layout)

		self.Layout.setX(1)
		self.Layout.setY(1)
		self.Layout.setSize(self.contentView.getWidth() - 2, self.contentView.getHeight() - 2)
//		self.Layout.backgroundColor = .red

		item.layer.cornerRadius = 10
		item.setSize(self.Layout.getWidth() - 20, self.Layout.getWidth() - 20)
		item.setX(10)
		item.setY(10)

		itemLayout.setX(0)
		itemLayout.setY(0)
		itemLayout.setSize(item.getWidth(), item.getHeight())

		title.setX(15)
		title.setY(3, relative: item)
//        title.font = UIFont.boldSystemFont(ofSize: 12)
		title.setSize(self.Layout.getWidth() - 10, 14)
        title.textColor = UIColor(red: 0.36, green: 0.36, blue: 0.36, alpha: 1)
        title.font = UIFont(name: "SFProText-Medium", size: 12)

		subtitle.setX(15)
//        subtitle.textAlignment = .center
		subtitle.setY(0, relative: title)
		subtitle.font = UIFont.systemFont(ofSize: 11)
		subtitle.setSize(self.Layout.getWidth() - 10, 14)
        subtitle.textColor = UIColor(red: 0.36, green: 0.36, blue: 0.36, alpha: 1)
        subtitle.font = UIFont(name: "SFProText-Regular", size: 10)

		title.text = doc.Name

		subtitle.text = "0 scans"

		
		folderLayout.clipsToBounds = true
		folderLayout.layer.masksToBounds = true
		folderLayout.layer.contentsGravity = CALayerContentsGravity.resizeAspectFill
		folderLayout.setY(0)
		folderLayout.setX(0)
		folderLayout.setSize(itemLayout.getWidth(), itemLayout.getHeight())
		folderLayout.backgroundColor = UIColor(red: 0.96, green: 0.9, blue: 0.59, alpha: 1)
		folderLayout.layer.cornerRadius = 3
		folderLayout.layer.borderWidth = 1
		folderLayout.layer.borderColor = UIColor(red: 0.94, green: 0.81, blue: 0.34, alpha: 1).cgColor
		

		//---

        let folderFilesCount = Core.shared().getRealm.objects(DocModelFile.self).filter("ParentId = '\(doc.Id)'").count

		subtitle.text = "\(folderFilesCount) scans"

		if(folderFilesCount > 0)
		{
			var ii = 0
			for image in Core.shared().getRealm.objects(DocModelFile.self).filter("ParentId = '\(doc.Id)'")
			{
				if(ii >= 3)
				{
					break
				}

				let path = docDirURL.appendingPathComponent(image.FileName)
				let data = try? Data(contentsOf: path)
				let imageData = UIImage(data: data!)!

				var imageSize = getImageSize(image: imageData, width: 0, height: folderLayout.getHeight() - 25)

				let preview = UIImageView()
				preview.setX(0)
				preview.setY(0)
				preview.image = imageData
				preview.setSize(imageSize[0], imageSize[1])
				folderLayout.addSubview(preview)

				if(imageSize[1] > imageSize[0])
				{
					ii += 1
				}
			}
		}

		//---

		let coverFolderImage = UIImage(named: "folder_cover")

		var coverFolderImageSize = getImageSize(image: coverFolderImage!, width: folderLayout.getWidth(), height: 0)
		
		coverFolder.image = coverFolderImage
		coverFolder.setSize(coverFolderImageSize[0], coverFolderImageSize[1])
		coverFolder.setX(0)
		coverFolder.setY(folderLayout.getHeight() - coverFolderImageSize[1])
        
        itemLayout.bringSubviewToFront(coverFolder)
        
        //-----
        
        checkbox.setSize(20, 20)
        checkbox.backgroundColor = UIColor(red: 0.77, green: 0.79, blue: 0.79, alpha: 1)
        checkbox.layer.cornerRadius = 2
        checkbox.setX(self.Layout.getWidth() - 15 - checkbox.getWidth())
        checkbox.setY(15)
        checkbox.isHidden = true
        
        checkboxImage.image = UIImage(named: "folder_checkbox")
        checkboxImage.setX(0)
        checkboxImage.setY(0)
        checkboxImage.setSize(checkbox.getWidth(), checkbox.getHeight())
        checkboxImage.isHidden = true
		
	}
    
    func setEditable(isEditable: Bool)
    {
        if(isEditable)
        {
            checkbox.isHidden = false
        }
        else
        {
            checkbox.isHidden = true
        }
        
        checkboxImage.isHidden = true
    }
    
    func setChecked()
    {
        if(!IsChecked)
        {
            checkbox.backgroundColor = UIColor(red: 0, green: 0.6, blue: 0.77, alpha: 1)
            checkboxImage.isHidden = false
            self.IsChecked = true
        }
        else
        {
            checkbox.backgroundColor = UIColor(red: 0.77, green: 0.79, blue: 0.79, alpha: 1)
            checkboxImage.isHidden = true
            self.IsChecked = false
        }
    }
}
