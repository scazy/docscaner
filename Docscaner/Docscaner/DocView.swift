//
//  ViewDoc.swift
//  Docscaner
//
//  Created by artem on 04/04/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit

protocol DocViewDelegate
{
    func DocViewImportFromLibrary(id: String)
    
    func DocViewOpenEdit(id: String)
}

class DocView: UIView, DocViewTopDelegate
{
    var startOffset: CGFloat = 0
    
    var delegate: DocViewDelegate!
    
    var top: DocViewTop!
    
    private var _id: String = ""
    
    var docName = ""
    
    var bottom: DocViewBottom!
    
    var slides = [DocModelFile]()
    
    func reloadView()
    {
        for i in self.subviews
        {
            i.removeFromSuperview()
        }
        
        self.backgroundColor = UIColor.white
        
        var bg = UIImageView()
        bg.setX(0)
        bg.setY(0)
        bg.setSize(self.getWidth(), self.getHeight())
        bg.image = UIImage(named: "doc_view_bg")
        self.addSubview(bg)
        
        if let doc = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(self._id)'").first
        {
            self.docName = doc.Name
            
            for i in Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(self._id)'")
            {
                slides.append(i)
            }
            
            self.initControls(slideCount: slides.count)
            
            var nameBlock = UIView()
            //            nameBlock.backgroundColor = .red
            nameBlock.setX(0)
            nameBlock.setY(0, relative: top)
            nameBlock.setSize(self.getWidth(), 50)
            self.addSubview(nameBlock)
            
            //----
            
            var allText = String(format: s("file_name"), self.docName)
            let messageMutable = NSMutableAttributedString(string: allText)
            
            var start = allText.count - self.docName.count
            var end = self.docName.count
            
            messageMutable.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 18), range: NSRange(location: start, length: end))
            messageMutable.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: NSRange(location: start, length: end))
            messageMutable.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range: NSRange(location: start, length: end))
            
            var nameLabel = UILabel()
            nameLabel.tag = -8978
            nameLabel.setX(0)
            nameLabel.setY(0)
            nameLabel.setSize(nameBlock.getWidth(), nameBlock.getHeight())
            nameLabel.textAlignment = .center
            nameLabel.font = UIFont.systemFont(ofSize: 18)
            nameLabel.textColor = UIColor.white.withAlphaComponent(0.75)
            nameLabel.attributedText = messageMutable
            nameBlock.addSubview(nameLabel)
            
            var nameButton = UIButton()
            nameButton.setX(0)
            nameButton.setY(0)
            nameButton.setSize(nameBlock.getWidth(), nameBlock.getHeight())
            nameButton.addTarget(self, action: "changeNameAction:", for: .touchUpInside)
            nameBlock.addSubview(nameButton)
            
            //-------
            
            print("*** DOCS COUNT \(slides.count)")
            print(self.slides)
            
            if(slides.count == 1)
            {
                var docView = UIView()
                docView.setX(0)
                docView.setY(0, relative: nameBlock)
                docView.setSize(self.getWidth(), self.getHeight() - nameBlock.getHeight() - top.getHeight() - bottom.getHeight())
                self.addSubview(docView)
                
                
                var dataImage = slides[0].getImage()
                
                let imageWidthOrigin = dataImage.size.width
                
                let imageHeightOrigin = dataImage.size.height
                
                let maxImageWidth: CGFloat = docView.getWidth() - 40
                
                let maxImageHeight: CGFloat = docView.getHeight() - 40
                
                var imageHeight: CGFloat = maxImageHeight
                
                var factor: CGFloat = CGFloat(imageHeight) / CGFloat(imageHeightOrigin)
                
                var imageWidth: CGFloat = CGFloat(imageWidthOrigin) * factor
                
                if(imageWidth > maxImageWidth)
                {
                    imageWidth = maxImageWidth
                    
                    factor = CGFloat(imageWidth) / CGFloat(imageWidthOrigin)
                    
                    imageHeight = CGFloat(imageHeightOrigin) * factor
                }
                
                var imageView = UIImageView()
                imageView.tag = 3000
                //                imageView.layer.borderColor = UIColor.black.cgColor
                //                imageView.layer.borderWidth = 1
                imageView.isUserInteractionEnabled = true
                imageView.setSize(imageWidth, imageHeight)
                imageView.toCenterX(docView)
                imageView.toCenterY(docView)
                imageView.image = dataImage
                docView.addSubview(imageView)
                
                var deleteLayout = UIView()
                deleteLayout.tag = -1200
                deleteLayout.backgroundColor = UIColor(red: 0.84, green: 0.22, blue: 0.22, alpha: 1)
                deleteLayout.setSize(45, 45)
                deleteLayout.layer.cornerRadius = deleteLayout.getWidth() / 2
                deleteLayout.setX(imageView.getX() + imageView.getWidth() - (deleteLayout.getWidth() / 2) - 5)
                deleteLayout.setY(imageView.getY() - (deleteLayout.getHeight() / 2) + 5)
                docView.addSubview(deleteLayout)
                
                var deleteIcon = UIImageView()
                deleteIcon.image = UIImage(named: "doc_view_delete")
                deleteIcon.setSize(deleteLayout.getWidth() * 0.4, deleteLayout.getWidth() * 0.4)
                deleteIcon.toCenterX(deleteLayout)
                deleteIcon.toCenterY(deleteLayout)
                deleteLayout.addSubview(deleteIcon)
                
                var deleteButton = UIButton()
                deleteButton.setSize(deleteLayout.getWidth(), deleteLayout.getWidth())
                deleteButton.setX(0)
                deleteButton.setY(0)
                deleteButton.addTarget(self, action: "deleteSlideAction:", for: .touchUpInside)
                deleteLayout.addSubview(deleteButton)
                
                self.initSign(slideId: self._currentSlide, parentView: docView)

            }
            else
            {
                central = UIView()
                //                central.layer.borderColor = UIColor.white.cgColor
                //                central.layer.borderWidth = 1
                central.setY(0, relative: nameBlock)
                //                central.setX(50)
                
                central.setX(20)
                central.setSize(ScreenSize.SCREEN_WIDTH - 100, self.getHeight() - nameBlock.getHeight() - top.getHeight() - bottom.getHeight() - 20)
                self.addSubview(central)
                
                right = UIView()
                //                right.layer.borderColor = UIColor.white.cgColor
                //                right.layer.borderWidth = 1
                right.setY(20, relative: nameBlock)
                right.setX(20, relative: central)
                right.setSize(ScreenSize.SCREEN_WIDTH - 140, self.getHeight() - nameBlock.getHeight() - top.getHeight() - bottom.getHeight() - 60)
                self.addSubview(right)
                
                left = UIView()
                //                left.layer.borderColor = UIColor.white.cgColor
                //                left.layer.borderWidth = 1
                left.setY(20, relative: nameBlock)
                left.setSize(ScreenSize.SCREEN_WIDTH - 140, self.getHeight() - nameBlock.getHeight() - top.getHeight() - bottom.getHeight() - 60)
                left.setX(central.getX() - 20 - left.getWidth())
                self.addSubview(left)
                
                //----
                
                
                //----
                
                var s = 0
                
                var slidesdOffset: CGFloat = 0
                
                for slide in self.slides
                {
                    switch(s)
                    {
                    case 0:
                        self.initSlide(slideId: s, parentView: central)
                        break;
                    case 1:
                        self.initSlide(slideId: s, parentView: right)
                        
                        slidesdOffset = self.right.getX() + self.right.getWidth() + 20
                        break;
                    default:
                        
                        self.initSlide(slideId: s, parentView: right, offsetX: slidesdOffset)
                        
                        slidesdOffset += slidesdOffset + self.right.getWidth() + 20
                        break;
                    }
                    
                    s += 1
                }
                
                initSign(slideId: self._currentSlide, parentView: self)
                
                if let firstSlide = self.viewWithTag(3000 + self._currentSlide) as? UIImageView
                {
                    var deleteLayout = UIView()
                    deleteLayout.tag = -1200
                    deleteLayout.backgroundColor = UIColor(red: 0.84, green: 0.22, blue: 0.22, alpha: 1)
                    deleteLayout.setSize(45, 45)
                    deleteLayout.layer.cornerRadius = deleteLayout.getWidth() / 2
                    deleteLayout.setX(firstSlide.getX() + firstSlide.getWidth() - (deleteLayout.getWidth() / 2) - 5)
                    deleteLayout.setY(firstSlide.getY() - (deleteLayout.getHeight() / 2) + 5)
                    self.addSubview(deleteLayout)
                    
                    var deleteIcon = UIImageView()
                    deleteIcon.image = UIImage(named: "doc_view_delete")
                    deleteIcon.setSize(deleteLayout.getWidth() * 0.4, deleteLayout.getWidth() * 0.4)
                    deleteIcon.toCenterX(deleteLayout)
                    deleteIcon.toCenterY(deleteLayout)
                    deleteLayout.addSubview(deleteIcon)
                    
                    var deleteButton = UIButton()
                    deleteButton.setSize(deleteLayout.getWidth(), deleteLayout.getWidth())
                    deleteButton.setX(0)
                    deleteButton.setY(0)
                    deleteButton.addTarget(self, action: "deleteSlideAction:", for: .touchUpInside)
                    deleteLayout.addSubview(deleteButton)
                }
            }
            

        }
    }
    
    init(id: String)
    {
        super.init(frame: CGRect(x: ScreenSize.SCREEN_WIDTH, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))
        
        startOffset = UIApplication.shared.statusBarFrame.size.height
        
        self._id = id
        
        reloadView()
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.leftSwipe(_:)))
        leftSwipe.direction = UISwipeGestureRecognizer.Direction.left
        self.addGestureRecognizer(leftSwipe)
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.rightSwipe(_:)))
        rightSwipe.direction = UISwipeGestureRecognizer.Direction.right
        self.addGestureRecognizer(rightSwipe)
    }
    
    @objc func deleteSlideAction(_ sender: AnyObject)
    {
        var items = [
            ActionMenuItem(title: s("remove_slide"), itemId: 1)
        ]
        
        if(self.slides.count > 1)
        {
            items.append(ActionMenuItem(title: s("remove_all_slides"), itemId: 2))
        }

        let menu = ActionsMenu(menuId: 5, items: items)
        menu.delegate = self
        self.addSubview(menu)
        
        menu.Show()
    }
    
    var _currentSlide = 0
    
    var central: UIView!
    
    var right: UIView!
    
    var left: UIView!
    
    func hideDelete()
    {
        if let delete = self.viewWithTag(-1200)
        {
            delete.alpha = 0
        }
    }
    
    @objc func removeSign(_ sender: AnyObject)
    {
        let alertController = UIAlertController(title: s("remove_signature"), message: s("remove_signature_desc"), preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: s("remove_title"), style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
            DispatchQueue.main.async(execute: {
                try! Core.shared().getRealm.write {
                    self.slides[self._currentSlide].IsSignature = false
                    self.slides[self._currentSlide].SignatureX = 0
                    self.slides[self._currentSlide].SignatureY = 0
                    self.slides[self._currentSlide].SignatureWidth = 0
                    self.slides[self._currentSlide].SignatureHeight = 0
                    self.slides[self._currentSlide].SignatureDocWidth = 0
                    self.slides[self._currentSlide].SignatureDocHeight = 0
                }
                
                self.SignatureEditUpdateSign()
            })
        })
        
        alertController.addAction(okAction)
        
        let cancelAction = UIAlertAction(title: s("botton_cancel"), style: .cancel, handler: {
            (action : UIAlertAction!) -> Void in
        })
        
        alertController.addAction(cancelAction)
        
        self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    func showDelete()
    {
        if let delete = self.viewWithTag(-1200)
        {
            if let firstSlide = self.viewWithTag(3000 + self._currentSlide) as? UIImageView
            {
                delete.setX(firstSlide.getX() + firstSlide.getWidth() - (delete.getWidth() / 2) - 5)
                delete.setY(firstSlide.getY() - (delete.getHeight() / 2) + 5)
                
                UIView.animate(withDuration: 0.2, animations: {
                    delete.alpha = 1
                }) { (success) in
                    
                }
            }
        }
    }
    
    func moveLeft()
    {
        if let sign = self.viewWithTag(-987)
        {
            sign.removeFromSuperview()
        }
        
        
        
        moveSlide(slideId: self._currentSlide, parentView: self.left, offsetX: 0, finish: {
            
        })
        
        moveSlide(slideId: self._currentSlide + 1, parentView: self.central, offsetX: 0, finish: {
            self.showDelete()
            
        })
        
        if self.slides.indices.contains(self._currentSlide - 1)
        {
            moveSlide(slideId: self._currentSlide - 1, parentView: self.left, offsetX: self.left.getX() - 20 - self.left.getWidth(), finish: {
                
            })
        }
        
        self._currentSlide += 1
        
        self.initSign(slideId: self._currentSlide, parentView: self)
        
        self.top.setCurrentSlide(slide: self._currentSlide + 1)
        
        if self.slides.indices.contains(self._currentSlide + 1)
        {
            moveSlide(slideId: self._currentSlide + 1, parentView: self.right, offsetX: 0, finish: {
                
            })
        }
    }
    
    func moveRight()
    {
        if let sign = self.viewWithTag(-987)
        {
            sign.removeFromSuperview()
        }
        
        
        moveSlide(slideId: self._currentSlide, parentView: self.right, offsetX: 0, finish: {
            
        })
        
        moveSlide(slideId: self._currentSlide - 1, parentView: self.central, offsetX: 0, finish: {
            self.showDelete()
            
//
        })
        
        if self.slides.indices.contains(self._currentSlide + 1)
        {
            moveSlide(slideId: self._currentSlide + 1, parentView: self.right, offsetX: self.right.getX() + 20 + self.right.getWidth(), finish: {
                
            })
        }
        
        self._currentSlide -= 1
        
        self.initSign(slideId: self._currentSlide, parentView: self)
        
        self.top.setCurrentSlide(slide: self._currentSlide + 1)
        
        if self.slides.indices.contains(self._currentSlide - 1)
        {
            moveSlide(slideId: self._currentSlide - 1, parentView: self.left, offsetX: 0, finish: {
                
            })
        }
    }
    
    @objc func leftSwipe(_ sender: AnyObject)
    {
        if(self._currentSlide + 1 == self.slides.count)
        {
            return
        }
        
        self.hideDelete()
        
        print("*** leftSwipe 1 \(self._currentSlide)")
        if self.slides.indices.contains(self._currentSlide + 1)
        {
            if(self._currentSlide == 0)
            {
                self.central.setX(50)
                right.setX(20, relative: central)
                left.setX(central.getX() - 20 - left.getWidth())
            }
            
            if(self._currentSlide + 1 == self.slides.count - 1)
            {
                print("*** leftSwipe 2 \(self._currentSlide)")
                self.central.setX(50 + 20)
                right.setX(20, relative: central)
                left.setX(central.getX() - 20 - left.getWidth())
            }
            
            moveLeft()
        }
    }
    
    @objc func rightSwipe(_ sender: AnyObject)
    {
        if(self._currentSlide == 0)
        {
            return
        }
        
        self.hideDelete()
        
        if(self._currentSlide == self.slides.count - 1)
        {
            self.central.setX(50)
            right.setX(20, relative: central)
            left.setX(central.getX() - 20 - left.getWidth())
        }
        
        if(self._currentSlide - 1 == 0)
        {
            self.central.setX(20)
            right.setX(20, relative: central)
            left.setX(central.getX() - 20 - left.getWidth())
        }
        
        if self.slides.indices.contains(self._currentSlide - 1)
        {
            moveRight()
        }
    }
    
    func moveSlide(slideId: Int, parentView: UIView, offsetX: CGFloat, finish: @escaping () -> ())
    {
        var dataImage = slides[slideId].getImage()
        
        let imageWidthOrigin = dataImage.size.width
        
        let imageHeightOrigin = dataImage.size.height
        
        var imageHeight: CGFloat = parentView.getHeight()
        
        var factor: CGFloat = CGFloat(imageHeight) / CGFloat(imageHeightOrigin)
        
        var imageWidth: CGFloat = CGFloat(imageWidthOrigin) * factor
        
        if(imageWidth > parentView.getWidth())
        {
            imageWidth = parentView.getWidth()
            
            factor = CGFloat(imageWidth) / CGFloat(imageWidthOrigin)
            
            imageHeight = CGFloat(imageHeightOrigin) * factor
        }
        
        if let image = self.viewWithTag(3000 + slideId) as? UIImageView
        {
            UIView.animate(withDuration: 0.5, animations: {
                image.setSize(imageWidth, imageHeight)
                
                if(offsetX != 0)
                {
                    image.setX(offsetX)
                }
                else
                {
                    image.center = parentView.center
                }
            }) { (success) in
                finish()
            }
        }
    }
    
    func initSign(slideId: Int, parentView: UIView)
    {
        print("**** initSign \(slideId)")
        
        if let sign = self.viewWithTag(-987)
        {
            sign.removeFromSuperview()
        }
        
        if let imageView = self.viewWithTag(3000 + slideId) as? UIImageView
        {
            print("**** initSign *2*  \(slideId)")
            if(slides[slideId].IsSignature)
            {
                print("**** initSign *3*  \(slideId)")
                print("*** IS SIGN")
                let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)
                
                let path = docDirURL.appendingPathComponent(Constants.SIGN_IMAGE)
                
                let data = try? Data(contentsOf: path)
                
                var sign = UIImage(data: data!)
                
                var signSize = getImageSize(image: sign!, width: imageView.getWidth() * 0.33, height: 0)
                
                var signImageBlock = UIView()
                signImageBlock.alpha = 0
                signImageBlock.tag = -987
                signImageBlock.layer.borderWidth = 2
                signImageBlock.layer.borderColor = UIColor(red: 0, green: 0.72, blue: 0.87, alpha: 0.5).cgColor
                signImageBlock.setSize(signSize[0], signSize[1])
                
                parentView.addSubview(signImageBlock)
                
                UIView.animate(withDuration: 0.6, animations: {
                    signImageBlock.alpha = 1
                }) { (success) in

                }
                
                
                if(slides[slideId].SignatureDocHeight > 0 &&
                    slides[slideId].SignatureDocWidth > 0 &&
                    slides[slideId].SignatureHeight > 0 &&
                    slides[slideId].SignatureWidth > 0 &&
                    slides[slideId].SignatureX > 0 &&
                    slides[slideId].SignatureY > 0
                    )
                {
                    var factorSign: CGFloat = CGFloat(slides[slideId].SignatureDocWidth) / imageView.getWidth()
                    
                    print("*** factorSign \(factorSign)")
                    
                    signImageBlock.setSize(CGFloat(slides[slideId].SignatureWidth) * factorSign, CGFloat(slides[slideId].SignatureHeight) * factorSign)
                    signImageBlock.setX(CGFloat(slides[slideId].SignatureX) * factorSign)
                    signImageBlock.setY(CGFloat(slides[slideId].SignatureY) * factorSign)
                }
                else
                {
                    if(slides[0].IsSignature)
                    {
                        signImageBlock.setSize(signSize[0], signSize[1])
                        signImageBlock.setX(imageView.getX() + imageView.getWidth() - signImageBlock.getWidth() - 15)
                        signImageBlock.setY(imageView.getY() + imageView.getHeight() - signImageBlock.getHeight() - 15)
                    }
                }
                
                var signImage = UIImageView()
                signImage.setSize(signImageBlock.getWidth(), signImageBlock.getHeight())
                signImage.setX(0)
                signImage.image = sign
                signImage.setY(0)
                signImageBlock.addSubview(signImage)
                
                var deleteSign = UIButton()
                deleteSign.setTitle("✕", for: .normal)
                deleteSign.titleLabel?.textColor = .white
                deleteSign.setSize(20, 20)
                deleteSign.backgroundColor = UIColor(red: 0, green: 0.72, blue: 0.87, alpha: 0.5)
                deleteSign.setX(-10)
                deleteSign.setY(-10)
                deleteSign.layer.cornerRadius = deleteSign.getWidth() / 2
                deleteSign.addTarget(self, action: "removeSign:", for: .touchUpInside)
                signImageBlock.addSubview(deleteSign)
            }
        }
        

    }
    
    func initSlide(slideId: Int, parentView: UIView, offsetX: CGFloat = 0)
    {
        var dataImage = slides[slideId].getImage()
        
        let imageWidthOrigin = dataImage.size.width
        
        let imageHeightOrigin = dataImage.size.height

        var imageHeight: CGFloat = parentView.getHeight()
        
        var factor: CGFloat = CGFloat(imageHeight) / CGFloat(imageHeightOrigin)
        
        var imageWidth: CGFloat = CGFloat(imageWidthOrigin) * factor
        
        if(imageWidth > parentView.getWidth())
        {
            imageWidth = parentView.getWidth()
            
            factor = CGFloat(imageWidth) / CGFloat(imageWidthOrigin)
            
            imageHeight = CGFloat(imageHeightOrigin) * factor
        }
        
        var imageView = UIImageView()
        imageView.tag = 3000 + slideId
//        imageView.layer.borderColor = UIColor.black.cgColor
//        imageView.layer.borderWidth = 1
        imageView.isUserInteractionEnabled = true
        imageView.setSize(imageWidth, imageHeight)
//        imageView.toCenterX(parentView)
//        imageView.toCenterY(parentView)
        imageView.image = dataImage
        self.addSubview(imageView)
        
        imageView.center = parentView.center
        
        if(offsetX != 0)
        {
            imageView.setX(offsetX)
        }
        
        //------
        

    }
    
    @objc func changeNameAction(_ sender: AnyObject)
    {
        print("*** changeNameAction")
//        var docId = self._data[self._selected[0]].Id
        
        if let doc = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(self._id)'").first
        {
            var folderName = ""
            
            let alert = UIAlertController(title: (doc.IsFolder) ? s("folder_name_title") : s("file_name_title"), message: nil, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: s("botton_cancel"), style: .cancel, handler: { (action) -> Void in}))
            
            let saveAction = UIAlertAction(title: s("button_rename"), style: .default, handler: { (action) -> Void in
                NotificationCenter.default.removeObserver(self, name: UITextField.textDidChangeNotification, object: self)
                
                self.renameDoc(folderName)
            })
            
            alert.addAction(saveAction)
            
            alert.addTextField(configurationHandler: { (textField) in
                textField.text = doc.Name
                textField.placeholder = (doc.IsFolder) ? s("folder_name_title") : s("file_name_title")
                saveAction.isEnabled = false
                NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main) { (notification) in
                    saveAction.isEnabled = textField.text!.count > 0
                    folderName = textField.text!
                }
            })
            
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    func renameDoc(_ name: String)
    {
        if let doc = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(self._id)'").first
        {
            try! Core.shared().getRealm.write {
                doc.Name = name
            }
            
            if let label = self.viewWithTag(-8978) as? UILabel
            {
//                label.text = name
                
                var allText = String(format: s("file_name"), name)
                let messageMutable = NSMutableAttributedString(string: allText)
                
                var start = allText.count - self.docName.count
                var end = self.docName.count
                
                messageMutable.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 18), range: NSRange(location: start, length: end))
                messageMutable.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: NSRange(location: start, length: end))
                messageMutable.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range: NSRange(location: start, length: end))
                
                label.attributedText = messageMutable
                
                
            }
            
//            self.reinitData(parentId: self.parentId)
//
//            self.DisableEditMode(true)
        }
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initControls(slideCount: Int)
    {
        top = DocViewTop(slideCount: slideCount)
        top.setX(0)
        top.setY(0)
        top.delegate = self
        self.addSubview(top)
        
        bottom = DocViewBottom()
        bottom.delegate = self
        bottom.setX(0)
        bottom.setY(ScreenSize.SCREEN_HEIGHT - bottom.getHeight())
        self.addSubview(bottom)
    }
    
    func DocViewTopBack()
    {
        UIView.animate(withDuration: 0.15, animations: {
            self.setX(ScreenSize.SCREEN_WIDTH)
        }) { (success) in
            self.removeFromSuperview()
            EventsManager.shared().sendNotify(Constants.EVENT_RELOAD_LIB)
            
            Core.shared().changeViewController(name: "dashboardController")
        }
        

    }
    
    func DocViewTopSave()
    {
        
    }
}

extension DocView: ActionsMenuDelegate
{
    func ActionsMenuAction(menuId: Int, itemId: Int)
    {
        switch(menuId)
        {
        case 1:
            _addActions(itemId: itemId)
            break
        case 3:
            _addActionShare(itemId: itemId)
            break;
        case 5:
            _removeActions(itemId: itemId)
        default:
            break
        }
    }
    
    func _removeActions(itemId: Int)
    {
        var text = ""

        text = s("remove_image_slide")
       
        if(itemId == 2)
        {
            text = s("remove_whole_image")
        }
        
        let alertController = UIAlertController(title: s("remove_image_title"), message: text, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: s("remove_title"), style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
            DispatchQueue.main.async(execute: {
                if(itemId == 2)
                {
                    self.removeWholeDoc()
                }
                else
                {
                    if(self.slides.count == 1)
                    {
                        self.removeWholeDoc()
                    }
                    else
                    {
                        self.removeCurrentSlide()
                    }
                }
            })
        })
        
        alertController.addAction(okAction)
        
        let cancelAction = UIAlertAction(title: s("botton_cancel"), style: .cancel, handler: {
            (action : UIAlertAction!) -> Void in
        })
        
        alertController.addAction(cancelAction)
        
        self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    func removeCurrentSlide()
    {

    }
    
    func removeWholeDoc()
    {
        if let doc = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(self._id)'").first
        {
            try! Core.shared().getRealm.write {
                Core.shared().getRealm.delete(doc)
            }
        }
        
        EventsManager.shared().sendNotify(Constants.EVENT_RELOAD_LIB)
        
        self.removeFromSuperview()
    }
    
    func _addActionShare(itemId: Int)
    {
        switch(itemId)
        {
        case 1: //share_image
             EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._id, "type": "image"])
            break;
        case 2: //share_pdf
            EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._id, "type": "pdf"])
            break;
        case 3: //save_to_library
            EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._id, "type": "image_save"])
            break;
        case 5:
            EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._id, "type": "text"])
            break
        case 4:
            EventsManager.shared().sendNotify(Constants.EVENT_DO_SHARE, data: ["id": self._id, "type": "ocrpdf"])
            break;
        default:
            break
        }
    }
    
    func _shareImage()
    {
       
    }
    
    func _sharePDF()
    {
        
    }
    
    func _shareToLibrary()
    {
        
    }
    
    func _addActions(itemId: Int)
    {
        switch(itemId)
        {
        case 1:
            _addFromLibrary()
            break
        case 2:
            _addFromCamera()
            break
        default:
            break
        }
    }
    
    func _addFromLibrary()
    {
        self.delegate.DocViewImportFromLibrary(id: self._id)
        
        self.removeFromSuperview()
    }
    
    func _addFromCamera()
    {
        
    }
    
    func ActionsMenuCancel()
    {
        
    }
}

extension DocView: DocViewBottomDelegate
{
    func DocViewBottomOCR()
    {

        if(!Core.shared().isPremium && !Core.shared().IsDebug)
        {
            EventsManager.shared().sendNotify(Constants.EVENT_SHOW_PREMIUM)
            return
        }
        
        var ocr = OCRScanner(id: self._id)
        self.addSubview(ocr)

    }
    
    func DocViewBottomAdd()
    {
        print("**** DocViewBottomAdd")
        
        var items = [
            ActionMenuItem(title: s("actions_from_library"), itemId: 1),
            ActionMenuItem(title: s("actions_from_camera"), itemId: 2),
        ]
        
        let menu = ActionsMenu(menuId: 1, items: items)
        menu.delegate = self
        self.addSubview(menu)
        
        menu.Show()
    }
    
    func DocViewBottomSign()
    {
        print("**** DocViewBottomSign")
        
        if(!Core.shared().isPremium && !Core.shared().IsDebug)
        {
            EventsManager.shared().sendNotify(Constants.EVENT_SHOW_PREMIUM)
            return
        }
        
        if let settings = Core.shared().getRealm.objects(SettingsModel.self).first
        {
            if settings.Signature
            {
                var sign = SignatureEdit(id: self._id, slide: _currentSlide)
                sign.delegate = self
                self.addSubview(sign)
            }
            else
            {
                let alertController = UIAlertController(title: s("signature_havent_title"), message: s("signature_havent_desc"), preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: s("signature_havent_ok"), style: .default, handler: {
                    (action : UIAlertAction!) -> Void in
                    
                    DispatchQueue.main.async(execute: {
                        Core.shared().changeViewController(name: "settingsController")
                        
                        self.removeFromSuperview()
                    })
                })
                
                alertController.addAction(okAction)
                
                let cancelAction = UIAlertAction(title: s("botton_cancel"), style: .cancel, handler: {
                    (action : UIAlertAction!) -> Void in
                })
                
                alertController.addAction(cancelAction)
                
                self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func DocViewBottomShare()
    {
        print("**** DocViewBottomShare")
        
        if(!Core.shared().isPremium && !Core.shared().IsDebug)
        {
            EventsManager.shared().sendNotify(Constants.EVENT_SHOW_PREMIUM)
            return
        }
        
        if let doc = Core.shared().getRealm.objects(DocModel.self).filter("Id = '\(self._id)'").first
        {
            var items = [
                ActionMenuItem(title: s("share_image"), itemId: 1),
                ActionMenuItem(title: s("share_pdf"), itemId: 2),
                ActionMenuItem(title: s("save_to_library"), itemId: 3)
            ]
            
            if let docfile = Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(self._id)'").first
            {
                if(docfile.ORCText.count > 0)
                {
                    items.append(ActionMenuItem(title: s("share_recognized_pdf"), itemId: 4))
                    items.append(ActionMenuItem(title: s("share_recognized_plain_text"), itemId: 5))
                }
            }
            
            let menu = ActionsMenu(menuId: 3, items: items)
            
            menu.delegate = self
            
            self.addSubview(menu)
            
            menu.Show()
        }
    }
    
    func DocViewBottomEdit()
    {
        print("**** DocViewBottomEdit")
        
        self.delegate.DocViewOpenEdit(id: self._id)
        
        self.removeFromSuperview()
    }
}

extension DocView: SignatureEditDelegate
{
    func SignatureEditUpdateSign()
    {
        self.slides = [DocModelFile]()
        
        for i in Core.shared().getRealm.objects(DocModelFile.self).filter("DocId = '\(self._id)'")
        {
            slides.append(i)
        }
        
        
        if(self.slides.count > 1)
        {
            self.initSign(slideId: self._currentSlide, parentView: self)
        }
        else
        {
            if let  docView = self.viewWithTag(3000)
            {
                 self.initSign(slideId: self._currentSlide, parentView: docView)
            }
        }
    }
}
