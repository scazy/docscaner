//
//  DocViewTop.swift
//  Docscaner
//
//  Created by artem on 04/04/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit

protocol DocViewTopDelegate
{
    func DocViewTopBack()
    
    func DocViewTopSave()
}

class DocViewTop: UIView
{
    var delegate: DocViewTopDelegate!
    
    var libTitle: UILabel!
    
    var slideCount = 0
    
    init(slideCount: Int)
    {
        var startOffset = UIApplication.shared.statusBarFrame.size.height
        
        self.slideCount = slideCount
                
        super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 42 + startOffset))
        
        self.clipsToBounds = true
        
        var bg_top = UIImageView()
        bg_top.setX(0)
        bg_top.setY(0)
        bg_top.setSize(self.getWidth(), self.getHeight())
        bg_top.image = UIImage(named: "doc_view_top")
        self.addSubview(bg_top)
        ///------
        
        let iconSize: CGFloat = 50 / 2.5
        
        var topPart = self.getWidth() / 7
        
        //----
        
        var backLayout = UIView()
        backLayout.setX(5)
        backLayout.setY(startOffset)
        backLayout.setSize(42, 42)
        self.addSubview(backLayout)
        
        var lib_i_5 = UIImage(named: "doc_view_back")
        var lib_i_5Size = getImageSize(image: lib_i_5!, width: 0, height: backLayout.getHeight() - 20)
        
        var backIcon = UIImageView()
        backIcon.image = lib_i_5
        backIcon.setSize(backLayout.getWidth() - 14, backLayout.getWidth() - 14)
        backIcon.setX(7)
        backIcon.setY(7)
        backLayout.addSubview(backIcon)
        
        var backButton = UIButton()
        backButton.setX(0)
        backButton.setY(0)
        backButton.setSize(backLayout.getWidth(), backLayout.getHeight())
        backButton.addTarget(self, action: "backAction:", for: .touchUpInside)
        backLayout.addSubview(backButton)
        
        libTitle = UILabel()
        //        libTitle.backgroundColor = .green
        libTitle.setX(0, relative: backLayout)
        libTitle.setY(startOffset)
        libTitle.setSize(ScreenSize.SCREEN_WIDTH - backLayout.getWidth() - backLayout.getWidth(), 42)
        libTitle.textColor = UIColor.white
        libTitle.textAlignment = .center
        libTitle.text = "1/\(self.slideCount)"
        libTitle.font = UIFont.boldSystemFont(ofSize: 17)
        self.addSubview(libTitle)
        
        var buttonActionLayout = UIView()
        //        buttonActionLayout.backgroundColor = .red
        buttonActionLayout.setX(ScreenSize.SCREEN_WIDTH - (42 * 2) - 15)
        buttonActionLayout.setY(startOffset)
        buttonActionLayout.setSize(42 * 2, 42)
        self.addSubview(buttonActionLayout)
        
        var buttonActionLabel = UILabel()
        buttonActionLabel.isHidden = true
        buttonActionLabel.tag = -23847
        buttonActionLabel.text = "Save"
        buttonActionLabel.setX(0)
        buttonActionLabel.setY(0)
        buttonActionLabel.setSize(buttonActionLayout.getWidth(), buttonActionLayout.getHeight())
        buttonActionLabel.textColor =  UIColor.white
        buttonActionLabel.textAlignment = .right
        buttonActionLabel.font = UIFont.systemFont(ofSize: 17)
        buttonActionLayout.addSubview(buttonActionLabel)
        
        var buttonAction = UIButton()
        buttonAction.tag = -23848
        buttonAction.setX(0)
        buttonAction.setY(0)
        buttonAction.setSize(buttonActionLayout.getWidth(), buttonActionLayout.getHeight())
        buttonAction.addTarget(self, action: "saveAction:", for: .touchUpInside)
        buttonActionLayout.addSubview(buttonAction)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func saveAction(_ sender: AnyObject)
    {
        self.delegate.DocViewTopSave()
    }
    
    @objc func backAction(_ sender: AnyObject)
    {
        self.delegate.DocViewTopBack()
    }
    
    func setCurrentSlide(slide: Int)
    {
        libTitle.text = "\(slide)/\(self.slideCount)"
    }
}
