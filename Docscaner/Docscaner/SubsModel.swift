//
//  SubsModel.swift
//  Docscaner
//
//  Created by Artem  on 28/02/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import RealmSwift

class SubscriptionModel: Object
{
	override static func primaryKey() -> String?
	{
		return "PurchaseId"
	}

	@objc dynamic var Title: String = ""

	@objc dynamic var Description: String = ""

	@objc dynamic var ApplicationId: Int = 0

	//	@objc dynamic var Price: Int = 0

	@objc dynamic var PurchaseId: String = ""

	@objc dynamic var Currency: String = ""

	@objc dynamic var TitleLocale: String = ""

	@objc dynamic var PriceLocale: Double = 0
}
