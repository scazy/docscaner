//
//  CameraNoAccess.swift
//  Docscaner
//
//  Created by lynxmac on 22/03/2019.
//  Copyright © 2019 Videomaks. All rights reserved.
//

import Foundation
import UIKit

protocol CameraNoAccessDelegate
{
    func CameraNoAccessOpenSettings()
}

class CameraNoAccess: UIView
{
    var delegate: CameraNoAccessDelegate!
    
    init()
    {
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    func initUI()
    {
        self.backgroundColor = UIColor(red: 16/255, green: 63/255, blue: 94/255, alpha: 1)
        
        var sadSize: CGFloat = self.getWidth() / 4
        
        var sad = UIImageView()
        sad.image = UIImage(named: "sad")
        sad.setSize(sadSize, sadSize)
        sad.toCenterX(self)
        sad.toCenterY(self)
        self.addSubview(sad)
        
        sad.setY(sad.getY() - sadSize)
        
        var title = UILabel()
        title.setX(0)
        title.setY(40, relative: sad)
        title.setSize(self.getWidth(), 30)
        title.font = UIFont(name: "Circe-Bold", size: 23)
        title.textAlignment = .center
        title.textColor = .white
        title.text = s("camera_no_access")
        self.addSubview(title)
        
        var subtitleText = s("noaccess_title")
        
        var subtitleTextSize = getLabelSizeForFont(str: subtitleText, size: 18, fontName: "Circe-Regular", setWidth: self.getWidth() - 80)
        
        var subtitle = UILabel()
//        subtitle.backgroundColor = .red
        subtitle.font = UIFont(name: "Circe-Regular", size: 18)
        subtitle.text = subtitleText
        subtitle.setX(40)
        subtitle.setY(20, relative: title)
        subtitle.textAlignment = .center
        subtitle.textColor = .white
        subtitle.setSize(subtitleTextSize.width, subtitleTextSize.height)
//        subtitle.sizeToFit()
        subtitle.numberOfLines = 0
//        subtitle.lineBreakMode = .byWordWrapping
        self.addSubview(subtitle)
        
        var button = UIButton()
        button.layer.cornerRadius = 12
        button.backgroundColor = .white
//        button.titleLabel?.font =
//        button.titleLabel?.textColor = UIColor(red: 0, green: 0.72, blue: 0.87, alpha: 1)
        button.titleLabel?.font = UIFont(name: "Circe-ExtraBold", size: 18)
        button.setTitle(s("noaccess_button"), for: .normal)
        button.setX(40)
        button.setY(35, relative: subtitle)
        button.setSize(self.getWidth() - 80, 45)
        button.setTitleColor(UIColor(red: 0, green: 0.72, blue: 0.87, alpha: 1), for: .normal)
        button.addTarget(self, action: "openSettings:", for: .touchUpInside)
        self.addSubview(button)
    }
    
    @objc func openSettings(_ sender: AnyObject)
    {
        self.delegate.CameraNoAccessOpenSettings()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
}
