/*
 Localizable.strings
 Docscaner
 
 Created by artem on 03/04/2019.
 Copyright © 2019 Videomaks. All rights reserved.
 */

start_title = "Best Scanner - \nSimple, useful and powerful";

scan_start = "Scan will start in %@ sec.";
retake_title = "Retake";
scan_close = "Use";
date_format = "yyyy-MM-dd-HH-mm";
file_name_format = "%@";

new_folder_title = "New Folder";
new_folder_placeholder = "Enter new folder name";

button_add = "Create";
botton_cancel = "Cancel";

library_title_default = "Your Scans";
library_back = "Back";

library_search = "Search";
folder_search = "Search in this folder";

actions_from_library = "From Library";
actions_from_camera = "From Camera";

importing = "Importing...";
selected_items = "Selected: %@";


menu_edit_edit = "Edit";
menu_edit_sign = "Sign";
menu_edit_add = "Add page";
menu_edit_ocr = "OCR";
menu_edit_share = "Share";
menu_edit_more = "Other";
menu_edit_rename = "Rename";
menu_edit_remove = "Delete";

share_image = "Share Image";
share_pdf = "Share PDF";
save_to_library = "Save to phone library";
share_recognized_pdf = "Share recognized PDF";
share_recognized_plain_text = "Share recognized plain text";

saved_title = "Saved";
saved_title_failed = "Failed";

page_title = "Page";
recognizing_title = "Recognizing...";
recognize_processing = "Recognition in progress...";

remove_image_slide = "Are you sure you want to delete this page?";
remove_whole_image = "Are you sure you want to delete all pages? This will remove the entire document.";
remove_image_title = "Deletion Info";
remove_title = "Delete";

camera_mode1 = "Black&White";
camera_mode2 = "Color";

noaccess_title = "Please, go to settings and allow access to camera on your device.";
noaccess_button = "OPEN SETTINGS";

settings_signature = "Signature";
settings_signature_setup = "Setup";
settings_signature_exists = "Installed";
settings_pincode = "Use PIN-code";
settings_startup = "On app launch";
settings_startup_lib = "Library";
settings_startup_scanner = "Camera";
settings_flash = "Flashlight";
settings_flash_auto = "Auto";
settings_flash_none = "None";
settings_flash_always = "Always";
settings_filter = "Filter";
settings_scan_type = "Scans format";
settings_scan_type_jpg = "JPEG";
settings_scan_type_pdf_jpg = "PDF/JPEG";
settings_scan_save_library = "Save the original to the Library";
settings_support = "Support";
settings_subs = "Subscription";

signature_edit_title = "Signature";
signature_havent_title = "Signature not set";
signature_havent_desc = "Before you will put your signature on the documents, you need to set it in the settings.";
signature_havent_ok = "Set";

button_rename = "Rename";

button_restore = "RESTORE";
subs1_title = "Without any limits";
subs2_title = "Batch documents scanning";
subs3_title = "Text recognition (OCR)";
subs4_title = "Sign, edit and share";
subs_policy = "The price of the subscription is %.2f %@ per year; Every year the subscription renews; Payment will be charged to iTunes Account at confirmation of purchase; Subscription automatically renews unless auto-renew is turned off at least 24-hours before the end of the current period; Account will be charged for renewal within 24-hours prior to the end of the current period, and identify the cost of the renewal; Subscriptions may be managed by the user and auto-renewal may be turned off by going to the user's Account; Any unused portion of a free trial period (if offered) will be forfeited when you purchase a premium subscription during the free trial period; You can cancel the automatic renewal of subscription via this url: https://support.apple.com/en-us/HT202039. The cancellation will take effect the day after the last day of the current subscription period, and you will be downgraded to the free service.";
subs_policy_title = "Terms of Service";
subs_policy_title2 = "Privacy Policy";

loading_title = "Loading...";

clear_title = "Clear";
signature_title = "Signature";

subs_title_premium = "Premium";
subs_title_free = "Free";
setting_title = "Settings";
file_name = "Name: %@";
remove_slide = "Delete this page";
remove_all_slides = "Delete all pages";
remove_signature = "Delete signature";
remove_signature_desc = "Are you sure you want to delete the signature? You can add a new signature in the Edit section.";
folder_name_title = "Folder Name";
file_name_title = "File Name";
save_title = "Save";

switch_mode_auto = "Auto";
switch_mode_manual = "Manual";
camera_no_access = "No access to camera";
remove_multi_items = "Delete item(s)";
remove_multi_desc = "Are you sure you want to delete %@ doc(s)?";
dashboard_empty_title = "Make your first scan by clicking the button below and save it as you like";
dashboard_no_files = "You have no scans yet!";

settings_ocr = "OCR Languages";
ocr_eng = "English";
ocr_deu = "German";
ocr_fra = "French";
ocr_ita = "Italian";
ocr_spa = "Spanish";
ocr_tur = "Turkish";
ocr_por = "Portuguese";

ocr_languages = "languages";
ocr_error = "Recognition Error";
ocr_error_desc = "You haven't got free attempts to recognize text";

sort_name = "Sort by Name";
sort_date = "Sort by Date";
sort_type = "Sort by Type";
sort_size = "Sort by Size";

promo_1_title = "Hello and Welcome!";
promo_1_subtitle = "docScanner will turn your iPhone into a simple and useful mobile scanner.";
promo_2_title = "Forget about bulky office scanners";
promo_2_subtitle = "Scan any important docs easily by using only iPhone. Edit, sign, save and share scans in PDF/JPEG format.";
promo_3_title = "Keep your scans in safety and handy";
promo_3_subtitle = "Store scans locally on your device and protect them by PIN, Touch ID or Face ID.";
promo_next = "Next";
promo_finish = "Unlimited Access";
promo_trial_desc = "Trial duration 3 days, then %.2f %@/year";

promo_subscribe = "Subscribe";
pin_set = "Enter PIN-Code";
pin_set_placeholder = "PIN-Code";
button_enter = "Enter";
